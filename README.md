A full fledged REST API Java application for school management.

### Tech used:
- Hibernate
- Spring Boot
- Jetty
- MySQL-JDBC
- Guava and Apache Commons
- Jackson
- Redisson
- Logback
- AWS SDK (for uploading to AWS S3)
- CSV commons (for reading CSV files)

API Authentication happens via JWT.

To Build, go to project directory (containing pom.xml) and type

```bash
mvn clean compile install
```

To run, go to project directory (containing pom.xml) and type

```bash
java -jar target/Backend-1.0-SNAPSHOT.jar
```