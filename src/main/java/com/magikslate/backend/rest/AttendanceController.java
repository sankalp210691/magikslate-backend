/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.rest;

import com.google.common.base.Joiner;
import com.magikslate.backend.dto.AttendanceDisplayDto;
import com.magikslate.backend.dto.StudentDto;
import com.magikslate.backend.entity.Attendance;
import com.magikslate.backend.entity.Student;
import com.magikslate.backend.entity.StudentClassMapping;
import com.magikslate.backend.entity.StudentStudyGroupMapping;
import com.magikslate.backend.entity.Teacher;
import com.magikslate.backend.services.attendance.AttendanceService;
import com.magikslate.backend.util.Constants;
import java.time.Year;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author sankalpkulshrestha
 */
@RestController
@RequestMapping("/v1/secure/app/attendance")
public class AttendanceController {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AttendanceController.class);

    @Autowired
    private AttendanceService attendanceService;

    @PostMapping("/class")
    public ResponseEntity<Void> classAttendance(@Valid @RequestBody Map<Integer, Boolean> attendanceMap,//scmId is the key
            @RequestParam("notify") Boolean notifyParents,
            @RequestParam("markAt") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date markAt,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer teacherId,
            @RequestAttribute("userId") Integer userId,
            @RequestAttribute("role") String role) {

        LOGGER.info("Marking class attendance with attendanceMap = {}, notify = {}", attendanceMap, notifyParents);

        if (!Constants.TEACHER.equals(role)) {
            LOGGER.info("Role is not TEACHER. Returning status = {}", HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        Teacher teacher = new Teacher(teacherId);
        List<Attendance> attendanceList = convertToAttendanceList(true, attendanceMap, teacher);
        if (attendanceList.isEmpty()) {
            
            LOGGER.info("AttendanceList is empty. Returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        
        List<Integer> idList = attendanceService.createBulk(attendanceList, markAt);
        LOGGER.debug("Attendance marked with idList = {}", idList);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Resource-Id", Joiner.on(",").join(idList));
        
        LOGGER.info("Returning headers = {}, status = {}", headers, HttpStatus.CREATED);
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @PostMapping("/studyGroup")
    public ResponseEntity<Void> studyGroupAttendance(@Valid @RequestBody Map<Integer, Boolean> attendanceMap,//ssgmId is the key
            @RequestParam("notify") Boolean notifyParents,
            @RequestParam("markAt") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss Z") Date markAt,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer teacherId,
            @RequestAttribute("userId") Integer userId,
            @RequestAttribute("role") String role) {

        LOGGER.info("Marking studygroup attendance with attendanceMap = {}, notify = {}", attendanceMap, notifyParents);

        if (!Constants.TEACHER.equals(role)) {
            LOGGER.info("Role is not TEACHER. Returning status = {}", HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        Teacher teacher = new Teacher(teacherId);
        List<Attendance> attendanceList = convertToAttendanceList(false, attendanceMap, teacher);
        if (attendanceList.isEmpty()) {
            
            LOGGER.info("AttendanceList is empty. Returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<Integer> idList = attendanceService.createBulk(attendanceList, markAt);
        LOGGER.debug("Attendance marked with idList = {}", idList);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Resource-Id", Joiner.on(",").join(idList));
        
        LOGGER.info("Returning headers = {}, status = {}", headers, HttpStatus.CREATED);
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @PatchMapping("")
    public ResponseEntity<Void> updateClassAttendance(@Valid @RequestBody Map<Integer, Boolean> attendanceMap,//attendance id is the key
            @RequestParam("notify") Boolean notifyParents,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer teacherId,
            @RequestAttribute("userId") Integer userId,
            @RequestAttribute("role") String role) {

        LOGGER.info("Patching attendance with attendanceMap = {}, notify = {}", attendanceMap, notifyParents);

        if (!Constants.TEACHER.equals(role)) {
            
            LOGGER.info("Role is not TEACHER. Returning status = {}", HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        List<Attendance> attendanceList = convertToAttendanceList(attendanceMap);
        if (attendanceList.isEmpty()) {
            
            LOGGER.info("AttendanceList is empty. Returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        attendanceService.updateBulk(attendanceList);
        
        LOGGER.info("Returning status = {}", HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/class/{classId}")
    public ResponseEntity<AttendanceDisplayDto> classAttendance(
            @Valid @NotNull @Min(value = 1) @PathVariable("classId") Integer classId,
            @RequestParam(name = "sessionYear") Integer sessionYear,
            @RequestParam(name = "date", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date date,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer roleId,
            @RequestAttribute(name = "studentId", required = false) Integer studentId,
            @RequestAttribute("userId") Integer userId,
            @RequestAttribute("role") String role) {

        LOGGER.info("Fetching attendance of a class with classId = {}, sessionYear = {}, date = {}", classId, sessionYear, date);

        List<Attendance> attendanceList = attendanceService
                .getAttendance(schoolId, classId, null, sessionYear, date);
        LOGGER.debug("attendanceList = {}", attendanceList);

        AttendanceDisplayDto attendanceDisplayDto = accumulateClassAttendanceData(attendanceList, studentId);
        
        LOGGER.info("Returning body = {}, status = {}", attendanceDisplayDto, HttpStatus.OK);
        return new ResponseEntity<>(attendanceDisplayDto, HttpStatus.OK);
    }

    @GetMapping("/studyGroup/{studyGroupId}")
    public ResponseEntity<AttendanceDisplayDto> studyGroupAttendance(
            @Valid @NotNull @Min(value = 1) @PathVariable("studyGroupId") Integer studyGroupId,
            @RequestParam(name = "sessionYear") Integer sessionYear,
            @RequestParam(name = "date", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date date,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer roleId,
            @RequestAttribute(name = "studentId", required = false) Integer studentId,
            @RequestAttribute("userId") Integer userId,
            @RequestAttribute("role") String role) {

        LOGGER.info("Fetching attendance of a studyGroup with classId = {}, sessionYear = {}, date = {}", studyGroupId, sessionYear, date);

        List<Attendance> attendanceList = attendanceService
                .getAttendance(schoolId, null, studyGroupId, sessionYear, date);
        LOGGER.debug("attendanceList = {}", attendanceList);

        AttendanceDisplayDto attendanceDisplayDto = accumulateStudyGroupAttendanceData(attendanceList, studentId);
        
        LOGGER.info("Returning body = {}, status = {}", attendanceDisplayDto, HttpStatus.OK);
        return new ResponseEntity<>(attendanceDisplayDto, HttpStatus.OK);
    }

    private List<Attendance> convertToAttendanceList(Boolean isClass, Map<Integer, Boolean> attendanceMap, Teacher teacher) {
        Date date = new Date();
        List<Attendance> attendanceList = new ArrayList();
        attendanceMap.keySet().stream().map((studentLinkingId) -> {
            Attendance attendance = new Attendance();
            attendance.setSessionYear(Year.now().getValue());
            attendance.setTeacher(teacher);
            if (isClass) {
                StudentClassMapping scm = new StudentClassMapping(studentLinkingId);
                attendance.setStudentClassMapping(scm);
            } else {
                StudentStudyGroupMapping ssgm = new StudentStudyGroupMapping(studentLinkingId);
                attendance.setStudentStudyGroupMapping(ssgm);
            }
            attendance.setPresent(attendanceMap.get(studentLinkingId));
            return attendance;
        }).map((attendance) -> {
            attendance.setCreatedAt(date);
            return attendance;
        }).map((attendance) -> {
            attendance.setUpdatedAt(date);
            return attendance;
        }).forEachOrdered((attendance) -> {
            attendanceList.add(attendance);
        });
        return attendanceList;
    }

    private List<Attendance> convertToAttendanceList(Map<Integer, Boolean> attendanceMap) {
        List<Attendance> attendanceList = new ArrayList();
        attendanceMap.forEach((attendanceId, isPresent) -> {
            Attendance attendance = new Attendance(attendanceId);
            attendance.setPresent(isPresent);
            attendanceList.add(attendance);
        });
        return attendanceList;
    }

    private AttendanceDisplayDto accumulateClassAttendanceData(
            List<Attendance> attendanceList, Integer studentId) {

        AttendanceDisplayDto attendanceDisplayDto = new AttendanceDisplayDto();

        Map<String, Integer> datePresenceCountMap = new HashMap(Constants.DAYS_IN_YEAR);
        Map<String, Integer> dateTotalStrengthMap = new HashMap(Constants.DAYS_IN_YEAR);

        attendanceList.stream().map((attendance) -> {
            String dateString = attendance.getCreatedAt().getDate() + Constants.UNDERSCORE_SEPARATOR
                    + attendance.getCreatedAt().getMonth()
                    + Constants.UNDERSCORE_SEPARATOR
                    + attendance.getCreatedAt().getYear();
            if (attendance.getPresent()) {
                if (!datePresenceCountMap.containsKey(dateString)) {
                    datePresenceCountMap.put(dateString, 1);
                } else {
                    datePresenceCountMap.put(dateString, datePresenceCountMap.get(dateString) + 1);
                }
            }
            return dateString;
        }).forEachOrdered((dateString) -> {
            if (!dateTotalStrengthMap.containsKey(dateString)) {
                dateTotalStrengthMap.put(dateString, 1);
            } else {
                dateTotalStrengthMap.put(dateString, dateTotalStrengthMap.get(dateString) + 1);
            }
        });

        Double classAveragePresencePerc = 0.0;
        int totalDays = 0;
        for (String dateString : dateTotalStrengthMap.keySet()) {
            int presenceCount = datePresenceCountMap.get(dateString) == null ? 0 : datePresenceCountMap.get(dateString);
            classAveragePresencePerc += presenceCount / (double) dateTotalStrengthMap.get(dateString);
            totalDays++;
        }
        classAveragePresencePerc = 100 * (classAveragePresencePerc / (double) totalDays);
        attendanceDisplayDto.setPercClassAverageAttendance(classAveragePresencePerc);

        if (studentId == null) {
            //calculate for all students

            Map<Integer, StudentDto> studentDtoMap = new HashMap();
            Map<Integer, Integer> studentDtoTotalDaysMap = new HashMap();
            attendanceList.forEach((attendance) -> {
                StudentClassMapping scm = attendance.getStudentClassMapping();
                Student student = scm.getStudent();
                StudentDto studentDto = studentDtoMap.get(student.getId());
                if (studentDto == null) {
                    studentDto = new StudentDto();
                    studentDtoMap.put(student.getId(), studentDto);

                    studentDto.setId(student.getId());
                    studentDto.setName(student.getName());
                    studentDto.setProfilePicUrl(student.getProfilePic());
                    studentDto.setStudentClassMappingId(scm.getId());
                    studentDto.setAdmissionCode(student.getAdmissionCode());
                }

                if (studentDto.getPercAttendance() == null) {
                    studentDto.setPercAttendance(0.0);
                }

                if (attendance.getPresent()) {
                    studentDto.setPercAttendance(studentDto.getPercAttendance() + 1);
                } else {
                    Set<Date> absentDates = studentDto.getAbsentDates();
                    if (absentDates == null) {
                        absentDates = new HashSet();
                        studentDto.setAbsentDates(absentDates);
                    }
                    absentDates.add(attendance.getCreatedAt());
                }

                if (!studentDtoTotalDaysMap.containsKey(student.getId())) {
                    studentDtoTotalDaysMap.put(student.getId(), 1);
                } else {
                    studentDtoTotalDaysMap.put(student.getId(), studentDtoTotalDaysMap.get(student.getId()) + 1);
                }
            });

            studentDtoTotalDaysMap.keySet().forEach((sid) -> {
                studentDtoMap.get(sid).setPercAttendance(100 * (studentDtoMap.get(sid).getPercAttendance() / (double) studentDtoTotalDaysMap.get(sid)));
            });

            attendanceDisplayDto.setStudents(new ArrayList(studentDtoMap.values()));
        } else {
            //calculate for specific student

            StudentDto studentDto = new StudentDto();
            int totalDaysForStudent = 0;
            for (Attendance attendance : attendanceList) {
                StudentClassMapping scm = attendance.getStudentClassMapping();
                Student student = scm.getStudent();
                if (student.getId().equals(studentId)) {
                    studentDto.setId(student.getId());
                    studentDto.setName(student.getName());
                    studentDto.setProfilePicUrl(student.getProfilePic());
                    studentDto.setStudentClassMappingId(scm.getId());
                    studentDto.setAdmissionCode(student.getAdmissionCode());

                    if (studentDto.getPercAttendance() == null) {
                        studentDto.setPercAttendance(0.0);
                    }

                    if (attendance.getPresent()) {
                        studentDto.setPercAttendance(studentDto.getPercAttendance() + 1);
                    } else {
                        Set<Date> absentDates = studentDto.getAbsentDates();
                        if (absentDates == null) {
                            absentDates = new HashSet();
                            studentDto.setAbsentDates(absentDates);
                        }
                        absentDates.add(attendance.getCreatedAt());
                    }
                    totalDaysForStudent++;
                }
            }
            studentDto.setPercAttendance(100 * (studentDto.getPercAttendance() / (double) totalDaysForStudent));
            List<StudentDto> studentDtoList = new ArrayList();
            studentDtoList.add(studentDto);
            attendanceDisplayDto.setStudents(studentDtoList);
        }

        return attendanceDisplayDto;
    }

    private AttendanceDisplayDto accumulateStudyGroupAttendanceData(List<Attendance> attendanceList, Integer studentId) {
        AttendanceDisplayDto attendanceDisplayDto = new AttendanceDisplayDto();

        Map<String, Integer> datePresenceCountMap = new HashMap(Constants.DAYS_IN_YEAR);
        Map<String, Integer> dateTotalStrengthMap = new HashMap(Constants.DAYS_IN_YEAR);

        attendanceList.stream().map((attendance) -> {
            String dateString = attendance.getCreatedAt().getDate() + Constants.UNDERSCORE_SEPARATOR
                    + attendance.getCreatedAt().getMonth()
                    + Constants.UNDERSCORE_SEPARATOR
                    + attendance.getCreatedAt().getYear();
            if (attendance.getPresent()) {
                if (!datePresenceCountMap.containsKey(dateString)) {
                    datePresenceCountMap.put(dateString, 1);
                } else {
                    datePresenceCountMap.put(dateString, datePresenceCountMap.get(dateString) + 1);
                }
            }
            return dateString;
        }).forEachOrdered((dateString) -> {
            if (!dateTotalStrengthMap.containsKey(dateString)) {
                dateTotalStrengthMap.put(dateString, 1);
            } else {
                dateTotalStrengthMap.put(dateString, dateTotalStrengthMap.get(dateString) + 1);
            }
        });

        Double studyGroupAveragePresencePerc = 0.0;
        int totalDays = 0;
        for (String dateString : dateTotalStrengthMap.keySet()) {
            int presenceCount = datePresenceCountMap.get(dateString) == null ? 0 : datePresenceCountMap.get(dateString);
            studyGroupAveragePresencePerc += presenceCount / (double) dateTotalStrengthMap.get(dateString);
            totalDays++;
        }
        studyGroupAveragePresencePerc = 100 * (studyGroupAveragePresencePerc / (double) totalDays);
        attendanceDisplayDto.setPercClassAverageAttendance(studyGroupAveragePresencePerc);

        if (studentId == null) {
            //calculate for all students

            Map<Integer, StudentDto> studentDtoMap = new HashMap();
            Map<Integer, Integer> studentDtoTotalDaysMap = new HashMap();
            attendanceList.forEach((attendance) -> {
                StudentStudyGroupMapping ssgm = attendance.getStudentStudyGroupMapping();
                Student student = ssgm.getStudent();
                StudentDto studentDto = studentDtoMap.get(student.getId());
                if (studentDto == null) {
                    studentDto = new StudentDto();
                    studentDtoMap.put(student.getId(), studentDto);

                    studentDto.setId(student.getId());
                    studentDto.setName(student.getName());
                    studentDto.setProfilePicUrl(student.getProfilePic());
                    studentDto.setStudentStudyGroupMappingId(ssgm.getId());
                }

                if (studentDto.getPercAttendance() == null) {
                    studentDto.setPercAttendance(0.0);
                }

                if (attendance.getPresent()) {
                    studentDto.setPercAttendance(studentDto.getPercAttendance() + 1);
                } else {
                    Set<Date> absentDates = studentDto.getAbsentDates();
                    if (absentDates == null) {
                        absentDates = new HashSet();
                        studentDto.setAbsentDates(absentDates);
                    }
                    absentDates.add(attendance.getCreatedAt());
                }

                if (!studentDtoTotalDaysMap.containsKey(student.getId())) {
                    studentDtoTotalDaysMap.put(student.getId(), 1);
                } else {
                    studentDtoTotalDaysMap.put(student.getId(), studentDtoTotalDaysMap.get(student.getId() + 1));
                }
            });

            studentDtoTotalDaysMap.keySet().forEach((sid) -> {
                studentDtoMap.get(sid).setPercAttendance(100 * (studentDtoMap.get(sid).getPercAttendance() / (double) studentDtoTotalDaysMap.get(sid)));
            });

            attendanceDisplayDto.setStudents(new ArrayList(studentDtoMap.values()));
        } else {
            //calculate for specific student

            StudentDto studentDto = new StudentDto();
            int totalDaysForStudent = 0;
            for (Attendance attendance : attendanceList) {
                StudentStudyGroupMapping ssgm = attendance.getStudentStudyGroupMapping();
                Student student = ssgm.getStudent();
                if (student.getId().equals(studentId)) {
                    studentDto.setId(student.getId());
                    studentDto.setName(student.getName());
                    studentDto.setProfilePicUrl(student.getProfilePic());
                    studentDto.setStudentClassMappingId(ssgm.getId());

                    if (studentDto.getPercAttendance() == null) {
                        studentDto.setPercAttendance(0.0);
                    }

                    if (attendance.getPresent()) {
                        studentDto.setPercAttendance(studentDto.getPercAttendance() + 1);
                    } else {
                        Set<Date> absentDates = studentDto.getAbsentDates();
                        if (absentDates == null) {
                            absentDates = new HashSet();
                            studentDto.setAbsentDates(absentDates);
                        }
                        absentDates.add(attendance.getCreatedAt());
                    }
                    totalDaysForStudent++;
                }
            }
            studentDto.setPercAttendance(100 * (studentDto.getPercAttendance() / (double) totalDaysForStudent));
            List<StudentDto> studentDtoList = new ArrayList();
            studentDtoList.add(studentDto);
            attendanceDisplayDto.setStudents(studentDtoList);
        }

        return attendanceDisplayDto;
    }

}
