/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.rest;

import com.magikslate.backend.dto.EventDto;
import com.magikslate.backend.entity.Event;
import com.magikslate.backend.entity.User;
import com.magikslate.backend.entity.Class;
import com.magikslate.backend.entity.Parent;
import com.magikslate.backend.entity.School;
import com.magikslate.backend.entity.Teacher;
import com.magikslate.backend.services.clazz.ClassService;
import com.magikslate.backend.services.event.EventService;
import com.magikslate.backend.services.parent.ParentService;
import com.magikslate.backend.services.teacher.TeacherService;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import io.vertx.core.json.JsonObject;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author sankalpkulshrestha
 */
@RestController
@RequestMapping("/v1/secure/app/event")
public class EventController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventController.class);

    @Autowired
    private EventService eventService;

    @Autowired
    private ClassService classService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private ParentService parentService;

    @PostMapping("")
    public ResponseEntity<JsonObject> event(@RequestBody EventDto eventDto,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("role") String role,
            @RequestAttribute("userId") Integer userId,
            UriComponentsBuilder ucBuilder) {

        LOGGER.info("Creating an event with eventDto = {} , schoolId = {}, role = {} , userId = {}", eventDto, schoolId, role, userId);

        if (eventDto.isEventForParents() == false && eventDto.isEventForStaff() == false) {
            JsonObject error = Utils.generateErrorJson("Event should be created for either staff or parent. Both cannot be false");
            LOGGER.info("Returning {} with status = {}", error.toString(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }

        List<Class> classList = classService.getClassListBySchool(schoolId, null);
        Set<Integer> classIdSet = classList.stream().map(clazz -> clazz.getId()).collect(Collectors.toCollection(HashSet::new));

        LOGGER.debug("classIdSet = {}", classIdSet);
        List<Integer> givenClassIdList = eventDto.getClassList();
        List<Class> givenClassList = new ArrayList();
        for (Integer classId : givenClassIdList) {
            if (!classIdSet.contains(classId)) {
                JsonObject error = Utils.generateErrorJson("One or more invalid class");
                LOGGER.info("Returning {} with status = {}", error.toString(), HttpStatus.UNAUTHORIZED);
                return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
            } else {
                givenClassList.add(new Class(classId));
            }
        }

        List<Teacher> teacherList = null;
        List<Parent> parentList = null;

        if (eventDto.isEventForParents()) {
            parentList = parentService.getParentsByClassList(givenClassList);
        }

        if (eventDto.isEventForStaff()) {
            teacherList = teacherService.getTeachersByClassList(givenClassList);
        }

        Event event = convertToEvent(eventDto, role, userId, schoolId);
        if (eventDto.isEventForParents()) {
            event.setParentSentCount(parentList.size());
        }
        if (eventDto.isEventForStaff()) {
            event.setTeacherSentCount(teacherList.size());
        }
        Integer id = eventService.add(event);
        LOGGER.debug("Event created with id = {}", id);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/event/{id}").buildAndExpand(id).toUri());

        LOGGER.info("Returning header = {} with status = {}", headers, HttpStatus.CREATED);
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EventDto> event(@PathVariable("id") Integer eventId,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("role") String role,
            @RequestAttribute("userId") Integer userId) {

        LOGGER.info("get event by id = {}", eventId);

        Event event = eventService.get(eventId, schoolId);
        if (event == null) {

            LOGGER.info("No event found. Returning status = {}", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            LOGGER.debug("Event found as event = {}. Coverting to eventDto", event);
            EventDto eventDto = EventDto.convertToEventDto(event, userId);

            LOGGER.info("Returning body = {} with status = {}", eventDto, HttpStatus.OK);
            return new ResponseEntity<>(eventDto, HttpStatus.OK);
        }
    }

    @GetMapping("")
    public ResponseEntity<JsonObject> event(
            @RequestParam(name = "startDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date startDate,
            @RequestParam(name = "endDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date endDate,
            @RequestParam(name = "eventStartDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date eventStartDate,
            @RequestParam(name = "eventEndDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date eventEndDate,
            @RequestParam(name = "offset") Integer offset, @RequestAttribute("role") String role, @RequestAttribute("roleId") Integer roleId,
            @RequestParam(name = "self", required = false) boolean isSelfNeeded,
            @RequestParam(name = "received", required = false) boolean isReceivedNeeded,
            @RequestAttribute("userId") Integer userId, @RequestAttribute("schoolId") Integer schoolId) {

        LOGGER.info("Getting events by filter. role = {} , roleId = {} , startDate = {} ,"
                + " endDate = {} , eventStartDate = {} , eventEndDate = {} , offset = {} ,"
                + " userId = {} , schoolId = {}", role, roleId, startDate, endDate,
                eventStartDate, eventEndDate, offset, userId, schoolId);

        if (!isSelfNeeded && !isReceivedNeeded) {

            LOGGER.info("Caller didn't specify neither self nor received. So returning status = ", HttpStatus.NO_CONTENT);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        if ((startDate != null && endDate != null) || (eventStartDate != null)) {
            //right now not checking if the person calling this API has access or not.
            offset = offset == null ? 0 : offset;
            List<List<Event>> eventListList = eventService
                    .getAll(userId, role, roleId, schoolId, startDate,
                            endDate, eventStartDate, eventEndDate, offset, isSelfNeeded, isReceivedNeeded);

            LOGGER.debug("eventListList = {}", eventListList);

            JsonObject json = new JsonObject();

            if (isSelfNeeded) {

                List<EventDto> selfEventDtoList = new ArrayList();
                List<Event> selfEventList = eventListList.get(0);
                if (selfEventList != null) {
                    selfEventList.forEach((event) -> {
                        selfEventDtoList.add(EventDto.convertToEventDto(event, userId));
                    });
                }

                if (!selfEventDtoList.isEmpty()) {
                    json.put("self", selfEventDtoList);
                }
            }

            if (isReceivedNeeded) {

                List<EventDto> recEventDtoList = new ArrayList();
                List<Event> recEventList = eventListList.get(1);
                if (recEventList != null) {
                    recEventList.forEach((event) -> {
                        recEventDtoList.add(EventDto.convertToEventDto(event, userId));
                    });
                }

                if (!recEventDtoList.isEmpty()) {
                    json.put("received", recEventDtoList);
                }
            }

            if (json.isEmpty()) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(json, HttpStatus.OK);
            }
        } else {

            LOGGER.info("Returning status = {} because (startDate != null && endDate != null) || (eventStartDate != null && eventEndDate != null) --> {}", HttpStatus.BAD_REQUEST, (startDate != null && endDate != null) || (eventStartDate != null && eventEndDate != null));
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping("{id}/confirm")
    public ResponseEntity<Void> event(@PathVariable("id") Integer eventId,
            @RequestParam("status") Integer status,
            @RequestAttribute("userId") Integer userId,
            @RequestAttribute("role") String role,
            @RequestAttribute("roleId") Integer roleId,
            @RequestAttribute("schoolId") Integer schoolId) {

        LOGGER.info("Confirming an event with eventId = {}, status = {}, userId = {}, role = {}, roleId = {}, schoolId = {}",
                eventId, status, userId, role, roleId, schoolId);

        if (Constants.TEACHER.equals(role) || Constants.PARENT.equals(role)) {
            boolean sucessfullySaved = eventService.confirm(userId, eventId, role, roleId, status);
            if (sucessfullySaved) {
                LOGGER.info("Returning status = {}", HttpStatus.CREATED);
                return new ResponseEntity<>(HttpStatus.CREATED);
            } else {
                LOGGER.info("Could not save confirmation or creator is trying to respond. Returning status = {}", HttpStatus.BAD_REQUEST);
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {
            LOGGER.info("Returning status = {} because required role is TEACHER or PARENT but role = {}", HttpStatus.UNAUTHORIZED, role);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    private Event convertToEvent(EventDto eventDto, String role, Integer userId, Integer schoolId) {
        Event event = new Event();
        event.setEventForParents(eventDto.isEventForParents());
        event.setEventForStaff(eventDto.isEventForStaff());
        event.setTitle(eventDto.getTitle());
        event.setDescription(eventDto.getDescription());
        event.setUser(new User(userId));
        event.setSchool(new School(schoolId));
        event.setCreatedAt(new Date());
        event.setCreatorRole(role);
        event.setEventStartDate(eventDto.getEventStartDate());

        Calendar myCal = Calendar.getInstance();
        myCal.setTime(eventDto.getEventStartDate());
        myCal.add(Calendar.HOUR, +Constants.HOURS_PER_DAY);
        event.setEventEndDate(myCal.getTime());

        List<Class> classList = new ArrayList();
        eventDto.getClassList().forEach((id) -> {
            classList.add(new Class(id));
        });
        event.setClassList(classList);
        return event;
    }

}
