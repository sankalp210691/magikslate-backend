/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.rest;

import com.magikslate.backend.dto.AttachmentDto;
import com.magikslate.backend.dto.ContentDto;
import com.magikslate.backend.dto.NoteDto;
import com.magikslate.backend.dto.UrlContentDto;
import com.magikslate.backend.entity.Attachment;
import com.magikslate.backend.entity.Content;
import com.magikslate.backend.entity.Note;
import com.magikslate.backend.entity.School;
import com.magikslate.backend.entity.StudyGroupTeacherMapping;
import com.magikslate.backend.entity.SubjectClassTeacherMapping;
import com.magikslate.backend.entity.Teacher;
import com.magikslate.backend.entity.UrlContent;
import com.magikslate.backend.services.content.ContentService;
import com.magikslate.backend.util.AWSUtil;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author sankalp.kulshrestha
 */
@RestController
@Validated
@RequestMapping("/v1/secure/app/content")
public class ContentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContentController.class);

    @Autowired
    private ContentService contentService;

    @Autowired
    private AWSUtil awsUtil;

    @PostMapping("/url")
    public ResponseEntity<UrlContentDto> urlContent(@Valid @RequestBody UrlContentDto urlContentDto,
            UriComponentsBuilder ucBuilder,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer teacherId,
            @RequestAttribute("userId") Integer userId,
            @RequestAttribute("role") String role) {

        LOGGER.info("Creating URL content with urlContentDto = {}", urlContentDto);
        if (!Constants.TEACHER.equals(role)) {
            LOGGER.info("Role IS NOT TEACHER. Returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        urlContentDto.setType(Constants.URL_CONTENT_TYPE);
        UrlContent urlContent = convertToUrlContent(urlContentDto, teacherId, schoolId);

        if (urlContent != null) {
            Integer id = contentService.add(urlContent);
            LOGGER.debug("urlContent created with Id = {}", id);
            if (id != null) {
                urlContentDto.setId(id);
                HttpHeaders headers = new HttpHeaders();
                headers.setLocation(ucBuilder.path("/secure/app/content/url/{id}").buildAndExpand(id).toUri());

                LOGGER.info("Returning body = {}, headers = {}, status = {}", urlContentDto, headers, HttpStatus.CREATED);

                return new ResponseEntity<>(urlContentDto, headers, HttpStatus.CREATED);
            } else {
                LOGGER.info("UrlContentId generated is NULL. Returning status = {}", HttpStatus.BAD_REQUEST);
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {
            LOGGER.info("UrlContent is NULL. Returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    //convertToUrlContentDto etc could be converted to 1 function using generics
    @GetMapping("/url/{cid}")
    public ResponseEntity<UrlContentDto> urlContent(@Valid @NotNull @Min(value = 1) @PathVariable("cid") Integer id,
            UriComponentsBuilder ucBuilder) {

        LOGGER.info("Fetching urlContent with id = {}" + id);

        Content content = contentService.get(id);
        if (Constants.URL_CONTENT_TYPE.equals(content.getType())) {
            UrlContentDto urlContentDto = UrlContentDto.convertToUrlContentDto(content);

            LOGGER.info("Returning body = {}, status = {}", urlContentDto, HttpStatus.OK);

            return new ResponseEntity<>(urlContentDto, HttpStatus.OK);
        } else {
            LOGGER.info("No such UrlContent found. Returning body = null, status = {}", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/attachment", headers = ("content-type=multipart/*"))
    public ResponseEntity<AttachmentDto> attachment(@RequestParam MultipartFile file,
            UriComponentsBuilder ucBuilder,
            @RequestParam(required = false, value = "sgtmList") String sctmIdStringList,
            @RequestParam(required = false, value = "sctmList") String sgtmIdStringList,
            @RequestParam(required = false, value = "description") String desc,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer teacherId,
            @RequestAttribute("userId") Integer userId,
            @RequestAttribute("role") String role) throws IOException {

        LOGGER.info("Creating Attachment content with sctmIdStringList = {} , sgtmIdStringList = {} "
                + ", desc = {} , schoolId = {} , teacherId = {} , userId = {}", sctmIdStringList,
                sgtmIdStringList, desc, schoolId, teacherId, userId);
        
        if (!Constants.TEACHER.equals(role)) {
            LOGGER.info("Role is not TEACHER. Returning status = {}", HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        if (file.isEmpty()) {
            LOGGER.info("File is empty. Returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            Date date = new Date();

            List<Integer> sctmIdList = sctmIdStringList != null ? Stream.of(sctmIdStringList.split(","))
                    .map(Integer::parseInt)
                    .collect(Collectors.toList()) : null;

            List<Integer> sgtmIdList = sgtmIdStringList != null ? Stream.of(sgtmIdStringList.split(","))
                    .map(Integer::parseInt)
                    .collect(Collectors.toList()) : null;

            if (sctmIdList == null && sgtmIdList == null) {
                LOGGER.info("Both sagtmList and sctmList are empty. Returning status = {}", HttpStatus.BAD_REQUEST);
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            String randomUUID = UUID.randomUUID().toString();
            String folder = Constants.S3_ATTACHMENT_FOLDER.
                    replace(Constants.SCHOOL_ID_MACRO, schoolId.toString())
                    .replace(Constants.DATE_STRUCTURE_MACRO, dateFormat.format(date))
                    + randomUUID
                    + Constants.DOT_SEPARATOR
                    + FilenameUtils.getExtension(file.getOriginalFilename());

            LOGGER.debug("Folder = {}", folder);

            String fileType = Utils.getFileType(file);
            String filename = FilenameUtils.getName(file.getOriginalFilename());

            LOGGER.debug("FileType = {}", fileType);
            File localFile = Utils.writeToDisk(file, "/disk1/temp/" + randomUUID + Constants.DOT_SEPARATOR + fileType);
            LOGGER.debug("Uploading localFile = {} to folder = {} in S3 bucket = {}", localFile, folder, Constants.S3_SCHOOL_BUCKET);
            if (awsUtil.uploadToS3(localFile, folder, Constants.S3_SCHOOL_BUCKET)) {

                LOGGER.debug("File successfully uploaded.");

                FileUtils.forceDelete(localFile);
                LOGGER.debug("Local copy of file deleted");

                AttachmentDto attachmentContentDto = new AttachmentDto();
                if (sctmIdList != null) {
                    attachmentContentDto.setSctmList(sctmIdList);
                }
                if (sgtmIdList != null) {
                    attachmentContentDto.setSctmList(sgtmIdList);
                }
                attachmentContentDto.setFilename(filename);
                attachmentContentDto.setFileUrl(Constants.STATIC_RESOURCE_DNS + folder);
                attachmentContentDto.setType(Constants.ATTACHMENT_CONTENT_TYPE);
                attachmentContentDto.setDescription(desc);

                Attachment attachment = convertToAttachment(attachmentContentDto, teacherId, schoolId);

                if (attachment != null) {
                    Integer id = contentService.add(attachment);

                    if (id != null) {

                        LOGGER.debug("attachment created with id = {}", id);

                        attachmentContentDto.setId(id);
                        HttpHeaders headers = new HttpHeaders();
                        headers.setLocation(ucBuilder.path("/secure/app/content/attachment/{id}").buildAndExpand(id).toUri());

                        LOGGER.info("Returning body = {}, headers = {}, status = {}", attachmentContentDto, headers, HttpStatus.CREATED);
                        return new ResponseEntity<>(attachmentContentDto, headers, HttpStatus.CREATED);
                    } else {
                        LOGGER.info("attachment id is null. Returning status = {}", HttpStatus.BAD_REQUEST);
                        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                    }

                } else {
                    LOGGER.info("attachment created in NULL. Returning status = {}", HttpStatus.BAD_REQUEST);
                    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                }
            } else {
                FileUtils.forceDelete(localFile);
                LOGGER.error("File could not be uploaded to S3");
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }

        }

    }

    @GetMapping("/attachment/{cid}")
    public ResponseEntity<AttachmentDto> attachment(@Valid @NotNull @Min(value = 1) @PathVariable("cid") Integer id,
            UriComponentsBuilder ucBuilder) {

        LOGGER.info("Fetching /attachment/{}", id);
        Content content = contentService.get(id);
        if (content == null) {
            LOGGER.info("No such content found. Returning status = {}", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } else {
            if (Constants.ATTACHMENT_CONTENT_TYPE.equals(content.getType())) {
                AttachmentDto attachmentDto = AttachmentDto.convertToAttachmentDto(content);

                LOGGER.info("Returning body = {}, status = {}", attachmentDto, HttpStatus.OK);
                return new ResponseEntity<>(attachmentDto, HttpStatus.OK);
            } else {
                LOGGER.info("Invalid content type = {}. Expected {}. Returning status = {}", content.getType(), Constants.ATTACHMENT_CONTENT_TYPE, HttpStatus.NOT_FOUND);
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        }
    }

    @PostMapping("/note")
    public ResponseEntity<NoteDto> note(@Valid @RequestBody NoteDto noteDto,
            UriComponentsBuilder ucBuilder,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer teacherId,
            @RequestAttribute("userId") Integer userId,
            @RequestAttribute("role") String role) {

        LOGGER.info("Creating Note content");
        if (!Constants.TEACHER.equals(role)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        noteDto.setType(Constants.NOTE_CONTENT_TYPE);
        Note note = convertToNote(noteDto, teacherId, schoolId);
        if (note != null) {
            Integer id = contentService.add(note);
            if (id != null) {
                noteDto.setId(id);
                HttpHeaders headers = new HttpHeaders();
                headers.setLocation(ucBuilder.path("/secure/app/content/note/{id}").buildAndExpand(id).toUri());
                return new ResponseEntity<>(noteDto, headers, HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/note/{cid}")
    public ResponseEntity<NoteDto> note(@Valid @NotNull @Min(value = 1) @PathVariable("cid") Integer id,
            UriComponentsBuilder ucBuilder) {

        LOGGER.info("Fetching /note/" + id);
        Content content = contentService.get(id);
        if (Constants.NOTE_CONTENT_TYPE.equals(content.getType())) {
            NoteDto noteDto = NoteDto.convertToNoteDto(content);
            return new ResponseEntity<>(noteDto, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/")
    public ResponseEntity<JsonArray> content(
            @RequestParam("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date startDate,
            @RequestParam("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date endDate,
            @Min(value = 1) @RequestParam(name = "classId", required = false) Integer classId,
            @Min(value = 1) @RequestParam(name = "studyGroupId", required = false) Integer studyGroupId,
            @Min(value = 1) @RequestParam(name = "subjectId", required = false) Integer subjectId,
            @RequestParam("offset") Integer offset,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer roleId,
            @RequestAttribute(name = "studentId", required = false) Integer studentId,
            @RequestAttribute("userId") Integer userId,
            @RequestAttribute("role") String role) {

        LOGGER.info("Listing content by filters: startDate = {}, endDate = {} , classId = {} ,"
                + " studyGroupId = {} , subjectId = {} , offset = {} , schoolId = {} ,"
                + " roleId = {} , studentId = {}, userId = {} , role = {}", startDate, endDate,
                classId, studyGroupId, subjectId, offset, schoolId, roleId, studentId, userId,
                role);

        if (!Constants.TEACHER.equals(role) && !Constants.PARENT.equals(role)) {

            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        List<Content> contentList = contentService.getContent(role, roleId, schoolId, subjectId,
                studentId, classId, studyGroupId, startDate, endDate, offset);

        if (contentList == null || contentList.isEmpty()) {

            LOGGER.info("contentList is null or empty. Returning status = {}", HttpStatus.NO_CONTENT);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } else {

            JsonArray jsonArray = convertToContentList(contentList);
            LOGGER.info("Returning body = {}, status = {}", jsonArray, HttpStatus.OK);
            return new ResponseEntity<>(jsonArray, HttpStatus.OK);
        }
    }

    @GetMapping("/stats")
    public ResponseEntity<Map<String, Integer>> contentStats(
            @RequestParam("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date startDate,
            @RequestParam("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date endDate,
            @RequestParam(name = "teacherId", required = false) Integer teacherId,
            @RequestParam(name = "classId", required = false) String classIdListString,
            @RequestParam(name = "studyGroupId", required = false) String studyGroupListString,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer roleId,
            @RequestAttribute(name = "studentId", required = false) Integer studentId,
            @RequestAttribute("userId") Integer userId,
            @RequestAttribute("role") String role) {

        LOGGER.info("Content stats");

        if (classIdListString == null && studyGroupListString == null) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        Integer[] classIdArray = null;
        if (classIdListString != null) {
            String[] classIdStringArray = classIdListString.split(Constants.COMMA_SEPARATOR);
            classIdArray = new Integer[classIdStringArray.length];

            int index = 0;
            for (String classIdString : classIdStringArray) {
                classIdArray[index] = Integer.parseInt(classIdString);
                index++;
            }
        }

        Integer[] studyGroupIdArray = null;
        if (studyGroupListString != null) {
            String[] studyGroupIdStringArray = studyGroupListString.split(Constants.COMMA_SEPARATOR);
            studyGroupIdArray = new Integer[studyGroupIdStringArray.length];

            int index = 0;
            for (String studyGroupIdString : studyGroupIdStringArray) {
                studyGroupIdArray[index] = Integer.parseInt(studyGroupIdString);
                index++;
            }
        }

        return new ResponseEntity<>(contentService.getStats(startDate, endDate,
                teacherId, classIdArray, studyGroupIdArray, schoolId), HttpStatus.OK);
    }

    private UrlContent convertToUrlContent(ContentDto contentDto, Integer teacherId, Integer schoolId) {
        Content content = convertToContent(contentDto, teacherId, schoolId);

        if (content == null) {
            return null;
        }

        UrlContent urlContent = new UrlContent();
        urlContent.setUrl(((UrlContentDto) contentDto).getUrl());
        urlContent.setContent(content);

        return urlContent;
    }

    private Attachment convertToAttachment(ContentDto contentDto, Integer teacherId, Integer schoolId) {
        Content content = convertToContent(contentDto, teacherId, schoolId);

        if (content == null) {
            return null;
        }

        Attachment attachment = new Attachment();
        attachment.setFileUrl(((AttachmentDto) contentDto).getFileUrl());
        attachment.setFileName(((AttachmentDto) contentDto).getFilename());
        attachment.setContent(content);

        return attachment;
    }

    private Note convertToNote(ContentDto contentDto, Integer teacherId, Integer schoolId) {
        Content content = convertToContent(contentDto, teacherId, schoolId);

        if (content == null) {
            return null;
        }

        Note note = new Note();
        note.setName(((NoteDto) contentDto).getName());
        note.setNoteContent(((NoteDto) contentDto).getNoteContent());
        note.setContent(content);

        return note;
    }

    private Content convertToContent(ContentDto contentDto, Integer teacherId, Integer schoolId) {
        Content content = new Content();
        content.setType(contentDto.getType());
        content.setDescription(contentDto.getDescription());
        content.setCreatedAt(new Date());
        content.setSchool(new School(schoolId));

        if ((contentDto.getSctmList() == null || contentDto.getSctmList().isEmpty())
                && (contentDto.getSgtmList() == null || contentDto.getSgtmList().isEmpty())) {
            return null;
        }

        if (contentDto.getSctmList() != null) {
            List<SubjectClassTeacherMapping> sctmList = new ArrayList();
            content.setSubjectClassTeacherMappingList(sctmList);
            contentDto.getSctmList().stream().map((sctmMappingId) -> {
                SubjectClassTeacherMapping sctm = new SubjectClassTeacherMapping();
                sctm.setId(sctmMappingId);
                sctm.setTeacher(new Teacher(teacherId));
                return sctm;
            }).forEachOrdered((sctm) -> {
                sctmList.add(sctm);
            });
        } else {
            List<StudyGroupTeacherMapping> sgtmList = new ArrayList();
            content.setStudyGroupTeacherMappingList(sgtmList);
            contentDto.getSgtmList().stream().map((sgtmId) -> {
                StudyGroupTeacherMapping sgtm = new StudyGroupTeacherMapping();
                sgtm.setId(sgtmId);
                sgtm.setTeacher(new Teacher(teacherId));
                return sgtm;
            }).forEachOrdered((sgtm) -> {
                sgtmList.add(sgtm);
            });
        }

        return content;
    }

    private JsonArray convertToContentList(List<Content> contentList) {
        JsonArray jsonArray = new JsonArray();

        contentList.forEach(content -> {
            switch (content.getType()) {
                case Constants.URL_CONTENT_TYPE:
                    UrlContentDto urlContentDto = UrlContentDto.convertToUrlContentDto(content);
                    jsonArray.add(new JsonObject(Json.encode(urlContentDto)));
                    break;
                case Constants.ATTACHMENT_CONTENT_TYPE:
                    AttachmentDto attachmentDto = AttachmentDto.convertToAttachmentDto(content);
                    jsonArray.add(new JsonObject(Json.encode(attachmentDto)));
                    break;
                case Constants.NOTE_CONTENT_TYPE:
                    NoteDto noteDto = NoteDto.convertToNoteDto(content);
                    jsonArray.add(new JsonObject(Json.encode(noteDto)));
                    break;
            }
        });

        return jsonArray;
    }

}
