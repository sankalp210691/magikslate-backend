/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.rest;

/**
 *
 * @author sankalpkulshrestha
 */
import com.magikslate.backend.dto.ClassDto;
import com.magikslate.backend.dto.ParentDto;
import com.magikslate.backend.dto.StudentDto;
import com.magikslate.backend.dto.SubjectDto;
import com.magikslate.backend.dto.UserDto;
import com.magikslate.backend.entity.Attendance;
import com.magikslate.backend.entity.Class;
import com.magikslate.backend.entity.School;
import com.magikslate.backend.entity.Student;
import com.magikslate.backend.entity.StudentClassMapping;
import com.magikslate.backend.entity.StudentParentMapping;
import com.magikslate.backend.entity.Subject;
import com.magikslate.backend.exception.InvalidOnboardingInputException;
import com.magikslate.backend.onboarding.StudentInput;
import com.magikslate.backend.onboarding.StudentInput.ParentInput;
import com.magikslate.backend.services.attendance.AttendanceService;
import com.magikslate.backend.services.student.StudentService;
import com.magikslate.backend.services.subject.SubjectService;
import com.magikslate.backend.util.AWSUtil;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import io.vertx.core.json.JsonObject;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.time.Year;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class StudentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentController.class);

    @Autowired
    private StudentService studentService;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private AttendanceService attendanceService;

    @Autowired
    private AWSUtil awsUtil;

    @GetMapping("/v1/secure/app/student/{id}")
    public ResponseEntity<StudentDto> user(@RequestAttribute("userId") Integer userId,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer adminId,
            @RequestAttribute("role") String role,
            @Valid @NotNull @Min(value = 1) @PathVariable("id") Integer studentId) {

        LOGGER.info("Get student by Id = {}", studentId);

        Student student = studentService.get(studentId);
        if (student == null) {
            LOGGER.info("No such student found. Returning status = {}", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else if (!student.getSchool().getId().equals(schoolId)) {
            LOGGER.info("You do not have access to this student. Returning status = {}", HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } else {
            StudentDto studentDto = convertToStudentDto(student);
            studentDto.setClassAvgAttendance(attendanceService.getClassAvgAttendance(schoolId, student.getStudentClassMappingList().get(0).getClass1().getId()));
            LOGGER.info("Returning body = {} , status = {}", studentDto, HttpStatus.OK);
            return new ResponseEntity<>(studentDto, HttpStatus.OK);
        }
    }

    @GetMapping("/v1/secure/app/student")
    public ResponseEntity<List<StudentDto>> studentsBySchool(
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("role") String role) {

        LOGGER.info("Get students for schoolId = {} requested by role = {}", schoolId, role);

        if (Constants.ADMIN.equals(role)) {
            List<Student> studentList = studentService.getStudentListBySchool(schoolId);
            List<StudentDto> studentDtoList = new ArrayList();
            studentList.stream().map((student) -> convertToStudentDto(student)).forEachOrdered(studentDtoList::add);
            LOGGER.info("Returning body = {} , status = {}", studentDtoList, HttpStatus.OK);
            return new ResponseEntity<>(studentDtoList, HttpStatus.OK);
        } else {
            LOGGER.info("Returning body = {} , status = {}", null, HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping(value = "/v1/secure/app/student/profilepic", headers = ("content-type=multipart/*"))
    public ResponseEntity<Void> profilePic(@RequestParam MultipartFile file,
            @RequestAttribute("schoolId") Integer schoolId) {

        if (file != null && !file.isEmpty()) {
            String fileType = Utils.getFileType(file);

            if (Constants.JPEG.equals(fileType) || Constants.JPG.equals(fileType) || Constants.PNG.equals(fileType)) {

                String randomUUID = UUID.randomUUID().toString();
                String folder = Constants.S3_STUDENT_DP_FOLDER.
                        replace(Constants.SCHOOL_ID_MACRO, schoolId + "")
                        + randomUUID
                        + Constants.DOT_SEPARATOR
                        + FilenameUtils.getExtension(file.getOriginalFilename());

                LOGGER.debug("Folder = {}", folder);

                try {
                    File localFile = Utils.writeToDisk(file, "/disk1/temp/" + randomUUID + Constants.DOT_SEPARATOR + fileType);
                    LOGGER.debug("Uploading localFile = {} to folder = {} in S3 bucket = {}", localFile, folder, Constants.S3_SCHOOL_BUCKET);

                    if (awsUtil.uploadToS3(localFile, folder, Constants.S3_SCHOOL_BUCKET)) {
                        LOGGER.debug("File successfully uploaded.");

                        FileUtils.forceDelete(localFile);
                        LOGGER.debug("Local copy of file deleted");

                        HttpHeaders headers = new HttpHeaders();
                        headers.setLocation(URI.create(Constants.STATIC_RESOURCE_DNS + folder));

                        LOGGER.info("Returning header = {} with status {}", headers, HttpStatus.CREATED);
                        return new ResponseEntity<>(headers, HttpStatus.CREATED);
                    } else {
                        FileUtils.forceDelete(localFile);
                        LOGGER.error("File could not be uploaded to S3. Returning status = {} ", HttpStatus.INTERNAL_SERVER_ERROR);
                        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                } catch (IOException ex) {
                    LOGGER.error("Exception while writing to disk. Returning status = {}. Error = {} ", HttpStatus.INTERNAL_SERVER_ERROR, ex);
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }

            } else {
                LOGGER.info("File is not valid image. Returning status = {}", HttpStatus.BAD_REQUEST);
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {
            LOGGER.info("File is null or empty. Returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/v1/secure/app/student/{id}")
    public ResponseEntity<Void> student(@Valid @NotNull @Min(value = 1) @PathVariable("id") Integer studentId,
            @RequestAttribute("role") String role) {

        LOGGER.info("Delete student by Id = {}", studentId);

        if (!Constants.ADMIN.equals(role)) {
            LOGGER.info("Returning body = {} , status = {}", null, HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }

        studentService.remove(new Student(studentId));
        LOGGER.info("Returning status {}", HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/v1/secure/app/student/{id}")
    public ResponseEntity<JsonObject> studentEdit(
            @RequestBody StudentDto studentDto,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("role") String role) {

        LOGGER.info("Editing a student for schoolId = {} with role = {} with studentDto = {}", schoolId, role, studentDto);

        HttpHeaders headers = new HttpHeaders();

        Student student = studentDto.convertToStudent();
        if (student.getStudentParentMappingList() == null || student.getStudentParentMappingList().isEmpty()) {
            LOGGER.info("No parents sent. Returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(Utils.generateErrorJson("Atleast 1 parent should be provided"), HttpStatus.BAD_REQUEST);
        }
        student.setSchool(new School(schoolId));

        try {
            studentService.update(student);
            LOGGER.debug("Student updated with id {}", student.getId());

            studentDto = convertToStudentDto(student);
            Map<String, Object> resultMap = new HashMap();
            resultMap.put("student", studentDto);
            JsonObject jsonObject = new JsonObject(resultMap);

            LOGGER.info("Returning header = {} and body = {} with status {}", headers, jsonObject, HttpStatus.OK);
            return new ResponseEntity<>(jsonObject, headers, HttpStatus.OK);
        } catch (InvalidOnboardingInputException e) {
            LOGGER.info("InvalidOnboardingInputException = {} Returning status = {}", Utils.generateErrorJson(e.getMessage()), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(Utils.generateErrorJson(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/v1/secure/app/student")
    public ResponseEntity<JsonObject> student(
            @RequestBody StudentDto studentDto,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("role") String role) {

        LOGGER.info("Creating a student for school Id = {} with role = {} with studentDto = {}", schoolId, role, studentDto);

        HttpHeaders headers = new HttpHeaders();

        StudentInput studentInput = convertToStudentInput(studentDto);
        if (studentInput == null) {
            LOGGER.info("StudentDto conversion resulted in null. Returning status {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        try {
            Student student = studentService.add(studentInput, schoolId);
            if (student == null) {
                LOGGER.info("Addition of student failed. Returning status {}", HttpStatus.BAD_REQUEST);
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            LOGGER.debug("Student created with id {}", student.getId());

            headers.set("Resource-Id", String.valueOf(student.getId()));

            studentDto = convertToStudentDto(student);
            Map<String, Object> resultMap = new HashMap();
            resultMap.put("student", studentDto);
            JsonObject jsonObject = new JsonObject(resultMap);

            LOGGER.info("Returning header = {} and body = {} with status {}", headers, jsonObject, HttpStatus.CREATED);
            return new ResponseEntity<>(jsonObject, headers, HttpStatus.CREATED);
        } catch (InvalidOnboardingInputException e) {
            LOGGER.info("InvalidOnboardingInputException = {} Returning status = {}", Utils.generateErrorJson(e.getMessage()), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(Utils.generateErrorJson(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    private StudentDto convertToStudentDto(Student student) {
        StudentDto studentDto = new StudentDto();
        studentDto.setId(student.getId());
        studentDto.setName(student.getName());
        studentDto.setProfilePicUrl(student.getProfilePic());
        studentDto.setAdmissionCode(student.getAdmissionCode());
        studentDto.setDob(student.getDateOfBirth());
        studentDto.setGender(student.getGender());

        if (student.getStudentClassMappingList() != null && !student.getStudentClassMappingList().isEmpty()) {
            studentDto.setStudentClassMappingId(student.getStudentClassMappingList().get(0).getId());
        }

        if (student.getStudentStudyGroupMappingList() != null && !student.getStudentStudyGroupMappingList().isEmpty()) {
            studentDto.setStudentStudyGroupMappingId(student.getStudentStudyGroupMappingList().get(0).getId());
        }

        List<StudentParentMapping> studentParentMappingList = student.getStudentParentMappingList();
        List<ParentDto> parentDtoList = new ArrayList();

        for (StudentParentMapping spm : studentParentMappingList) {
            ParentDto pd = new ParentDto();
            pd.setId(spm.getParent().getId());
            UserDto ud = new UserDto();
            ud.setName(spm.getParent().getUser().getName());
            ud.setId(spm.getParent().getUser().getId());
            ud.setEmail(spm.getParent().getUser().getEmail());
            ud.setPhone(spm.getParent().getUser().getPhone());
            ud.setProfilePic(spm.getParent().getUser().getProfilePic());
            pd.setUser(ud);
            pd.setAddress(spm.getParent().getAddress());
            parentDtoList.add(pd);
        }

        studentDto.setParent(parentDtoList);

        List<StudentClassMapping> studentClassMappingList = student.getStudentClassMappingList();
        for (StudentClassMapping scm : studentClassMappingList) {
            Class clazz = scm.getClass1();
            ClassDto classDto = new ClassDto();
            classDto.setId(clazz.getId());
            classDto.setStandard(clazz.getStandard());
            classDto.setSection(clazz.getSection());
            studentDto.setClazz(classDto);
            break;
        }

        List<Attendance> attendanceList = student.getStudentClassMappingList().get(0).getAttendanceList();
        studentDto.setPercAttendance(Utils.calculatePercAttendance(attendanceList, student.getId()));

        if (attendanceList != null) {
            Set<Date> absetDateSet = new HashSet();
            attendanceList.stream().filter((attendance) -> (attendance.getSessionYear().equals(Year.now().getValue()) && !attendance.getPresent())).forEachOrdered((attendance) -> {
                absetDateSet.add(attendance.getUpdatedAt());
            });
            studentDto.setAbsentDates(absetDateSet);
        }

        //subjectlist
        List<Subject> subjectList = subjectService.getSubjectsForStudent(student);

        List<SubjectDto> subjectDtoList = new ArrayList();
        subjectList.stream().map((subject) -> {
            SubjectDto sd = new SubjectDto();
            sd.setId(subject.getId());
            sd.setName(subject.getName());
            return sd;
        }).forEachOrdered((sd) -> {
            subjectDtoList.add(sd);
        });

        studentDto.setSubjectList(subjectDtoList);

        return studentDto;
    }

    private StudentInput convertToStudentInput(StudentDto studentDto) {

        StudentInput studentInput = null;

        List<ParentDto> parentDtoList = studentDto.getParent();
        if (parentDtoList == null) {
            return null;
        }
        List<ParentInput> parentInputList = new ArrayList();

        try {
            parentDtoList.stream().map((parentDto) -> new ParentInput(parentDto.getUser().getName(),
                    parentDto.getUser().getEmail(), parentDto.getUser().getPhone(),
                    parentDto.getAddress(), 0)).forEachOrdered(parentInputList::add);

            studentInput = new StudentInput(studentDto.getAdmissionCode(),
                    studentDto.getName(), studentDto.getDob(),
                    studentDto.getClazz().getStandard(),
                    studentDto.getClazz().getSection(),
                    studentDto.getGender(),
                    parentInputList, studentDto.getProfilePicUrl());
        } catch (InvalidOnboardingInputException ex) {
            LOGGER.debug(ex.toString());
        }
        return studentInput;
    }
}
