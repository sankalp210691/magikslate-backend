/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.rest;

import static com.google.common.io.Files.map;
import com.magikslate.backend.dto.SchoolReviewDto;
import com.magikslate.backend.entity.Parent;
import com.magikslate.backend.entity.ParentSchoolReview;
import com.magikslate.backend.entity.School;
import com.magikslate.backend.services.review.ReviewService;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import io.vertx.core.json.JsonObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author sankalpkulshrestha
 */
@RestController
@RequestMapping("/v1")
public class SchoolReviewController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SchoolReviewController.class);

    @Autowired
    private ReviewService reviewService;

    @PostMapping("/secure/app/review")
    public ResponseEntity<JsonObject> review(@RequestBody SchoolReviewDto schoolReviewDto,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer roleId,
            @RequestAttribute("role") String role,
            @RequestAttribute("userId") Integer userId,
            UriComponentsBuilder ucBuilder) {
        LOGGER.info("Creating a school review with dto = {}", schoolReviewDto);

        //TODO: we could put it in dto annotation...but would it work only for non-null values?
        if (schoolReviewDto.getRating() == null || schoolReviewDto.getRating() < 1 || schoolReviewDto.getRating() > 5) {
            JsonObject error = Utils.generateErrorJson("Invalid rating data");
            LOGGER.info("Invalid rating data. Returning body = {} , status = {}", error.toString(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }

        //TODO: we could put it in dto annotation...but would it work only for non-null values?
        if (schoolReviewDto.getReview() == null || schoolReviewDto.getReview().length() < Constants.MIN_REVIEW_SIZE || schoolReviewDto.getReview().length() > Constants.MAX_REVIEW_SIZE) {
            JsonObject error = Utils.generateErrorJson("Review is either too short or too big");
            LOGGER.info("Review is either too short or too big. Returning body = {} , status = {}", error.toString(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }

        if (role.equals(Constants.PARENT)) {
            LOGGER.debug("Role matched PARENT");
            ParentSchoolReview parentSchoolReview = createParentSchoolReview(schoolReviewDto, roleId, schoolId);
            if (reviewService.getAll(role, roleId, schoolId, Constants.OLD_DATE, Constants.FUTURE_DATE, 0, true, true).size() > 0) {
                JsonObject error = Utils.generateErrorJson("You have already written a review earlier. Please update it");
                LOGGER.info("Cannot write another review for same school by same user. Returning body = {} , status = {}", error.toString(), HttpStatus.UNAUTHORIZED);
                return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
            }
            //since this parent has not written review earlier, we allow him to write
            int id = reviewService.add(parentSchoolReview);

            LOGGER.debug("ParentSchoolReview created with id = {}", id);

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(ucBuilder.path("/secure/app/review/{id}").buildAndExpand(id).toUri());

            LOGGER.info("Returning headers = {}, status = {}", headers, HttpStatus.CREATED);
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        } else {
            JsonObject error = Utils.generateErrorJson("You are unauthorized to create reviews");
            LOGGER.info("role did not match PARENT. Returning body = {} , status = {}", error.toString(), HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
        }
    }

    @PatchMapping("/secure/app/review")
    public ResponseEntity<JsonObject> review(@RequestBody SchoolReviewDto schoolReviewDto,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer roleId,
            @RequestAttribute("role") String role) {

        if (schoolReviewDto.getId() == null) {
            JsonObject error = Utils.generateErrorJson("JSON body should have id field ");
            LOGGER.info("JSON body should have id field. Returning body = {} , status = {}", error.toString(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }

        //TODO: we could put it in dto annotation...but would it work only for non-null values?
        if (schoolReviewDto.getRating() != null && (schoolReviewDto.getRating() < 1 || schoolReviewDto.getRating() > 5)) {
            JsonObject error = Utils.generateErrorJson("Invalid rating data");
            LOGGER.info("Invalid rating data. Returning body = {} , status = {}", error.toString(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }
        //TODO: we could put it in dto annotation...but would it work only for non-null values?
        if (schoolReviewDto.getReview() != null && (schoolReviewDto.getReview().length() < Constants.MIN_REVIEW_SIZE || schoolReviewDto.getReview().length() > Constants.MAX_REVIEW_SIZE)) {
            JsonObject error = Utils.generateErrorJson("Review is either too short or too big");
            LOGGER.info("Review is either too short or too big. Returning body = {} , status = {}", error.toString(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }

        if (!Constants.PARENT.equals(role)) {
            JsonObject error = Utils.generateErrorJson("You are unauthorized to update this review because of incorrect role");
            LOGGER.info("role did not match PARENT. Returning body = {} , status = {}", error.toString(), HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
        }

        ParentSchoolReview psr = createParentSchoolReview(schoolReviewDto, roleId, schoolId);
        if (reviewService.updateParentSchoolReview(psr, roleId)) {
            LOGGER.info("Returning status = {}", HttpStatus.NO_CONTENT);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            LOGGER.info("reviewService.updateParentSchoolReview returned false. Returning status = {}", HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

    }
    
    @GetMapping("/secure/app/review/count")
    public ResponseEntity<JsonObject> reviewCount(
            @RequestAttribute("roleId") Integer roleId,
            @RequestAttribute("role") String role,
            @RequestAttribute("schoolId") Integer schoolId) {
        
        LOGGER.info("Getting review count for schoolId = {}", schoolId);
        
        long count = reviewService.getReviewCountForSchool(schoolId);
        String result = "{\"count\" : " + count + "}";
        JsonObject json = new JsonObject(result);
        
        LOGGER.info("Returning status = {}, body = {} ", HttpStatus.OK, json);
        return new ResponseEntity<>(json, HttpStatus.OK);
    }
    
    @GetMapping("/review/{schoolId}")
    public ResponseEntity<JsonObject> review(@PathVariable("schoolId") Integer schoolId,
            @RequestParam(name = "offset") Integer offset) {
        LOGGER.info("Getting reviews for schoolId = {}", schoolId);
        
        List<ParentSchoolReview> reviewList = reviewService
                .getAll(null, null, schoolId, Constants.OLD_DATE, Constants.FUTURE_DATE, offset, true, false);
        double averageRating = reviewService.getAverageRating(reviewList);
        
        LOGGER.debug("reviewList = {}", reviewList);

        List<SchoolReviewDto> reviewDtoList = new ArrayList();
        if (reviewList != null) {
            reviewList.forEach((review) -> {
                SchoolReviewDto reviewDto = createParentSchoolReviewDto(review, -1);
                reviewDtoList.add(reviewDto);
            });
            
            Map<String, Object> map = new HashMap();
            map.put("averageRating", averageRating);
            map.put("reviews", reviewDtoList);
            JsonObject json = new JsonObject(map);
            return new ResponseEntity<>(json, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/secure/app/review")
    public ResponseEntity<JsonObject> review(@RequestAttribute("roleId") Integer roleId,
            @RequestAttribute("role") String role, @RequestAttribute("schoolId") Integer schoolId,
            @RequestParam(name = "startDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date endDate,
            @RequestParam(name = "offset") Integer offset,
            @RequestParam(name = "filterBySchool", required = false) boolean filterBySchool,
            @RequestParam(name = "filterByRoleId", required = false) boolean filterByRoleId) {

        LOGGER.info("Getting reviews by filter. role = {} , "
                + "roleId = {} , schoolId = {} , startDate = {} , endDate = "
                + "{} , offset = {} , filterByRoleId = {}, received", role, roleId,
                schoolId, startDate, endDate, offset, filterBySchool, filterByRoleId);

        if (!filterBySchool && !filterByRoleId) {

            LOGGER.info("Caller didn't specify neither filterBySchool nor filterByRoleId. So returning status = ", HttpStatus.NO_CONTENT);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        List<ParentSchoolReview> reviewList = reviewService
                .getAll(role, roleId, schoolId, startDate, endDate, offset, filterBySchool, filterByRoleId);
        double averageRating = reviewService.getAverageRating(reviewList);

        LOGGER.debug("reviewList = {}", reviewList);

        List<SchoolReviewDto> reviewDtoList = new ArrayList();
        if (reviewList != null) {
            reviewList.forEach((review) -> {
                SchoolReviewDto reviewDto = createParentSchoolReviewDto(review, roleId);
                reviewDtoList.add(reviewDto);
            });
            
            Map<String, Object> map = new HashMap();
            map.put("averageRating", averageRating);
            map.put("reviews", reviewDtoList);
            JsonObject json = new JsonObject(map);
            return new ResponseEntity<>(json, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
    }

    private ParentSchoolReview createParentSchoolReview(SchoolReviewDto schoolReviewDto, int parentId, int schoolId) {
        ParentSchoolReview review = new ParentSchoolReview();
        review.setId(schoolReviewDto.getId());
        review.setParent(new Parent(parentId));
        review.setSchool(new School(schoolId));
        if (schoolReviewDto.getRating() != null) {
            review.setRating(schoolReviewDto.getRating());
        } else {
            review.setRating(-1);
        }
        if (schoolReviewDto.getReview() != null) {
            review.setReview(schoolReviewDto.getReview());
        }
        review.setCreatedAt(new Date());
        return review;
    }

    private SchoolReviewDto createParentSchoolReviewDto(ParentSchoolReview review, Integer roleId) {
        SchoolReviewDto reviewDto = new SchoolReviewDto();
        reviewDto.setId(review.getId());
        reviewDto.setSchoolName(review.getSchool().getName());
        reviewDto.setRating(review.getRating());
        reviewDto.setReview(review.getReview());
        reviewDto.setCreatedAt(review.getCreatedAt());
        reviewDto.setIsSelfReview(Objects.equals(review.getParent().getId(), roleId));
        return reviewDto;
    }

}
