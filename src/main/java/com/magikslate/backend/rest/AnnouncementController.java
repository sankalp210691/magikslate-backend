/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.rest;

import com.magikslate.backend.dto.AnnouncementDto;
import com.magikslate.backend.entity.Admin;
import com.magikslate.backend.entity.Announcement;
import com.magikslate.backend.entity.Class;
import com.magikslate.backend.services.announcement.AnnouncementService;
import com.magikslate.backend.services.clazz.ClassService;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import io.vertx.core.json.JsonObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author sankalpkulshrestha
 */
@RestController
@RequestMapping("/v1/secure/app/announcement")
public class AnnouncementController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnnouncementController.class);

    @Autowired
    private AnnouncementService announcementService;

    @Autowired
    private ClassService classService;

    @PostMapping("")
    public ResponseEntity<JsonObject> announcement(@RequestBody AnnouncementDto announcementDto,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer adminId,
            @RequestAttribute("role") String role,
            UriComponentsBuilder ucBuilder) {
        LOGGER.info("Creating an announcement with dto = {}", announcementDto);

        if (role.equals(Constants.ADMIN)) {
            LOGGER.debug("Role matched ADMIN");
            if (announcementDto.isSendToParents() || announcementDto.isSendToStaff()) {
                List<Class> classList = classService.getClassListBySchool(schoolId, null);
                LOGGER.debug("classList = {}", classList);
                Set<Integer> classSet = classList.stream().map(clazz -> clazz.getId()).collect(Collectors.toCollection(HashSet::new));
                for (Integer classId : announcementDto.getClassList()) {
                    if (!classSet.contains(classId)) {
                        JsonObject error = Utils.generateErrorJson("One or more invalid class");
                        LOGGER.info("Returning body = {}, status = {}", error.toString(), HttpStatus.UNAUTHORIZED);
                        return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
                    }
                }

                Announcement announcement = convertToAnnouncement(announcementDto, adminId);
                Integer id = announcementService.add(announcement);

                LOGGER.debug("Announcement created with id = {}", id);

                HttpHeaders headers = new HttpHeaders();
                headers.setLocation(ucBuilder.path("/secure/app/announcement/{id}").buildAndExpand(id).toUri());

                LOGGER.info("Returning headers = {}, status = {}", headers, HttpStatus.CREATED);

                return new ResponseEntity<>(headers, HttpStatus.CREATED);
            } else {
                JsonObject error = Utils.generateErrorJson("Nobody receiver for this announcement");

                LOGGER.info("isSendToParents = {} , isSendToStaff = {}. Returning body = {} , status = {}",
                        announcementDto.isSendToParents(), announcementDto
                        .isSendToStaff(), error.toString(), HttpStatus.BAD_REQUEST);

                return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
            }
        } else {
            JsonObject error = Utils.generateErrorJson("You are unauthorized to create announcements");
            LOGGER.info("role did not match ADMIN. Returning body = {} , status = {}", error.toString(), HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<AnnouncementDto> announcement(@Valid @NotNull @Min(value = 1) @PathVariable("id") Integer announcementId,
            @RequestAttribute("schoolId") Integer schoolId) {

        LOGGER.info("get announcement by Id = {}", announcementId);

        Announcement announcement = announcementService.get(announcementId, schoolId);
        if (announcement == null) {
            LOGGER.info("Announcement is NULL. Returning status = {}", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            LOGGER.debug("announcement = {}", announcement);
            AnnouncementDto announcementDto = AnnouncementDto.convertToAnnouncementDto(announcement);
            LOGGER.info("Returning body = {} , status = {}", announcementDto, HttpStatus.OK);
            return new ResponseEntity<>(announcementDto, HttpStatus.OK);
        }
    }

    @GetMapping("")
    public ResponseEntity<JsonObject> announcement(@RequestAttribute("roleId") Integer roleId,
            @RequestAttribute("role") String role, @RequestAttribute("schoolId") Integer schoolId,
            @RequestParam(name = "startDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date endDate,
            @RequestParam(name = "offset") Integer offset,
            @RequestParam(name = "self", required = false) boolean isSelfNeeded,
            @RequestParam(name = "received", required = false) boolean isReceivedNeeded,
            @RequestAttribute("userId") Integer userId) {

        LOGGER.info("Getting announcements by filter. role = {} , "
                + "roleId = {} , schoolId = {} , startDate = {} , endDate = "
                + "{} , offset = {} , userId = {}, self = {}, received", role, roleId,
                schoolId, startDate, endDate, offset, userId, isSelfNeeded, isReceivedNeeded);

        if (!isSelfNeeded && !isReceivedNeeded) {

            LOGGER.info("Caller didn't specify neither self nor received. So returning status = ", HttpStatus.NO_CONTENT);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        List<List<Announcement>> announcementListList = announcementService
                .getAll(userId, role, roleId, schoolId, startDate, endDate, offset, isSelfNeeded, isReceivedNeeded);

        LOGGER.debug("announcementListList = {}", announcementListList);

        JsonObject json = new JsonObject();

        if (isSelfNeeded) {

            List<AnnouncementDto> selfAnnouncementDtoList = new ArrayList();
            List<Announcement> selfAnnouncementList = announcementListList.get(0);
            if (selfAnnouncementList != null) {
                selfAnnouncementList.forEach((announcement) -> {
                    selfAnnouncementDtoList.add(AnnouncementDto.convertToAnnouncementDto(announcement));
                });
            }

            if (!selfAnnouncementDtoList.isEmpty()) {
                json.put("self", selfAnnouncementDtoList);
            }
        }

        if (isReceivedNeeded) {

            List<AnnouncementDto> recAnnouncementDtoList = new ArrayList();
            List<Announcement> recAnnouncementList = announcementListList.get(1);
            if (recAnnouncementList != null) {
                recAnnouncementList.forEach((announcement) -> {
                    recAnnouncementDtoList.add(AnnouncementDto.convertToAnnouncementDto(announcement));
                });
            }

            if (!recAnnouncementDtoList.isEmpty()) {
                json.put("received", recAnnouncementDtoList);
            }
        }

        if (json.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
    }

    private Announcement convertToAnnouncement(AnnouncementDto announcementDto, Integer adminId) {
        Announcement announcement = new Announcement();
        announcement.setSendToParents(announcementDto.isSendToParents());
        announcement.setSendToStaff(announcementDto.isSendToStaff());
        announcement.setTitle(announcementDto.getTitle());
        announcement.setDescription(announcementDto.getDescription());
        announcement.setAdmin(new Admin(adminId));
        announcement.setCreatedAt(new Date());
        List<Class> classList = new ArrayList();
        announcementDto.getClassList().forEach((id) -> {
            classList.add(new Class(id));
        });
        announcement.setClassList(classList);
        return announcement;
    }

}
