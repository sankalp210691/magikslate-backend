/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.rest;

import com.magikslate.backend.dto.SubjectDto;
import com.magikslate.backend.entity.School;
import com.magikslate.backend.entity.Subject;
import com.magikslate.backend.exception.InvalidOnboardingInputException;
import com.magikslate.backend.onboarding.StudentTeacherInput;
import com.magikslate.backend.services.subject.SubjectService;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import io.vertx.core.json.JsonObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author sankalpkulshrestha
 */
@RestController
@RequestMapping("/v1")
public class SubjectController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubjectController.class);

    @Autowired
    private SubjectService subjectService;

    @PostMapping("/secure/app/subject")
    public ResponseEntity<JsonObject> subject(@RequestBody SubjectDto subjectDto,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer roleId,
            @RequestAttribute("role") String role,
            @RequestAttribute("userId") Integer userId) {

        List<StudentTeacherInput> studentTeacherInputList = new ArrayList();

        String subject = subjectDto.getName();
        Map<String, String> classTeacherMap = subjectDto.getClassTeacherMap();

        for (String classInfoString : classTeacherMap.keySet()) {
            String teacherInfoString = classTeacherMap.get(classInfoString);

            String[] classInfoArray = classInfoString.split(Constants.UNDERSCORE_SEPARATOR);
            String[] teacherInfoArray = teacherInfoString.split(Constants.UNDERSCORE_SEPARATOR);

            String employeeId = teacherInfoArray[1];
            String standard = classInfoArray[1];
            String section = classInfoArray[2];

            StudentTeacherInput studentTeacherInput = new StudentTeacherInput(employeeId, subject, standard, section, null);
            studentTeacherInputList.add(studentTeacherInput);
        }

        List<Subject> subjectList = null;
        try {
            subjectList = subjectService.createBulk(studentTeacherInputList, schoolId);
        } catch (InvalidOnboardingInputException e) {
            LOGGER.error(e.toString());

            LOGGER.info("Returning status = {}", HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity<>(Utils.generateErrorJson(e.getMessage()), HttpStatus.BAD_REQUEST);
        }

        if (subjectList != null && !subjectList.isEmpty()) {
            LOGGER.info("Returning status = {}", HttpStatus.CREATED);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } else {

            LOGGER.info("Returning response = {} with status = {}", Utils.generateErrorJson("No new teacher-student mapping created").toString(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(Utils.generateErrorJson("No new teacher-student mapping created"), HttpStatus.BAD_REQUEST);
        }
    }

//    @PatchMapping("/secure/app/subject")
//    public ResponseEntity<Void> editSubject(@RequestBody SubjectDto subjectDto,
//            @RequestAttribute("schoolId") Integer schoolId,
//            @RequestAttribute("roleId") Integer roleId,
//            @RequestAttribute("role") String role) {
//
//        Subject subject = subjectDto.convertToSubject(new School(schoolId));
//        
//        subjectService.update(subject);
//    }
//
//    @GetMapping("/secure/app/subject")
//    public ResponseEntity<JsonObject> listSubjects(
//            @RequestAttribute("schoolId") Integer schoolId,
//            @RequestAttribute("roleId") Integer roleId,
//            @RequestAttribute("role") String role) {
//
//    }

}
