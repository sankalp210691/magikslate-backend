/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.rest;

import com.google.common.base.Joiner;
import com.magikslate.backend.dto.StudyGroupClassIdDto;
import com.magikslate.backend.dto.TeacherDto;
import com.magikslate.backend.dto.UserDto;
import com.magikslate.backend.entity.ClassTeacherMapping;
import com.magikslate.backend.entity.School;
import com.magikslate.backend.entity.Teacher;
import com.magikslate.backend.exception.InvalidOnboardingInputException;
import com.magikslate.backend.onboarding.TeacherInput;
import com.magikslate.backend.services.subject.SubjectService;
import com.magikslate.backend.services.teacher.TeacherService;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import io.vertx.core.json.JsonObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author sankalpkulshrestha
 */
@RestController
@RequestMapping("/v1/secure/app/teacher")
public class TeacherController {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(TeacherController.class);

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private TeacherService teacherService;

    @PostMapping("")
    public ResponseEntity<JsonObject> teacher(
            @RequestBody TeacherDto teacherDto,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("role") String role) {

        LOGGER.info("Creating a teacher for school Id = {} with role = {} with teacherDto = {}", schoolId, role, teacherDto);

        HttpHeaders headers = new HttpHeaders();
        
        try {
            TeacherInput teacherInput = new TeacherInput(teacherDto.getEmployeeId(),
                teacherDto.getUser().getName(), teacherDto.getUser().getPhone(),
                teacherDto.getUser().getEmail(), teacherDto.getCtClass());
            
            Teacher teacher = teacherService.add(teacherInput, schoolId);
            if (teacher == null) {
                LOGGER.info("Addition of teacher failed. Returning status {}", HttpStatus.BAD_REQUEST);
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            LOGGER.debug("Teacher created with id {}", teacher.getId());

            headers.set("Resource-Id", String.valueOf(teacher.getId()));

            teacherDto = convertToTeacherDto(teacher);
            Map<String, Object> resultMap = new HashMap();
            resultMap.put("teacher", teacherDto);
            JsonObject jsonObject = new JsonObject(resultMap);

            LOGGER.info("Returning header = {} and body = {} with status {}", headers, jsonObject, HttpStatus.CREATED);
            return new ResponseEntity<>(jsonObject, headers, HttpStatus.CREATED);
        } catch (InvalidOnboardingInputException e) {
            LOGGER.info("InvalidOnboardingInputException = {} Returning status = {}", Utils.generateErrorJson(e.getMessage()), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(Utils.generateErrorJson(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> teacher(@Valid @NotNull @Min(value = 1) @PathVariable("id") Integer teacherId,
            @RequestAttribute("role") String role) {

        LOGGER.info("Delete teacher by Id = {}", teacherId);

        if (!Constants.ADMIN.equals(role)) {
            LOGGER.info("Returning body = {} , status = {}", null, HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }

        teacherService.remove(new Teacher(teacherId));
        LOGGER.info("Returning status {}", HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @PutMapping("/{id}")
    public ResponseEntity<JsonObject> teacherEdit(
            @RequestBody TeacherDto teacherDto,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("role") String role) {

        LOGGER.info("Editing a teacher for school Id = {} with role = {} with teacherDto = {}", schoolId, role, teacherDto);

        HttpHeaders headers = new HttpHeaders();
        
        Teacher teacher = teacherDto.convertToTeacher();
        if (teacher == null) {
            LOGGER.info("Couldn't convert to Teacher object. Returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(Utils.generateErrorJson("Invalid teacher information"), HttpStatus.BAD_REQUEST);
        }
        teacher.setSchool(new School(schoolId));

        try {
            teacherService.update(teacher);
            LOGGER.debug("Teacher updated with id {}", teacher.getId());

            teacherDto = convertToTeacherDto(teacher);
            Map<String, Object> resultMap = new HashMap();
            resultMap.put("teacher", teacherDto);
            JsonObject jsonObject = new JsonObject(resultMap);

            LOGGER.info("Returning header = {} and body = {} with status {}", headers, jsonObject, HttpStatus.OK);
            return new ResponseEntity<>(jsonObject, headers, HttpStatus.OK);
        } catch (InvalidOnboardingInputException e) {
            LOGGER.info("InvalidOnboardingInputException = {} Returning status = {}", Utils.generateErrorJson(e.getMessage()), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(Utils.generateErrorJson(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/{teacherId}/subject/{subjectId}")
    public ResponseEntity<Void> assign(@RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer roleId, @RequestAttribute("role") String role,
            @Valid @RequestBody StudyGroupClassIdDto sciDto,
            @Valid @NotNull @Min(value = 1) @PathVariable("teacherId") Integer teacherId,
            @Valid @NotNull @Min(value = 1) @PathVariable("subjectId") Integer subjectId) {

        LOGGER.info("Assigning subject and class/studnts to a teacher");

        if (!Constants.ADMIN.equals(role)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        if ((sciDto.getStudyGroupId() == null || sciDto.getStudyGroupId().isEmpty())
                && (sciDto.getClassId() == null || sciDto.getClassId().isEmpty())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<Integer> idList = null;
        if (sciDto.getClassId() != null && !sciDto.getClassId().isEmpty()) {
            idList = subjectService.assignTeacher(true, sciDto.getClassId(), schoolId, roleId, teacherId, subjectId);
        } else {
            idList = subjectService.assignTeacher(false, sciDto.getStudyGroupId(), schoolId, roleId, teacherId, subjectId);
        }
        if (idList == null) {
            LOGGER.info("Invalid subjectid/teacherid");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.set("Resource-Id", Joiner.on(",").join(idList));
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @GetMapping("")
    public ResponseEntity<List<TeacherDto>> teacherBySchool(@RequestAttribute("schoolId") Integer schoolId) {

        LOGGER.info("Get teachers for school id = {}", schoolId);

        List<Teacher> teacherList = teacherService.getTeacherBySchool(schoolId);
        List<TeacherDto> teacherDtoList = new ArrayList();
        teacherList.stream().map((teacher) -> convertToTeacherDto(teacher)).forEachOrdered(teacherDtoList::add);
        LOGGER.info("Returning body = {} , status = {}", teacherDtoList, HttpStatus.OK);
        return new ResponseEntity<>(teacherDtoList, HttpStatus.OK);
    }

    private TeacherDto convertToTeacherDto(Teacher teacher) {
        TeacherDto teacherDto = new TeacherDto();
        teacherDto.setId(teacher.getId());
        teacherDto.setEmployeeId(teacher.getEmployeeId());
        
        if(teacher.getClassTeacherMappingList() != null && !teacher.getClassTeacherMappingList().isEmpty()) {
            teacher.getClassTeacherMappingList().stream().filter((ctm) -> (ctm.getDeletedAt() == null)).forEachOrdered((ctm) -> {
                teacherDto.setCtClass(ctm.getClass1().getId());
            });
        }

        UserDto userDto = new UserDto();
        userDto.setId(teacher.getUser().getId());
        userDto.setName(teacher.getUser().getName());
        userDto.setPhone(teacher.getUser().getPhone());
        userDto.setEmail(teacher.getUser().getEmail());
        userDto.setProfilePic(teacher.getUser().getProfilePic());

        teacherDto.setUser(userDto);
        return teacherDto;
    }
}
