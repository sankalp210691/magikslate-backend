/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.rest;

/**
 *
 * @author sankalpkulshrestha
 */
import com.magikslate.backend.dto.ChangePasswordDto;
import com.magikslate.backend.dto.CityDto;
import com.magikslate.backend.dto.ClassDto;
import com.magikslate.backend.dto.CountryDto;
import com.magikslate.backend.dto.ForgotPasswordDto;
import com.magikslate.backend.dto.LocalityDto;
import com.magikslate.backend.dto.LoginDto;
import com.magikslate.backend.dto.OtpDto;
import com.magikslate.backend.dto.ParentDto;
import com.magikslate.backend.dto.ResendOtpDto;
import com.magikslate.backend.dto.SchoolDto;
import com.magikslate.backend.dto.SchoolRoleDto;
import com.magikslate.backend.dto.StateDto;
import com.magikslate.backend.dto.StudentClassDto;
import com.magikslate.backend.dto.StudentDto;
import com.magikslate.backend.dto.TeacherClassDto;
import com.magikslate.backend.dto.TeacherDto;
import com.magikslate.backend.dto.UserDto;
import com.magikslate.backend.entity.Admin;
import com.magikslate.backend.entity.Parent;
import com.magikslate.backend.entity.School;
import com.magikslate.backend.entity.Student;
import com.magikslate.backend.entity.StudentClassMapping;
import com.magikslate.backend.entity.StudentParentMapping;
import com.magikslate.backend.entity.StudentStudyGroupMapping;
import com.magikslate.backend.entity.StudyGroup;
import com.magikslate.backend.entity.StudyGroupTeacherMapping;
import com.magikslate.backend.entity.Subject;
import com.magikslate.backend.entity.SubjectClassTeacherMapping;
import com.magikslate.backend.entity.Teacher;
import com.magikslate.backend.entity.User;
import com.magikslate.backend.exception.AuthenticationException;
import com.magikslate.backend.exception.EntityExistsException;
import com.magikslate.backend.exception.InvalidOnboardingInputException;
import com.magikslate.backend.onboarding.OnboardingFileReader;
import com.magikslate.backend.onboarding.OnboardingFileReaderFactory;
import com.magikslate.backend.onboarding.StudentInput;
import com.magikslate.backend.onboarding.StudentTeacherInput;
import com.magikslate.backend.onboarding.TeacherInput;
import com.magikslate.backend.services.student.StudentService;
import com.magikslate.backend.services.subject.SubjectService;
import com.magikslate.backend.services.teacher.TeacherService;
import com.magikslate.backend.services.user.UserService;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import io.jsonwebtoken.ExpiredJwtException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.apache.commons.lang.RandomStringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private OnboardingFileReaderFactory onboardingFileReaderFactory;

    @Autowired
    private StudentService studentService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/v1/secure/user")
    public ResponseEntity<UserDto> user(@RequestAttribute("userId") Integer userId) {

        LOGGER.info("Get user by Id = {}", userId);

        User user = userService.get(userId);
        if (user == null) {
            LOGGER.info("No such user found. Returning status = {}", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else if (!user.getId().equals(userId)) {
            LOGGER.info("Only self user can get his/her own information. Returning status = {}", HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } else {
            user.setPassword(null);
            UserDto userDto = convertToUserDto(user);
            LOGGER.info("Returning body = {} , status = {}", userDto, HttpStatus.OK);
            return new ResponseEntity<>(userDto, HttpStatus.OK);
        }
    }

    @PostMapping("/v1/user")
    public ResponseEntity<Void> user(@Valid @RequestBody UserDto userDto, UriComponentsBuilder ucBuilder) {
        LOGGER.info("Creating a user with dto = {}", userDto);
        HttpHeaders headers = new HttpHeaders();

        User user = convertToUser(userDto);
        Integer id;
        try {
            id = userService.add(user);
            LOGGER.debug("User created with id {}", id);

            headers.set("Resource-Id", String.valueOf(id));
            headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(id).toUri());

            LOGGER.info("Returning header = {} with status {}", headers, HttpStatus.CREATED);
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        } catch (EntityExistsException ex) {
            LOGGER.info("{} . Returning {}", ex.toString(), HttpStatus.CONFLICT);
            return new ResponseEntity<>(headers, HttpStatus.CONFLICT);
        }
    }

    @PostMapping("/v1/user/verify")
    public ResponseEntity<Void> user(@Valid @RequestBody OtpDto otpDto) {

        HttpHeaders headers = new HttpHeaders();

        LOGGER.info("Verifying user with dto = {}", otpDto);

        User user = new User();
        user.setId(otpDto.getUserId());
        user.setOtp(otpDto.getOtp());
        boolean verificationSuccess = userService.verifyOtpAndActivate(user);
        LOGGER.info("Returning status {}", verificationSuccess ? HttpStatus.CREATED : HttpStatus.UNAUTHORIZED);

        return new ResponseEntity<>(headers, verificationSuccess ? HttpStatus.CREATED : HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/v1/secure/user/listRoles")
    public ResponseEntity<Map<String, List<SchoolRoleDto>>> listRoles(@RequestAttribute("userId") Integer userId) {
        LOGGER.info("List User's roles and schools with userId = {}", userId);

        User user = userService.getRolewiseSchools(userId);
        if (user == null) {
            LOGGER.info("user is NULL. Returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Map<String, List<SchoolRoleDto>> respBody = convertToUserSchoolRoleDto(user);
        LOGGER.info("Returning body = {}, status = {}", respBody, HttpStatus.OK);
        return new ResponseEntity<>(respBody, HttpStatus.OK);
    }

    @GetMapping("/v1/secure/user/roleauth/{role}/{roleId}")
    public ResponseEntity<Void> roleAuth(@RequestAttribute("userId") Integer userId,
            @PathVariable("role") String role, @PathVariable("roleId") Integer roleId,
            @RequestParam("sid") Integer schoolId, @RequestParam(name = "stid", required = false) Integer studentId) {

        LOGGER.info("Give role auth token with role = {} and roleId = {} "
                + "for userId = {}, schoolId = {}, studentId = {}", role, roleId,
                userId, schoolId, studentId);

        String roleAuthToken = userService.hasRoleAccess(role, userId, roleId, schoolId, studentId);
        if (roleAuthToken != null) {

            LOGGER.debug("roleAuthToken = {}", roleAuthToken);

            HttpHeaders headers = new HttpHeaders();
            headers.add("roleauth", "Bearer " + roleAuthToken);

            LOGGER.info("Returning headers = {}, status = {}", headers, HttpStatus.OK);
            return new ResponseEntity<>(headers, HttpStatus.OK);
        } else {
            LOGGER.info("roleAuthToken is NULL. Returning status {}", HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @PatchMapping("/v1/user/forgotpassword")
    public ResponseEntity<Void> forgotPasswordwithPhone(@RequestBody ForgotPasswordDto forgotPasswordDto) {

        HttpHeaders headers = new HttpHeaders();

        LOGGER.info("Forgot password 1 for number = {}", forgotPasswordDto.getPhone());

        if (Utils.isNullOrEmpty(forgotPasswordDto.getPhone())) {
            LOGGER.info("Forgot password phone is empty. Returning with status {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
        }

        try {
            if (userService.forgotPassword(forgotPasswordDto.getPhone())) {
                LOGGER.info("Returning with status {}", HttpStatus.OK);
                return new ResponseEntity<>(headers, HttpStatus.OK);
            } else {
                LOGGER.info("User not found. Returning with status {}", HttpStatus.BAD_REQUEST);
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }

        } catch (Exception ex) {
            LOGGER.error("Exception encountered: {}. Returning with status {}", ex, HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity<>(headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/v1/user/forgotpassword")
    public ResponseEntity<Void> forgotPasswordWithOtp(@RequestBody ForgotPasswordDto forgotPasswordDto) {

        HttpHeaders headers = new HttpHeaders();

        LOGGER.info("Forgot password 2 for number = {} and otp = {}", forgotPasswordDto.getPhone(), forgotPasswordDto.getOtp());

        Integer otp = null;
        try {
            otp = Integer.parseInt(forgotPasswordDto.getOtp());

            try {
                Integer userId = userService.forgotPassword(forgotPasswordDto.getPhone(), otp);

                if (userId == -1) {
                    LOGGER.info("User not found. Returning with status {}", HttpStatus.BAD_REQUEST);
                    return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
                } else {
                    String passwordAuthToken = Utils.getUserForgotPasswordToken(userId);

                    headers.add("fpauthorization", "Bearer " + passwordAuthToken);
                    LOGGER.info("Returning with headers = {} and status {}", headers, HttpStatus.OK);
                    return new ResponseEntity<>(headers, HttpStatus.OK);
                }

            } catch (Exception ex) {
                LOGGER.error("Exception encountered : {}. Returning with status {}", ex, HttpStatus.INTERNAL_SERVER_ERROR);
                return new ResponseEntity<>(headers, HttpStatus.INTERNAL_SERVER_ERROR);
            }

        } catch (NumberFormatException ex) {
            LOGGER.info("Exception encountered : {}. Returning with status {}", ex, HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/v1/user/login")
    public ResponseEntity<JsonObject> login(@Valid @RequestBody LoginDto loginDto) {

        HttpHeaders headers = new HttpHeaders();

        LOGGER.info("Logging in a user with dto = {}", loginDto);
        
        if(loginDto.getPhone() == null && loginDto.getEmail() == null) {
            LOGGER.info("Null email and phone. Returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        User user = convertToUser(loginDto);
        LOGGER.debug("user = {}", user);
        try {
            String[] result = userService.authenticate(user);
            String status = result[0];
            String token = result[1];
            Integer userId = Integer.parseInt(result[2]);

            if (Constants.ACTIVE.equals(status) || Constants.REG_PASSWORD_CHANGE_PENDING.equals(status)) {
                headers.add("authorization", "Bearer " + token);
            }
            if(Constants.REG_PASSWORD_CHANGE_PENDING.equals(status)) {
                headers.add("fpauthorization", "Bearer " + Utils.getUserForgotPasswordToken(userId));
            }

            JsonObject resp = new JsonObject("{\"status\": \"" + status + "\", \"userId\": " + userId + "}");

            LOGGER.info("Returning body = {}, headers = {}, status = {}", status, headers, HttpStatus.OK);
            return new ResponseEntity<>(resp, headers, HttpStatus.OK);
        } catch (AuthenticationException ex) {
            LOGGER.info("AuthenticationException. Returning header = {}, status = {}", headers, HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(headers, HttpStatus.UNAUTHORIZED);
        }
    }

    @PatchMapping("/v1/user/updatePassword")
    public ResponseEntity<Void> updatePasswordWithoutLogin(@Valid @RequestBody ChangePasswordDto changePasswordDto, @RequestHeader HttpHeaders reqHeaders) {

        HttpHeaders headers = new HttpHeaders();

        LOGGER.info("Changing password with dto = {}", changePasswordDto);

        List<String> fpauthorization = reqHeaders.get("fpauthorization");
        if (fpauthorization == null || fpauthorization.isEmpty() || !fpauthorization.get(0).startsWith("Bearer ")) {

            LOGGER.info("Invalid fpauthHeader = {}. Returning status = {}", fpauthorization, HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(headers, HttpStatus.UNAUTHORIZED);
        } else {

            final String token = fpauthorization.get(0).substring(7);

            try {
                Map<String, Object> claimMap = Utils.decipherUserForgotPasswordToken(token);
                String type = (String) claimMap.get("type");

                if (!Constants.FORGOT_PASSWORD_TOKEN.equals(type)) {

                    LOGGER.info("Invalid fpauthHeader = {} because type != fp. Returning status = {}", HttpStatus.UNAUTHORIZED);
                    return new ResponseEntity<>(headers, HttpStatus.UNAUTHORIZED);
                }

                Integer userId = (Integer) claimMap.get("userId");
                if (userId == null) {
                    LOGGER.info("Extracted userId is NULL. Returning status = {}", HttpStatus.UNAUTHORIZED);
                    return new ResponseEntity<>(headers, HttpStatus.UNAUTHORIZED);
                } else {
                    LOGGER.info("Request userId = {}", userId);

                    if (!changePasswordDto.getNewPassword().equals(changePasswordDto.getRepeatNewPassword())) {
                        LOGGER.info("New Password and Repeat New Password do not match. Returning status = {}", HttpStatus.BAD_REQUEST);
                        return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
                    }

                    boolean passwordChangedSuccessfully = userService.changePassword(userId,
                            changePasswordDto.getOldPassword(), changePasswordDto.getNewPassword(),
                            false);
                    LOGGER.debug("passwordChangedSuccessfully = {}", passwordChangedSuccessfully);

                    if (passwordChangedSuccessfully) {
                        LOGGER.info("Returning headers = {} , status = {}", headers, HttpStatus.CREATED);
                        return new ResponseEntity<>(headers, HttpStatus.CREATED);
                    } else {
                        LOGGER.info("Returning status = {}", headers, HttpStatus.BAD_REQUEST);
                        return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
                    }
                }
            } catch (ExpiredJwtException ex) {
                LOGGER.info("ExpiredJwtException ex = {}. Returning status = {}", ex.toString(), HttpStatus.UNAUTHORIZED);
                return new ResponseEntity<>(headers, HttpStatus.UNAUTHORIZED);
            }
        }
    }

    @PatchMapping("/v1/secure/user/updatePassword")
    public ResponseEntity<Void> updatePassword(@Valid @RequestBody ChangePasswordDto changePasswordDto,
            @RequestAttribute("userId") Integer userId) {
        LOGGER.info("Changing password with dto = {} and userId = {}", changePasswordDto, userId);

        if (Utils.isNullOrEmpty(changePasswordDto.getOldPassword())) {
            LOGGER.info("Old password cannot be empty. Returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (!changePasswordDto.getNewPassword().equals(changePasswordDto.getRepeatNewPassword())) {
            LOGGER.info("New Password and Repeat New Password do not match. Returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        HttpHeaders headers = new HttpHeaders();
        boolean passwordChangedSuccessfully = userService.changePassword(userId,
                changePasswordDto.getOldPassword(), changePasswordDto.getNewPassword(),
                true);
        LOGGER.debug("passwordChangedSuccessfully = {}", passwordChangedSuccessfully);

        if (passwordChangedSuccessfully) {
            LOGGER.info("Returning headers = {} , status = {}", headers, HttpStatus.CREATED);
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        } else {
            LOGGER.info("Returning status = {}", headers, HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    //step 3 (student) and 4 (teacher) and 5 (teacher-subject-student mapping)
    @PostMapping(value = "/v1/secure/app/user/{userType}", headers = ("content-type=multipart/*"))
    public ResponseEntity<JsonObject> school(@RequestParam MultipartFile file,
            @PathVariable("userType") String userType, @RequestAttribute("schoolId") Integer schoolId) {

        LOGGER.info("Uploading " + userType + " file");

        if (file.isEmpty()) {
            return new ResponseEntity<>(Utils.generateErrorJson("Empty file"), HttpStatus.BAD_REQUEST);
        } else {
            try {
                String fileType = Utils.getFileType(file);
                OnboardingFileReader onboardingFileReader = onboardingFileReaderFactory.get(fileType);
                if (onboardingFileReader == null) {
                    LOGGER.info("{}. Returning status = {}", Utils.generateErrorJson("Unsupported file format uploaded. Please use csv format.").toString(), HttpStatus.BAD_REQUEST);
                    return new ResponseEntity<>(Utils.generateErrorJson("Unsupported file format uploaded. Please use csv format."), HttpStatus.BAD_REQUEST);
                }
                HttpHeaders headers = new HttpHeaders();
                String filename = Constants.UPLOAD_FILE_LOCATION + schoolId + Constants.UNDERSCORE_SEPARATOR + RandomStringUtils.randomAlphanumeric(10) + Constants.UNDERSCORE_SEPARATOR + userType + Constants.DOT_SEPARATOR + fileType;
                switch (userType) {
                    case Constants.STUDENT_TYPE:
                        LOGGER.debug("Going to read student file input");
                        List<StudentInput> studentInputList = onboardingFileReader.readStudentInput(Utils.writeToDisk(file, filename));
                        LOGGER.debug("Calling createBulk for student creation");
                        List<Student> studentList = studentService.createBulk(studentInputList, schoolId);

                        if (!studentList.isEmpty()) {
                            JsonArray classJsonArray = convertStudentListToClassJsonArray(studentList);
                            Map<String, Object> respObjectMap = new HashMap();
                            respObjectMap.put("data", classJsonArray);
                            JsonObject resp = new JsonObject(respObjectMap);
                            headers.set(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");

                            LOGGER.info("Returning response = {} , headers = {} with status = {}", resp.toString(), headers, HttpStatus.CREATED);
                            return new ResponseEntity<>(resp, headers, HttpStatus.CREATED);
                        } else {

                            LOGGER.info("Returning response = {} with status = {}", Utils.generateErrorJson("No new students created").toString(), HttpStatus.BAD_REQUEST);
                            return new ResponseEntity<>(Utils.generateErrorJson("No new students created"), HttpStatus.BAD_REQUEST);
                        }
                    case Constants.TEACHER_TYPE:
                        LOGGER.debug("Going to read teacher file input");
                        List<TeacherInput> teacherInputList = onboardingFileReader.readTeacherInput(Utils.writeToDisk(file, filename));
                        LOGGER.debug("Calling createBulk for teacher creation");
                        List<Teacher> teacherList = teacherService.createBulk(teacherInputList, schoolId);

                        if (!teacherList.isEmpty()) {
                            JsonArray teacherJsonArray = convertTeacherListToJsonArray(teacherList);
                            Map<String, Object> respObjectMap = new HashMap();
                            respObjectMap.put("data", teacherJsonArray);
                            JsonObject resp = new JsonObject(respObjectMap);
                            headers.set(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");

                            LOGGER.info("Returning response = {} , headers = {} with status = {}", resp.toString(), headers, HttpStatus.CREATED);
                            return new ResponseEntity<>(resp, headers, HttpStatus.CREATED);
                        } else {

                            LOGGER.info("Returning response = {} with status = {}", Utils.generateErrorJson("No new teachers created").toString(), HttpStatus.BAD_REQUEST);
                            return new ResponseEntity<>(Utils.generateErrorJson("No new teachers created"), HttpStatus.BAD_REQUEST);
                        }
                    case Constants.STUDENT_TEACHER_TYPE:
                        LOGGER.debug("Going to read studentTeacherMapping file input");
                        List<StudentTeacherInput> studentTeacherInputList = onboardingFileReader.readStudentTeacherInput(Utils.writeToDisk(file, filename));
                        LOGGER.debug("Calling createBulk for studentTeacherMapping creation");
                        List<Subject> subjectList = subjectService.createBulk(studentTeacherInputList, schoolId);

                        if (!subjectList.isEmpty()) {
                            List<TeacherClassDto> classDtoList = convertToTeacherClassDto(subjectList);
                            JsonArray resp = new JsonArray(classDtoList);
                            headers.set(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");
                            Map<String, Object> map = new HashMap();
                            map.put("result", resp);
                            JsonObject response = new JsonObject(map);

                            LOGGER.info("Returning response = {}, headers = {} with status = {}", response.toString(), headers, HttpStatus.CREATED);
                            return new ResponseEntity<>(response, headers, HttpStatus.CREATED);
                        } else {

                            LOGGER.info("Returning response = {} with status = {}", Utils.generateErrorJson("No new teacher-student mapping created").toString(), HttpStatus.BAD_REQUEST);
                            return new ResponseEntity<>(Utils.generateErrorJson("No new teacher-student mapping created"), HttpStatus.BAD_REQUEST);
                        }
                    default:

                        LOGGER.info("Returning response = {} with status = {}", Utils.generateErrorJson("Invalid user type").toString(), HttpStatus.BAD_REQUEST);
                        return new ResponseEntity<>(Utils.generateErrorJson("Invalid user type"), HttpStatus.BAD_REQUEST);
                }
            } catch (IOException e) {
                LOGGER.error(e.toString());

                LOGGER.info("Returning status = {}", HttpStatus.INTERNAL_SERVER_ERROR);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (InvalidOnboardingInputException e) {
                LOGGER.error(e.toString());

                LOGGER.info("Returning status = {}", HttpStatus.INTERNAL_SERVER_ERROR);
                return new ResponseEntity<>(Utils.generateErrorJson(e.getMessage()), HttpStatus.BAD_REQUEST);
            }
        }
    }

    @PostMapping("/user/resendOtp")
    public ResponseEntity<JsonObject> login(@Valid @RequestBody ResendOtpDto resendOtpDto) {

//        HttpHeaders headers = new HttpHeaders();
//
//        LOGGER.info("Logging in a user with dto = {}", loginDto);
//
//        User user = convertToUser(loginDto);
//        LOGGER.debug("user = {}", user);
//        try {
//            String[] result = userService.authenticate(user);
//            String status = result[0];
//            String token = result[1];
//            Integer userId = Integer.parseInt(result[2]);
//            headers.add("authorization", "Bearer " + token);
//
//            JsonObject resp = new JsonObject("{\"status\": \"" + status + "\", \"userId\": "+ userId +"}");
//
//            LOGGER.info("Returning body = {}, headers = {}, status = {}", status, headers, HttpStatus.OK);
//            return new ResponseEntity<>(resp, headers, HttpStatus.OK);
//        } catch (AuthenticationException ex) {
//            LOGGER.info("AuthenticationException. Returning header = {}, status = {}", headers, HttpStatus.UNAUTHORIZED);
//            return new ResponseEntity<>(headers, HttpStatus.UNAUTHORIZED);
//        }
        return null;
    }

    private User convertToUser(UserDto userDto) {
        return modelMapper.map(userDto, User.class);
    }

    private User convertToUser(LoginDto loginDto) {
        return modelMapper.map(loginDto, User.class);
    }

    private Map<String, List<SchoolRoleDto>> convertToUserSchoolRoleDto(User user) {
        List<Admin> adminList = user.getAdminList() == null ? new ArrayList() : user.getAdminList();
        List<Teacher> teacherList = user.getTeacherList() == null ? new ArrayList() : user.getTeacherList();
        List<Parent> parentList = user.getParentList() == null ? new ArrayList() : user.getParentList();

        LOGGER.debug("adminList = {} , teacherList = {} , parentList = {}", adminList, teacherList, parentList);

        Map<String, List<SchoolRoleDto>> srdMap = new HashMap();
        Map<Integer, SchoolDto> sdMap = new HashMap();

        for (Admin admin : adminList) {
            SchoolRoleDto srd = new SchoolRoleDto();
            School school = admin.getSchool();
            srd.setRoleId(admin.getId());

            SchoolDto sd = sdMap.get(admin.getSchool().getId());
            if (sd == null) {
                sd = new SchoolDto();
                sd.setId(admin.getSchool().getId());
                sd.setName(school.getName());
                sd.setStage(school.getStage());

                LocalityDto localityDto = new LocalityDto();
                localityDto.setId(school.getLocality().getId());
                localityDto.setName(school.getLocality().getName());
                localityDto.setCity(new CityDto());
                localityDto.getCity().setId(school.getLocality().getCity().getId());
                localityDto.getCity().setName(school.getLocality().getCity().getName());
                localityDto.getCity().setCode(school.getLocality().getCity().getCode());
                localityDto.getCity().setState(new StateDto());
                localityDto.getCity().getState().setId(school.getLocality().getCity().getState().getId());
                localityDto.getCity().getState().setName(school.getLocality().getCity().getState().getName());
                localityDto.getCity().getState().setCountry(new CountryDto());
                localityDto.getCity().getState().getCountry().setId(school.getLocality().getCity().getState().getCountry().getId());
                localityDto.getCity().getState().getCountry().setName(school.getLocality().getCity().getState().getCountry().getName());
                localityDto.getCity().getState().getCountry().setCode(school.getLocality().getCity().getState().getCountry().getCode());
                localityDto.getCity().getState().getCountry().setInternationalPhoneCode(school.getLocality().getCity().getState().getCountry().getInternationalPhoneCode());

                sd.setLocality(localityDto);
                sd.setAddress(school.getAddress());
                sd.setPincode(school.getPincode());
                if (school.getBoard() != null) {
                    sd.setBoardId(school.getBoard().getId());
                    sd.setBoardName(school.getBoard().getName());
                }
                sd.setCreatedAt(school.getCreatedAt());
                sd.setUpdatedAt(school.getUpdatedAt());
                sd.setCurrentUserAdminId(admin.getId());
                sd.setDisplayPic(school.getDisplayPic());
                sdMap.put(sd.getId(), sd);
            }
            srd.setSchool(sd);
            List<SchoolRoleDto> srdList = srdMap.get(Constants.ADMIN);
            if (srdList == null) {
                srdList = new ArrayList();
                srdMap.put(Constants.ADMIN, srdList);
            }
            srdList.add(srd);
        }

        for (Teacher teacher : teacherList) {
            SchoolRoleDto srd = new SchoolRoleDto();
            School school = teacher.getSchool();
            srd.setRoleId(teacher.getId());

            if (teacher.getSchool().getStage() < 5) {
                continue;
            }

            SchoolDto sd = sdMap.get(teacher.getSchool().getId());
            if (sd == null) {
                sd = new SchoolDto();
                sd.setId(school.getId());
                sd.setName(school.getName());
                sd.setStage(school.getStage());

                LocalityDto localityDto = new LocalityDto();
                localityDto.setId(school.getLocality().getId());
                localityDto.setName(school.getLocality().getName());
                localityDto.setCity(new CityDto());
                localityDto.getCity().setId(school.getLocality().getCity().getId());
                localityDto.getCity().setName(school.getLocality().getCity().getName());
                localityDto.getCity().setCode(school.getLocality().getCity().getCode());
                localityDto.getCity().setState(new StateDto());
                localityDto.getCity().getState().setId(school.getLocality().getCity().getState().getId());
                localityDto.getCity().getState().setName(school.getLocality().getCity().getState().getName());
                localityDto.getCity().getState().setCountry(new CountryDto());
                localityDto.getCity().getState().getCountry().setId(school.getLocality().getCity().getState().getCountry().getId());
                localityDto.getCity().getState().getCountry().setName(school.getLocality().getCity().getState().getCountry().getName());
                localityDto.getCity().getState().getCountry().setCode(school.getLocality().getCity().getState().getCountry().getCode());
                localityDto.getCity().getState().getCountry().setInternationalPhoneCode(school.getLocality().getCity().getState().getCountry().getInternationalPhoneCode());

                sd.setLocality(localityDto);
                sd.setAddress(school.getAddress());
                sd.setPincode(school.getPincode());
                if (school.getBoard() != null) {
                    sd.setBoardId(school.getBoard().getId());
                    sd.setBoardName(school.getBoard().getName());
                }
                sd.setCreatedAt(school.getCreatedAt());
                sd.setUpdatedAt(school.getUpdatedAt());
                sdMap.put(sd.getId(), sd);
            }
            srd.setSchool(sd);
            List<SchoolRoleDto> srdList = srdMap.get(Constants.TEACHER);
            if (srdList == null) {
                srdList = new ArrayList();
                srdMap.put(Constants.TEACHER, srdList);
            }
            srdList.add(srd);
        }

        for (Parent parent : parentList) {
            List<StudentParentMapping> studentParentMappingList = parent.getStudentParentMappingList();
            //TODO: all these checks need to go to service itself
            studentParentMappingList.stream().filter((spm) -> spm.getDeletedAt() == null && spm.getStudent().getDeletedAt() == null && spm.getStudent().getSchool().getStage() == 5).map((spm) -> {
                SchoolRoleDto srd = new SchoolRoleDto();
                srd.setRoleId(parent.getId());
                StudentDto std = new StudentDto();
                School school = spm.getStudent().getSchool();
                std.setId(spm.getStudent().getId());
                std.setName(spm.getStudent().getName());
                std.setCreatedAt(spm.getStudent().getCreatedAt());
                std.setUpdatedAt(spm.getStudent().getUpdatedAt());
                SchoolDto sd = sdMap.get(school.getId());
                if (sd == null) {
                    sd = new SchoolDto();
                    sd.setId(school.getId());
                    sd.setName(school.getName());
                    sd.setStage(school.getStage());

                    LocalityDto localityDto = new LocalityDto();
                    localityDto.setId(school.getLocality().getId());
                    localityDto.setName(school.getLocality().getName());
                    localityDto.setCity(new CityDto());
                    localityDto.getCity().setId(school.getLocality().getCity().getId());
                    localityDto.getCity().setName(school.getLocality().getCity().getName());
                    localityDto.getCity().setCode(school.getLocality().getCity().getCode());
                    localityDto.getCity().setState(new StateDto());
                    localityDto.getCity().getState().setId(school.getLocality().getCity().getState().getId());
                    localityDto.getCity().getState().setName(school.getLocality().getCity().getState().getName());
                    localityDto.getCity().getState().setCountry(new CountryDto());
                    localityDto.getCity().getState().getCountry().setId(school.getLocality().getCity().getState().getCountry().getId());
                    localityDto.getCity().getState().getCountry().setName(school.getLocality().getCity().getState().getCountry().getName());
                    localityDto.getCity().getState().getCountry().setCode(school.getLocality().getCity().getState().getCountry().getCode());
                    localityDto.getCity().getState().getCountry().setInternationalPhoneCode(school.getLocality().getCity().getState().getCountry().getInternationalPhoneCode());

                    sd.setLocality(localityDto);
                    sd.setAddress(school.getAddress());
                    sd.setPincode(school.getPincode());
                    if (school.getBoard() != null) {
                        sd.setBoardId(school.getBoard().getId());
                        sd.setBoardName(school.getBoard().getName());
                    }
                    sd.setCreatedAt(school.getCreatedAt());
                    sd.setUpdatedAt(school.getUpdatedAt());
                    sd.setDisplayPic(school.getDisplayPic());
                    sdMap.put(sd.getId(), sd);
                }
                std.setSchool(sd);
                ClassDto classDto = new ClassDto();
                List<StudentClassMapping> studentClassMappingList = spm.getStudent().getStudentClassMappingList();
                for (StudentClassMapping scm : studentClassMappingList) {
                    if (scm.getClass1().getSchool().getId().equals(spm.getStudent().getSchool().getId())) {
                        classDto.setId(scm.getClass1().getId());
                        classDto.setStandard(scm.getClass1().getStandard());
                        classDto.setSection(scm.getClass1().getSection());
                    }
                }
                std.setClazz(classDto);
                srd.setStudent(std);
                return srd;
            }).forEachOrdered((srd) -> {
                List<SchoolRoleDto> srdList = srdMap.get(Constants.PARENT);
                if (srdList == null) {
                    srdList = new ArrayList();
                    srdMap.put(Constants.PARENT, srdList);
                }
                srdList.add(srd);
            });
        }

        LOGGER.debug("srdMap = {}", srdMap);
        return srdMap;
    }

    private UserDto convertToUserDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setPhone(user.getPhone());
        userDto.setProfilePic(user.getProfilePic());
        userDto.setEmail(user.getEmail());
        return userDto;
    }

    private JsonArray convertTeacherListToJsonArray(List<Teacher> teacherList) {
        List<TeacherDto> teacherDtoList = new ArrayList();
        teacherList.forEach((teacher) -> {
            TeacherDto teacherDto = new TeacherDto();
            teacherDtoList.add(teacherDto);
            teacherDto.setId(teacher.getId());
            teacherDto.setName(teacher.getUser().getName());
            teacherDto.setEmail(teacher.getUser().getEmail());
            teacherDto.setPhone(teacher.getUser().getPhone());
            teacherDto.setEmployeeId(teacher.getEmployeeId());
        });
        return new JsonArray(teacherDtoList);
    }

    private JsonArray convertStudentListToClassJsonArray(List<Student> studentList) {

        Map<com.magikslate.backend.entity.Class, StudentClassDto> scdcMap = new HashMap();
        studentList.forEach(student -> {
            List<StudentClassMapping> studentClassMappingList = student.getStudentClassMappingList();
            studentClassMappingList.stream().map((scm) -> scm.getClass1()).forEachOrdered((clazz) -> {
                if (scdcMap.containsKey(clazz)) {
                    StudentClassDto studentClassDto = scdcMap.get(clazz);

                    StudentDto studentDto = new StudentDto();
                    studentDto.setId(student.getId());
                    studentDto.setName(student.getName());
                    List<ParentDto> parentDtoList = new ArrayList();
                    List<StudentParentMapping> studentParentMappingList = student.getStudentParentMappingList();
                    studentParentMappingList.stream().map((spm) -> {
                        ParentDto parentDto = new ParentDto();
                        parentDto.setId(spm.getParent().getId());
                        parentDto.setAddress(spm.getParent().getAddress());
                        UserDto userDto = new UserDto();
                        User user = spm.getParent().getUser();
                        userDto.setId(user.getId());
                        userDto.setName(user.getName());
                        userDto.setEmail(user.getEmail());
                        userDto.setPhone(user.getPhone());
                        parentDto.setUser(userDto);
                        return parentDto;
                    }).forEachOrdered((parentDto) -> {
                        parentDtoList.add(parentDto);
                    });
                    studentDto.setParent(parentDtoList);

                    studentClassDto.getStudent().add(studentDto);
                } else {
                    StudentClassDto studentClassDto = new StudentClassDto();
                    studentClassDto.setClassId(clazz.getId());
                    studentClassDto.setSection(clazz.getSection());
                    studentClassDto.setStandard(clazz.getStandard());

                    StudentDto studentDto = new StudentDto();
                    studentDto.setId(student.getId());
                    studentDto.setName(student.getName());
                    List<ParentDto> parentDtoList = new ArrayList();
                    List<StudentParentMapping> studentParentMappingList = student.getStudentParentMappingList();
                    studentParentMappingList.stream().map((spm) -> {
                        ParentDto parentDto = new ParentDto();
                        parentDto.setId(spm.getParent().getId());
                        parentDto.setAddress(spm.getParent().getAddress());
                        UserDto userDto = new UserDto();//use a different dto. this one contains password...even though null, it doesnt look good
                        User user = spm.getParent().getUser();
                        userDto.setId(user.getId());
                        userDto.setName(user.getName());
                        userDto.setEmail(user.getEmail());
                        userDto.setPhone(user.getPhone());
                        parentDto.setUser(userDto);
                        return parentDto;
                    }).forEachOrdered((parentDto) -> {
                        parentDtoList.add(parentDto);
                    });
                    studentDto.setParent(parentDtoList);

                    List<StudentDto> studentDtoList = new ArrayList();
                    studentDtoList.add(studentDto);
                    studentClassDto.setStudent(studentDtoList);
                    scdcMap.put(clazz, studentClassDto);
                }
            });
        });
        return new JsonArray(new ArrayList(scdcMap.values()));

    }

    private List<TeacherClassDto> convertToTeacherClassDto(List<Subject> subjectList) {
        List<TeacherClassDto> tcdList = new ArrayList();

        subjectList.forEach((subject) -> {
            List<SubjectClassTeacherMapping> subjectClassTeacherMappingList = subject.getSubjectClassTeacherMappingList();
            List<StudyGroup> studyGroupList = subject.getStudyGroupList();
            if (subjectClassTeacherMappingList != null && !subjectClassTeacherMappingList.isEmpty()) {
                subjectClassTeacherMappingList.stream().map((sctm) -> {
                    TeacherClassDto tcd = new TeacherClassDto();
                    tcd.setTeacherId(sctm.getTeacher().getId());
                    tcd.setTeacherName(sctm.getTeacher().getUser().getName());
                    tcd.setClassId(sctm.getClass1().getId());
                    tcd.setStandard(sctm.getClass1().getStandard());
                    tcd.setSection(sctm.getClass1().getSection());
                    return tcd;
                }).map((tcd) -> {
                    tcd.setSubjectId(subject.getId());
                    return tcd;
                }).map((tcd) -> {
                    tcd.setSubjectName(subject.getName());
                    return tcd;
                }).forEachOrdered((tcd) -> {
                    tcdList.add(tcd);
                });
            }
            if (studyGroupList != null && !studyGroupList.isEmpty()) {
                studyGroupList.forEach((studyGroup) -> {
                    List<StudyGroupTeacherMapping> studyGroupTeacherMappingList = studyGroup.getStudyGroupTeacherMappingList();
                    if (studyGroupTeacherMappingList != null && !studyGroupTeacherMappingList.isEmpty()) {
                        studyGroupTeacherMappingList.stream().map((sgtm) -> {
                            TeacherClassDto tcd = new TeacherClassDto();
                            tcd.setTeacherId(sgtm.getTeacher().getId());
                            tcd.setTeacherName(sgtm.getTeacher().getUser().getName());
                            tcd.setTeacherName(sgtm.getTeacher().getUser().getName());
                            tcd.setStudyGroupId(sgtm.getStudyGroup().getId());
                            return tcd;
                        }).map((tcd) -> {
                            tcd.setSubjectId(studyGroup.getSubject().getId());
                            return tcd;
                        }).map((tcd) -> {
                            tcd.setSubjectName(studyGroup.getSubject().getName());
                            return tcd;
                        }).map((tcd) -> {
                            List<StudentStudyGroupMapping> studentStudyGroupMappingList = studyGroup.getStudentStudyGroupMappingList();
                            studentStudyGroupMappingList.forEach((ssgm) -> {
                                List<StudentDto> studentDtoList = tcd.getStudent();
                                if (studentDtoList == null) {
                                    studentDtoList = new ArrayList();
                                    tcd.setStudent(studentDtoList);
                                }
                                StudentDto sd = new StudentDto();
                                sd.setId(ssgm.getStudent().getId());
                                sd.setName(ssgm.getStudent().getName());
                                studentDtoList.add(sd);
                            });
                            return tcd;
                        }).forEachOrdered((tcd) -> {
                            tcdList.add(tcd);
                        });

                    }
                });
            }
        });
        return tcdList;
    }
}
