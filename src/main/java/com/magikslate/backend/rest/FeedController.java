/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.rest;

import com.magikslate.backend.dto.FeedDto;
import com.magikslate.backend.feed.Feed;
import com.magikslate.backend.services.admin.AdminService;
import com.magikslate.backend.services.parent.ParentService;
import com.magikslate.backend.util.Constants;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author sankalp.kulshrestha
 */
@RestController
@RequestMapping("/v1/secure/app/feed")
@Validated
public class FeedController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FeedController.class);

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ParentService parentService;
    
    @Autowired
    private AdminService adminService;

    @GetMapping("")
    public ResponseEntity<List<FeedDto>> feed(
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer roleId,
            @RequestAttribute("userId") Integer userId,
            @RequestAttribute("role") String role,
            @RequestParam(name = "offset") Integer offset,
            @RequestAttribute(name = "studentId", required = false) Integer studentId) {

        LOGGER.info("Feed request. role = {}, roleId = {}, schoolId = {}, userId = {}, studentId = {} offset = {}", role, roleId, schoolId, userId, studentId, offset);

        if (Constants.PARENT.equals(role) && studentId == null) {

            LOGGER.info("Parent role should have studentId. Returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        Feed feed = null;
        switch (role) {
            case Constants.PARENT:
                feed = parentService.getFeed(userId, studentId, schoolId, offset, role, roleId);
                break;
            case Constants.ADMIN:
                feed = adminService.getFeed(userId, schoolId, offset, role, roleId);
                break;
            default:
                LOGGER.info("Invalid role. Returning status = {}", HttpStatus.BAD_REQUEST);
                return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        List<FeedDto> feedDtoList = FeedDto.convertToFeedDto(feed, userId, modelMapper);

        if (feedDtoList.isEmpty()) {
            LOGGER.info("Empty feedlist. Returning status = {}", HttpStatus.NO_CONTENT);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } else {
            LOGGER.info("Returning feedDtoList = {} status = {}", feedDtoList, HttpStatus.OK);
            return new ResponseEntity<>(feedDtoList, HttpStatus.OK);
        }
    }

}
