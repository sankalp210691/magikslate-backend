/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.rest;

/**
 *
 * @author sankalpkulshrestha
 */
import com.magikslate.backend.dto.ApplyDto;
import com.magikslate.backend.dto.CityDto;
import com.magikslate.backend.dto.ClassDto;
import com.magikslate.backend.dto.CountryDto;
import com.magikslate.backend.dto.InstituteCategoryDto;
import com.magikslate.backend.dto.LocalityDto;
import com.magikslate.backend.dto.SchoolDto;
import com.magikslate.backend.dto.StateDto;
import com.magikslate.backend.entity.Admin;
import com.magikslate.backend.entity.Board;
import com.magikslate.backend.entity.Class;
import com.magikslate.backend.entity.InstituteCategory;
import com.magikslate.backend.entity.Locality;
import com.magikslate.backend.entity.School;
import com.magikslate.backend.entity.SchoolInstituteCategoryMapping;
import com.magikslate.backend.entity.User;
import com.magikslate.backend.entity.UserSchoolInterest;
import com.magikslate.backend.services.apply.ApplyService;
import com.magikslate.backend.services.school.SchoolService;
import com.magikslate.backend.util.AWSUtil;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/v1")
public class SchoolController {

    @Autowired
    private SchoolService schoolService;

    @Autowired
    private AWSUtil awsUtil;

    @Autowired
    private ApplyService applyService;

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentController.class);
    private static final int SCHOOL_INSTITUTE_CATEGORY_ID = 3;

    //step 1
    @PostMapping(value = "/secure/school", headers = ("content-type=multipart/*"))
    public ResponseEntity<JsonObject> school(@RequestParam(required = false) MultipartFile file,
            @RequestParam("name") String name,
            @RequestParam(name = "boardId", required = false) Integer boardId,
            @RequestParam("instituteCategoryList") String instituteCategoryDtoListString,
            @RequestParam("address") String address,
            @RequestParam("localityId") Integer localityId,
            @RequestParam("pincode") String pincode,
            @RequestAttribute("userId") Integer userId) {

        LOGGER.info("Creating a school with DP");

        SchoolDto schoolDto = new SchoolDto();
        schoolDto.setName(name);
        schoolDto.setBoardId(boardId);
        schoolDto.setAddress(address);
        schoolDto.setLocality(new LocalityDto());
        schoolDto.getLocality().setId(localityId);
        schoolDto.setPincode(pincode);
        JsonArray instituteCategoryDtoListJson = new JsonArray(instituteCategoryDtoListString);
        List<InstituteCategoryDto> instituteCategoryDtoList = new ArrayList();
        for (int index = 0; index < instituteCategoryDtoListJson.size(); index++) {
            JsonObject icJson = instituteCategoryDtoListJson.getJsonObject(index);
            InstituteCategoryDto ic = new InstituteCategoryDto();
            ic.setId(icJson.getInteger("id"));
            ic.setName(icJson.getString("name"));
            instituteCategoryDtoList.add(ic);
        }
        schoolDto.setInstituteCategoryList(instituteCategoryDtoList);

        School school = convertToSchool(schoolDto, true);
        if (school == null) {
            LOGGER.error("Returning response = {} with status = {} because school is null", null, HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        school.setCreatedAt(new Date());
        User user = new User(userId);
        school.setStage(1);

        List<SchoolInstituteCategoryMapping> sicmList = convertToSicmList(school, schoolDto.getInstituteCategoryList());

        LOGGER.debug("Going to add school now");
        Integer id = schoolService.add(school, user, sicmList);
        LOGGER.debug("School created with id = {}", id);
        Integer adminId = school.getAdminList().get(0).getId();

        Map<String, Object> respObjectMap = new HashMap();
        respObjectMap.put("schoolId", id);
        respObjectMap.put("adminId", adminId);
        JsonObject resp = new JsonObject(respObjectMap);

        //FILE UPLOAD
        if (file != null && !file.isEmpty()) {
            String fileType = Utils.getFileType(file);
            if (Constants.JPEG.equals(fileType) || Constants.JPG.equals(fileType) || Constants.PNG.equals(fileType)) {

                String randomUUID = UUID.randomUUID().toString();
                String folder = Constants.S3_SCHOOL_DP_FOLDER.
                        replace(Constants.SCHOOL_ID_MACRO, id + "")
                        + randomUUID
                        + Constants.DOT_SEPARATOR
                        + FilenameUtils.getExtension(file.getOriginalFilename());

                LOGGER.debug("Folder = {}", folder);

                try {
                    File localFile = Utils.writeToDisk(file, "/disk1/temp/" + randomUUID + Constants.DOT_SEPARATOR + fileType);
                    LOGGER.debug("Uploading localFile = {} to folder = {} in S3 bucket = {}", localFile, folder, Constants.S3_SCHOOL_BUCKET);

                    if (awsUtil.uploadToS3(localFile, folder, Constants.S3_SCHOOL_BUCKET)) {
                        LOGGER.debug("File successfully uploaded.");

                        FileUtils.forceDelete(localFile);
                        LOGGER.debug("Local copy of file deleted");

                        schoolService.updateDisplayPic(id, Constants.STATIC_RESOURCE_DNS + folder);
                    } else {
                        FileUtils.forceDelete(localFile);
                        LOGGER.error("File could not be uploaded to S3");
                    }
                } catch (IOException ex) {
                    LOGGER.error("Exception while writing to disk: " + ex);
                }
            } else {
                LOGGER.debug("Invalid image uploaded");
            }
        } else {
            LOGGER.debug("file uploaded is empty");
        }

        if (id != null) {
            LOGGER.info("Returning response = {} with status = {}", resp.toString(), HttpStatus.CREATED);
            return new ResponseEntity<>(resp, HttpStatus.CREATED);
        } else {
            LOGGER.error("Returning response = {} with status = {} because school id generated is null", null, HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/secure/school/pcpic", headers = ("content-type=multipart/*"))
    public ResponseEntity<JsonObject> editSchoolProfileCoverPic(@RequestParam MultipartFile file,
            @RequestParam("pictureType") String pictureType,
            @RequestParam("schoolId") Integer schoolId,
            @RequestAttribute("userId") Integer userId) {

        if (!pictureType.equals(Constants.SCHOOL_PROFILE_PICTURE_TYPE) && !pictureType.equals(Constants.SCHOOL_COVER_PICTURE_TYPE)) {
            LOGGER.info("Invalid pictureType. Returning status = {}.", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        if (schoolId == null) {
            LOGGER.info("SchoolId not present. Returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (schoolService.getAdminId(userId, schoolId) == null) {
            LOGGER.info("Returning status = {}. User is not admin of the school.", HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        //FILE UPLOAD
        if (file != null && !file.isEmpty()) {
            String fileType = Utils.getFileType(file);
            if (Constants.JPEG.equals(fileType) || Constants.JPG.equals(fileType) || Constants.PNG.equals(fileType)) {

                String randomUUID = UUID.randomUUID().toString();
                String folder = Constants.S3_SCHOOL_DP_FOLDER.
                        replace(Constants.SCHOOL_ID_MACRO, schoolId + "")
                        + randomUUID
                        + Constants.DOT_SEPARATOR
                        + FilenameUtils.getExtension(file.getOriginalFilename());

                LOGGER.debug("Folder = {}", folder);

                try {
                    File localFile = Utils.writeToDisk(file, "/disk1/temp/" + randomUUID + Constants.DOT_SEPARATOR + fileType);
                    LOGGER.debug("Uploading localFile = {} to folder = {} in S3 bucket = {}", localFile, folder, Constants.S3_SCHOOL_BUCKET);

                    if (awsUtil.uploadToS3(localFile, folder, Constants.S3_SCHOOL_BUCKET)) {
                        LOGGER.debug("File successfully uploaded.");

                        FileUtils.forceDelete(localFile);
                        LOGGER.debug("Local copy of file deleted");

                        if (pictureType.equals(Constants.SCHOOL_PROFILE_PICTURE_TYPE)) {
                            schoolService.updateDisplayPic(schoolId, Constants.STATIC_RESOURCE_DNS + folder);
                        } else {
                            schoolService.updateCoverPic(schoolId, Constants.STATIC_RESOURCE_DNS + folder);
                        }
                    } else {
                        FileUtils.forceDelete(localFile);
                        LOGGER.error("File could not be uploaded to S3");
                        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                } catch (IOException ex) {
                    LOGGER.error("Exception while writing to disk: {}. Returning status = {}", ex, HttpStatus.BAD_REQUEST);
                    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                }
            } else {
                LOGGER.info("Invalid image uploaded. Returning status = {}", HttpStatus.BAD_REQUEST);
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {
            LOGGER.info("File uploaded is empty. Returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        LOGGER.info("File uploaded successfully. Returning status = {}", HttpStatus.CREATED);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    //You can call this API only if you are logged in
    @GetMapping("/secure/school/{id}")
    public ResponseEntity<SchoolDto> schoolForAdmin(@PathVariable("id") Integer id, @RequestAttribute("userId") Integer userId) {
        LOGGER.info("Getting a school with id = {} for admin with userId = {}", id, userId);

        School school = schoolService.get(id);
        if (school == null) {
            LOGGER.info("Returning response = {} with status = {}", null, HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        } else {
            SchoolDto schoolDto = convertToSchoolDto(school);
            List<Admin> adminList = school.getAdminList();
            if (adminList != null && !adminList.isEmpty()) {
                adminList.stream().filter((admin) -> (userId.equals(admin.getUser().getId()))).forEachOrdered((admin) -> {
                    schoolDto.setCurrentUserAdminId(admin.getId());
                });
            }

            LOGGER.info("Returning response = {} with status = {}", schoolDto, HttpStatus.OK);
            return new ResponseEntity<>(schoolDto, HttpStatus.OK);
        }
    }

    //Anyone can call this API
    @GetMapping("/school/{schoolId}")
    public ResponseEntity<SchoolDto> school(@PathVariable("schoolId") Integer schoolId) {
        LOGGER.info("Getting a school with id = {}", schoolId);

        School school = schoolService.get(schoolId);
        if (school == null) {

            LOGGER.info("Returning response = {} with status = {}", null, HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        } else {
            SchoolDto schoolDto = convertToSchoolDto(school);
            LOGGER.info("Returning response = {} with status = {}", schoolDto, HttpStatus.OK);
            return new ResponseEntity<>(schoolDto, HttpStatus.OK);
        }
    }

    @PatchMapping("/secure/app/school/stage")
    public ResponseEntity<Void> stage(@RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("role") String role) {

        LOGGER.info("Increase stage for school with id = {} by role = {}", schoolId, role);

        if (!Constants.ADMIN.equals(role)) {
            LOGGER.info("Role is not admin. Returning status = {}", HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        Integer stage = schoolService.increaseStage(schoolId);

        LOGGER.info("Returning status = {}", HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping("/secure/school/{schoolId}/apply")
    public ResponseEntity<Void> apply(
            @RequestAttribute(name = "userId", required = false) Integer userId,
            @PathVariable("schoolId") Integer schoolId) {

        LOGGER.info("Applying to school id = {} with userId = {}", schoolId, userId);

        if (userId == null) {
            LOGGER.info("Returning status = {} because userId is null", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        UserSchoolInterest userSchoolInterest = new UserSchoolInterest();
        userSchoolInterest.setUser(new User(userId));
        userSchoolInterest.setSchool(new School(schoolId));
        userSchoolInterest.setCreatedAt(new Date());
        int id = applyService.add(userSchoolInterest);

        LOGGER.info("Interest submitted with id = {}. Returning status = {}", id, HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/school/{schoolId}/apply")
    public ResponseEntity<Void> applyOffline(
            @RequestBody(required = false) ApplyDto applyDto,
            @PathVariable("schoolId") Integer schoolId) {

        LOGGER.info("Applying to school id = {} with applyDto = {}", schoolId, applyDto);

        if (applyDto == null) {
            LOGGER.info("Returning status = {} because applyDto is null", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        UserSchoolInterest userSchoolInterest = new UserSchoolInterest();
        Map<String, Object> map = new HashMap();
        map.put("parentName", applyDto.getName());
        map.put("parentEmail", applyDto.getEmail());
        map.put("parentPhone", applyDto.getPhone());
        JsonObject json = new JsonObject(map);
        userSchoolInterest.setGuestUser(json.encode());
        userSchoolInterest.setSchool(new School(schoolId));
        userSchoolInterest.setCreatedAt(new Date());
        int id = applyService.add(userSchoolInterest);

        LOGGER.info("Interest submitted with id = {}. Returning status = {}", id, HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/secure/school")
    public ResponseEntity<Void> editSchool(
            @RequestBody SchoolDto schoolDto,
            @RequestAttribute(name = "userId") Integer userId) {

        LOGGER.info("Editting school with schoolDto = {}", schoolDto);

        if (schoolDto.getId() == null) {
            LOGGER.info("SchoolId not present. Returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        //THIS CALL CAN BE AVOIDED ONCE WE HAVE ROLEAUTH HEADER
        if (schoolService.getAdminId(userId, schoolDto.getId()) == null) {
            LOGGER.info("Returning status = {}. User is not admin of the school.", HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        School school = convertToSchool(schoolDto, false);
        if (school == null) {
            LOGGER.info("SchoolDto could not be converted to school. Returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        schoolService.update(school);

        LOGGER.info("Returning status = {}. School updated successfully.", HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    private School convertToSchool(SchoolDto schoolDto, boolean strictCheck) {

        Date today = new Date();

        if (schoolDto == null) {
            return null;
        }

        if (strictCheck && (schoolDto.getName() == null || schoolDto.getAddress() == null
                || schoolDto.getPincode() == null || schoolDto.getLocality() == null
                || schoolDto.getInstituteCategoryList() == null)) {

            return null;
        }

        School school = new School();
        if (schoolDto.getId() != null) {
            school.setId(schoolDto.getId());
        }
        school.setName(schoolDto.getName());
        school.setAddress(schoolDto.getAddress());
        school.setAbout(schoolDto.getAbout());
        school.setStudentCount(schoolDto.getStudentCount());
        school.setTeacherCount(schoolDto.getTeacherCount());
        school.setLatitude(schoolDto.getLatitude());
        school.setLongitude(schoolDto.getLongitude());

        List<SchoolInstituteCategoryMapping> schoolInstituteCategoryMappingList = new ArrayList();
        for (InstituteCategoryDto icd : schoolDto.getInstituteCategoryList()) {
            if (icd.getId() == SCHOOL_INSTITUTE_CATEGORY_ID && schoolDto.getBoardId() == null) {
                return null;
            }

            SchoolInstituteCategoryMapping sicm;
            InstituteCategory ic = new InstituteCategory();
            ic.setId(icd.getId());
            if (icd.getSchoolInstituteCategoryMappingId() == null) {
                sicm = new SchoolInstituteCategoryMapping();
                sicm.setCreatedAt(today);
            } else {
                sicm = new SchoolInstituteCategoryMapping(icd.getSchoolInstituteCategoryMappingId());
                sicm.setCreatedAt(icd.getSicmCreatedAt());
            }
            sicm.setInstituteCategory(ic);
            sicm.setSchool(school);
            schoolInstituteCategoryMappingList.add(sicm);
        }
        school.setSchoolInstituteCategoryMappingList(schoolInstituteCategoryMappingList);

        if (schoolDto.getBoardId() != null) {
            Board board = new Board(schoolDto.getBoardId());
            List<School> schoolList = new ArrayList();
            schoolList.add(school);
            board.setSchoolList(schoolList);
            school.setBoard(board);
        }

        if (schoolDto.getLocality() != null && schoolDto.getLocality().getId() != null) {
            Locality locality = new Locality(schoolDto.getLocality().getId());
            school.setLocality(locality);
        }

        if (schoolDto.getPincode() != null) {
            school.setPincode(schoolDto.getPincode());
        }

        return school;
    }

    private SchoolDto convertToSchoolDto(School school) {
        SchoolDto schoolDto = new SchoolDto();
        schoolDto.setId(school.getId());
        schoolDto.setAverageRating(school.getAverageRating());
        schoolDto.setDisplayPic(school.getDisplayPic());
        schoolDto.setCoverPic(school.getCoverPic());
        schoolDto.setRatingCount(school.getRatingCount());
        schoolDto.setStudentCount(school.getStudentCount());
        schoolDto.setTeacherCount(school.getTeacherCount());
        schoolDto.setAddress(school.getAddress());
        schoolDto.setAbout(school.getAbout());
        schoolDto.setWebsite(school.getWebsite());
        schoolDto.setSearchable(school.getSearchable());
        schoolDto.setLatitude(school.getLatitude());
        schoolDto.setLongitude(school.getLongitude());
        if (school.getBoard() != null) {
            schoolDto.setBoardId(school.getBoard().getId());
            schoolDto.setBoardName(school.getBoard().getName());
        }
        schoolDto.setName(school.getName());
        schoolDto.setPincode(school.getPincode());
        List<Class> classList = school.getClassList();
        List<ClassDto> classDtoList = new ArrayList();
        if (classList != null) {
            classList.stream().map((clazz) -> {
                ClassDto classDto = new ClassDto();
                classDto.setId(clazz.getId());
                classDto.setStandard(clazz.getStandard());
                classDto.setSection(clazz.getSection());
                return classDto;
            }).forEachOrdered((classDto) -> {
                classDtoList.add(classDto);
            });
        }
        schoolDto.setClassList(classDtoList);

        if (school.getSchoolInstituteCategoryMappingList() != null) {
            List<InstituteCategoryDto> icDtoList = new ArrayList();
            school.getSchoolInstituteCategoryMappingList().forEach(mapping -> {
                InstituteCategoryDto icDto = new InstituteCategoryDto();
                icDto.setId(mapping.getInstituteCategory().getId());
                icDto.setName(mapping.getInstituteCategory().getName());
                icDto.setSchoolInstituteCategoryMappingId(mapping.getId());
                icDto.setSicmCreatedAt(mapping.getCreatedAt());
                icDtoList.add(icDto);
            });
            schoolDto.setInstituteCategoryList(icDtoList);
        }

        LocalityDto localityDto = new LocalityDto();
        localityDto.setId(school.getLocality().getId());
        localityDto.setName(school.getLocality().getName());
        localityDto.setCity(new CityDto());
        localityDto.getCity().setId(school.getLocality().getCity().getId());
        localityDto.getCity().setName(school.getLocality().getCity().getName());
        localityDto.getCity().setCode(school.getLocality().getCity().getCode());
        localityDto.getCity().setState(new StateDto());
        localityDto.getCity().getState().setId(school.getLocality().getCity().getState().getId());
        localityDto.getCity().getState().setName(school.getLocality().getCity().getState().getName());
        localityDto.getCity().getState().setCountry(new CountryDto());
        localityDto.getCity().getState().getCountry().setId(school.getLocality().getCity().getState().getCountry().getId());
        localityDto.getCity().getState().getCountry().setName(school.getLocality().getCity().getState().getCountry().getName());
        localityDto.getCity().getState().getCountry().setCode(school.getLocality().getCity().getState().getCountry().getCode());
        localityDto.getCity().getState().getCountry().setInternationalPhoneCode(school.getLocality().getCity().getState().getCountry().getInternationalPhoneCode());

        schoolDto.setLocality(localityDto);

        schoolDto.setStage(school.getStage());
        schoolDto.setCreatedAt(school.getCreatedAt());
        schoolDto.setUpdatedAt(school.getUpdatedAt());
        schoolDto.setAdminIdList(school.getAdminList().stream()
                .map(admin -> admin.getId()).collect(Collectors.toList()));
        return schoolDto;
    }

    private List<SchoolInstituteCategoryMapping> convertToSicmList(School school, List<InstituteCategoryDto> instituteCategoryList) {
        Date date = new Date();
        List<SchoolInstituteCategoryMapping> sicmList = new ArrayList();
        for (InstituteCategoryDto icDto : instituteCategoryList) {
            InstituteCategory ic = new InstituteCategory(icDto.getId());
            ic.setName(icDto.getName());

            SchoolInstituteCategoryMapping sicm = new SchoolInstituteCategoryMapping();
            sicm.setCreatedAt(date);
            sicm.setSchool(school);
            sicm.setInstituteCategory(ic);

            sicmList.add(sicm);
        }
        return sicmList;
    }

}
