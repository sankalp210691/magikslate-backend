/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.rest;

import com.magikslate.backend.dto.HelpDto;
import com.magikslate.backend.entity.Help;
import com.magikslate.backend.services.help.HelpService;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author sankalpkulshrestha
 */
@RestController
public class HelpController {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(HelpController.class);
    
    @Autowired
    private HelpService helpService;
    
    @Autowired
    private ModelMapper modelMapper;
    
    @Autowired
    private String faqJson;
    
    @PostMapping("/v1/secure/help")
    public ResponseEntity<Void> help(@Valid @RequestBody HelpDto helpDto, 
            @RequestAttribute("userId") Integer userId,
            UriComponentsBuilder ucBuilder) {
        
        LOGGER.info("Creating a help message");

        helpDto.setUserId(userId);
        Help help = convertToHelp(helpDto);
        Integer id = helpService.add(help);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/help/{id}").buildAndExpand(id).toUri());
        LOGGER.info("Help message created with id = {}", id);
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    private Help convertToHelp(HelpDto helpDto) {
        return modelMapper.map(helpDto, Help.class);
    }
    
    @GetMapping("/v1/secure/faq")
    public ResponseEntity<String> help() {
        LOGGER.info("Showing FAQ");

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        return new ResponseEntity<>(faqJson, headers, HttpStatus.OK);
    }
    
}
