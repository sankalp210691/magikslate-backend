/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.rest;

import com.google.common.base.Joiner;
import com.magikslate.backend.dto.ClassDto;
import com.magikslate.backend.dto.ClassSectionDto;
import com.magikslate.backend.dto.StudentDto;
import com.magikslate.backend.dto.StudyGroupDto;
import com.magikslate.backend.dto.TeacherClassDto;
import com.magikslate.backend.entity.Class;
import com.magikslate.backend.entity.School;
import com.magikslate.backend.entity.StudyGroup;
import com.magikslate.backend.services.clazz.ClassService;
import com.magikslate.backend.services.studygroup.StudyGroupService;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import io.vertx.core.json.JsonArray;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author sankalp.kulshrestha
 */
@RestController
@RequestMapping("/v1/secure/app/class")
@Validated
public class ClassController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClassController.class);

    @Autowired
    private ClassService classService;
    
    @Autowired
    private StudyGroupService studyGroupServiceService;

    @GetMapping("/")
    public ResponseEntity<JsonArray> clazz(
            @RequestParam(name = "fbt", required = false) Boolean filterByTeacher, // filter by teacher
            @RequestAttribute("roleId") Integer teacherId,
            @RequestAttribute("role") String role,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestParam(name = "offset", required = false) Integer offset) {

        LOGGER.info("List classes by teacher/school with filterByTeacher = {}, offset = {}", filterByTeacher, offset);

        filterByTeacher = filterByTeacher == null ? false : filterByTeacher;
        offset = offset == null ? 0 : offset;

        if (filterByTeacher) {
            if (Constants.TEACHER.equals(role)) {
                LOGGER.debug("role IS TEACHER");
                //this isnt proper way to do it. should return concrete clss rather than just objects
                List<Object[]> classDataObjectArrayList = classService.getClassListByTeacher(teacherId, offset);

                LOGGER.debug("classDataObjectArrayList = {}", classDataObjectArrayList);

                List<TeacherClassDto> classDtoList = convertToTeacherClassDto(classDataObjectArrayList);

                JsonArray body = new JsonArray(classDtoList);
                LOGGER.info("Returning body = {}, status = {}", body, HttpStatus.OK);

                return new ResponseEntity<>(body, HttpStatus.OK);
            } else {

                LOGGER.info("Returning status = {} because fbt is only supported for TEACHER role but request role is {}", HttpStatus.BAD_REQUEST, role);
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {
            LOGGER.debug("role IS NOT TEACHER");

            List<Class> classList = null;
            List<StudyGroup> studyGroupList = null;
            classList = classService.getClassListBySchool(schoolId, offset);
            studyGroupList = studyGroupServiceService.getStudyGroupBySchool(schoolId, offset);
            LOGGER.debug("classList = {} , studyGroupList = {}", classList, studyGroupList);
            List<ClassDto> classDtoList = new ArrayList();
            List<StudyGroupDto> studyGroupDtoList = new ArrayList();
            classList.forEach((clazz) -> {
                classDtoList.add(convertToClassDto(clazz));
            });
            studyGroupList.forEach((studyGroup) -> {
                studyGroupDtoList.add(convertToStudyGroupDto(studyGroup));
            });

            JsonArray result = new JsonArray();
            JsonArray classDtoArray = new JsonArray(classDtoList);
            JsonArray studyGroupDtoArray = new JsonArray(studyGroupDtoList);
            
            if(!classDtoArray.isEmpty()) {
                result.addAll(classDtoArray)    ;
            }
            if(!studyGroupDtoArray.isEmpty()) {
                result.addAll(studyGroupDtoArray);
            }
            LOGGER.info("Returning body = {}, status = {}", result, HttpStatus.OK);

            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }
    
    //step 2
    @PostMapping("")
    public ResponseEntity<Void> clazz(@Valid @RequestBody List<ClassSectionDto> classSectionDtoList,
            @RequestAttribute("schoolId") Integer schoolId) {

        LOGGER.info("Adding classes to school");

        List<Class> classList = getClassList(classSectionDtoList, schoolId);

        List<Integer> idList = classService.createBulk(classList);
        LOGGER.debug("got class idlist as {}", idList);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Resource-Id", Joiner.on(",").join(idList));

        LOGGER.info("Returning response = {} with status = {}", headers, HttpStatus.CREATED);
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }
    
    @PatchMapping("")
    public ResponseEntity<Void> clazzPatch(@Valid @RequestBody List<ClassSectionDto> classSectionDtoList, @RequestAttribute("schoolId") Integer schoolId) {
        LOGGER.info("Patching classes to school");
        List<Class> classList = getClassList(classSectionDtoList, schoolId);

        List<Integer> idList = classService.createBulk(classList);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Resource-Id", Joiner.on(",").join(idList));

        LOGGER.info("Returning header = {} with status = {}", headers, HttpStatus.CREATED);
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }
    
    private List<Class> getClassList(List<ClassSectionDto> classSectionDtoList, Integer schoolId) {
        List<Class> classList = new ArrayList();
        School school = new School(schoolId);
        classSectionDtoList.forEach((classSectionDto) -> {
            for (int index = 0; index < classSectionDto.getSectionCount(); index++) {
                Class clazz = new Class();
                clazz.setStandard(Utils.sanitizeClassStandard(classSectionDto.getStandard()));
                clazz.setSection(Character.toString((char) (Constants.FIRST_SECTION + index)));
                clazz.setSchool(school);
                classList.add(clazz);
            }
        });
        return classList;
    }

    private List<TeacherClassDto> convertToTeacherClassDto(List<Object[]> clazzDetailObjectArrayList) {
        Map<String, TeacherClassDto> classIdSubjectIdStudentMap = new HashMap();
        clazzDetailObjectArrayList.forEach((row) -> {
            String type = (String) row[0];

            if (type.equals(Constants.CLASS_TYPE)) {

                Integer classId = (Integer) row[1];
                String standard = (String) row[2];
                String section = (String) row[3];
                Integer subjectId = (Integer) row[4];
                String subjectName = (String) row[5];
                Integer studentId = (Integer) row[6];
                String studentName = (String) row[7];
                Integer sctmId = (Integer) row[8];
                Integer scmId = (Integer) row[9];
                String admissionCode = (String) row[10];

                String key = Constants.CLASS_TYPE + Constants.UNDERSCORE_SEPARATOR + classId + Constants.UNDERSCORE_SEPARATOR + subjectId;
                if (classIdSubjectIdStudentMap.containsKey(key)) {
                    StudentDto studentDto = new StudentDto();
                    studentDto.setId(studentId);
                    studentDto.setName(studentName);
                    studentDto.setStudentClassMappingId(scmId);
                    studentDto.setAdmissionCode(admissionCode);
                    classIdSubjectIdStudentMap.get(key).getStudent().add(studentDto);
                } else {
                    StudentDto studentDto = new StudentDto();
                    studentDto.setId(studentId);
                    studentDto.setName(studentName);
                    studentDto.setStudentClassMappingId(scmId);
                    studentDto.setAdmissionCode(admissionCode);

                    List<StudentDto> studentList = new ArrayList();
                    studentList.add(studentDto);

                    TeacherClassDto teacherClassDto = new TeacherClassDto();
                    teacherClassDto.setClassId(classId);
                    teacherClassDto.setStandard(standard);
                    teacherClassDto.setSection(section);
                    teacherClassDto.setSubjectId(subjectId);
                    teacherClassDto.setSubjectName(subjectName);
                    teacherClassDto.setStudent(studentList);
                    teacherClassDto.setSctmId(sctmId);

                    classIdSubjectIdStudentMap.put(key, teacherClassDto);
                }
            } else {

                Integer studyGroupId = (Integer) row[1];
                Integer subjectId = (Integer) row[2];
                String subjectName = (String) row[3];
                Integer studentId = (Integer) row[4];
                String studentName = (String) row[5];
                Integer sgtmId = (Integer) row[6];
                Integer ssgmId = (Integer) row[7];
                String admissionCode = (String) row[8];

                String key = Constants.STUDYGROUP_TYPE + Constants.UNDERSCORE_SEPARATOR + studyGroupId + Constants.UNDERSCORE_SEPARATOR + subjectId;
                if (classIdSubjectIdStudentMap.containsKey(key)) {
                    StudentDto studentDto = new StudentDto();
                    studentDto.setId(studentId);
                    studentDto.setName(studentName);
                    studentDto.setStudentStudyGroupMappingId(ssgmId);
                    studentDto.setAdmissionCode(admissionCode);
                    classIdSubjectIdStudentMap.get(key).getStudent().add(studentDto);
                } else {
                    StudentDto studentDto = new StudentDto();
                    studentDto.setId(studentId);
                    studentDto.setName(studentName);
                    studentDto.setStudentStudyGroupMappingId(ssgmId);
                    studentDto.setAdmissionCode(admissionCode);

                    List<StudentDto> studentList = new ArrayList();
                    studentList.add(studentDto);

                    TeacherClassDto teacherClassDto = new TeacherClassDto();
                    teacherClassDto.setStudyGroupId(studyGroupId);
                    teacherClassDto.setSubjectId(subjectId);
                    teacherClassDto.setSubjectName(subjectName);
                    teacherClassDto.setStudent(studentList);
                    teacherClassDto.setSgtmId(sgtmId);

                    classIdSubjectIdStudentMap.put(key, teacherClassDto);
                }
            }

        });

        return new ArrayList(classIdSubjectIdStudentMap.values());
    }

    private ClassDto convertToClassDto(Class clazz) {
        ClassDto classDto = new ClassDto();
        classDto.setId(clazz.getId());
        classDto.setStandard(clazz.getStandard());
        classDto.setSection(clazz.getSection());
        return classDto;
    }

    private StudyGroupDto convertToStudyGroupDto(StudyGroup studyGroup) {
        StudyGroupDto sgd = new StudyGroupDto();
        sgd.setId(studyGroup.getId());
        sgd.setSubjectName(studyGroup.getSubject().getName());
        return sgd;
    }
}
