/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.rest;

import com.magikslate.backend.ApplicationConfiguration;
import com.magikslate.backend.dto.AnecdotalRecordDto;
import com.magikslate.backend.dto.DiaryDto;
import com.magikslate.backend.dto.ExtraCurricularDiaryDto;
import com.magikslate.backend.dto.GeneralDiaryDto;
import com.magikslate.backend.dto.KudoDto;
import com.magikslate.backend.dto.LessonProgressDto;
import com.magikslate.backend.dto.TestDiaryDto;
import com.magikslate.backend.entity.AnecdotalRecord;
import com.magikslate.backend.entity.Diary;
import com.magikslate.backend.entity.ExtraCurricularDiary;
import com.magikslate.backend.entity.GeneralDiary;
import com.magikslate.backend.entity.Kudo;
import com.magikslate.backend.entity.LessonProgress;
import com.magikslate.backend.entity.School;
import com.magikslate.backend.entity.Student;
import com.magikslate.backend.entity.StudyGroupTeacherMapping;
import com.magikslate.backend.entity.SubjectClassTeacherMapping;
import com.magikslate.backend.entity.Teacher;
import com.magikslate.backend.entity.Test;
import com.magikslate.backend.services.clazz.ClassService;
import com.magikslate.backend.services.diary.DiaryService;
import com.magikslate.backend.services.studygroup.StudyGroupService;
import com.magikslate.backend.util.Constants;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.Min;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author sankalpkulshrestha
 */
@RestController
@RequestMapping("/v1/secure/app/diary")
public class DiaryController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationConfiguration.class);

    @Autowired
    private DiaryService diaryService;

    @Autowired
    private ClassService classService;

    @Autowired
    private StudyGroupService studyGroupService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/{id}")
    public ResponseEntity<DiaryDto> diary(@RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer teacherId,
            @RequestAttribute("role") String role,
            @PathVariable("id") Integer diaryId) {

        LOGGER.info("Get diary by id");

        Diary diary = diaryService.get(diaryId);
        DiaryDto diaryDto = DiaryDto.convertToDiaryDto(diary, diary.getType(), modelMapper);

        StudyGroupTeacherMapping studyGroupTeacherMapping = diary.getStudyGroupTeacherMapping();
        if (studyGroupTeacherMapping != null && !studyGroupTeacherMapping.getTeacher().getSchool().getId().equals(schoolId)) {

            LOGGER.info("Teacher (obtained from studyGroupTeacherMapping) of another school. Returning status = {}", HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        SubjectClassTeacherMapping subjectClassTeacherMapping = diary.getSubjectClassTeacherMapping();
        if (subjectClassTeacherMapping != null && !subjectClassTeacherMapping.getTeacher().getSchool().getId().equals(schoolId)) {

            LOGGER.info("Teacher (obtained from subjectClassTeacherMapping) of another school. Returning status = {}", HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        LOGGER.info("Returning diary = {} with status = {}", diaryDto, HttpStatus.OK);
        return new ResponseEntity<>(diaryDto, HttpStatus.OK);
    }

    @PostMapping("/general")
    public ResponseEntity<DiaryDto> general(@RequestBody GeneralDiaryDto generalDiaryDto,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer teacherId,
            @RequestAttribute("role") String role,
            UriComponentsBuilder ucBuilder) {

        LOGGER.info("Creating a general diary entry");

        if (!Constants.TEACHER.equals(role)) {
            LOGGER.info("Role is not teacher. Returning {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        GeneralDiary generalDiary = (GeneralDiary) convertToDiaryType(generalDiaryDto, schoolId, teacherId);

        if (generalDiary != null) {
            Integer id = diaryService.add(generalDiary);
            if (id != null) {
                generalDiaryDto.setId(id);
                HttpHeaders headers = new HttpHeaders();
                headers.setLocation(ucBuilder.path("/secure/app/diary/general/{id}").buildAndExpand(id).toUri());
                return new ResponseEntity<>(generalDiaryDto, headers, HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {
            LOGGER.info("NULL generalDiary created. Returning {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/kudo")
    public ResponseEntity<DiaryDto> kudo(@RequestBody KudoDto kudoDto,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer teacherId,
            @RequestAttribute("role") String role,
            UriComponentsBuilder ucBuilder) {

        LOGGER.info("Creating a kudo diary entry with kudoDto = {} , schoolId = {} , roleId = {}, role = {}", kudoDto, schoolId, teacherId, role);

        if (!Constants.TEACHER.equals(role)) {

            LOGGER.info("Role is not teacher. Hence returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Kudo kudoDiary = (Kudo) convertToDiaryType(kudoDto, schoolId, teacherId);

        if (kudoDiary != null) {

            Integer id = diaryService.add(kudoDiary);
            LOGGER.debug("Kudo diary created with id = {}", id);
            if (id != null) {

                kudoDto.setId(id);
                HttpHeaders headers = new HttpHeaders();
                headers.setLocation(ucBuilder.path("/secure/app/diary/kudo/{id}").buildAndExpand(id).toUri());

                LOGGER.info("Kudo Diary created with id = {}. Returning body = {} , headers = {}, status = {}", id, kudoDto, headers, HttpStatus.CREATED);
                return new ResponseEntity<>(kudoDto, headers, HttpStatus.CREATED);
            } else {

                LOGGER.info("kudoDiary id is null. Hence returning status = {}", HttpStatus.BAD_REQUEST);
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {

            LOGGER.info("kudoDiary created from Dto is null. Hence returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/ar")
    public ResponseEntity<DiaryDto> ar(@RequestBody AnecdotalRecordDto arDto,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer teacherId,
            @RequestAttribute("role") String role,
            UriComponentsBuilder ucBuilder) {

        LOGGER.info("Creating a AnecdotalRecord diary entry with arDto = {} , schoolId = {} , roleId = {}, role = {}", arDto, schoolId, teacherId, role);

        if (!Constants.TEACHER.equals(role)) {

            LOGGER.info("Role is not teacher. Hence returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        AnecdotalRecord anecdotalRecordDiary = (AnecdotalRecord) convertToDiaryType(arDto, schoolId, teacherId);

        if (anecdotalRecordDiary != null) {

            Integer id = diaryService.add(anecdotalRecordDiary);
            LOGGER.debug("AnecdotalRecord diary created with id = {}", id);
            if (id != null) {

                arDto.setId(id);
                HttpHeaders headers = new HttpHeaders();
                headers.setLocation(ucBuilder.path("/secure/app/diary/ar/{id}").buildAndExpand(id).toUri());

                LOGGER.info("AnecdotalRecord Diary created with id = {}. Returning body = {} , headers = {}, status = {}", id, arDto, headers, HttpStatus.CREATED);
                return new ResponseEntity<>(arDto, headers, HttpStatus.CREATED);
            } else {

                LOGGER.info("AnecdotalRecord id is null. Hence returning status = {}", HttpStatus.BAD_REQUEST);
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {

            LOGGER.info("AnecdotalRecord created from Dto is null. Hence returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/lp")
    public ResponseEntity<DiaryDto> lp(@RequestBody LessonProgressDto lpDto,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer teacherId,
            @RequestAttribute("role") String role,
            UriComponentsBuilder ucBuilder) {

        LOGGER.info("Creating a LessonProgress diary entry with lpDto = {} , schoolId = {} , roleId = {}, role = {}", lpDto, schoolId, teacherId, role);

        if (!Constants.TEACHER.equals(role)) {

            LOGGER.info("Role is not teacher. Hence returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        LessonProgress lessonProgressDiary = (LessonProgress) convertToDiaryType(lpDto, schoolId, teacherId);

        if (lessonProgressDiary != null) {

            Integer id = diaryService.add(lessonProgressDiary);
            LOGGER.debug("LessonProgress diary created with id = {}", id);
            if (id != null) {

                lpDto.setId(id);
                HttpHeaders headers = new HttpHeaders();
                headers.setLocation(ucBuilder.path("/secure/app/diary/lp/{id}").buildAndExpand(id).toUri());

                LOGGER.info("LessonProgress Diary created with id = {}. Returning body = {} , headers = {}, status = {}", id, lpDto, headers, HttpStatus.CREATED);
                return new ResponseEntity<>(lpDto, headers, HttpStatus.CREATED);
            } else {

                LOGGER.info("LessonProgress id is null. Hence returning status = {}", HttpStatus.BAD_REQUEST);
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {

            LOGGER.info("LessonProgress created from Dto is null. Hence returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/ec")
    public ResponseEntity<DiaryDto> ec(@RequestBody ExtraCurricularDiaryDto ecDto,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer teacherId,
            @RequestAttribute("role") String role,
            UriComponentsBuilder ucBuilder) {

        LOGGER.info("Creating a ExtraCurricularDiary diary entry with ecDto = {} , schoolId = {} , roleId = {}, role = {}", ecDto, schoolId, teacherId, role);

        if (!Constants.TEACHER.equals(role)) {

            LOGGER.info("Role is not teacher. Hence returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        ExtraCurricularDiary ecDiary = (ExtraCurricularDiary) convertToDiaryType(ecDto, schoolId, teacherId);

        if (ecDiary != null) {

            Integer id = diaryService.add(ecDiary);
            LOGGER.debug("ExtraCurricularDiary created with id = {}", id);
            if (id != null) {

                ecDto.setId(id);
                HttpHeaders headers = new HttpHeaders();
                headers.setLocation(ucBuilder.path("/secure/app/diary/ec/{id}").buildAndExpand(id).toUri());

                LOGGER.info("ExtraCurricularDiary created with id = {}. Returning body = {} , headers = {}, status = {}", id, ecDto, headers, HttpStatus.CREATED);
                return new ResponseEntity<>(ecDto, headers, HttpStatus.CREATED);
            } else {

                LOGGER.info("ExtraCurricularDiary id is null. Hence returning status = {}", HttpStatus.BAD_REQUEST);
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {

            LOGGER.info("ExtraCurricularDiary created from Dto is null. Hence returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/test")
    public ResponseEntity<DiaryDto> test(@RequestBody TestDiaryDto testDto,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer teacherId,
            @RequestAttribute("role") String role,
            UriComponentsBuilder ucBuilder) {

        LOGGER.info("Creating a TestDiary diary entry with testDto = {} , schoolId = {} , roleId = {}, role = {}", testDto, schoolId, teacherId, role);

        if (!Constants.TEACHER.equals(role)) {

            LOGGER.info("Role is not teacher. Hence returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Test testDiary = (Test) convertToDiaryType(testDto, schoolId, teacherId);

        if (testDiary != null) {
            Integer id = diaryService.add(testDiary);
            LOGGER.debug("TestDiary created with id = {}", id);

            if (id != null) {

                testDto.setId(id);
                HttpHeaders headers = new HttpHeaders();
                headers.setLocation(ucBuilder.path("/secure/app/diary/test/{id}").buildAndExpand(id).toUri());

                LOGGER.info("TestDiary created with id = {}. Returning body = {} , headers = {}, status = {}", id, testDto, headers, HttpStatus.CREATED);
                return new ResponseEntity<>(testDto, headers, HttpStatus.CREATED);
            } else {

                LOGGER.info("TestDiary id is null. Hence returning status = {}", HttpStatus.BAD_REQUEST);
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {

            LOGGER.info("TestDiary created from Dto is null. Hence returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("")
    public ResponseEntity<JsonArray> diary(
            @Min(value = 1) @RequestParam(name = "classId", required = false) Integer classId,
            @Min(value = 1) @RequestParam(name = "studyGroupId", required = false) Integer studyGroupId,
            @Min(value = 1) @RequestParam(name = "subjectId", required = false) Integer subjectId,
            @Min(value = 0) @RequestParam(name = "offset") Integer offset,
            @RequestAttribute("schoolId") Integer schoolId,
            @RequestAttribute("roleId") Integer roleId,
            @RequestAttribute(name = "studentId", required = false) Integer studentId,
            @RequestAttribute("userId") Integer userId,
            @RequestAttribute("role") String role) {

        LOGGER.info("Get diaries with classId = {} ,studyGroupId={}, subjectId={},"
                + " offset = {}, schoolId = {}, roleId = {}, studentId = {}, userId = {},"
                + "role = {}", classId, studyGroupId, subjectId, offset, schoolId, roleId,
                studentId, userId, role);

        if (!Constants.TEACHER.equals(role) && !Constants.PARENT.equals(role)) {

            LOGGER.info("Role should be TEACHER or PARENT. Returning status = {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<Diary> diaryList = diaryService.getDiary(role, roleId, schoolId, studentId, classId, subjectId, studyGroupId, offset);
        JsonArray jsonArray = convertToDiaryList(diaryList);

        LOGGER.info("Returning body = {} with status = {}", jsonArray, HttpStatus.OK);
        return new ResponseEntity<>(jsonArray, HttpStatus.OK);
    }

    private <T extends DiaryDto> Object convertToDiaryType(T diaryDto, Integer schoolId, Integer teacherId) {
        Diary diary = convertToDiary(diaryDto, teacherId);

        if (diary == null) {
            return null;
        }

        switch (diaryDto.getType()) {
            case Constants.GENERAL_DIARY:
                GeneralDiary gDiary = new GeneralDiary();
                gDiary.setDiary(diary);
                gDiary.setDescription(((GeneralDiaryDto) diaryDto).getDescription());
                gDiary.setTitle(((GeneralDiaryDto) diaryDto).getTitle());
                return gDiary;
            case Constants.KUDO_DIARY:
                Kudo kDiary = new Kudo();
                kDiary.setDiary(diary);
                kDiary.setDescription(((KudoDto) diaryDto).getDescription());
                return kDiary;
            case Constants.ANECDOTAL_RECORD_DIARY:
                AnecdotalRecord arDiary = new AnecdotalRecord();
                arDiary.setDiary(diary);
                arDiary.setDescription(((AnecdotalRecordDto) diaryDto).getDescription());
                return arDiary;
            case Constants.EX_DIARY:
                ExtraCurricularDiary ecDiary = new ExtraCurricularDiary();
                ecDiary.setDiary(diary);
                ecDiary.setDescription(((ExtraCurricularDiaryDto) diaryDto).getDescription());
                ecDiary.setType(((ExtraCurricularDiaryDto) diaryDto).getType());
                return ecDiary;
            case Constants.LESSON_PROGRESS_DIARY:
                LessonProgress lDiary = new LessonProgress();
                lDiary.setDiary(diary);
                lDiary.setChapterName(((LessonProgressDto) diaryDto).getChapterName());
                lDiary.setPagesCompleted(((LessonProgressDto) diaryDto).getPagesCompleted());
                return lDiary;
            case Constants.TEST_DIARY:
                Test tDiary = new Test();
                tDiary.setDiary(diary);
                tDiary.setCreatedAt(diary.getCreatedAt());
                tDiary.setDescription(((TestDiaryDto) diaryDto).getDescription());
                tDiary.setMaxMarks(((TestDiaryDto) diaryDto).getMaxMarks());
                tDiary.setSchool(new School(schoolId));
                tDiary.setStudyGroupTeacherMapping(diary.getStudyGroupTeacherMapping());
                tDiary.setSubjectClassTeacherMapping(diary.getSubjectClassTeacherMapping());
                tDiary.setTestDate(((TestDiaryDto) diaryDto).getTestDate());
                tDiary.setTitle(((TestDiaryDto) diaryDto).getTitle());
                return tDiary;
            default:
                return null;
        }
    }

    private <T extends DiaryDto> Diary convertToDiary(T diaryDto, Integer teacherId) {
        Diary diary = new Diary();
        diary.setType(diaryDto.getType());
        diary.setCreatedAt(new Date());
        diary.setTeacher(new Teacher(teacherId));

        if (diaryDto.getSctm() == null && diaryDto.getSsgm() == null) {
            return null;
        }

        if (diaryDto.getSctm() != null) {

            SubjectClassTeacherMapping sctm = classService.getSubjectClassTeacherMapping(diaryDto.getSctm());
            if (!sctm.getTeacher().getId().equals(teacherId)) {
                return null;
            }
            diary.setSubject(sctm.getSubject());
            if (diaryDto.getStudentId() == null || diaryDto.getStudentId().isEmpty()) {
                diary.setSubjectClassTeacherMapping(sctm);
            }
        } else if (diaryDto.getSsgm() != null) {

            StudyGroupTeacherMapping ssgm = studyGroupService.getStudyGroupTeacherMapping(diaryDto.getSsgm());
            if (!ssgm.getTeacher().getId().equals(teacherId)) {
                return null;
            }
            diary.setSubject(ssgm.getStudyGroup().getSubject());
            if (diaryDto.getStudentId() == null || diaryDto.getStudentId().isEmpty()) {
                diary.setStudyGroupTeacherMapping(ssgm);
            }
        }

        if (diaryDto.getStudentId() != null && !diaryDto.getStudentId().isEmpty()) {
            List<Student> studentList = new ArrayList();
            diaryDto.getStudentId().forEach(studentId -> {
                studentList.add(new Student(studentId));
            });
            diary.setStudentList(studentList);
        }
        return diary;
    }

    private JsonArray convertToDiaryList(List<Diary> diaryList) {
        JsonArray jsonArray = new JsonArray();

        diaryList.forEach(diary -> {
            DiaryDto diaryDto = DiaryDto.convertToDiaryDto(diary, diary.getType(), modelMapper);
            jsonArray.add(new JsonObject(Json.encode(diaryDto)));
        });

        return jsonArray;
    }

}
