/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserSchoolInterest.findAll", query = "SELECT u FROM UserSchoolInterest u")
    , @NamedQuery(name = "UserSchoolInterest.findById", query = "SELECT u FROM UserSchoolInterest u WHERE u.id = :id")
    , @NamedQuery(name = "UserSchoolInterest.findByGuestUser", query = "SELECT u FROM UserSchoolInterest u WHERE u.guestUser = :guestUser")
    , @NamedQuery(name = "UserSchoolInterest.findByCreatedAt", query = "SELECT u FROM UserSchoolInterest u WHERE u.createdAt = :createdAt")
    , @NamedQuery(name = "UserSchoolInterest.findByUpdatedAt", query = "SELECT u FROM UserSchoolInterest u WHERE u.updatedAt = :updatedAt")
    , @NamedQuery(name = "UserSchoolInterest.findByDeletedAt", query = "SELECT u FROM UserSchoolInterest u WHERE u.deletedAt = :deletedAt")})
public class UserSchoolInterest implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Size(max = 200)
    private String guestUser;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt = new Date();
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @JoinColumn(name = "School_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private School school;
    @JoinColumn(name = "User_Id", referencedColumnName = "Id")
    @ManyToOne
    private User user;

    public UserSchoolInterest() {
    }

    public UserSchoolInterest(Integer id) {
        this.id = id;
    }

    public UserSchoolInterest(Integer id, Date createdAt, Date updatedAt) {
        this.id = id;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGuestUser() {
        return guestUser;
    }

    public void setGuestUser(String guestUser) {
        this.guestUser = guestUser;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserSchoolInterest)) {
            return false;
        }
        UserSchoolInterest other = (UserSchoolInterest) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.UserSchoolInterest[ id=" + id + " ]";
    }
    
}
