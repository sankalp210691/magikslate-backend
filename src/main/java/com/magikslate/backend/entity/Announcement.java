/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Announcement.findAll", query = "SELECT a FROM Announcement a")
    , @NamedQuery(name = "Announcement.findById", query = "SELECT a FROM Announcement a WHERE a.id = :id")
    , @NamedQuery(name = "Announcement.findByTitle", query = "SELECT a FROM Announcement a WHERE a.title = :title")
    , @NamedQuery(name = "Announcement.findByDescription", query = "SELECT a FROM Announcement a WHERE a.description = :description")
    , @NamedQuery(name = "Announcement.findBySendToStaff", query = "SELECT a FROM Announcement a WHERE a.sendToStaff = :sendToStaff")
    , @NamedQuery(name = "Announcement.findBySendToParents", query = "SELECT a FROM Announcement a WHERE a.sendToParents = :sendToParents")
    , @NamedQuery(name = "Announcement.findByCreatedAt", query = "SELECT a FROM Announcement a WHERE a.createdAt = :createdAt")
    , @NamedQuery(name = "Announcement.findByUpdatedAt", query = "SELECT a FROM Announcement a WHERE a.updatedAt = :updatedAt")
    , @NamedQuery(name = "Announcement.findByDeletedAt", query = "SELECT a FROM Announcement a WHERE a.deletedAt = :deletedAt")})
public class Announcement implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    private String title;
    @Size(max = 1000)
    private String description;
    @Basic(optional = false)
    @NotNull
    private boolean sendToStaff;
    @Basic(optional = false)
    @NotNull
    private boolean sendToParents;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt = new Date();
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @JoinTable(name = "ClassAnnouncementMapping", joinColumns = {
        @JoinColumn(name = "Announcement_Id", referencedColumnName = "Id")}, inverseJoinColumns = {
        @JoinColumn(name = "Class_Id", referencedColumnName = "Id")})
    @ManyToMany
    private List<Class> classList;
    @JoinColumn(name = "Admin_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Admin admin;

    public Announcement() {
    }

    public Announcement(Integer id) {
        this.id = id;
    }

    public Announcement(Integer id, String title, boolean sendToStaff, boolean sendToParents, Date createdAt) {
        this.id = id;
        this.title = title;
        this.sendToStaff = sendToStaff;
        this.sendToParents = sendToParents;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getSendToStaff() {
        return sendToStaff;
    }

    public void setSendToStaff(boolean sendToStaff) {
        this.sendToStaff = sendToStaff;
    }

    public boolean getSendToParents() {
        return sendToParents;
    }

    public void setSendToParents(boolean sendToParents) {
        this.sendToParents = sendToParents;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    @XmlTransient
    public List<Class> getClassList() {
        return classList;
    }

    public void setClassList(List<Class> classList) {
        this.classList = classList;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Announcement)) {
            return false;
        }
        Announcement other = (Announcement) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.Announcement[ id=" + id + " ]";
    }
    
}
