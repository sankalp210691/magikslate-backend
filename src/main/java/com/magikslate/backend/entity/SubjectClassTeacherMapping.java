/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SubjectClassTeacherMapping.findAll", query = "SELECT s FROM SubjectClassTeacherMapping s")
    , @NamedQuery(name = "SubjectClassTeacherMapping.findById", query = "SELECT s FROM SubjectClassTeacherMapping s WHERE s.id = :id")
    , @NamedQuery(name = "SubjectClassTeacherMapping.findBySessionYear", query = "SELECT s FROM SubjectClassTeacherMapping s WHERE s.sessionYear = :sessionYear")
    , @NamedQuery(name = "SubjectClassTeacherMapping.findByCreatedAt", query = "SELECT s FROM SubjectClassTeacherMapping s WHERE s.createdAt = :createdAt")
    , @NamedQuery(name = "SubjectClassTeacherMapping.findByDeletedAt", query = "SELECT s FROM SubjectClassTeacherMapping s WHERE s.deletedAt = :deletedAt")
    , @NamedQuery(name = "SubjectClassTeacherMapping.findByUpdatedAt", query = "SELECT s FROM SubjectClassTeacherMapping s WHERE s.updatedAt = :updatedAt")})
public class SubjectClassTeacherMapping implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    private Integer sessionYear;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt = new Date();
    @ManyToMany(mappedBy = "subjectClassTeacherMappingList")
    private List<Content> contentList;
    @JoinColumn(name = "Teacher_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Teacher teacher;
    @JoinColumn(name = "Class_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Class class1;
    @JoinColumn(name = "Subject_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Subject subject;
    @OneToMany(mappedBy = "subjectClassTeacherMapping")
    private List<Diary> diaryList;
    @OneToMany(mappedBy = "subjectClassTeacherMapping")
    private List<Test> testList;

    public SubjectClassTeacherMapping() {
    }

    public SubjectClassTeacherMapping(Integer id) {
        this.id = id;
    }

    public SubjectClassTeacherMapping(Integer id, Integer sessionYear, Date createdAt, Date updatedAt) {
        this.id = id;
        this.sessionYear = sessionYear;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSessionYear() {
        return sessionYear;
    }

    public void setSessionYear(Integer sessionYear) {
        this.sessionYear = sessionYear;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @XmlTransient
    public List<Content> getContentList() {
        return contentList;
    }

    public void setContentList(List<Content> contentList) {
        this.contentList = contentList;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Class getClass1() {
        return class1;
    }

    public void setClass1(Class class1) {
        this.class1 = class1;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    @XmlTransient
    public List<Diary> getDiaryList() {
        return diaryList;
    }

    public void setDiaryList(List<Diary> diaryList) {
        this.diaryList = diaryList;
    }

    @XmlTransient
    public List<Test> getTestList() {
        return testList;
    }

    public void setTestList(List<Test> testList) {
        this.testList = testList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SubjectClassTeacherMapping)) {
            return false;
        }
        SubjectClassTeacherMapping other = (SubjectClassTeacherMapping) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.SubjectClassTeacherMapping[ id=" + id + " ]";
    }
    
}
