/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClassTeacherMapping.findAll", query = "SELECT c FROM ClassTeacherMapping c")
    , @NamedQuery(name = "ClassTeacherMapping.findById", query = "SELECT c FROM ClassTeacherMapping c WHERE c.id = :id")
    , @NamedQuery(name = "ClassTeacherMapping.findByCreatedAt", query = "SELECT c FROM ClassTeacherMapping c WHERE c.createdAt = :createdAt")
    , @NamedQuery(name = "ClassTeacherMapping.findByDeletedAt", query = "SELECT c FROM ClassTeacherMapping c WHERE c.deletedAt = :deletedAt")
    , @NamedQuery(name = "ClassTeacherMapping.findByClassTeacher", query = "SELECT c FROM ClassTeacherMapping c WHERE c.classTeacher = :classTeacher")
    , @NamedQuery(name = "ClassTeacherMapping.findByDeputyTeacher", query = "SELECT c FROM ClassTeacherMapping c WHERE c.deputyTeacher = :deputyTeacher")
    , @NamedQuery(name = "ClassTeacherMapping.findBySessionYear", query = "SELECT c FROM ClassTeacherMapping c WHERE c.sessionYear = :sessionYear")})
public class ClassTeacherMapping implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @Basic(optional = false)
    @NotNull
    private boolean classTeacher;
    @Basic(optional = false)
    @NotNull
    private boolean deputyTeacher;
    @Basic(optional = false)
    @NotNull
    private Integer sessionYear;
    @JoinColumn(name = "Class_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Class class1;
    @JoinColumn(name = "Teacher_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Teacher teacher;

    public ClassTeacherMapping() {
    }

    public ClassTeacherMapping(Integer id) {
        this.id = id;
    }

    public ClassTeacherMapping(Integer id, Date createdAt, boolean classTeacher, boolean deputyTeacher, Integer sessionYear) {
        this.id = id;
        this.createdAt = createdAt;
        this.classTeacher = classTeacher;
        this.deputyTeacher = deputyTeacher;
        this.sessionYear = sessionYear;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public boolean getClassTeacher() {
        return classTeacher;
    }

    public void setClassTeacher(boolean classTeacher) {
        this.classTeacher = classTeacher;
    }

    public boolean getDeputyTeacher() {
        return deputyTeacher;
    }

    public void setDeputyTeacher(boolean deputyTeacher) {
        this.deputyTeacher = deputyTeacher;
    }

    public Integer getSessionYear() {
        return sessionYear;
    }

    public void setSessionYear(Integer sessionYear) {
        this.sessionYear = sessionYear;
    }

    public Class getClass1() {
        return class1;
    }

    public void setClass1(Class class1) {
        this.class1 = class1;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClassTeacherMapping)) {
            return false;
        }
        ClassTeacherMapping other = (ClassTeacherMapping) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.ClassTeacherMapping[ id=" + id + " ]";
    }
    
}
