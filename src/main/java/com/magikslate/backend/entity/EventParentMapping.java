/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EventParentMapping.findAll", query = "SELECT e FROM EventParentMapping e")
    , @NamedQuery(name = "EventParentMapping.findByEventId", query = "SELECT e FROM EventParentMapping e WHERE e.eventParentMappingPK.eventId = :eventId")
    , @NamedQuery(name = "EventParentMapping.findByParentId", query = "SELECT e FROM EventParentMapping e WHERE e.eventParentMappingPK.parentId = :parentId")
    , @NamedQuery(name = "EventParentMapping.findByStatus", query = "SELECT e FROM EventParentMapping e WHERE e.status = :status")
    , @NamedQuery(name = "EventParentMapping.findByCreatedAt", query = "SELECT e FROM EventParentMapping e WHERE e.createdAt = :createdAt")})
public class EventParentMapping implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected EventParentMappingPK eventParentMappingPK;
    private Boolean status;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @JoinColumn(name = "Event_Id", referencedColumnName = "Id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Event event;
    @JoinColumn(name = "Parent_Id", referencedColumnName = "Id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Parent parent;

    public EventParentMapping() {
    }

    public EventParentMapping(EventParentMappingPK eventParentMappingPK) {
        this.eventParentMappingPK = eventParentMappingPK;
    }

    public EventParentMapping(EventParentMappingPK eventParentMappingPK, Date createdAt) {
        this.eventParentMappingPK = eventParentMappingPK;
        this.createdAt = createdAt;
    }

    public EventParentMapping(int eventId, int parentId) {
        this.eventParentMappingPK = new EventParentMappingPK(eventId, parentId);
    }

    public EventParentMappingPK getEventParentMappingPK() {
        return eventParentMappingPK;
    }

    public void setEventParentMappingPK(EventParentMappingPK eventParentMappingPK) {
        this.eventParentMappingPK = eventParentMappingPK;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Parent getParent() {
        return parent;
    }

    public void setParent(Parent parent) {
        this.parent = parent;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (eventParentMappingPK != null ? eventParentMappingPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EventParentMapping)) {
            return false;
        }
        EventParentMapping other = (EventParentMapping) object;
        if ((this.eventParentMappingPK == null && other.eventParentMappingPK != null) || (this.eventParentMappingPK != null && !this.eventParentMappingPK.equals(other.eventParentMappingPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.EventParentMapping[ eventParentMappingPK=" + eventParentMappingPK + " ]";
    }
    
}
