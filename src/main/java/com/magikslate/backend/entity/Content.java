/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Content.findAll", query = "SELECT c FROM Content c")
    , @NamedQuery(name = "Content.findById", query = "SELECT c FROM Content c WHERE c.id = :id")
    , @NamedQuery(name = "Content.findByDescription", query = "SELECT c FROM Content c WHERE c.description = :description")
    , @NamedQuery(name = "Content.findByType", query = "SELECT c FROM Content c WHERE c.type = :type")
    , @NamedQuery(name = "Content.findByCreatedAt", query = "SELECT c FROM Content c WHERE c.createdAt = :createdAt")
    , @NamedQuery(name = "Content.findByUpdatedAt", query = "SELECT c FROM Content c WHERE c.updatedAt = :updatedAt")
    , @NamedQuery(name = "Content.findByDeletedAt", query = "SELECT c FROM Content c WHERE c.deletedAt = :deletedAt")})
public class Content implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Size(max = 100)
    private String description;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    private String type;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt = new Date();
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @JoinTable(name = "StudyGroupTeacherContentMapping", joinColumns = {
        @JoinColumn(name = "Content_Id", referencedColumnName = "Id")}, inverseJoinColumns = {
        @JoinColumn(name = "StudyGroupTeacherMapping_Id", referencedColumnName = "Id")})
    @ManyToMany
    private List<StudyGroupTeacherMapping> studyGroupTeacherMappingList;
    @JoinTable(name = "SubjectClassTeacherContentMapping", joinColumns = {
        @JoinColumn(name = "Content_Id", referencedColumnName = "Id")}, inverseJoinColumns = {
        @JoinColumn(name = "SubjectClassTeacherMapping_Id", referencedColumnName = "Id")})
    @ManyToMany
    private List<SubjectClassTeacherMapping> subjectClassTeacherMappingList;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "content")
    private Attachment attachment;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "content")
    private UrlContent urlContent;
    @JoinColumn(name = "School_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private School school;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "content")
    private Note note;

    public Content() {
    }

    public Content(Integer id) {
        this.id = id;
    }

    public Content(Integer id, String type, Date createdAt, Date updatedAt) {
        this.id = id;
        this.type = type;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    @XmlTransient
    public List<StudyGroupTeacherMapping> getStudyGroupTeacherMappingList() {
        return studyGroupTeacherMappingList;
    }

    public void setStudyGroupTeacherMappingList(List<StudyGroupTeacherMapping> studyGroupTeacherMappingList) {
        this.studyGroupTeacherMappingList = studyGroupTeacherMappingList;
    }

    @XmlTransient
    public List<SubjectClassTeacherMapping> getSubjectClassTeacherMappingList() {
        return subjectClassTeacherMappingList;
    }

    public void setSubjectClassTeacherMappingList(List<SubjectClassTeacherMapping> subjectClassTeacherMappingList) {
        this.subjectClassTeacherMappingList = subjectClassTeacherMappingList;
    }

    public Attachment getAttachment() {
        return attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    public UrlContent getUrlContent() {
        return urlContent;
    }

    public void setUrlContent(UrlContent urlContent) {
        this.urlContent = urlContent;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Content)) {
            return false;
        }
        Content other = (Content) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.Content[ id=" + id + " ]";
    }
    
}
