/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author sankalpkulshrestha
 */
@Embeddable
public class EventParentMappingPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "Event_Id")
    private int eventId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Parent_Id")
    private int parentId;

    public EventParentMappingPK() {
    }

    public EventParentMappingPK(int eventId, int parentId) {
        this.eventId = eventId;
        this.parentId = parentId;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) eventId;
        hash += (int) parentId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EventParentMappingPK)) {
            return false;
        }
        EventParentMappingPK other = (EventParentMappingPK) object;
        if (this.eventId != other.eventId) {
            return false;
        }
        if (this.parentId != other.parentId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.EventParentMappingPK[ eventId=" + eventId + ", parentId=" + parentId + " ]";
    }
    
}
