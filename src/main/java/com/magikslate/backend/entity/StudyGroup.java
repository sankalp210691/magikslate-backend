/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StudyGroup.findAll", query = "SELECT s FROM StudyGroup s")
    , @NamedQuery(name = "StudyGroup.findById", query = "SELECT s FROM StudyGroup s WHERE s.id = :id")
    , @NamedQuery(name = "StudyGroup.findByCreatedAt", query = "SELECT s FROM StudyGroup s WHERE s.createdAt = :createdAt")
    , @NamedQuery(name = "StudyGroup.findByDeletedAt", query = "SELECT s FROM StudyGroup s WHERE s.deletedAt = :deletedAt")})
public class StudyGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @JoinColumn(name = "School_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private School school;
    @JoinColumn(name = "Subject_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Subject subject;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "studyGroup")
    private List<StudyGroupTeacherMapping> studyGroupTeacherMappingList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "studyGroup")
    private List<StudentStudyGroupMapping> studentStudyGroupMappingList;

    public StudyGroup() {
    }

    public StudyGroup(Integer id) {
        this.id = id;
    }

    public StudyGroup(Integer id, Date createdAt) {
        this.id = id;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    @XmlTransient
    public List<StudyGroupTeacherMapping> getStudyGroupTeacherMappingList() {
        return studyGroupTeacherMappingList;
    }

    public void setStudyGroupTeacherMappingList(List<StudyGroupTeacherMapping> studyGroupTeacherMappingList) {
        this.studyGroupTeacherMappingList = studyGroupTeacherMappingList;
    }

    @XmlTransient
    public List<StudentStudyGroupMapping> getStudentStudyGroupMappingList() {
        return studentStudyGroupMappingList;
    }

    public void setStudentStudyGroupMappingList(List<StudentStudyGroupMapping> studentStudyGroupMappingList) {
        this.studentStudyGroupMappingList = studentStudyGroupMappingList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StudyGroup)) {
            return false;
        }
        StudyGroup other = (StudyGroup) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.StudyGroup[ id=" + id + " ]";
    }
    
}
