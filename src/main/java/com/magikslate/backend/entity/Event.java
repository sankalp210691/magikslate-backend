/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Event.findAll", query = "SELECT e FROM Event e")
    , @NamedQuery(name = "Event.findById", query = "SELECT e FROM Event e WHERE e.id = :id")
    , @NamedQuery(name = "Event.findByTitle", query = "SELECT e FROM Event e WHERE e.title = :title")
    , @NamedQuery(name = "Event.findByDescription", query = "SELECT e FROM Event e WHERE e.description = :description")
    , @NamedQuery(name = "Event.findByEventForStaff", query = "SELECT e FROM Event e WHERE e.eventForStaff = :eventForStaff")
    , @NamedQuery(name = "Event.findByEventForParents", query = "SELECT e FROM Event e WHERE e.eventForParents = :eventForParents")
    , @NamedQuery(name = "Event.findByEventStartDate", query = "SELECT e FROM Event e WHERE e.eventStartDate = :eventStartDate")
    , @NamedQuery(name = "Event.findByEventFrequency", query = "SELECT e FROM Event e WHERE e.eventFrequency = :eventFrequency")
    , @NamedQuery(name = "Event.findByEventEndDate", query = "SELECT e FROM Event e WHERE e.eventEndDate = :eventEndDate")
    , @NamedQuery(name = "Event.findByCreatedAt", query = "SELECT e FROM Event e WHERE e.createdAt = :createdAt")
    , @NamedQuery(name = "Event.findByCreatorRole", query = "SELECT e FROM Event e WHERE e.creatorRole = :creatorRole")
    , @NamedQuery(name = "Event.findByDeletedAt", query = "SELECT e FROM Event e WHERE e.deletedAt = :deletedAt")
    , @NamedQuery(name = "Event.findByTeacherSentCount", query = "SELECT e FROM Event e WHERE e.teacherSentCount = :teacherSentCount")
    , @NamedQuery(name = "Event.findByParentSentCount", query = "SELECT e FROM Event e WHERE e.parentSentCount = :parentSentCount")
    , @NamedQuery(name = "Event.findByAdminSentCount", query = "SELECT e FROM Event e WHERE e.adminSentCount = :adminSentCount")})
public class Event implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    private String title;
    @Size(max = 1000)
    private String description;
    @Basic(optional = false)
    @NotNull
    private boolean eventForStaff;
    @Basic(optional = false)
    @NotNull
    private boolean eventForParents;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date eventStartDate;
    @Basic(optional = false)
    @NotNull
    private int eventFrequency;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date eventEndDate;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String creatorRole;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @Basic(optional = false)
    @NotNull
    private int teacherSentCount;
    @Basic(optional = false)
    @NotNull
    private int parentSentCount;
    @Basic(optional = false)
    @NotNull
    private int adminSentCount;
    @JoinTable(name = "ClassEventMapping", joinColumns = {@JoinColumn(name = "Event_Id", referencedColumnName = "Id")}, inverseJoinColumns = {@JoinColumn(name = "Class_Id", referencedColumnName = "Id")})@ManyToMany
    private List<Class> classList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "event")
    private List<EventTeacherMapping> eventTeacherMappingList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "event")
    private List<EventParentMapping> eventParentMappingList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "event")
    private List<EventAdminMapping> eventAdminMappingList;
    @JoinColumn(name = "School_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private School school;
    @JoinColumn(name = "User_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private User user;

    public Event() {
    }

    public Event(Integer id) {
        this.id = id;
    }

    public Event(Integer id, String title, boolean eventForStaff, boolean eventForParents, Date eventStartDate, int eventFrequency, Date eventEndDate, Date createdAt, String creatorRole, int teacherSentCount, int parentSentCount, int adminSentCount) {
        this.id = id;
        this.title = title;
        this.eventForStaff = eventForStaff;
        this.eventForParents = eventForParents;
        this.eventStartDate = eventStartDate;
        this.eventFrequency = eventFrequency;
        this.eventEndDate = eventEndDate;
        this.createdAt = createdAt;
        this.creatorRole = creatorRole;
        this.teacherSentCount = teacherSentCount;
        this.parentSentCount = parentSentCount;
        this.adminSentCount = adminSentCount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getEventForStaff() {
        return eventForStaff;
    }

    public void setEventForStaff(boolean eventForStaff) {
        this.eventForStaff = eventForStaff;
    }

    public boolean getEventForParents() {
        return eventForParents;
    }

    public void setEventForParents(boolean eventForParents) {
        this.eventForParents = eventForParents;
    }

    public Date getEventStartDate() {
        return eventStartDate;
    }

    public void setEventStartDate(Date eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public int getEventFrequency() {
        return eventFrequency;
    }

    public void setEventFrequency(int eventFrequency) {
        this.eventFrequency = eventFrequency;
    }

    public Date getEventEndDate() {
        return eventEndDate;
    }

    public void setEventEndDate(Date eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatorRole() {
        return creatorRole;
    }

    public void setCreatorRole(String creatorRole) {
        this.creatorRole = creatorRole;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public int getTeacherSentCount() {
        return teacherSentCount;
    }

    public void setTeacherSentCount(int teacherSentCount) {
        this.teacherSentCount = teacherSentCount;
    }

    public int getParentSentCount() {
        return parentSentCount;
    }

    public void setParentSentCount(int parentSentCount) {
        this.parentSentCount = parentSentCount;
    }

    public int getAdminSentCount() {
        return adminSentCount;
    }

    public void setAdminSentCount(int adminSentCount) {
        this.adminSentCount = adminSentCount;
    }

    @XmlTransient
    public List<Class> getClassList() {
        return classList;
    }

    public void setClassList(List<Class> classList) {
        this.classList = classList;
    }

    @XmlTransient
    public List<EventTeacherMapping> getEventTeacherMappingList() {
        return eventTeacherMappingList;
    }

    public void setEventTeacherMappingList(List<EventTeacherMapping> eventTeacherMappingList) {
        this.eventTeacherMappingList = eventTeacherMappingList;
    }

    @XmlTransient
    public List<EventParentMapping> getEventParentMappingList() {
        return eventParentMappingList;
    }

    public void setEventParentMappingList(List<EventParentMapping> eventParentMappingList) {
        this.eventParentMappingList = eventParentMappingList;
    }

    @XmlTransient
    public List<EventAdminMapping> getEventAdminMappingList() {
        return eventAdminMappingList;
    }

    public void setEventAdminMappingList(List<EventAdminMapping> eventAdminMappingList) {
        this.eventAdminMappingList = eventAdminMappingList;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Event)) {
            return false;
        }
        Event other = (Event) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.Event[ id=" + id + " ]";
    }
    
}
