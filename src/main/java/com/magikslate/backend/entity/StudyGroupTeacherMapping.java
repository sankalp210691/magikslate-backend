/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StudyGroupTeacherMapping.findAll", query = "SELECT s FROM StudyGroupTeacherMapping s")
    , @NamedQuery(name = "StudyGroupTeacherMapping.findById", query = "SELECT s FROM StudyGroupTeacherMapping s WHERE s.id = :id")
    , @NamedQuery(name = "StudyGroupTeacherMapping.findByCreatedAt", query = "SELECT s FROM StudyGroupTeacherMapping s WHERE s.createdAt = :createdAt")
    , @NamedQuery(name = "StudyGroupTeacherMapping.findByDeletedAt", query = "SELECT s FROM StudyGroupTeacherMapping s WHERE s.deletedAt = :deletedAt")})
public class StudyGroupTeacherMapping implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @ManyToMany(mappedBy = "studyGroupTeacherMappingList")
    private List<Content> contentList;
    @OneToMany(mappedBy = "studyGroupTeacherMapping")
    private List<Diary> diaryList;
    @OneToMany(mappedBy = "studyGroupTeacherMapping")
    private List<Test> testList;
    @JoinColumn(name = "StudyGroup_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private StudyGroup studyGroup;
    @JoinColumn(name = "Teacher_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Teacher teacher;

    public StudyGroupTeacherMapping() {
    }

    public StudyGroupTeacherMapping(Integer id) {
        this.id = id;
    }

    public StudyGroupTeacherMapping(Integer id, Date createdAt) {
        this.id = id;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    @XmlTransient
    public List<Content> getContentList() {
        return contentList;
    }

    public void setContentList(List<Content> contentList) {
        this.contentList = contentList;
    }

    @XmlTransient
    public List<Diary> getDiaryList() {
        return diaryList;
    }

    public void setDiaryList(List<Diary> diaryList) {
        this.diaryList = diaryList;
    }

    @XmlTransient
    public List<Test> getTestList() {
        return testList;
    }

    public void setTestList(List<Test> testList) {
        this.testList = testList;
    }

    public StudyGroup getStudyGroup() {
        return studyGroup;
    }

    public void setStudyGroup(StudyGroup studyGroup) {
        this.studyGroup = studyGroup;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StudyGroupTeacherMapping)) {
            return false;
        }
        StudyGroupTeacherMapping other = (StudyGroupTeacherMapping) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.StudyGroupTeacherMapping[ id=" + id + " ]";
    }
    
}
