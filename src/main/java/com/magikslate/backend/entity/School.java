/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "School.findAll", query = "SELECT s FROM School s")
    , @NamedQuery(name = "School.findById", query = "SELECT s FROM School s WHERE s.id = :id")
    , @NamedQuery(name = "School.findByName", query = "SELECT s FROM School s WHERE s.name = :name")
    , @NamedQuery(name = "School.findByCreatedAt", query = "SELECT s FROM School s WHERE s.createdAt = :createdAt")
    , @NamedQuery(name = "School.findByAddress", query = "SELECT s FROM School s WHERE s.address = :address")
    , @NamedQuery(name = "School.findByPincode", query = "SELECT s FROM School s WHERE s.pincode = :pincode")
    , @NamedQuery(name = "School.findByDisplayPic", query = "SELECT s FROM School s WHERE s.displayPic = :displayPic")
    , @NamedQuery(name = "School.findByStage", query = "SELECT s FROM School s WHERE s.stage = :stage")
    , @NamedQuery(name = "School.findByDeletedAt", query = "SELECT s FROM School s WHERE s.deletedAt = :deletedAt")
    , @NamedQuery(name = "School.findByUpdatedAt", query = "SELECT s FROM School s WHERE s.updatedAt = :updatedAt")
    , @NamedQuery(name = "School.findByLatitude", query = "SELECT s FROM School s WHERE s.latitude = :latitude")
    , @NamedQuery(name = "School.findByLongitude", query = "SELECT s FROM School s WHERE s.longitude = :longitude")
    , @NamedQuery(name = "School.findByPhone", query = "SELECT s FROM School s WHERE s.phone = :phone")
    , @NamedQuery(name = "School.findByEmail", query = "SELECT s FROM School s WHERE s.email = :email")
    , @NamedQuery(name = "School.findByWebsite", query = "SELECT s FROM School s WHERE s.website = :website")
    , @NamedQuery(name = "School.findBySearchable", query = "SELECT s FROM School s WHERE s.searchable = :searchable")
    , @NamedQuery(name = "School.findByStudentCount", query = "SELECT s FROM School s WHERE s.studentCount = :studentCount")
    , @NamedQuery(name = "School.findByTeacherCount", query = "SELECT s FROM School s WHERE s.teacherCount = :teacherCount")
    , @NamedQuery(name = "School.findByAverageRating", query = "SELECT s FROM School s WHERE s.averageRating = :averageRating")
    , @NamedQuery(name = "School.findByRatingCount", query = "SELECT s FROM School s WHERE s.ratingCount = :ratingCount")
    , @NamedQuery(name = "School.findByAbout", query = "SELECT s FROM School s WHERE s.about = :about")
    , @NamedQuery(name = "School.findByCoverPic", query = "SELECT s FROM School s WHERE s.coverPic = :coverPic")})
public class School implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    private String name;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    private String address;
    @Size(max = 7)
    private String pincode;
    @Size(max = 500)
    private String displayPic;
    @Basic(optional = false)
    @NotNull
    private int stage;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt = new Date();
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    private Float latitude;
    private Float longitude;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 200)
    private String phone;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 200)
    private String email;
    @Size(max = 100)
    private String website;
    @Basic(optional = false)
    @NotNull
    private boolean searchable;
    @Basic(optional = false)
    @NotNull
    private int studentCount;
    @Basic(optional = false)
    @NotNull
    private int teacherCount;
    @Basic(optional = false)
    @NotNull
    private double averageRating;
    @Basic(optional = false)
    @NotNull
    private int ratingCount;
    @Size(max = 500)
    private String about;
    @Size(max = 500)
    private String coverPic;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "school")
    private List<SchoolInstituteCategoryMapping> schoolInstituteCategoryMappingList;
    @JoinColumn(name = "Board_Id", referencedColumnName = "Id")
    @ManyToOne
    private Board board;
    @JoinColumn(name = "Locality_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Locality locality;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "school")
    private List<StudyGroup> studyGroupList;
    @OneToMany(mappedBy = "school")
    private List<Test> testList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "school")
    private List<Teacher> teacherList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "school")
    private List<Admin> adminList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "school")
    private List<Student> studentList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "school")
    private List<Subject> subjectList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "school")
    private List<UserSchoolInterest> userSchoolInterestList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "school")
    private List<Content> contentList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "school")
    private List<Event> eventList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "school")
    private List<Class> classList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "school")
    private List<ParentSchoolReview> parentSchoolReviewList;

    public School() {
    }

    public School(Integer id) {
        this.id = id;
    }

    public School(Integer id, String name, Date createdAt, String address, int stage, Date updatedAt, boolean searchable, int studentCount, int teacherCount, double averageRating, int ratingCount) {
        this.id = id;
        this.name = name;
        this.createdAt = createdAt;
        this.address = address;
        this.stage = stage;
        this.updatedAt = updatedAt;
        this.searchable = searchable;
        this.studentCount = studentCount;
        this.teacherCount = teacherCount;
        this.averageRating = averageRating;
        this.ratingCount = ratingCount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getDisplayPic() {
        return displayPic;
    }

    public void setDisplayPic(String displayPic) {
        this.displayPic = displayPic;
    }

    public int getStage() {
        return stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public boolean getSearchable() {
        return searchable;
    }

    public void setSearchable(boolean searchable) {
        this.searchable = searchable;
    }

    public int getStudentCount() {
        return studentCount;
    }

    public void setStudentCount(int studentCount) {
        this.studentCount = studentCount;
    }

    public int getTeacherCount() {
        return teacherCount;
    }

    public void setTeacherCount(int teacherCount) {
        this.teacherCount = teacherCount;
    }

    public double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(double averageRating) {
        this.averageRating = averageRating;
    }

    public int getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(int ratingCount) {
        this.ratingCount = ratingCount;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getCoverPic() {
        return coverPic;
    }

    public void setCoverPic(String coverPic) {
        this.coverPic = coverPic;
    }

    @XmlTransient
    public List<SchoolInstituteCategoryMapping> getSchoolInstituteCategoryMappingList() {
        return schoolInstituteCategoryMappingList;
    }

    public void setSchoolInstituteCategoryMappingList(List<SchoolInstituteCategoryMapping> schoolInstituteCategoryMappingList) {
        this.schoolInstituteCategoryMappingList = schoolInstituteCategoryMappingList;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public Locality getLocality() {
        return locality;
    }

    public void setLocality(Locality locality) {
        this.locality = locality;
    }

    @XmlTransient
    public List<StudyGroup> getStudyGroupList() {
        return studyGroupList;
    }

    public void setStudyGroupList(List<StudyGroup> studyGroupList) {
        this.studyGroupList = studyGroupList;
    }

    @XmlTransient
    public List<Test> getTestList() {
        return testList;
    }

    public void setTestList(List<Test> testList) {
        this.testList = testList;
    }

    @XmlTransient
    public List<Teacher> getTeacherList() {
        return teacherList;
    }

    public void setTeacherList(List<Teacher> teacherList) {
        this.teacherList = teacherList;
    }

    @XmlTransient
    public List<Admin> getAdminList() {
        return adminList;
    }

    public void setAdminList(List<Admin> adminList) {
        this.adminList = adminList;
    }

    @XmlTransient
    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    @XmlTransient
    public List<Subject> getSubjectList() {
        return subjectList;
    }

    public void setSubjectList(List<Subject> subjectList) {
        this.subjectList = subjectList;
    }

    @XmlTransient
    public List<UserSchoolInterest> getUserSchoolInterestList() {
        return userSchoolInterestList;
    }

    public void setUserSchoolInterestList(List<UserSchoolInterest> userSchoolInterestList) {
        this.userSchoolInterestList = userSchoolInterestList;
    }

    @XmlTransient
    public List<Content> getContentList() {
        return contentList;
    }

    public void setContentList(List<Content> contentList) {
        this.contentList = contentList;
    }

    @XmlTransient
    public List<Event> getEventList() {
        return eventList;
    }

    public void setEventList(List<Event> eventList) {
        this.eventList = eventList;
    }

    @XmlTransient
    public List<Class> getClassList() {
        return classList;
    }

    public void setClassList(List<Class> classList) {
        this.classList = classList;
    }

    @XmlTransient
    public List<ParentSchoolReview> getParentSchoolReviewList() {
        return parentSchoolReviewList;
    }

    public void setParentSchoolReviewList(List<ParentSchoolReview> parentSchoolReviewList) {
        this.parentSchoolReviewList = parentSchoolReviewList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof School)) {
            return false;
        }
        School other = (School) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.School[ id=" + id + ", name=" + name + ", address= " + address + ", locality= " + locality + ", board=" + board + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + ", stage=" + stage + ", displayPic=" + displayPic + ", pincode=" + pincode + " ]";
    }
    
}
