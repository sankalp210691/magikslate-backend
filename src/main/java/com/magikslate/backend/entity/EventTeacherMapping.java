/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EventTeacherMapping.findAll", query = "SELECT e FROM EventTeacherMapping e")
    , @NamedQuery(name = "EventTeacherMapping.findByEventId", query = "SELECT e FROM EventTeacherMapping e WHERE e.eventTeacherMappingPK.eventId = :eventId")
    , @NamedQuery(name = "EventTeacherMapping.findByTeacherId", query = "SELECT e FROM EventTeacherMapping e WHERE e.eventTeacherMappingPK.teacherId = :teacherId")
    , @NamedQuery(name = "EventTeacherMapping.findByStatus", query = "SELECT e FROM EventTeacherMapping e WHERE e.status = :status")
    , @NamedQuery(name = "EventTeacherMapping.findByCreatedAt", query = "SELECT e FROM EventTeacherMapping e WHERE e.createdAt = :createdAt")})
public class EventTeacherMapping implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected EventTeacherMappingPK eventTeacherMappingPK;
    private Boolean status;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @JoinColumn(name = "Event_Id", referencedColumnName = "Id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Event event;
    @JoinColumn(name = "Teacher_Id", referencedColumnName = "Id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Teacher teacher;

    public EventTeacherMapping() {
    }

    public EventTeacherMapping(EventTeacherMappingPK eventTeacherMappingPK) {
        this.eventTeacherMappingPK = eventTeacherMappingPK;
    }

    public EventTeacherMapping(EventTeacherMappingPK eventTeacherMappingPK, Date createdAt) {
        this.eventTeacherMappingPK = eventTeacherMappingPK;
        this.createdAt = createdAt;
    }

    public EventTeacherMapping(int eventId, int teacherId) {
        this.eventTeacherMappingPK = new EventTeacherMappingPK(eventId, teacherId);
    }

    public EventTeacherMappingPK getEventTeacherMappingPK() {
        return eventTeacherMappingPK;
    }

    public void setEventTeacherMappingPK(EventTeacherMappingPK eventTeacherMappingPK) {
        this.eventTeacherMappingPK = eventTeacherMappingPK;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (eventTeacherMappingPK != null ? eventTeacherMappingPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EventTeacherMapping)) {
            return false;
        }
        EventTeacherMapping other = (EventTeacherMapping) object;
        if ((this.eventTeacherMappingPK == null && other.eventTeacherMappingPK != null) || (this.eventTeacherMappingPK != null && !this.eventTeacherMappingPK.equals(other.eventTeacherMappingPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.EventTeacherMapping[ eventTeacherMappingPK=" + eventTeacherMappingPK + " ]";
    }
    
}
