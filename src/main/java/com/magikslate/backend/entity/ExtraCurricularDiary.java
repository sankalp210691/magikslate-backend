/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExtraCurricularDiary.findAll", query = "SELECT e FROM ExtraCurricularDiary e")
    , @NamedQuery(name = "ExtraCurricularDiary.findByDiaryId", query = "SELECT e FROM ExtraCurricularDiary e WHERE e.diaryId = :diaryId")
    , @NamedQuery(name = "ExtraCurricularDiary.findByType", query = "SELECT e FROM ExtraCurricularDiary e WHERE e.type = :type")
    , @NamedQuery(name = "ExtraCurricularDiary.findByDescription", query = "SELECT e FROM ExtraCurricularDiary e WHERE e.description = :description")})
public class ExtraCurricularDiary implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Diary_Id")
    private Integer diaryId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    private String type;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    private String description;
    @JoinColumn(name = "Diary_Id", referencedColumnName = "Id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Diary diary;

    public ExtraCurricularDiary() {
    }

    public ExtraCurricularDiary(Integer diaryId) {
        this.diaryId = diaryId;
    }

    public ExtraCurricularDiary(Integer diaryId, String type, String description) {
        this.diaryId = diaryId;
        this.type = type;
        this.description = description;
    }

    public Integer getDiaryId() {
        return diaryId;
    }

    public void setDiaryId(Integer diaryId) {
        this.diaryId = diaryId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Diary getDiary() {
        return diary;
    }

    public void setDiary(Diary diary) {
        this.diary = diary;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (diaryId != null ? diaryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExtraCurricularDiary)) {
            return false;
        }
        ExtraCurricularDiary other = (ExtraCurricularDiary) object;
        if ((this.diaryId == null && other.diaryId != null) || (this.diaryId != null && !this.diaryId.equals(other.diaryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.ExtraCurricularDiary[ diaryId=" + diaryId + ", type=" + type + ", description=" + description + " ]";
    }
    
}
