/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Diary.findAll", query = "SELECT d FROM Diary d")
    , @NamedQuery(name = "Diary.findById", query = "SELECT d FROM Diary d WHERE d.id = :id")
    , @NamedQuery(name = "Diary.findByType", query = "SELECT d FROM Diary d WHERE d.type = :type")
    , @NamedQuery(name = "Diary.findByCreatedAt", query = "SELECT d FROM Diary d WHERE d.createdAt = :createdAt")
    , @NamedQuery(name = "Diary.findByDeletedAt", query = "SELECT d FROM Diary d WHERE d.deletedAt = :deletedAt")
    , @NamedQuery(name = "Diary.findByUpdatedAt", query = "SELECT d FROM Diary d WHERE d.updatedAt = :updatedAt")})
public class Diary implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 22)
    private String type;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt = new Date();
    @JoinTable(name = "StudentDiaryMapping", joinColumns = {
        @JoinColumn(name = "Diary_Id", referencedColumnName = "Id")}, inverseJoinColumns = {
        @JoinColumn(name = "Student_Id", referencedColumnName = "Id")})
    @ManyToMany
    private List<Student> studentList;
    @JoinColumn(name = "StudyGroupTeacherMapping_Id", referencedColumnName = "Id")
    @ManyToOne
    private StudyGroupTeacherMapping studyGroupTeacherMapping;
    @JoinColumn(name = "Subject_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Subject subject;
    @JoinColumn(name = "SubjectClassTeacherMapping_Id", referencedColumnName = "Id")
    @ManyToOne
    private SubjectClassTeacherMapping subjectClassTeacherMapping;
    @JoinColumn(name = "Teacher_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Teacher teacher;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "diary")
    private GeneralDiary generalDiary;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "diary")
    private ExtraCurricularDiary extraCurricularDiary;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "diary")
    private List<Test> testList;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "diary")
    private LessonProgress lessonProgress;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "diary")
    private AnecdotalRecord anecdotalRecord;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "diary")
    private Kudo kudo;

    public Diary() {
    }

    public Diary(Integer id) {
        this.id = id;
    }

    public Diary(Integer id, String type, Date createdAt, Date updatedAt) {
        this.id = id;
        this.type = type;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @XmlTransient
    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    public StudyGroupTeacherMapping getStudyGroupTeacherMapping() {
        return studyGroupTeacherMapping;
    }

    public void setStudyGroupTeacherMapping(StudyGroupTeacherMapping studyGroupTeacherMapping) {
        this.studyGroupTeacherMapping = studyGroupTeacherMapping;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public SubjectClassTeacherMapping getSubjectClassTeacherMapping() {
        return subjectClassTeacherMapping;
    }

    public void setSubjectClassTeacherMapping(SubjectClassTeacherMapping subjectClassTeacherMapping) {
        this.subjectClassTeacherMapping = subjectClassTeacherMapping;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public GeneralDiary getGeneralDiary() {
        return generalDiary;
    }

    public void setGeneralDiary(GeneralDiary generalDiary) {
        this.generalDiary = generalDiary;
    }

    public ExtraCurricularDiary getExtraCurricularDiary() {
        return extraCurricularDiary;
    }

    public void setExtraCurricularDiary(ExtraCurricularDiary extraCurricularDiary) {
        this.extraCurricularDiary = extraCurricularDiary;
    }

    @XmlTransient
    public List<Test> getTestList() {
        return testList;
    }

    public void setTestList(List<Test> testList) {
        this.testList = testList;
    }

    public LessonProgress getLessonProgress() {
        return lessonProgress;
    }

    public void setLessonProgress(LessonProgress lessonProgress) {
        this.lessonProgress = lessonProgress;
    }

    public AnecdotalRecord getAnecdotalRecord() {
        return anecdotalRecord;
    }

    public void setAnecdotalRecord(AnecdotalRecord anecdotalRecord) {
        this.anecdotalRecord = anecdotalRecord;
    }

    public Kudo getKudo() {
        return kudo;
    }

    public void setKudo(Kudo kudo) {
        this.kudo = kudo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Diary)) {
            return false;
        }
        Diary other = (Diary) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.Diary[ id=" + id + ", type=" + type + ", createdAt=" + createdAt + ", deletedAt=" + deletedAt + ", subject=" + subject + ", generalDiary=" + generalDiary +  ", extraCurricularDiary=" + extraCurricularDiary + ", testList=" + testList + ", anecdotalRecord=" + anecdotalRecord + ", kudo=" + kudo + ", lessonProgress=" + lessonProgress + " ]";
    }
    
}
