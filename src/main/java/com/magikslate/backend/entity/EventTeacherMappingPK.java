/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author sankalpkulshrestha
 */
@Embeddable
public class EventTeacherMappingPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "Event_Id")
    private int eventId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Teacher_Id")
    private int teacherId;

    public EventTeacherMappingPK() {
    }

    public EventTeacherMappingPK(int eventId, int teacherId) {
        this.eventId = eventId;
        this.teacherId = teacherId;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) eventId;
        hash += (int) teacherId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EventTeacherMappingPK)) {
            return false;
        }
        EventTeacherMappingPK other = (EventTeacherMappingPK) object;
        if (this.eventId != other.eventId) {
            return false;
        }
        if (this.teacherId != other.teacherId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.EventTeacherMappingPK[ eventId=" + eventId + ", teacherId=" + teacherId + " ]";
    }
    
}
