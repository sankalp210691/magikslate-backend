/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Kudo.findAll", query = "SELECT k FROM Kudo k")
    , @NamedQuery(name = "Kudo.findByDiaryId", query = "SELECT k FROM Kudo k WHERE k.diaryId = :diaryId")
    , @NamedQuery(name = "Kudo.findByDescription", query = "SELECT k FROM Kudo k WHERE k.description = :description")})
public class Kudo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Diary_Id")
    private Integer diaryId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    private String description;
    @JoinColumn(name = "Diary_Id", referencedColumnName = "Id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Diary diary;

    public Kudo() {
    }

    public Kudo(Integer diaryId) {
        this.diaryId = diaryId;
    }

    public Kudo(Integer diaryId, String description) {
        this.diaryId = diaryId;
        this.description = description;
    }

    public Integer getDiaryId() {
        return diaryId;
    }

    public void setDiaryId(Integer diaryId) {
        this.diaryId = diaryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Diary getDiary() {
        return diary;
    }

    public void setDiary(Diary diary) {
        this.diary = diary;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (diaryId != null ? diaryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Kudo)) {
            return false;
        }
        Kudo other = (Kudo) object;
        if ((this.diaryId == null && other.diaryId != null) || (this.diaryId != null && !this.diaryId.equals(other.diaryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.Kudo[ diaryId=" + diaryId + ", description=" + description + " ]";
    }
    
}
