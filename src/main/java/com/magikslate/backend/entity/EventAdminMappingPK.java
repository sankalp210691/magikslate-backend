/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author sankalpkulshrestha
 */
@Embeddable
public class EventAdminMappingPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "Event_Id")
    private int eventId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Admin_Id")
    private int adminId;

    public EventAdminMappingPK() {
    }

    public EventAdminMappingPK(int eventId, int adminId) {
        this.eventId = eventId;
        this.adminId = adminId;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getAdminId() {
        return adminId;
    }

    public void setAdminId(int adminId) {
        this.adminId = adminId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) eventId;
        hash += (int) adminId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EventAdminMappingPK)) {
            return false;
        }
        EventAdminMappingPK other = (EventAdminMappingPK) object;
        if (this.eventId != other.eventId) {
            return false;
        }
        if (this.adminId != other.adminId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.EventAdminMappingPK[ eventId=" + eventId + ", adminId=" + adminId + " ]";
    }
    
}
