/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Student.findAll", query = "SELECT s FROM Student s")
    , @NamedQuery(name = "Student.findById", query = "SELECT s FROM Student s WHERE s.id = :id")
    , @NamedQuery(name = "Student.findByName", query = "SELECT s FROM Student s WHERE s.name = :name")
    , @NamedQuery(name = "Student.findByDateOfBirth", query = "SELECT s FROM Student s WHERE s.dateOfBirth = :dateOfBirth")
    , @NamedQuery(name = "Student.findByAdmissionCode", query = "SELECT s FROM Student s WHERE s.admissionCode = :admissionCode")
    , @NamedQuery(name = "Student.findByRollNumber", query = "SELECT s FROM Student s WHERE s.rollNumber = :rollNumber")
    , @NamedQuery(name = "Student.findByProfilePic", query = "SELECT s FROM Student s WHERE s.profilePic = :profilePic")
    , @NamedQuery(name = "Student.findByGender", query = "SELECT s FROM Student s WHERE s.gender = :gender")
    , @NamedQuery(name = "Student.findByCreatedAt", query = "SELECT s FROM Student s WHERE s.createdAt = :createdAt")
    , @NamedQuery(name = "Student.findByDeletedAt", query = "SELECT s FROM Student s WHERE s.deletedAt = :deletedAt")
    , @NamedQuery(name = "Student.findByUpdatedAt", query = "SELECT s FROM Student s WHERE s.updatedAt = :updatedAt")})
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    private String dateOfBirth;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String admissionCode;
    private Integer rollNumber;
    @Size(max = 200)
    private String profilePic;
    @Size(max = 2)
    private String gender;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt = new Date();
    @ManyToMany(mappedBy = "studentList")
    private List<Diary> diaryList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "student")
    private List<StudentParentMapping> studentParentMappingList;
    @JoinColumn(name = "School_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private School school;
    @OneToMany(mappedBy = "student")
    private List<Student> studentList = null;
    @JoinColumn(name = "RootStudent", referencedColumnName = "Id")
    @ManyToOne
    private Student student = null;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "student")
    private List<StudentStudyGroupMapping> studentStudyGroupMappingList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "student")
    private List<StudentClassMapping> studentClassMappingList;

    public Student() {
    }

    public Student(Integer id) {
        this.id = id;
    }

    public Student(Integer id, String name, String dateOfBirth, String admissionCode, Date createdAt, Date updatedAt) {
        this.id = id;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.admissionCode = admissionCode;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAdmissionCode() {
        return admissionCode;
    }

    public void setAdmissionCode(String admissionCode) {
        this.admissionCode = admissionCode;
    }

    public Integer getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(Integer rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @XmlTransient
    public List<Diary> getDiaryList() {
        return diaryList;
    }

    public void setDiaryList(List<Diary> diaryList) {
        this.diaryList = diaryList;
    }

    @XmlTransient
    public List<StudentParentMapping> getStudentParentMappingList() {
        return studentParentMappingList;
    }

    public void setStudentParentMappingList(List<StudentParentMapping> studentParentMappingList) {
        this.studentParentMappingList = studentParentMappingList;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    @XmlTransient
    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @XmlTransient
    public List<StudentStudyGroupMapping> getStudentStudyGroupMappingList() {
        return studentStudyGroupMappingList;
    }

    public void setStudentStudyGroupMappingList(List<StudentStudyGroupMapping> studentStudyGroupMappingList) {
        this.studentStudyGroupMappingList = studentStudyGroupMappingList;
    }

    @XmlTransient
    public List<StudentClassMapping> getStudentClassMappingList() {
        return studentClassMappingList;
    }

    public void setStudentClassMappingList(List<StudentClassMapping> studentClassMappingList) {
        this.studentClassMappingList = studentClassMappingList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Student)) {
            return false;
        }
        Student other = (Student) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.Student[ id=" + id + ", name=" + name + ", dateOfBirth=" + dateOfBirth + ", admissionCode=" + admissionCode + ", school=" + school + ", rollNumber=" + rollNumber + ", profilePic=" + profilePic + ", gender=" + gender + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + ", deletedAt=" + deletedAt + " ]";
    }
    
}
