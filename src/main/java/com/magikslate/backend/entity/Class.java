/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Class.findAll", query = "SELECT c FROM Class c")
    , @NamedQuery(name = "Class.findById", query = "SELECT c FROM Class c WHERE c.id = :id")
    , @NamedQuery(name = "Class.findByStandard", query = "SELECT c FROM Class c WHERE c.standard = :standard")
    , @NamedQuery(name = "Class.findBySection", query = "SELECT c FROM Class c WHERE c.section = :section")})
public class Class implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    private String standard;
    @Size(max = 2)
    private String section;
    @ManyToMany(mappedBy = "classList")
    private List<Announcement> announcementList;
    @JoinTable(name = "ClassEventMapping", joinColumns = {
        @JoinColumn(name = "Class_Id", referencedColumnName = "Id")}, inverseJoinColumns = {
        @JoinColumn(name = "Event_Id", referencedColumnName = "Id")})
    @ManyToMany
    private List<Event> eventList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "class1")
    private List<SubjectClassTeacherMapping> subjectClassTeacherMappingList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "class1")
    private List<ClassTeacherMapping> classTeacherMappingList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "class1")
    private List<StudentClassMapping> studentClassMappingList;
    @JoinColumn(name = "School_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private School school;

    public Class() {
    }

    public Class(Integer id) {
        this.id = id;
    }

    public Class(Integer id, String standard) {
        this.id = id;
        this.standard = standard;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    @XmlTransient
    public List<Announcement> getAnnouncementList() {
        return announcementList;
    }

    public void setAnnouncementList(List<Announcement> announcementList) {
        this.announcementList = announcementList;
    }

    @XmlTransient
    public List<Event> getEventList() {
        return eventList;
    }

    public void setEventList(List<Event> eventList) {
        this.eventList = eventList;
    }

    @XmlTransient
    public List<SubjectClassTeacherMapping> getSubjectClassTeacherMappingList() {
        return subjectClassTeacherMappingList;
    }

    public void setSubjectClassTeacherMappingList(List<SubjectClassTeacherMapping> subjectClassTeacherMappingList) {
        this.subjectClassTeacherMappingList = subjectClassTeacherMappingList;
    }

    @XmlTransient
    public List<ClassTeacherMapping> getClassTeacherMappingList() {
        return classTeacherMappingList;
    }

    public void setClassTeacherMappingList(List<ClassTeacherMapping> classTeacherMappingList) {
        this.classTeacherMappingList = classTeacherMappingList;
    }

    @XmlTransient
    public List<StudentClassMapping> getStudentClassMappingList() {
        return studentClassMappingList;
    }

    public void setStudentClassMappingList(List<StudentClassMapping> studentClassMappingList) {
        this.studentClassMappingList = studentClassMappingList;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Class)) {
            return false;
        }
        Class other = (Class) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.Class[ id=" + id + ", standard=" + standard + ", section=" + section + " ]";
    }
    
}
