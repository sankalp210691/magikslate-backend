/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StudentClassMapping.findAll", query = "SELECT s FROM StudentClassMapping s")
    , @NamedQuery(name = "StudentClassMapping.findById", query = "SELECT s FROM StudentClassMapping s WHERE s.id = :id")
    , @NamedQuery(name = "StudentClassMapping.findBySessionYear", query = "SELECT s FROM StudentClassMapping s WHERE s.sessionYear = :sessionYear")
    , @NamedQuery(name = "StudentClassMapping.findByCreatedAt", query = "SELECT s FROM StudentClassMapping s WHERE s.createdAt = :createdAt")
    , @NamedQuery(name = "StudentClassMapping.findByDeletedAt", query = "SELECT s FROM StudentClassMapping s WHERE s.deletedAt = :deletedAt")})
public class StudentClassMapping implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    private Integer sessionYear;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @OneToMany(mappedBy = "studentClassMapping")
    private List<Attendance> attendanceList;
    @JoinColumn(name = "Class_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Class class1;
    @JoinColumn(name = "Student_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Student student;

    public StudentClassMapping() {
    }

    public StudentClassMapping(Integer id) {
        this.id = id;
    }

    public StudentClassMapping(Integer id, Integer sessionYear, Date createdAt) {
        this.id = id;
        this.sessionYear = sessionYear;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSessionYear() {
        return sessionYear;
    }

    public void setSessionYear(Integer sessionYear) {
        this.sessionYear = sessionYear;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    @XmlTransient
    public List<Attendance> getAttendanceList() {
        return attendanceList;
    }

    public void setAttendanceList(List<Attendance> attendanceList) {
        this.attendanceList = attendanceList;
    }

    public Class getClass1() {
        return class1;
    }

    public void setClass1(Class class1) {
        this.class1 = class1;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StudentClassMapping)) {
            return false;
        }
        StudentClassMapping other = (StudentClassMapping) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.StudentClassMapping[ id=" + id + " ]";
    }
    
}
