/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Test.findAll", query = "SELECT t FROM Test t")
    , @NamedQuery(name = "Test.findById", query = "SELECT t FROM Test t WHERE t.id = :id")
    , @NamedQuery(name = "Test.findByTitle", query = "SELECT t FROM Test t WHERE t.title = :title")
    , @NamedQuery(name = "Test.findByDescription", query = "SELECT t FROM Test t WHERE t.description = :description")
    , @NamedQuery(name = "Test.findByMaxMarks", query = "SELECT t FROM Test t WHERE t.maxMarks = :maxMarks")
    , @NamedQuery(name = "Test.findByTestDate", query = "SELECT t FROM Test t WHERE t.testDate = :testDate")
    , @NamedQuery(name = "Test.findByCreatedAt", query = "SELECT t FROM Test t WHERE t.createdAt = :createdAt")
    , @NamedQuery(name = "Test.findByDeletedAt", query = "SELECT t FROM Test t WHERE t.deletedAt = :deletedAt")
    , @NamedQuery(name = "Test.findByUpdatedAt", query = "SELECT t FROM Test t WHERE t.updatedAt = :updatedAt")})
public class Test implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String title;
    @Size(max = 3000)
    private String description;
    @Basic(optional = false)
    @NotNull
    private int maxMarks;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date testDate;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt = new Date();
    @JoinColumn(name = "Diary_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Diary diary;
    @JoinColumn(name = "School_Id", referencedColumnName = "Id")
    @ManyToOne
    private School school;
    @JoinColumn(name = "StudyGroupTeacherMapping_Id", referencedColumnName = "Id")
    @ManyToOne
    private StudyGroupTeacherMapping studyGroupTeacherMapping;
    @JoinColumn(name = "SubjectClassTeacherMapping_Id", referencedColumnName = "Id")
    @ManyToOne
    private SubjectClassTeacherMapping subjectClassTeacherMapping;

    public Test() {
    }

    public Test(Integer id) {
        this.id = id;
    }

    public Test(Integer id, String title, int maxMarks, Date testDate, Date createdAt, Date updatedAt) {
        this.id = id;
        this.title = title;
        this.maxMarks = maxMarks;
        this.testDate = testDate;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMaxMarks() {
        return maxMarks;
    }

    public void setMaxMarks(int maxMarks) {
        this.maxMarks = maxMarks;
    }

    public Date getTestDate() {
        return testDate;
    }

    public void setTestDate(Date testDate) {
        this.testDate = testDate;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Diary getDiary() {
        return diary;
    }

    public void setDiary(Diary diary) {
        this.diary = diary;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public StudyGroupTeacherMapping getStudyGroupTeacherMapping() {
        return studyGroupTeacherMapping;
    }

    public void setStudyGroupTeacherMapping(StudyGroupTeacherMapping studyGroupTeacherMapping) {
        this.studyGroupTeacherMapping = studyGroupTeacherMapping;
    }

    public SubjectClassTeacherMapping getSubjectClassTeacherMapping() {
        return subjectClassTeacherMapping;
    }

    public void setSubjectClassTeacherMapping(SubjectClassTeacherMapping subjectClassTeacherMapping) {
        this.subjectClassTeacherMapping = subjectClassTeacherMapping;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Test)) {
            return false;
        }
        Test other = (Test) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.Test[ id=" + id + ", title=" + title + ", description= " + description + ", maxMarks= " + maxMarks + ", testDate=" + testDate + " ]";
    }
    
}
