/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InstituteCategory.findAll", query = "SELECT i FROM InstituteCategory i")
    , @NamedQuery(name = "InstituteCategory.findById", query = "SELECT i FROM InstituteCategory i WHERE i.id = :id")
    , @NamedQuery(name = "InstituteCategory.findByName", query = "SELECT i FROM InstituteCategory i WHERE i.name = :name")})
public class InstituteCategory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "instituteCategory")
    private List<SchoolInstituteCategoryMapping> schoolInstituteCategoryMappingList;

    public InstituteCategory() {
    }

    public InstituteCategory(Integer id) {
        this.id = id;
    }

    public InstituteCategory(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public List<SchoolInstituteCategoryMapping> getSchoolInstituteCategoryMappingList() {
        return schoolInstituteCategoryMappingList;
    }

    public void setSchoolInstituteCategoryMappingList(List<SchoolInstituteCategoryMapping> schoolInstituteCategoryMappingList) {
        this.schoolInstituteCategoryMappingList = schoolInstituteCategoryMappingList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstituteCategory)) {
            return false;
        }
        InstituteCategory other = (InstituteCategory) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.InstituteCategory[ id=" + id + ", name=" + name + " ]";
    }
    
}
