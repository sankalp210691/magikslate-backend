/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Parent.findAll", query = "SELECT p FROM Parent p")
    , @NamedQuery(name = "Parent.findById", query = "SELECT p FROM Parent p WHERE p.id = :id")
    , @NamedQuery(name = "Parent.findByAddress", query = "SELECT p FROM Parent p WHERE p.address = :address")
    , @NamedQuery(name = "Parent.findByCreatedAt", query = "SELECT p FROM Parent p WHERE p.createdAt = :createdAt")
    , @NamedQuery(name = "Parent.findByDeletedAt", query = "SELECT p FROM Parent p WHERE p.deletedAt = :deletedAt")})
public class Parent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Size(max = 200)
    private String address;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @JoinColumn(name = "User_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private User user;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parent")
    private List<StudentParentMapping> studentParentMappingList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parent")
    private List<EventParentMapping> eventParentMappingList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parent")
    private List<ParentSchoolReview> parentSchoolReviewList;

    public Parent() {
    }

    public Parent(Integer id) {
        this.id = id;
    }

    public Parent(Integer id, Date createdAt) {
        this.id = id;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @XmlTransient
    public List<StudentParentMapping> getStudentParentMappingList() {
        return studentParentMappingList;
    }

    public void setStudentParentMappingList(List<StudentParentMapping> studentParentMappingList) {
        this.studentParentMappingList = studentParentMappingList;
    }

    @XmlTransient
    public List<EventParentMapping> getEventParentMappingList() {
        return eventParentMappingList;
    }

    public void setEventParentMappingList(List<EventParentMapping> eventParentMappingList) {
        this.eventParentMappingList = eventParentMappingList;
    }

    @XmlTransient
    public List<ParentSchoolReview> getParentSchoolReviewList() {
        return parentSchoolReviewList;
    }

    public void setParentSchoolReviewList(List<ParentSchoolReview> parentSchoolReviewList) {
        this.parentSchoolReviewList = parentSchoolReviewList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parent)) {
            return false;
        }
        Parent other = (Parent) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.Parent[ id=" + id + " ]";
    }
    
}
