/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ParentSchoolReview.findAll", query = "SELECT p FROM ParentSchoolReview p")
    , @NamedQuery(name = "ParentSchoolReview.findById", query = "SELECT p FROM ParentSchoolReview p WHERE p.id = :id")
    , @NamedQuery(name = "ParentSchoolReview.findByRating", query = "SELECT p FROM ParentSchoolReview p WHERE p.rating = :rating")
    , @NamedQuery(name = "ParentSchoolReview.findByReview", query = "SELECT p FROM ParentSchoolReview p WHERE p.review = :review")
    , @NamedQuery(name = "ParentSchoolReview.findByCreatedAt", query = "SELECT p FROM ParentSchoolReview p WHERE p.createdAt = :createdAt")
    , @NamedQuery(name = "ParentSchoolReview.findByUpdatedAt", query = "SELECT p FROM ParentSchoolReview p WHERE p.updatedAt = :updatedAt")
    , @NamedQuery(name = "ParentSchoolReview.findByDeletedAt", query = "SELECT p FROM ParentSchoolReview p WHERE p.deletedAt = :deletedAt")})
public class ParentSchoolReview implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    private double rating;
    @Size(max = 1000)
    private String review;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt = new Date();
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @JoinColumn(name = "Parent_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Parent parent;
    @JoinColumn(name = "School_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private School school;

    public ParentSchoolReview() {
    }

    public ParentSchoolReview(Integer id) {
        this.id = id;
    }

    public ParentSchoolReview(Integer id, double rating, Date createdAt, Date updatedAt) {
        this.id = id;
        this.rating = rating;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Parent getParent() {
        return parent;
    }

    public void setParent(Parent parent) {
        this.parent = parent;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParentSchoolReview)) {
            return false;
        }
        ParentSchoolReview other = (ParentSchoolReview) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.ParentSchoolReview[ id=" + id + " ]";
    }
    
}
