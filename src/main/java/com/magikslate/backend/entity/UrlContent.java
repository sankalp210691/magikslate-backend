/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UrlContent.findAll", query = "SELECT u FROM UrlContent u")
    , @NamedQuery(name = "UrlContent.findByContentId", query = "SELECT u FROM UrlContent u WHERE u.contentId = :contentId")
    , @NamedQuery(name = "UrlContent.findByUrl", query = "SELECT u FROM UrlContent u WHERE u.url = :url")})
public class UrlContent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Content_Id")
    private Integer contentId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    private String url;
    @JoinColumn(name = "Content_Id", referencedColumnName = "Id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Content content;

    public UrlContent() {
    }

    public UrlContent(Integer contentId) {
        this.contentId = contentId;
    }

    public UrlContent(Integer contentId, String url) {
        this.contentId = contentId;
        this.url = url;
    }

    public Integer getContentId() {
        return contentId;
    }

    public void setContentId(Integer contentId) {
        this.contentId = contentId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contentId != null ? contentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UrlContent)) {
            return false;
        }
        UrlContent other = (UrlContent) object;
        if ((this.contentId == null && other.contentId != null) || (this.contentId != null && !this.contentId.equals(other.contentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.UrlContent[ contentId=" + contentId + " ]";
    }
    
}
