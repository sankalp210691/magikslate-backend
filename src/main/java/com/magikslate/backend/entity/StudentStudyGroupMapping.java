/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StudentStudyGroupMapping.findAll", query = "SELECT s FROM StudentStudyGroupMapping s")
    , @NamedQuery(name = "StudentStudyGroupMapping.findById", query = "SELECT s FROM StudentStudyGroupMapping s WHERE s.id = :id")
    , @NamedQuery(name = "StudentStudyGroupMapping.findByCreatedAt", query = "SELECT s FROM StudentStudyGroupMapping s WHERE s.createdAt = :createdAt")
    , @NamedQuery(name = "StudentStudyGroupMapping.findByDeletedAt", query = "SELECT s FROM StudentStudyGroupMapping s WHERE s.deletedAt = :deletedAt")})
public class StudentStudyGroupMapping implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @OneToMany(mappedBy = "studentStudyGroupMapping")
    private List<Attendance> attendanceList;
    @JoinColumn(name = "Student_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Student student;
    @JoinColumn(name = "StudyGroup_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private StudyGroup studyGroup;

    public StudentStudyGroupMapping() {
    }

    public StudentStudyGroupMapping(Integer id) {
        this.id = id;
    }

    public StudentStudyGroupMapping(Integer id, Date createdAt) {
        this.id = id;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    @XmlTransient
    public List<Attendance> getAttendanceList() {
        return attendanceList;
    }

    public void setAttendanceList(List<Attendance> attendanceList) {
        this.attendanceList = attendanceList;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public StudyGroup getStudyGroup() {
        return studyGroup;
    }

    public void setStudyGroup(StudyGroup studyGroup) {
        this.studyGroup = studyGroup;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StudentStudyGroupMapping)) {
            return false;
        }
        StudentStudyGroupMapping other = (StudentStudyGroupMapping) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.StudentStudyGroupMapping[ id=" + id + " ]";
    }
    
}
