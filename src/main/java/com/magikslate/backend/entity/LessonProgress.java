/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LessonProgress.findAll", query = "SELECT l FROM LessonProgress l")
    , @NamedQuery(name = "LessonProgress.findByDiaryId", query = "SELECT l FROM LessonProgress l WHERE l.diaryId = :diaryId")
    , @NamedQuery(name = "LessonProgress.findByChapterName", query = "SELECT l FROM LessonProgress l WHERE l.chapterName = :chapterName")
    , @NamedQuery(name = "LessonProgress.findByPagesCompleted", query = "SELECT l FROM LessonProgress l WHERE l.pagesCompleted = :pagesCompleted")})
public class LessonProgress implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Diary_Id")
    private Integer diaryId;
    @Size(max = 200)
    private String chapterName;
    @Size(max = 50)
    private String pagesCompleted;
    @JoinColumn(name = "Diary_Id", referencedColumnName = "Id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Diary diary;

    public LessonProgress() {
    }

    public LessonProgress(Integer diaryId) {
        this.diaryId = diaryId;
    }

    public Integer getDiaryId() {
        return diaryId;
    }

    public void setDiaryId(Integer diaryId) {
        this.diaryId = diaryId;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getPagesCompleted() {
        return pagesCompleted;
    }

    public void setPagesCompleted(String pagesCompleted) {
        this.pagesCompleted = pagesCompleted;
    }

    public Diary getDiary() {
        return diary;
    }

    public void setDiary(Diary diary) {
        this.diary = diary;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (diaryId != null ? diaryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LessonProgress)) {
            return false;
        }
        LessonProgress other = (LessonProgress) object;
        if ((this.diaryId == null && other.diaryId != null) || (this.diaryId != null && !this.diaryId.equals(other.diaryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.LessonProgress[ diaryId=" + diaryId + ", chapterName=" + chapterName + ", pagesCompleted=" + pagesCompleted + " ]";
    }
    
}
