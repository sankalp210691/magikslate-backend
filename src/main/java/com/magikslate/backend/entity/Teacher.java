/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Teacher.findAll", query = "SELECT t FROM Teacher t")
    , @NamedQuery(name = "Teacher.findById", query = "SELECT t FROM Teacher t WHERE t.id = :id")
    , @NamedQuery(name = "Teacher.findByEmployeeId", query = "SELECT t FROM Teacher t WHERE t.employeeId = :employeeId")
    , @NamedQuery(name = "Teacher.findByCreatedAt", query = "SELECT t FROM Teacher t WHERE t.createdAt = :createdAt")
    , @NamedQuery(name = "Teacher.findByDeletedAt", query = "SELECT t FROM Teacher t WHERE t.deletedAt = :deletedAt")})
public class Teacher implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    private String employeeId;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "teacher")
    private List<SubjectClassTeacherMapping> subjectClassTeacherMappingList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "teacher")
    private List<Diary> diaryList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "teacher")
    private List<ClassTeacherMapping> classTeacherMappingList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "teacher")
    private List<StudyGroupTeacherMapping> studyGroupTeacherMappingList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "teacher")
    private List<Attendance> attendanceList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "teacher")
    private List<EventTeacherMapping> eventTeacherMappingList;
    @JoinColumn(name = "School_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private School school;
    @JoinColumn(name = "User_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private User user;

    public Teacher() {
    }

    public Teacher(Integer id) {
        this.id = id;
    }

    public Teacher(Integer id, String employeeId, Date createdAt) {
        this.id = id;
        this.employeeId = employeeId;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    @XmlTransient
    public List<SubjectClassTeacherMapping> getSubjectClassTeacherMappingList() {
        return subjectClassTeacherMappingList;
    }

    public void setSubjectClassTeacherMappingList(List<SubjectClassTeacherMapping> subjectClassTeacherMappingList) {
        this.subjectClassTeacherMappingList = subjectClassTeacherMappingList;
    }

    @XmlTransient
    public List<Diary> getDiaryList() {
        return diaryList;
    }

    public void setDiaryList(List<Diary> diaryList) {
        this.diaryList = diaryList;
    }

    @XmlTransient
    public List<ClassTeacherMapping> getClassTeacherMappingList() {
        return classTeacherMappingList;
    }

    public void setClassTeacherMappingList(List<ClassTeacherMapping> classTeacherMappingList) {
        this.classTeacherMappingList = classTeacherMappingList;
    }

    @XmlTransient
    public List<StudyGroupTeacherMapping> getStudyGroupTeacherMappingList() {
        return studyGroupTeacherMappingList;
    }

    public void setStudyGroupTeacherMappingList(List<StudyGroupTeacherMapping> studyGroupTeacherMappingList) {
        this.studyGroupTeacherMappingList = studyGroupTeacherMappingList;
    }

    @XmlTransient
    public List<Attendance> getAttendanceList() {
        return attendanceList;
    }

    public void setAttendanceList(List<Attendance> attendanceList) {
        this.attendanceList = attendanceList;
    }

    @XmlTransient
    public List<EventTeacherMapping> getEventTeacherMappingList() {
        return eventTeacherMappingList;
    }

    public void setEventTeacherMappingList(List<EventTeacherMapping> eventTeacherMappingList) {
        this.eventTeacherMappingList = eventTeacherMappingList;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Teacher)) {
            return false;
        }
        Teacher other = (Teacher) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.Teacher[ id=" + id + " ]";
    }
    
}
