/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SchoolInstituteCategoryMapping.findAll", query = "SELECT s FROM SchoolInstituteCategoryMapping s")
    , @NamedQuery(name = "SchoolInstituteCategoryMapping.findById", query = "SELECT s FROM SchoolInstituteCategoryMapping s WHERE s.id = :id")
    , @NamedQuery(name = "SchoolInstituteCategoryMapping.findByCreatedAt", query = "SELECT s FROM SchoolInstituteCategoryMapping s WHERE s.createdAt = :createdAt")
    , @NamedQuery(name = "SchoolInstituteCategoryMapping.findByUpdatedAt", query = "SELECT s FROM SchoolInstituteCategoryMapping s WHERE s.updatedAt = :updatedAt")
    , @NamedQuery(name = "SchoolInstituteCategoryMapping.findByDeletedAt", query = "SELECT s FROM SchoolInstituteCategoryMapping s WHERE s.deletedAt = :deletedAt")})
public class SchoolInstituteCategoryMapping implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt = new Date();
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @JoinColumn(name = "InstituteCategory_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private InstituteCategory instituteCategory;
    @JoinColumn(name = "School_Id", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private School school;

    public SchoolInstituteCategoryMapping() {
    }

    public SchoolInstituteCategoryMapping(Integer id) {
        this.id = id;
    }

    public SchoolInstituteCategoryMapping(Integer id, Date createdAt, Date updatedAt) {
        this.id = id;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public InstituteCategory getInstituteCategory() {
        return instituteCategory;
    }

    public void setInstituteCategory(InstituteCategory instituteCategory) {
        this.instituteCategory = instituteCategory;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SchoolInstituteCategoryMapping)) {
            return false;
        }
        SchoolInstituteCategoryMapping other = (SchoolInstituteCategoryMapping) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.SchoolInstituteCategoryMapping[ id=" + id + " ]";
    }
    
}
