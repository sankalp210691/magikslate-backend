/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EventAdminMapping.findAll", query = "SELECT e FROM EventAdminMapping e")
    , @NamedQuery(name = "EventAdminMapping.findByEventId", query = "SELECT e FROM EventAdminMapping e WHERE e.eventAdminMappingPK.eventId = :eventId")
    , @NamedQuery(name = "EventAdminMapping.findByAdminId", query = "SELECT e FROM EventAdminMapping e WHERE e.eventAdminMappingPK.adminId = :adminId")
    , @NamedQuery(name = "EventAdminMapping.findByStatus", query = "SELECT e FROM EventAdminMapping e WHERE e.status = :status")
    , @NamedQuery(name = "EventAdminMapping.findByCreatedAt", query = "SELECT e FROM EventAdminMapping e WHERE e.createdAt = :createdAt")})
public class EventAdminMapping implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected EventAdminMappingPK eventAdminMappingPK;
    private Boolean status;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @JoinColumn(name = "Admin_Id", referencedColumnName = "Id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Admin admin;
    @JoinColumn(name = "Event_Id", referencedColumnName = "Id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Event event;

    public EventAdminMapping() {
    }

    public EventAdminMapping(EventAdminMappingPK eventAdminMappingPK) {
        this.eventAdminMappingPK = eventAdminMappingPK;
    }

    public EventAdminMapping(EventAdminMappingPK eventAdminMappingPK, Date createdAt) {
        this.eventAdminMappingPK = eventAdminMappingPK;
        this.createdAt = createdAt;
    }

    public EventAdminMapping(int eventId, int adminId) {
        this.eventAdminMappingPK = new EventAdminMappingPK(eventId, adminId);
    }

    public EventAdminMappingPK getEventAdminMappingPK() {
        return eventAdminMappingPK;
    }

    public void setEventAdminMappingPK(EventAdminMappingPK eventAdminMappingPK) {
        this.eventAdminMappingPK = eventAdminMappingPK;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (eventAdminMappingPK != null ? eventAdminMappingPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EventAdminMapping)) {
            return false;
        }
        EventAdminMapping other = (EventAdminMapping) object;
        if ((this.eventAdminMappingPK == null && other.eventAdminMappingPK != null) || (this.eventAdminMappingPK != null && !this.eventAdminMappingPK.equals(other.eventAdminMappingPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.EventAdminMapping[ eventAdminMappingPK=" + eventAdminMappingPK + " ]";
    }
    
}
