/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sankalpkulshrestha
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Attendance.findAll", query = "SELECT a FROM Attendance a")
    , @NamedQuery(name = "Attendance.findById", query = "SELECT a FROM Attendance a WHERE a.id = :id")
    , @NamedQuery(name = "Attendance.findByPresent", query = "SELECT a FROM Attendance a WHERE a.present = :present")
    , @NamedQuery(name = "Attendance.findByCreatedAt", query = "SELECT a FROM Attendance a WHERE a.createdAt = :createdAt")
    , @NamedQuery(name = "Attendance.findByUpdatedAt", query = "SELECT a FROM Attendance a WHERE a.updatedAt = :updatedAt")
    , @NamedQuery(name = "Attendance.findBySessionYear", query = "SELECT a FROM Attendance a WHERE a.sessionYear = :sessionYear")})
public class Attendance implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    private boolean present;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt = new Date();
    @Basic(optional = false)
    @NotNull
    private Integer sessionYear;
    @JoinColumn(name = "StudentClassMapping_Id", referencedColumnName = "Id")
    @ManyToOne
    private StudentClassMapping studentClassMapping;
    @JoinColumn(name = "StudentStudyGroupMapping_Id", referencedColumnName = "Id")
    @ManyToOne
    private StudentStudyGroupMapping studentStudyGroupMapping;
    @JoinColumn(name = "MarkedBy", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Teacher teacher;

    public Attendance() {
    }

    public Attendance(Integer id) {
        this.id = id;
    }

    public Attendance(Integer id, boolean present, Date createdAt, Date updatedAt, Integer sessionYear) {
        this.id = id;
        this.present = present;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.sessionYear = sessionYear;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean getPresent() {
        return present;
    }

    public void setPresent(boolean present) {
        this.present = present;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getSessionYear() {
        return sessionYear;
    }

    public void setSessionYear(Integer sessionYear) {
        this.sessionYear = sessionYear;
    }

    public StudentClassMapping getStudentClassMapping() {
        return studentClassMapping;
    }

    public void setStudentClassMapping(StudentClassMapping studentClassMapping) {
        this.studentClassMapping = studentClassMapping;
    }

    public StudentStudyGroupMapping getStudentStudyGroupMapping() {
        return studentStudyGroupMapping;
    }

    public void setStudentStudyGroupMapping(StudentStudyGroupMapping studentStudyGroupMapping) {
        this.studentStudyGroupMapping = studentStudyGroupMapping;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Attendance)) {
            return false;
        }
        Attendance other = (Attendance) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magikslate.backend.entity.Attendance[ id=" + id + " ]";
    }
    
}
