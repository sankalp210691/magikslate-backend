/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.feed;

import com.magikslate.backend.entity.Announcement;
import com.magikslate.backend.entity.Attendance;
import com.magikslate.backend.entity.Content;
import com.magikslate.backend.entity.Diary;
import com.magikslate.backend.entity.Event;
import com.magikslate.backend.util.Constants;

/**
 *
 * @author sankalpkulshrestha
 */
public class FeedItem {

    private String feedType;

    private Event event;
    private Announcement announcement;
    private Diary diary;
    private Attendance attendance;
    private Content content;
    
    public FeedItem(Event event){
        this.event = event;
        feedType = Constants.EVENT;
    }
    
    public FeedItem(Announcement announcement){
        this.announcement = announcement;
        feedType = Constants.ANNOUNCEMENT;
    }
    
    public FeedItem(Diary diary){
        this.diary = diary;
        feedType = Constants.DIARY;
    }
    
    public FeedItem(Attendance attendance){
        this.attendance = attendance;
        feedType = Constants.ATTENDANCE;
    }
    
    public FeedItem(Content content){
        this.content = content;
        feedType = Constants.CONTENT;
    }

    /**
     * @return the feedType
     */
    public String getFeedType() {
        return feedType;
    }

    /**
     * @param feedType the feedType to set
     */
    public void setFeedType(String feedType) {
        this.feedType = feedType;
    }

    /**
     * @return the event
     */
    public Event getEvent() {
        return event;
    }

    /**
     * @return the announcement
     */
    public Announcement getAnnouncement() {
        return announcement;
    }

    /**
     * @return the diary
     */
    public Diary getDiary() {
        return diary;
    }

    /**
     * @return the attendance
     */
    public Attendance getAttendance() {
        return attendance;
    }

    /**
     * @return the content
     */
    public Content getContent() {
        return content;
    }

}
