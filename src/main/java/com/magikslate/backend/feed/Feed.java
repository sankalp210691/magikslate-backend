/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.feed;

import com.magikslate.backend.entity.Announcement;
import com.magikslate.backend.entity.Attendance;
import com.magikslate.backend.entity.Content;
import com.magikslate.backend.entity.Diary;
import com.magikslate.backend.entity.Event;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public class Feed {

    private final Iterator<FeedItem> iterator;
    private final Date VERY_OLD_DATE = new Date(0);
    private final int FEED_ITEM_TYPE_COUNT = 5;

    public Feed(List<Event> eventList, List<Announcement> announcementList, List<Diary> diaryList, List<Attendance> attendanceList, List<Content> contentList) {

        List<FeedItem> feedItemList = new ArrayList();

        Integer eventCounter = 0;
        Integer announcementCounter = 0;
        Integer diaryCounter = 0;
        Integer attendanceCounter = 0;
        Integer contentCounter = 0;

        Date[] dateArray = new Date[FEED_ITEM_TYPE_COUNT];

        while (true) {

            for (int index = 0; index < FEED_ITEM_TYPE_COUNT; index++) {
                dateArray[index] = VERY_OLD_DATE;
            }

            if (eventCounter < eventList.size()) {
                dateArray[0] = eventList.get(eventCounter).getCreatedAt();
            }
            if (announcementCounter < announcementList.size()) {
                dateArray[1] = announcementList.get(announcementCounter).getCreatedAt();
            }
            if (diaryCounter < diaryList.size()) {
                dateArray[2] = diaryList.get(diaryCounter).getCreatedAt();
            }
            if (attendanceCounter < attendanceList.size()) {
                dateArray[3] = attendanceList.get(attendanceCounter).getCreatedAt();
            }
            if (contentCounter < contentList.size()) {
                dateArray[4] = contentList.get(contentCounter).getCreatedAt();
            }

            Date maxDate = VERY_OLD_DATE;
            int maxIndex = -1;

            for (int index = 0; index < FEED_ITEM_TYPE_COUNT; index++) {

                if (dateArray[index].after(maxDate)) {
                    maxDate = dateArray[index];
                    maxIndex = index;
                }
            }

            if (maxIndex == -1) {
                break;
            }

            FeedItem feedItem = null;
            switch (maxIndex) {
                case 0:
                    feedItem = new FeedItem(eventList.get(eventCounter));
                    eventCounter++;
                    break;
                case 1:
                    feedItem = new FeedItem(announcementList.get(announcementCounter));
                    announcementCounter++;
                    break;
                case 2:
                    feedItem = new FeedItem(diaryList.get(diaryCounter));
                    diaryCounter++;
                    break;
                case 3:
                    feedItem = new FeedItem(attendanceList.get(attendanceCounter));
                    attendanceCounter++;
                    break;
                case 4:
                    feedItem = new FeedItem(contentList.get(contentCounter));
                    contentCounter++;
                    break;
            }

            feedItemList.add(feedItem);
        }

        iterator = feedItemList.iterator();
    }

    public boolean hasNext() {
        return iterator.hasNext();
    }

    public FeedItem next() {
        return iterator.next();
    }
}
