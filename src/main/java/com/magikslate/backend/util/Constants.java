/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.util;

import io.vertx.core.json.JsonObject;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.slf4j.LoggerFactory;

/**
 *
 * @author sankalpkulshrestha
 */
public class Constants {
    
    private static final Properties PROPERTIES = new Properties();
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(Constants.class);
    public static final Map<String, String> SUPPORTED_APP_VERSIONS_MAP = new HashMap();
    
    static {
        try {
            PROPERTIES.load(new FileInputStream(new File("/disk1/magikslate.properties")));
            if (PROPERTIES.getProperty("AUTH_CONSTANT_STRING") == null) {
                LOGGER.error("Properties file doesn't have AUTH_CONSTANT_STRING value");
                System.exit(0);
            }
            if (PROPERTIES.getProperty("supported.app.versions") == null) {
                LOGGER.error("Properties file doesn't have supported.app.versions value");
                System.exit(0);
            } else {
                JsonObject supportedAppVersionJson = new JsonObject(PROPERTIES.getProperty("supported.app.versions"));
                Set<String> fieldNames = supportedAppVersionJson.fieldNames();
                fieldNames.forEach((fieldName) -> {
                    SUPPORTED_APP_VERSIONS_MAP.put(fieldName, supportedAppVersionJson.getString(fieldName));
                });
                if(SUPPORTED_APP_VERSIONS_MAP.isEmpty()) {
                    LOGGER.error("SUPPORTED_APP_VERSIONS_MAP is empty");
                    System.exit(0);
                }
            }
        } catch (FileNotFoundException ex) {
            LOGGER.error(ex.getMessage());
            System.exit(0);
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage());
            System.exit(0);
        }
    }
    
    public static final String DEFAULT_ORIGIN = "https://www.magikslate.com";
    public static final String UPLOAD_FILE_LOCATION = PROPERTIES.getProperty("uploaded.file.location", "/disk1/temp/");
    public static final String FAQ_JSON_FILE_LOCATION = "/disk1/faqjson.js";
    public static final String AUTH_CONSTANT_STRING = PROPERTIES.getProperty("AUTH_CONSTANT_STRING");
    public static final String REG_PASSWORD_CHANGE_PENDING = "REG_PASSWORD_CHANGE_PENDING";
    public static final String OTP_VERIFICATION_PENDING = "OTP_VERIFICATION_PENDING";
    public static final String STATIC_RESOURCE_DNS = PROPERTIES.getProperty("static.resource.dns", "https://s3.us-east-2.amazonaws.com/mgkslt-stage-public-school/");
    public static final String HEALTHCHECK_URL = "/";
    public static final String ACTIVE = "ACTIVE";
    public static final int OTP_LENGTH = 6;
    public static final int HOURS_PER_DAY = 24;
    public static final char FIRST_SECTION = 'A';
    public static final String STUDENT_TYPE = "student";
    public static final String TEACHER_TYPE = "teacher";
    public static final String STUDENT_TEACHER_TYPE = "stmapping";
    public static final String FILE_UPLOAD_FOLDER = "/disk1/";
    public static final String UNDERSCORE_SEPARATOR = "_";
    public static final String PLUS_SEPARATOR = "+";
    public static final String DOT_SEPARATOR = ".";
    public static final String HYPHEN_SEPARATOR = "-";
    public static final String SLASH_SEPARATOR = "/";
    public static final String COMMA_SEPARATOR = ",";
    public static final Integer RANDOM_FILE_SALT_LENGTH = 6;
    public static final Integer DUMMY_PASSWORD_LENGTH = 6;
    public static final Integer DAYS_IN_YEAR = 366;
    public static final String PARENT1_NAME_COLUMN = "Parent 1 Name";
    public static final String PARENT1_EMAIL_COLUMN = "Parent 1 Email";
    public static final String PARENT1_PHONE_IC_COLUMN = "Parent 1 Phone Country Code";
    public static final String PARENT1_PHONE_COLUMN = "Parent 1 Phone";
    public static final String PARENT1_ADDRESS_COLUMN = "Parent 1 Address";
    public static final String PARENT2_NAME_COLUMN = "Parent 2 Name";
    public static final String PARENT2_EMAIL_COLUMN = "Parent 2 Email";
    public static final String PARENT2_PHONE_IC_COLUMN = "Parent 2 Phone Country Code";
    public static final String PARENT2_PHONE_COLUMN = "Parent 2 Phone";
    public static final String PARENT2_ADDRESS_COLUMN = "Parent 2 Address";
    public static final String ADMISSION_CODE_COLUMN = "Admission Code";
    public static final String STUDENT_NAME_COLUMN = "Student Name";
    public static final String DOB_COLUMN = "Date of Birth (DD/MM/YYYY)";
    public static final String STUDENT_STANDARD_COLUMN = "Standard";
    public static final String STUDENT_SECTION_COLUMN = "Section";
    public static final int HIBERNATE_BATCH_SIZE = 5;
    public static final String TEACHER_STANDARD_COLUMN = "Standard";
    public static final String TEACHER_SECTION_COLUMN = "Section";
    public static final String TEACHER_SUBJECT_COLUMN = "Subject";
    public static final String TEACHER_EMPLOYEE_ID_COLUMN = "Employee Id";
    public static final String TEACHER_NAME_COLUMN = "Name";
    public static final String TEACHER_PHONE_IC_COLUMN = "Phone Country Code";
    public static final String TEACHER_PHONE_COLUMN = "Phone";
    public static final String TEACHER_EMAIL_COLUMN = "Email";
    public static final String TEACHER_STUDENT_COLUMN = "Admission Code (optional comma separated list of students part of optional subjects)";
    public static final String CLASS_TEACHER_STANDARD = "Class Teacher Standard";
    public static final String CLASS_TEACHER_SECTION = "Class Teacher Section";
    public static final String DEPUTY_TEACHER_STANDARD = "Deputy Teacher Standard";
    public static final String DEPUTY_TEACHER_SECTION = "Deputy Teacher Section";
    public static final String EVENT_ACCEPTED = "ACCEPTED";
    public static final String EVENT_REJECTED = "REJECTED";
    public static final int LIST_LIMIT = 20;
    public static final String ADMIN = "ADMIN";
    public static final String TEACHER = "TEACHER";
    public static final String PARENT = "PARENT";
    public static final int MAX_EXCEL_COLUMN_COUNT = 20;
    public static final String ATTACHMENT_CONTENT_TYPE = "ATTACHMENT";
    public static final String NOTE_CONTENT_TYPE = "NOTE";
    public static final String URL_CONTENT_TYPE = "URL";
    public static final String CLASS_TYPE = "class";
    public static final String STUDYGROUP_TYPE = "studygroup";
    public static final String CSV = "csv";
    public static final String JPG = "jpg";
    public static final String JPEG = "jpeg";
    public static final String PNG = "png";
    public static final String TSV = "tsv";
    public static final String GENERAL_DIARY = "GENERAL";
    public static final String KUDO_DIARY = "KUDOS";
    public static final String ANECDOTAL_RECORD_DIARY = "ANECDOTAL_RECORD";
    public static final String LESSON_PROGRESS_DIARY = "LESSON_PROGRESS";
    public static final String TEST_DIARY = "TEST";
    public static final String EX_DIARY = "EXTRA_CURRICULAR_DIARY";
    public static final String EVENT = "EVENT";
    public static final String ANNOUNCEMENT = "ANNOUNCEMENT";
    public static final String ATTENDANCE = "ATTENDANCE";
    public static final String CONTENT = "CONTENT";
    public static final String DIARY = "DIARY";
    public static final String FORGOT_PASSWORD_TOKEN = "fp";
    public static final Date OLD_DATE = new Date(0l);
    public static final Date FUTURE_DATE = new Date(33083357016000l);
    public static final Integer MIN_REVIEW_SIZE = Integer.parseInt(PROPERTIES.getProperty("review.minReviewSize", "140"));
    public static final Integer MAX_REVIEW_SIZE = Integer.parseInt(PROPERTIES.getProperty("review.maxReviewSize", "1000"));
    public static final String SCHOOL_PROFILE_PICTURE_TYPE = "profile_pic";
    public static final String SCHOOL_COVER_PICTURE_TYPE = "cover_pic";
    
    public static final String S3_SCHOOL_BUCKET = PROPERTIES.getProperty("s3.school.bucket", "mgkslt-stage-public-school");
    public static final String SCHOOL_ID_MACRO = "{schoolId}";
    public static final String DATE_STRUCTURE_MACRO = "{dateStructure}";
    public static final String S3_ATTACHMENT_FOLDER = "{schoolId}/{dateStructure}/content/attachments/";
    public static final String S3_SCHOOL_DP_FOLDER = "{schoolId}/";
    public static final String S3_STUDENT_DP_FOLDER = "{schoolId}/students/profilepic/";
    
    public static final String CLASSES_BY_TEACHER = "select \"class\", c.id as classId, c.standard as standard, c.section as section, sb.id as subjectId, sb.name as subjectName, s.id as studentId,"
            + " s.name as studentName, sctm.id as sctmId, scm.id as scmId, s.admissionCode from SubjectClassTeacherMapping sctm join Class c on c.id = sctm.class_id join Subject sb on sb.id = sctm.subject_id join Teacher t "
            + "on t.id = sctm.teacher_id join StudentClassMapping scm on scm.class_id = c.id join Student s on s.id = scm.student_id join User u on u.id = t.user_id "
            + "where sctm.deletedAt IS NULL and s.deletedAt IS NULL and scm.deletedAt IS NULL and t.deletedAt IS NULL and u.deletedAt IS NULL  and t.id = :teacherId "
            + "and u.active = 1 and u.deletedAt IS NULL";
    public static final String SG_BY_TEACHER = "select \"studygroup\", sg.id as sgId, sb.id as subjectId, sb.name as subjectName, s.id as studentId,"
            + " s.name as studentName, sgtm.id as sgtmId, ssgm.id as ssgmId, s.admissionCode from StudyGroupTeacherMapping sgtm join StudyGroup sg on sg.id = sgtm.studygroup_id join Subject sb on sb.id = sg.subject_id join Teacher t "
            + "on t.id = sgtm.teacher_id join StudentStudyGroupMapping ssgm on ssgm.studygroup_id = sg.id join Student s on s.id = ssgm.student_id join User u on u.id = t.user_id "
            + "where sgtm.deletedAt IS NULL and s.deletedAt IS NULL and ssgm.deletedAt IS NULL and t.deletedAt IS NULL and u.deletedAt IS NULL  and t.id = :teacherId "
            + "and u.active = 1 and u.deletedAt IS NULL";
    public static final String CONTENT_BY_TEACHER_CLASS = "from Content c join UrlContent uc on c.id =uc.contentId join Attachment a on c.id = a.contentId join Note n on c.id = n.contentId"
            + "where c.teacher.id = :teacherId"
            + " and c.classList in (:classId) and c.created >= :startDate and c.createdAt <= :endDate order by c.createdAt desc";//group by date(createdAt)??
    public static final String DIARY_BY_CLASS = "from Diary d where d.class.id = :classId order by d.createdAt desc";
    public static final int TOKEN_EXPIRY_PERIOD = 3;//in months
    public static final int ROLE_TOKEN_EXPIRY_PERIOD = 30;//in seconds
    public static final String APP_VERSION_HEADER_KEY = "app-version";
}
