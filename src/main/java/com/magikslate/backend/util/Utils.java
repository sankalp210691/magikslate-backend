/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.util;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.magikslate.backend.entity.Class;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.Files;
import com.magikslate.backend.dto.StudentDto;
import com.magikslate.backend.entity.Attendance;
import com.magikslate.backend.entity.Student;
import com.magikslate.backend.entity.StudentClassMapping;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.vertx.core.json.JsonObject;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.regex.Pattern;
import javax.persistence.Parameter;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.query.Query;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author sankalpkulshrestha
 */
public class Utils {

    private static final Random random = new Random();

    private static final ImmutableMap<Object, Object> VALID_CLASS_TYPE_MAP = ImmutableMap.builder()
            .put("PRENURSERY", "PRE-NURSERY")
            .put("PRE NURSERY", "PRE-NURSERY")
            .put("PRE-NURSERY", "PRE-NURSERY")
            .put("NURSERY", "NURSERY")
            .put("UKG", "UKG")
            .put("LKG", "LKG")
            .put("FIRST", "FIRST")
            .put("I", "FIRST")
            .put(1, "FIRST")
            .put("1ST", "FIRST")
            .put("SECOND", "SECOND")
            .put("II", "SECOND")
            .put(2, "SECOND")
            .put("2ND", "SECOND")
            .put("THIRD", "THIRD")
            .put("III", "THIRD")
            .put(3, "THIRD")
            .put("3RD", "THIRD")
            .put("FOURTH", "FOURTH")
            .put("IV", "FOURTH")
            .put(4, "FOURTH")
            .put("4TH", "FOURTH")
            .put("FIFTH", "FIFTH")
            .put("V", "FIFTH")
            .put(5, "FIFTH")
            .put("5TH", "FIFTH")
            .put("SIXTH", "SIXTH")
            .put("VI", "SIXTH")
            .put(6, "SIXTH")
            .put("6TH", "SIXTH")
            .put("SEVENTH", "SEVENTH")
            .put("VII", "SEVENTH")
            .put(7, "SEVENTH")
            .put("7TH", "SEVENTH")
            .put("EIGHTH", "EIGHTH")
            .put("VIII", "EIGHTH")
            .put(8, "EIGHTH")
            .put("8TH", "EIGHTH")
            .put("NINTH", "NINTH")
            .put("IX", "NINTH")
            .put(9, "NINTH")
            .put("9TH", "NINTH")
            .put("TENTH", "TENTH")
            .put("X", "TENTH")
            .put(10, "TENTH")
            .put("10TH", "TENTH")
            .put("ELEVENTH", "ELEVENTH")
            .put("XI", "ELEVENTH")
            .put(11, "ELEVENTH")
            .put("11TH", "ELEVENTH")
            .put("TWELFTH", "TWELFTH")
            .put("XII", "TWELFTH")
            .put(12, "TWELFTH")
            .put("12TH", "TWELFTH")
            .build();

    private static final String EMAIL_REGEX_PATTERN = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";

    private static final String PHONE_REGEX_PATTERN = "^(\\+91[\\-\\s]?)?[0]?(91)?[789]\\d{9}$";

    public static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REGEX_PATTERN);

    public static final Pattern PHONE_PATTERN = Pattern.compile(PHONE_REGEX_PATTERN);

    public static String sanitizeClassStandard(String type) {
        if (NumberUtils.isNumber(type)) {
            int n = (int) Double.parseDouble(type);
            return (String) VALID_CLASS_TYPE_MAP.get(n);
        } else {
            return (String) VALID_CLASS_TYPE_MAP.get(type.toUpperCase().replace(".", ""));
        }
    }

    public static String sanitizeSectionType(String section) {
        return section == null ? null : section.toUpperCase();
    }

    public static boolean sanitizeCsvBoolean(String string) {
        return !(string == null || string.isEmpty() || string.toUpperCase().equals("NO") || string.toUpperCase().equals("FALSE"));
    }

    public static boolean isValidEmail(String email) {
        return email != null && !email.isEmpty() && EMAIL_PATTERN.matcher(email.toLowerCase()).matches();
    }

    //TODO: add 10 digit phon numerb check
    public static boolean isValidPhone(String phone) {
        return phone != null && !phone.isEmpty(); //PHONE_PATTERN.matcher(phone).matches(); 
    }

    public static File writeToDisk(MultipartFile file, String filename) throws IOException, InvalidFormatException {
        byte[] bytes = file.getBytes();
        File localFile = new File(filename);
        Files.write(bytes, localFile);
        return localFile;
    }

    public static Boolean isNullOrEmpty(String string) {
        return string == null || string.isEmpty();
    }

    public static String getClassCode(String standard, String section) {
        if (isNullOrEmpty(standard) || isNullOrEmpty(section)) {
            return null;
        }
        standard = standard.toUpperCase();
        section = section.toUpperCase();
        return standard + Constants.UNDERSCORE_SEPARATOR + section;
    }

    public static String getClassCode(Class clazz) {
        return getClassCode(clazz.getStandard(), clazz.getSection());
    }

    public static String getSubjectCode(String subject) {
        if (isNullOrEmpty(subject)) {
            return null;
        }
        return subject.replace(" ", "").toLowerCase();
    }

    public static JsonObject generateErrorJson(String error) {
        Map<String, Object> errorJsonMap = new HashMap();
        errorJsonMap.put("error", error);
        return new JsonObject(errorJsonMap);
    }

    public static Integer generateOtp(int otpLength) {
        if (otpLength > 6) {
            otpLength = 6;
        }
        if (otpLength < 1) {
            otpLength = 4;
        }
        int n = (int) Math.pow(10, otpLength - 1);
        return n + random.nextInt(9 * n);
    }

    public static String getFileType(MultipartFile file) {
        String lowerCaseFileName = file.getOriginalFilename().toLowerCase();
        return FilenameUtils.getExtension(lowerCaseFileName);
    }

    public static String generateRoleAuthToken(String role, Integer roleId, Integer userId, Integer schoolId, Integer studentId) {
        Date expiryDate = new Date();
        Calendar myCal = Calendar.getInstance();
        myCal.setTime(expiryDate);
        myCal.add(Calendar.SECOND, +Constants.ROLE_TOKEN_EXPIRY_PERIOD);
        expiryDate = myCal.getTime();

        String authToken = Jwts.builder()
                .claim("role", role.toUpperCase())
                .claim("roleId", roleId)
                .claim("userId", userId)
                .claim("schoolId", schoolId)
                .claim("studentId", studentId)
                .setExpiration(expiryDate)
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, Constants.AUTH_CONSTANT_STRING).compact();
        return authToken;
    }

    public static Map<String, Object> decipherRoleAuthToken(String authToken) {
        Map<String, Object> claimMap = new HashMap();
        Claims claims = null;
        try {
            claims = Jwts.parser().setSigningKey(Constants.AUTH_CONSTANT_STRING).parseClaimsJws(authToken).getBody();
        } catch (ExpiredJwtException ex) {
            claims = ex.getClaims();
        }
        claimMap.put("exp", claims.getExpiration());
        claimMap.put("role", claims.get("role", String.class).toUpperCase());
        claimMap.put("roleId", claims.get("roleId", Integer.class));
        claimMap.put("userId", claims.get("userId", Integer.class));
        claimMap.put("schoolId", claims.get("schoolId", Integer.class));
        claimMap.put("studentId", claims.get("studentId", Integer.class));
        return claimMap;
    }

    public static String getUserAuthToken(Integer userId) {
        Date expiryDate = new Date();
        Calendar myCal = Calendar.getInstance();
        myCal.setTime(expiryDate);
        myCal.add(Calendar.MONTH, +Constants.TOKEN_EXPIRY_PERIOD);
        expiryDate = myCal.getTime();

        String authToken = Jwts.builder()
                .claim("userId", userId)
                .setExpiration(expiryDate)
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, Constants.AUTH_CONSTANT_STRING).compact();//during update profile regenerate token???
        return authToken;
    }

    public static String getUserForgotPasswordToken(Integer userId) {
        Date expiryDate = new Date();
        Calendar myCal = Calendar.getInstance();
        myCal.setTime(expiryDate);
        myCal.add(Calendar.MONTH, +Constants.TOKEN_EXPIRY_PERIOD);
        expiryDate = myCal.getTime();

        String authToken = Jwts.builder()
                .claim("userId", userId)
                .claim("type", Constants.FORGOT_PASSWORD_TOKEN)
                .setExpiration(expiryDate)
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, Constants.AUTH_CONSTANT_STRING).compact();//during update profile regenerate token???
        return authToken;
    }

    public static Map<String, Object> decipherUserAuthToken(String authToken) throws ExpiredJwtException {
        final Claims claims = Jwts.parser().setSigningKey(Constants.AUTH_CONSTANT_STRING).parseClaimsJws(authToken).getBody();
        Map<String, Object> claimMap = new HashMap();
        claimMap.put("exp", claims.getExpiration());
        claimMap.put("userId", claims.get("userId", Integer.class));
        return claimMap;
    }

    public static Map<String, Object> decipherUserForgotPasswordToken(String authToken) throws ExpiredJwtException {
        final Claims claims = Jwts.parser().setSigningKey(Constants.AUTH_CONSTANT_STRING).parseClaimsJws(authToken).getBody();
        Map<String, Object> claimMap = new HashMap();
        claimMap.put("exp", claims.getExpiration());
        claimMap.put("userId", claims.get("userId", Integer.class));
        claimMap.put("type", claims.get("type", String.class));
        return claimMap;
    }

    public static Map<String, Object> listQueryParams(Query query) {
        if (query.getParameters() == null) {
            return new HashMap();
        } else {
            Map<String, Object> paramMap = new HashMap();
            for (Parameter parameter : query.getParameters()) {
                String key = parameter.getName();
                Object value = query.getParameterValue(key);
                paramMap.put(key, value);
            }
            return paramMap;
        }
    }

    public static String createPhoneNumber(String countryCode, String phone) {
        if (isNullOrEmpty(countryCode) || isNullOrEmpty(phone)) {
            return null;
        } else {
            return Constants.PLUS_SEPARATOR + countryCode + Constants.HYPHEN_SEPARATOR + phone;
        }
    }

    public static Double calculatePercAttendance(List<Attendance> attendanceList, Integer studentId) {
        
        if(attendanceList == null) {
            return 0.0;
        }

        StudentDto studentDto = new StudentDto();
        int totalDaysForStudent = 0;
        for (Attendance attendance : attendanceList) {
            StudentClassMapping scm = attendance.getStudentClassMapping();
            Student student = scm.getStudent();
            if (student.getId().equals(studentId)) {
                studentDto.setId(student.getId());
                studentDto.setName(student.getName());
                studentDto.setProfilePicUrl(student.getProfilePic());
                studentDto.setStudentClassMappingId(scm.getId());

                if (studentDto.getPercAttendance() == null) {
                    studentDto.setPercAttendance(0.0);
                }

                if (attendance.getPresent()) {
                    studentDto.setPercAttendance(studentDto.getPercAttendance() + 1);
                } else {
                    Set<Date> absentDates = studentDto.getAbsentDates();
                    if (absentDates == null) {
                        absentDates = new HashSet();
                        studentDto.setAbsentDates(absentDates);
                    }
                    absentDates.add(attendance.getCreatedAt());
                }
                totalDaysForStudent++;
            }
        }

        if (totalDaysForStudent == 0) {
            return 0.0;
        }

        return 100 * (studentDto.getPercAttendance() / (double) totalDaysForStudent);

    }

    public static String getClientIp(HttpServletRequest request) {

        String remoteAddr = "";

        if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
            }
        }

        return remoteAddr;
    }

}
