/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.util;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author sankalpkulshrestha
 */
public class AWSUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(AWSUtil.class);

    private final AmazonS3 s3client;

    public AWSUtil() {
        s3client = new AmazonS3Client(new ProfileCredentialsProvider());
    }

    public boolean uploadToS3(File file, String folder, String bucket) {

        if (file == null || bucket == null || folder == null) {
            LOGGER.error("Returning false. Either file or bucket not folder was null");
            return false;
        }

        LOGGER.debug("Uploading file = {} in bucket = {} on S3", file.getAbsoluteFile(), bucket);

        try {
            s3client.putObject(new PutObjectRequest(bucket, folder, file));
        } catch (AmazonServiceException ex) {
            LOGGER.error("Error Message: {} , HTTP Status Code: {}, AWS Error Code: {},"
                    + " Error Type: {} , Request ID: {}", ex.getMessage(), ex.getStatusCode(),
                    ex.getErrorCode(), ex.getErrorType(), ex.getRequestId());
            return false;
        } catch (AmazonClientException ex) {
            LOGGER.error("Error Message: " + ex.getMessage());
            return false;
        }
        return true;
    }
}
