/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend;

import com.magikslate.backend.onboarding.CsvReader;
import com.magikslate.backend.onboarding.OnboardingFileReaderFactory;
import com.magikslate.backend.util.AWSUtil;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.validator.AuthFilter;
import com.magikslate.backend.validator.GeneralFilter;
import com.magikslate.backend.validator.RoleFilter;
import java.io.File;
import java.io.IOException;
import java.util.Properties;
import org.apache.commons.io.FileUtils;
import org.hibernate.SessionFactory;
import org.modelmapper.ModelMapper;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 *
 * @author sankalpkulshrestha
 */
@Configuration
@EnableTransactionManagement
@PropertySources({
    @PropertySource("classpath:magikslate.properties")
    ,
    @PropertySource(value = "file:/disk1/magikslate.properties", ignoreResourceNotFound = true)
})
@ComponentScan({"com.magikslate.backend"})
public class ApplicationConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationConfiguration.class);

    @Autowired
    private Environment env;

    @Autowired
    private RoleFilter roleFilter;

    @Autowired
    private AuthFilter authFilter;

    @Autowired
    private GeneralFilter generalFilter;

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LOGGER.debug("Creating LocalSessionFactoryBean");
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setPackagesToScan(new String[]{"com.magikslate.backend.entity"});
        sessionFactory.setHibernateProperties(hibernateProperties());
        LOGGER.debug("LocalSessionFactoryBean created");
        return sessionFactory;
    }

    private Properties hibernateProperties() {
        LOGGER.debug("Setting hibernate properties");
        Properties properties = new Properties();

        properties.put("hibernate.default_schema", env.getRequiredProperty("hibernate.default_schema"));
        properties.put("hibernate.connection.driver_class", env.getRequiredProperty("hibernate.connection.driver_class"));
        properties.put("hibernate.connection.url", env.getRequiredProperty("hibernate.connection.url"));
        properties.put("hibernate.connection.username", env.getRequiredProperty("hibernate.connection.username"));
        properties.put("hibernate.connection.password", env.getRequiredProperty("hibernate.connection.password"));

        properties.put("hibernate.dialect", env.getRequiredProperty("hibernate.dialect"));
        properties.put("hibernate.show_sql", env.getRequiredProperty("hibernate.show_sql"));
        properties.put("hibernate.format_sql", env.getRequiredProperty("hibernate.format_sql"));
        properties.put("hibernate.jdbc.batch_size", env.getRequiredProperty("hibernate.jdbc.batch_size"));
        properties.put("hibernate.order_inserts", env.getRequiredProperty("hibernate.order_inserts"));
        properties.put("hibernate.order_updates", env.getRequiredProperty("hibernate.order_updates"));
        properties.put("hibernate.jdbc.batch_versioned_data", env.getRequiredProperty("hibernate.jdbc.batch_versioned_data"));

        properties.put("hibernate.connection.provider_class", "org.hibernate.c3p0.internal.C3P0ConnectionProvider");
        properties.put("hibernate.c3p0.min_size", env.getRequiredProperty("hibernate.c3p0.min_size"));
        properties.put("hibernate.c3p0.max_size", env.getRequiredProperty("hibernate.c3p0.max_size"));
        properties.put("hibernate.c3p0.timeout", env.getRequiredProperty("hibernate.c3p0.timeout"));
        properties.put("hibernate.c3p0.max_statements", env.getRequiredProperty("hibernate.c3p0.max_statements"));
        properties.put("hibernate.c3p0.idle_test_period", env.getRequiredProperty("hibernate.c3p0.idle_test_period"));

        LOGGER.debug("Hibernate properties set");
        return properties;
    }

    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory s) {
        LOGGER.debug("Setting up HibernateTransactionManager bean");
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(s);
        LOGGER.debug("HibernateTransactionManager bean set up");
        return txManager;
    }

    @Bean
    public RedissonClient redissonClient() {
        LOGGER.debug("Setting up RedissonClient bean");
        Config config = new Config();
        config.useSingleServer().setAddress(env.getRequiredProperty("redis.address"));
        LOGGER.debug("RedissonClient bean set up");
        return Redisson.create(config);
    }

    @Bean
    public RoleFilter roleFilter() {
        return new RoleFilter();
    }

    @Bean
    public AuthFilter authFilter() {
        return new AuthFilter();
    }

    @Bean
    public GeneralFilter generalFilter() {
        return new GeneralFilter();
    }

    @Bean
    public FilterRegistrationBean roleFilterRegisteration() {
        LOGGER.debug("Setting up roleFilterRegistrationBean bean");
        final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(roleFilter);
        registrationBean.setOrder(10);
        registrationBean.addUrlPatterns("/v1/secure/app/*");
        LOGGER.debug("roleFilterRegistrationBean bean set up");
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean authFilterRegisteration() {
        LOGGER.debug("Setting up authFilterRegistrationBean bean");
        final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(authFilter);
        registrationBean.setOrder(5);
        registrationBean.addUrlPatterns("/v1/secure/*");
        LOGGER.debug("authFilterRegistrationBean bean set up");
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean generalFilterRegisteration() {
        LOGGER.debug("Setting up generalFilterRegistrationBean bean");
        final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(generalFilter);
        registrationBean.setOrder(2);
        registrationBean.addUrlPatterns("/*");
        LOGGER.debug("generalFilterRegistrationBean bean set up");
        return registrationBean;
    }

    @Bean
    public CsvReader onboardingFileReader() {
        LOGGER.debug("Setting up CsvReader bean");
        return new CsvReader();
    }

    @Bean
    public OnboardingFileReaderFactory onboardingFileReaderFactory() {
        LOGGER.debug("Setting up OnboardingFileReaderFactory bean");
        return new OnboardingFileReaderFactory();
    }

    @Bean
    public AWSUtil awsUtil() {
        LOGGER.debug("Setting up AWSUtil bean");
        return new AWSUtil();
    }

    @Bean
    String faqJson() throws IOException {
        LOGGER.debug("Setting up FAQJson bean");
        return FileUtils.readFileToString(new File(Constants.FAQ_JSON_FILE_LOCATION));
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                String[] allowedOrigins = new String[env.getProperty("allowedOrigins") == null ? 1 : env.getProperty("allowedOrigins").split(Constants.COMMA_SEPARATOR).length];
                int index = 0;
                if (env.getProperty("allowedOrigins") != null) {
                    for (String allowedOrigin : env.getProperty("allowedOrigins").split(Constants.COMMA_SEPARATOR)) {
                        allowedOrigins[index] = allowedOrigin;
                        index++;
                    }
                } else {
                    allowedOrigins[index] = Constants.DEFAULT_ORIGIN;
                }
                registry.addMapping("/**")
                        .allowedOrigins(allowedOrigins)
                        .allowCredentials(true)
                        .allowedHeaders("*")
                        .allowedMethods("*")
                        .exposedHeaders("authorization", "fpauthorization", "roleauth", "Resource-Id", "location", "Location");
            }
        };
    }

}
