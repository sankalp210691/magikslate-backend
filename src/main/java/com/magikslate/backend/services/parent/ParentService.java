/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.parent;

import com.magikslate.backend.entity.Class;
import com.magikslate.backend.entity.Parent;
import com.magikslate.backend.feed.Feed;
import com.magikslate.backend.services.GenericService;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public interface ParentService extends GenericService<Parent, Integer> {

    public List<Integer> createBulk(List<Parent> parentList);

    public Feed getFeed(Integer userId, Integer studentId, Integer schoolId, Integer offset, String role, Integer roleId);

    public List<Parent> getParentsByClassList(List<Class> classList);

}
