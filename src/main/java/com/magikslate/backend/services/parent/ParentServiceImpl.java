/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.parent;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.dao.announcement.AnnouncementDao;
import com.magikslate.backend.dao.attendance.AttendanceDao;
import com.magikslate.backend.dao.content.ContentDao;
import com.magikslate.backend.dao.diary.DiaryDao;
import com.magikslate.backend.dao.event.EventDao;
import com.magikslate.backend.dao.parent.ParentDao;
import com.magikslate.backend.dao.student.StudentDao;
import com.magikslate.backend.dao.user.UserDao;
import com.magikslate.backend.entity.Announcement;
import com.magikslate.backend.entity.Attendance;
import com.magikslate.backend.entity.Class;
import com.magikslate.backend.entity.Content;
import com.magikslate.backend.entity.Diary;
import com.magikslate.backend.entity.Event;
import com.magikslate.backend.entity.Parent;
import com.magikslate.backend.entity.Student;
import com.magikslate.backend.entity.StudentClassMapping;
import com.magikslate.backend.entity.User;
import com.magikslate.backend.exception.InvalidOnboardingInputException;
import com.magikslate.backend.feed.Feed;
import com.magikslate.backend.services.GenericServiceImpl;
import com.magikslate.backend.util.Utils;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author sankalpkulshrestha
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ParentServiceImpl extends GenericServiceImpl<Parent, Integer>
        implements ParentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParentServiceImpl.class);
    
    private static final long LONG_FUTURE_UNIX_TIMESTAMP = 253402257599000l;
    private static final Date START_DATE = new Date(0);
    private static final Date END_DATE = new Date(LONG_FUTURE_UNIX_TIMESTAMP);//9999-12-31 11:59:59

    private UserDao userDao;
    private ParentDao parentDao;
    private EventDao eventDao;
    private AnnouncementDao announcementDao;
    private ContentDao contentDao;
    private DiaryDao diaryDao;
    private AttendanceDao attendanceDao;
    private StudentDao studentDao;

    @Autowired
    public ParentServiceImpl(@Qualifier("parentDaoImpl") GenericDao<Parent, Integer> parentDao,
            @Qualifier("userDaoImpl") GenericDao<User, Integer> userDao,
            @Qualifier("eventDaoImpl") GenericDao<Event, Integer> eventDao,
            @Qualifier("announcementDaoImpl") GenericDao<Announcement, Integer> announcementDao,
            @Qualifier("contentDaoImpl") GenericDao<Content, Integer> contentDao,
            @Qualifier("diaryDaoImpl") GenericDao<Diary, Integer> diaryDao,
            @Qualifier("attendanceDaoImpl") GenericDao<Attendance, Integer> attendanceDao,
            @Qualifier("studentDaoImpl") GenericDao<Student, Integer> studentDao) {

        super(parentDao);
        this.parentDao = (ParentDao) parentDao;
        this.userDao = (UserDao) userDao;
        this.eventDao = (EventDao) eventDao;
        this.diaryDao = (DiaryDao) diaryDao;
        this.announcementDao = (AnnouncementDao) announcementDao;
        this.contentDao = (ContentDao) contentDao;
        this.studentDao = (StudentDao) studentDao;
        this.attendanceDao = (AttendanceDao) attendanceDao;
    }

    @Override
    public List<Integer> createBulk(List<Parent> parentList) {

        LOGGER.debug("parentList = {}", parentList);

        List<String> phoneList = new ArrayList();
        List<String> emailList = new ArrayList();

        //collect all the phones and emails of all parents
        parentList.stream().map((p) -> {
            if (!Utils.isNullOrEmpty(p.getUser().getPhone())) {
                phoneList.add(p.getUser().getPhone());
            }
            return p;
        }).filter((p) -> (!Utils.isNullOrEmpty(p.getUser().getEmail()))).forEachOrdered((p) -> {
            emailList.add(p.getUser().getEmail());
        });

        LOGGER.debug("phoneList = {}, emailList = {}", phoneList, emailList);

        //get existing users
        List<User> existingUserList = userDao.getUserListByPhoneOrEmail(phoneList, emailList, true, true);
        LOGGER.debug("existingUserList = {}", existingUserList);

        //get existing user phone map
        Map<String, User> existingUserPhoneMap = new HashMap();
        Map<String, User> existingUserEmailMap = new HashMap();

        existingUserList.stream().map((existingUser) -> {
            existingUserPhoneMap.put(existingUser.getPhone(), existingUser);
            return existingUser;
        }).forEachOrdered((existingUser) -> {
            existingUserEmailMap.put(existingUser.getEmail(), existingUser);
        });

        List<Integer> parentIdList = new ArrayList();

        Map<String, Parent> existingPhoneParentMap = new HashMap();

        existingUserList.stream().map((user) -> user.getParentList())
                .filter((existingParentList) -> (existingParentList != null && !existingParentList.isEmpty()))
                .forEachOrdered((existingParentList) -> {

                    existingParentList.forEach((parent) -> {

                        parentIdList.add(parent.getId());
                        existingPhoneParentMap.put(parent.getUser().getPhone(), parent);
                    });
                });

        List<User> newUserList = new ArrayList();
        List<Parent> newParentList = new ArrayList();

        handleConflictsByThrowingError(parentList, existingUserPhoneMap, existingUserEmailMap, existingPhoneParentMap, newUserList, newParentList);

        List<Integer> newUserIdList = userDao.createBulk(newUserList);
        LOGGER.debug("newUserIdList = {}", newUserIdList);
        int index = 0;
        for (User user : newUserList) {
            user.setId(newUserIdList.get(index));
            index++;
        }

        parentIdList.addAll(parentDao.createBulk(newParentList));
        LOGGER.debug("Returning parentIdList = {}", parentIdList);
        return parentIdList;
    }

    @Override
    public Feed getFeed(Integer userId, Integer studentId, Integer schoolId, Integer offset, String role, Integer roleId) {
        Date startDate = START_DATE;
        Date endDate = END_DATE;

        List<List<Event>> eventList = eventDao.getAll(userId, role, roleId, schoolId, startDate, endDate, null, null, offset, false, true);
        List<List<Announcement>> announcementList = announcementDao.getAll(null, role, roleId, schoolId, startDate, endDate, offset, false, true);
        List<Diary> diaryList = diaryDao.getDiary(role, roleId, schoolId, studentId, null, null, null, offset, startDate, endDate);
        
        List<Attendance> attendanceList = null;
        StudentClassMapping currentscm = studentDao.getCurrentStudentClassMapping(studentId);
        if (currentscm != null) {
            attendanceList = attendanceDao.getAttendanceByStudentIdentifier(currentscm.getId(), true, startDate, endDate, offset);
        }
        
        List<Content> contentList = contentDao.getContent(role, roleId, schoolId, null, studentId, null, null, startDate, endDate, offset);

        Feed feed = new Feed(eventList.get(1), announcementList.get(1), diaryList, attendanceList, contentList);

        return feed;
    }

    @Override
    public List<Parent> getParentsByClassList(List<Class> classList) {
        return parentDao.getParentsByClassList(classList.stream().map(clazz -> clazz.getId()).collect(Collectors.toList()));
    }

    @Deprecated
    private void handleConflictsByMerging(List<Parent> parentList, Map<String, User> existingUserPhoneMap,
            Map<String, User> existingUserEmailMap, Map<String, Parent> existingPhoneParentMap,
            List<User> newUserList, List<Parent> newParentList) {
        
        parentList.forEach(parent -> {
            if (!existingUserPhoneMap.containsKey(parent.getUser().getPhone()) && ((parent.getUser().getEmail() != null && !existingUserEmailMap.containsKey(parent.getUser().getEmail())) || parent.getUser().getEmail() == null)) {

                LOGGER.debug("Adding new user = {}", parent.getUser());
                newUserList.add(parent.getUser());

                LOGGER.debug("Adding new parent = {}", parent);
                newParentList.add(parent);
            } else if (!existingUserPhoneMap.containsKey(parent.getUser().getPhone()) && (parent.getUser().getEmail() != null && existingUserEmailMap.containsKey(parent.getUser().getEmail()))) {

                LOGGER.debug("User is already there as user = {} but different phone. So updating phone number", existingUserEmailMap.get(parent.getUser().getEmail()));
                existingUserEmailMap.get(parent.getUser().getEmail()).setPhone(parent.getUser().getPhone());

                //User already there. Just checking if user is parent or not
                if (existingPhoneParentMap.containsKey(parent.getUser().getPhone())) {
                    LOGGER.debug("User is already a parent. Do nothing");
                } else {
                    LOGGER.debug("User is not parent. Adding new parent = {}", parent);
                    parent.setUser(existingUserEmailMap.get(parent.getUser().getEmail()));
                    newParentList.add(parent);
                }

            } else if (existingUserPhoneMap.containsKey(parent.getUser().getPhone()) && ((parent.getUser().getEmail() != null && !existingUserEmailMap.containsKey(parent.getUser().getEmail())) || parent.getUser().getEmail() == null)) {

                if (parent.getUser().getEmail() != null) {
                    LOGGER.debug("User is already there as user = {} but different email. So updating email address", existingUserPhoneMap.get(parent.getUser().getPhone()));
                    existingUserPhoneMap.get(parent.getUser().getPhone()).setEmail(parent.getUser().getEmail());
                } else {
                    LOGGER.debug("User is already there as user = {} but different email. But new email address is null. So not updating the email.");
                }

                //User already there. Just checking if user is parent or not
                if (existingPhoneParentMap.containsKey(parent.getUser().getPhone())) {
                    LOGGER.debug("User is already a parent. Do nothing");
                } else {
                    LOGGER.debug("User is not parent. Adding new parent = {}", parent);
                    parent.setUser(existingUserPhoneMap.get(parent.getUser().getPhone()));
                    newParentList.add(parent);
                }

            } else {
                LOGGER.debug("User is already there as user = {}", existingUserPhoneMap.get(parent.getUser().getPhone()));
                //User already there. Just checking if user is parent or not
                if (existingPhoneParentMap.containsKey(parent.getUser().getPhone())) {
                    LOGGER.debug("User is already a parent. Do nothing");
                } else {
                    LOGGER.debug("User is not parent. Adding new parent = {}", parent);
                    parent.setUser(existingUserPhoneMap.get(parent.getUser().getPhone()));
                    newParentList.add(parent);
                }
            }
        });
    }

    private void handleConflictsByThrowingError(List<Parent> parentList, Map<String, User> existingUserPhoneMap,
            Map<String, User> existingUserEmailMap, Map<String, Parent> existingPhoneParentMap,
            List<User> newUserList, List<Parent> newParentList) {
        
        parentList.forEach(parent -> {
            if (!existingUserPhoneMap.containsKey(parent.getUser().getPhone()) && ((parent.getUser().getEmail() != null && !existingUserEmailMap.containsKey(parent.getUser().getEmail())) || parent.getUser().getEmail() == null)) {

                LOGGER.debug("Adding new user = {}", parent.getUser());
                newUserList.add(parent.getUser());

                LOGGER.debug("Adding new parent = {}", parent);
                newParentList.add(parent);
            } else if (!existingUserPhoneMap.containsKey(parent.getUser().getPhone()) && (parent.getUser().getEmail() != null && existingUserEmailMap.containsKey(parent.getUser().getEmail()))) {

                LOGGER.debug("User is already there as user = {} but different phone. So throwing error.", existingUserEmailMap.get(parent.getUser().getEmail()));
                throw new InvalidOnboardingInputException("We already have a user with E-mail address " + parent.getUser().getEmail()
                        + " but a different phone number. Please contact this person to update "
                        + "their phone number and try again later.");

            } else if (existingUserPhoneMap.containsKey(parent.getUser().getPhone()) && ((parent.getUser().getEmail() != null && !existingUserEmailMap.containsKey(parent.getUser().getEmail())) || parent.getUser().getEmail() == null)) {

                if (parent.getUser().getEmail() != null) {
                    LOGGER.debug("User is already there as user = {} but different email. So throwing error", existingUserPhoneMap.get(parent.getUser().getPhone()));
                    throw new InvalidOnboardingInputException("We already have a user with phone number " + parent.getUser().getPhone()
                        + " but a different E-mail address. Please contact this person to update "
                        + "their E-mail address and try again later.");
                } else {
                    LOGGER.debug("User is already there as user = {} but different email. But new email address is null. So not updating the email.", existingUserPhoneMap.get(parent.getUser().getPhone()));
                }

                //User already there. Just checking if user is parent or not
                if (existingPhoneParentMap.containsKey(parent.getUser().getPhone())) {
                    LOGGER.debug("User is already a parent. Do nothing");
                } else {
                    LOGGER.debug("User is not parent. Adding new parent = {}", parent);
                    parent.setUser(existingUserPhoneMap.get(parent.getUser().getPhone()));
                    newParentList.add(parent);
                }

            } else {
                LOGGER.debug("User is already there as user = {}", existingUserPhoneMap.get(parent.getUser().getPhone()));
                //User already there. Just checking if user is parent or not
                if (existingPhoneParentMap.containsKey(parent.getUser().getPhone())) {
                    LOGGER.debug("User is already a parent. Do nothing");
                } else {
                    LOGGER.debug("User is not parent. Adding new parent = {}", parent);
                    parent.setUser(existingUserPhoneMap.get(parent.getUser().getPhone()));
                    newParentList.add(parent);
                }
            }
        });
    }

}
