/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.board;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.dao.board.BoardDao;
import com.magikslate.backend.entity.Board;
import com.magikslate.backend.services.GenericServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author sankalpkulshrestha
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class BoardServiceImpl extends GenericServiceImpl<Board, Integer>
        implements BoardService {

    private BoardDao boardDao;

    @Autowired
    public BoardServiceImpl(@Qualifier("boardDaoImpl") GenericDao<Board, Integer> boardDao) {
        super(boardDao);
        this.boardDao = (BoardDao) boardDao;
    }
}
