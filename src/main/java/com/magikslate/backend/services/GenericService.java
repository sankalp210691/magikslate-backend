/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services;

import com.magikslate.backend.exception.EntityExistsException;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 * @param <E>
 * @param <K>
 */
public interface GenericService<E, K>{

    public void saveOrUpdate(E entity);

    public List<E> getAll();

    public E get(K id);
    
    public List<E> get(List<K> idList);

    public K add(E entity) throws EntityExistsException;

    public void update(E entity);

    public void remove(E entity);
}
