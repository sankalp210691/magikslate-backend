/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.content;

import com.magikslate.backend.entity.Attachment;
import com.magikslate.backend.entity.Content;
import com.magikslate.backend.entity.Note;
import com.magikslate.backend.entity.UrlContent;
import com.magikslate.backend.services.GenericService;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author sankalpkulshrestha
 */
public interface ContentService extends GenericService<Content, Integer> {
    
    @Override
    public Content get(Integer contentId);

    public Integer add(UrlContent urlContent);

    public Integer add(Attachment attachment);

    public Integer add(Note note);

    public List<Content> getContent(String role, Integer roleId, Integer schoolId,
            Integer subjectId, Integer studentId, Integer classId,
            Integer studyGroupId, Date startDate, Date endDate, Integer offset);
    
    public Map<String, Integer> getStats(Date startDate, Date endDate, Integer teacherId,
                Integer[] classId, Integer[] studyGroupId, Integer schoolId);
}
