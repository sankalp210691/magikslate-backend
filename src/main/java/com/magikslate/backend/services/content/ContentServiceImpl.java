/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.content;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.dao.content.ContentDao;
import com.magikslate.backend.entity.Attachment;
import com.magikslate.backend.entity.Content;
import com.magikslate.backend.entity.Note;
import com.magikslate.backend.entity.UrlContent;
import com.magikslate.backend.rest.ContentController;
import com.magikslate.backend.services.GenericServiceImpl;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author sankalpkulshrestha
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ContentServiceImpl extends GenericServiceImpl<Content, Integer>
        implements ContentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContentServiceImpl.class);

    private ContentDao contentDao;

    @Autowired
    public ContentServiceImpl(@Qualifier("contentDaoImpl") GenericDao<Content, Integer> contentDao) {
        super(contentDao);
        this.contentDao = (ContentDao) contentDao;
    }

    @Override
    public Content get(Integer contentId) {

        Content content = contentDao.find(contentId);
        LOGGER.debug("content with id = {} found. content = {}", contentId, content);

        if (content == null) {
            return null;
        }

        if (content.getSubjectClassTeacherMappingList() != null && !content.getSubjectClassTeacherMappingList().isEmpty()) {
            content.getSubjectClassTeacherMappingList().forEach(sctm -> {
                sctm.getSubject();
            });
        }
        if (content.getStudyGroupTeacherMappingList() != null && !content.getStudyGroupTeacherMappingList().isEmpty()) {
            content.getStudyGroupTeacherMappingList().forEach(sgtm -> {
                sgtm.getStudyGroup().getSubject();
            });
        }
        return content;
    }

    @Override
    public Integer add(UrlContent urlContent) {
        Content content = urlContent.getContent();
        if (isAllowedToCreateContent(content)) {
            Integer id = contentDao.add(content);
            LOGGER.debug("content created with id = {}", id);
            urlContent.setContentId(id);
            
            LOGGER.debug("urlContent added");
            contentDao.add(urlContent);
            return id;
        } else {
            LOGGER.debug("isAllowedToCreateContent = false, hence returning null");
            return null;
        }
    }

    @Override
    public Integer add(Attachment attachment) {
        Content content = attachment.getContent();
        if (isAllowedToCreateContent(content)) {
            Integer id = contentDao.add(content);
            LOGGER.debug("content created with id = {}", id);
            attachment.setContentId(id);
            
            LOGGER.debug("attachment added");
            contentDao.add(attachment);
            return id;
        } else {
            LOGGER.debug("isAllowedToCreateContent = false, hence returning null");
            return null;
        }
    }

    @Override
    public Integer add(Note note) {
        Content content = note.getContent();
        if (isAllowedToCreateContent(content)) {
            Integer id = contentDao.add(content);
            LOGGER.debug("content created with id = {}", id);
            note.setContentId(id);
            
            LOGGER.debug("note added");
            contentDao.add(note);
            return id;
        } else {
            LOGGER.debug("isAllowedToCreateContent = false, hence returning null");
            return null;
        }
    }

    @Override
    public List<Content> getContent(String role, Integer roleId, Integer schoolId,
            Integer subjectId, Integer studentId, Integer classId,
            Integer studyGroupId, Date startDate, Date endDate, Integer offset) {

        return contentDao.getContent(role, roleId, schoolId, subjectId,
                studentId, classId, studyGroupId, startDate, endDate, offset);
    }

    private boolean isAllowedToCreateContent(Content content) {
        return contentDao.isAllowedToCreateContent(content);
    }

    @Override
    public Map<String, Integer> getStats(Date startDate, Date endDate, Integer teacherId, Integer[] classId, Integer[] studyGroupId, Integer schoolId) {
        return contentDao.getStats(startDate, endDate, teacherId, classId, studyGroupId, schoolId);
    }

}
