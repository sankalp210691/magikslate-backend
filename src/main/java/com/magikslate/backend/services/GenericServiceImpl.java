/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services;

import com.magikslate.backend.dao.GenericDao;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author sankalpkulshrestha
 * @param <E>
 * @param <K>
 */
@Transactional(rollbackFor = Exception.class)
@Service
public abstract class GenericServiceImpl<E, K>
        implements GenericService<E, K> {

    private GenericDao<E, K> genericDao;

    public GenericServiceImpl(GenericDao<E, K> genericDao) {
        this.genericDao = genericDao;
    }

    public GenericServiceImpl() {
    }

    @Override
    public void saveOrUpdate(E entity) {
        genericDao.saveOrUpdate(entity);
    }

    @Override
    public List<E> getAll() {
        return genericDao.getAll();
    }

    @Override
    public E get(K id) {
        return genericDao.find(id);
    }
    
    @Override
    public List<E> get(List<K> idList) {
        return genericDao.find(idList);
    }

    @Override
    public K add(E entity){
        return genericDao.add(entity);
    }

    @Override
    public void update(E entity) {
        genericDao.update(entity);
    }

    @Override
    public void remove(E entity) {
        genericDao.remove(entity);
    }
}
