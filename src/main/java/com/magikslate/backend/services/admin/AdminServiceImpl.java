/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.admin;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.dao.admin.AdminDao;
import com.magikslate.backend.dao.announcement.AnnouncementDao;
import com.magikslate.backend.dao.content.ContentDao;
import com.magikslate.backend.dao.diary.DiaryDao;
import com.magikslate.backend.dao.event.EventDao;
import com.magikslate.backend.dao.user.UserDao;
import com.magikslate.backend.entity.Admin;
import com.magikslate.backend.entity.Announcement;
import com.magikslate.backend.entity.Content;
import com.magikslate.backend.entity.Diary;
import com.magikslate.backend.entity.Event;
import com.magikslate.backend.entity.User;
import com.magikslate.backend.feed.Feed;
import com.magikslate.backend.services.GenericServiceImpl;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author sankalpkulshrestha
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AdminServiceImpl extends GenericServiceImpl<Admin, Integer>
        implements AdminService {

    private UserDao userDao;
    private AdminDao adminDao;
    private EventDao eventDao;
    private AnnouncementDao announcementDao;
    private ContentDao contentDao;
    private DiaryDao diaryDao;
    
    private static final long LONG_FUTURE_UNIX_TIMESTAMP = 253402257599000l;
    private static final Date START_DATE = new Date(0);
    private static final Date END_DATE = new Date(LONG_FUTURE_UNIX_TIMESTAMP);//9999-12-31 11:59:59

    @Autowired
    public AdminServiceImpl(@Qualifier("adminDaoImpl") GenericDao<Admin, Integer> adminDao,
             @Qualifier("eventDaoImpl") GenericDao<Event, Integer> eventDao,
            @Qualifier("announcementDaoImpl") GenericDao<Announcement, Integer> announcementDao,
            @Qualifier("contentDaoImpl") GenericDao<Content, Integer> contentDao,
            @Qualifier("diaryDaoImpl") GenericDao<Diary, Integer> diaryDao,
            @Qualifier("userDaoImpl") GenericDao<User, Integer> userDao) {
        super(adminDao);
        this.adminDao = (AdminDao) adminDao;
        this.userDao = (UserDao) userDao;
        this.eventDao = (EventDao) eventDao;
        this.announcementDao = (AnnouncementDao) announcementDao;
        this.diaryDao = (DiaryDao) diaryDao;
        this.contentDao = (ContentDao) contentDao;
    }

    @Override
    public User getUserByAdminId(int adminId) {
        return adminDao.getUserByAdminId(adminId);
    }

    @Override
    public Feed getFeed(Integer userId, Integer schoolId, Integer offset, String role, Integer roleId) {
        
        Date startDate = START_DATE;
        Date endDate = END_DATE;
        
        List<List<Event>> eventList = eventDao.getAll(userId, role, roleId, schoolId, startDate, endDate, null, null, offset, false, true);
        List<List<Announcement>> announcementList = announcementDao.getAll(null, role, roleId, schoolId, startDate, endDate, offset, false, true);
        List<Diary> diaryList = diaryDao.getDiary(role, roleId, schoolId, null, null, null, null, offset, startDate, endDate);
        
        List<Content> contentList = contentDao.getContent(role, roleId, schoolId, null, null, null, null, startDate, endDate, offset);

        Feed feed = new Feed(eventList.get(1), announcementList.get(1), diaryList, new ArrayList(), contentList);

        return feed;
    }

}
