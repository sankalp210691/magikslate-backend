/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.admin;

import com.magikslate.backend.entity.Admin;
import com.magikslate.backend.entity.User;
import com.magikslate.backend.feed.Feed;
import com.magikslate.backend.services.GenericService;

/**
 *
 * @author sankalpkulshrestha
 */
public interface AdminService extends GenericService<Admin, Integer> {

    public User getUserByAdminId(int adminId);

    public Feed getFeed(Integer userId, Integer schoolId, Integer offset, String role, Integer roleId);

}
