/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.clazz;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.dao.attendance.AttendanceDao;
import com.magikslate.backend.dao.clazz.ClassDao;
import com.magikslate.backend.dao.diary.DiaryDao;
import com.magikslate.backend.dao.school.SchoolDao;
import com.magikslate.backend.entity.Attendance;
import com.magikslate.backend.entity.Class;
import com.magikslate.backend.entity.Diary;
import com.magikslate.backend.entity.School;
import com.magikslate.backend.entity.SubjectClassTeacherMapping;
import com.magikslate.backend.services.GenericServiceImpl;
import com.magikslate.backend.util.Constants;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author sankalpkulshrestha
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ClassServiceImpl extends GenericServiceImpl<Class, Integer>
        implements ClassService {

    private ClassDao classDao;
    private AttendanceDao attendanceDao;
    private DiaryDao diaryDao;
    private SchoolDao schoolDao;

    @Autowired
    public ClassServiceImpl(@Qualifier("classDaoImpl") GenericDao<Class, Integer> classDao,
            @Qualifier("attendanceDaoImpl") GenericDao<Attendance, Integer> attendanceDao,
            @Qualifier("diaryDaoImpl") GenericDao<Diary, Integer> diaryDao,
            @Qualifier("schoolDaoImpl") GenericDao<School, Integer> schoolDao) {
        
        super(classDao);
        this.classDao = (ClassDao) classDao;
        this.attendanceDao = (AttendanceDao) attendanceDao;
        this.diaryDao = (DiaryDao) diaryDao;
        this.schoolDao = (SchoolDao) schoolDao;
    }

    @Override
    public List<Integer> createBulk(List<Class> classList) {
        if (classList.isEmpty()) {
            return new ArrayList();
        }
        List<Class> existingClassList = classDao.getClassListBySchool(classList.get(0).getSchool().getId(), null);
        Set existingClassIdSet = existingClassList.stream().map(clazz
                -> clazz.getStandard() + Constants.UNDERSCORE_SEPARATOR + clazz
                .getSection()).collect(Collectors.toCollection(HashSet::new));
        List<Class> finalClassList = new ArrayList();
        classList.forEach(clazz -> {
            if (!existingClassIdSet.contains(clazz.getStandard() + Constants.UNDERSCORE_SEPARATOR + clazz.getSection())) {
                finalClassList.add(clazz);
            }
        });
        List<Integer> idList = classDao.createBulk(finalClassList);
        classList.get(0).getSchool().setUpdatedAt(new Date());
        if (schoolDao.updateStage(2, classList.get(0).getSchool().getId())) {
            return idList;
        } else {
            return new ArrayList();
        }
    }

    @Override
    public List<Class> getClassListBySchool(Integer schoolId, Integer offset) {
        return classDao.getClassListBySchool(schoolId, offset);
    }

    @Override
    public List<Object[]> getClassListByTeacher(Integer teacherId, Integer offset) {
        return classDao.getClassListByTeacher(teacherId, offset);
    }

    @Override
    public Integer createDiaryEntry(Diary diary) {
        return diaryDao.add(diary);
    }

    @Override
    public List<Diary> listDiaryByClass(Integer classId, Integer offset) {
        return diaryDao.getByClass(classId, offset);
    }

    @Override
    public SubjectClassTeacherMapping getSubjectClassTeacherMapping(Integer sctm) {
        return classDao.getSubjectClassTeacherMapping(sctm);
    }

}
