/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.clazz;

import com.magikslate.backend.entity.Class;
import com.magikslate.backend.entity.Diary;
import com.magikslate.backend.entity.SubjectClassTeacherMapping;
import com.magikslate.backend.services.GenericService;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public interface ClassService extends GenericService<Class, Integer> {

    public List<Integer> createBulk(List<Class> classList);

    public List<Class> getClassListBySchool(Integer schoolId, Integer offset);

    public List<Object[]> getClassListByTeacher(Integer teacherId, Integer offset);

    public Integer createDiaryEntry(Diary diary);

    public List<Diary> listDiaryByClass(Integer classId, Integer offset);

    public SubjectClassTeacherMapping getSubjectClassTeacherMapping(Integer sctm);

}
