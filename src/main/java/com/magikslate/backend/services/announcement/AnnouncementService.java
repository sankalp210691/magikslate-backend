/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.announcement;

import com.magikslate.backend.entity.Announcement;
import com.magikslate.backend.services.GenericService;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public interface AnnouncementService extends GenericService<Announcement, Integer> {

    public Announcement get(Integer announcementId, Integer schoolId);
    
    public List<List<Announcement>> getAll(Integer userId, String role, Integer roleId,
            Integer schoolId, Date startDate, Date endDate, Integer offset,
            boolean isSelfNeeded, boolean isReceivedNeeded);
}
