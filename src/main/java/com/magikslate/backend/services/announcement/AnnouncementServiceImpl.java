/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.announcement;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.dao.announcement.AnnouncementDao;
import com.magikslate.backend.entity.Announcement;
import com.magikslate.backend.services.GenericServiceImpl;
import java.util.Date;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 *
 * @author sankalpkulshrestha
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AnnouncementServiceImpl extends GenericServiceImpl<Announcement, Integer>
        implements AnnouncementService {

    private AnnouncementDao announcementDao;

    public AnnouncementServiceImpl() {

    }

    @Override
    public Announcement get(Integer announcementId, Integer schoolId) {
        Announcement announcement = announcementDao.find(announcementId);
        if (announcement != null && announcement.getAdmin().getSchool().getId().equals(schoolId)) {
            return announcement;
        }
        return null;
    }

    @Autowired
    public AnnouncementServiceImpl(@Qualifier("announcementDaoImpl") GenericDao<Announcement, Integer> announcementDao) {
        super(announcementDao);
        this.announcementDao = (AnnouncementDao) announcementDao;
    }

    @Override
    public Integer add(Announcement announcement) {
        return announcementDao.add(announcement);
    }

    @Override
    public List<List<Announcement>> getAll(Integer userId, String role, Integer roleId,
            Integer schoolId, Date startDate, Date endDate, Integer offset,
            boolean isSelfNeeded, boolean isReceivedNeeded) {

        return announcementDao.getAll(userId, role, roleId, schoolId, startDate, endDate, offset, isSelfNeeded, isReceivedNeeded);
    }

}
