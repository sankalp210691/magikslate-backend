/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.user;

import com.magikslate.backend.entity.User;
import com.magikslate.backend.services.GenericService;


/**
 *
 * @author sankalpkulshrestha
 */
public interface UserService extends GenericService<User,Integer> {

    public String[] authenticate(User user);

    public boolean verifyOtpAndActivate(User user);

    public User getRolewiseSchools(Integer userId);

    public String hasRoleAccess(String role, Integer userId, Integer roleId, Integer schoolId, Integer studentId);

    public boolean changePassword(Integer userId, String oldPassword, String newPassword, Boolean isOldPasswordNeeded);

    public boolean forgotPassword(String phoneNumber);

    public Integer forgotPassword(String phone, Integer otp);

}
