/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.user;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.dao.admin.AdminDao;
import com.magikslate.backend.dao.parent.ParentDao;
import com.magikslate.backend.dao.teacher.TeacherDao;
import com.magikslate.backend.dao.user.UserDao;
import com.magikslate.backend.entity.Admin;
import com.magikslate.backend.entity.Board;
import com.magikslate.backend.entity.Parent;
import com.magikslate.backend.entity.School;
import com.magikslate.backend.entity.State;
import com.magikslate.backend.entity.Student;
import com.magikslate.backend.entity.StudentClassMapping;
import com.magikslate.backend.entity.StudentParentMapping;
import com.magikslate.backend.entity.Teacher;
import com.magikslate.backend.entity.User;
import com.magikslate.backend.exception.AuthenticationException;
import com.magikslate.backend.exception.EntityExistsException;
import com.magikslate.backend.services.GenericServiceImpl;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 *
 * @author sankalpkulshrestha
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserServiceImpl extends GenericServiceImpl<User, Integer>
        implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    private UserDao userDao;
    private TeacherDao teacherDao;
    private ParentDao parentDao;
    private AdminDao adminDao;

    public UserServiceImpl() {

    }

    @Autowired
    public UserServiceImpl(@Qualifier("userDaoImpl") GenericDao<User, Integer> userDao,
            @Qualifier("teacherDaoImpl") GenericDao<Teacher, Integer> teacherDao,
            @Qualifier("parentDaoImpl") GenericDao<Parent, Integer> parentDao,
            @Qualifier("adminDaoImpl") GenericDao<Admin, Integer> adminDao) {

        super(userDao);
        this.userDao = (UserDao) userDao;
        this.teacherDao = (TeacherDao) teacherDao;
        this.parentDao = (ParentDao) parentDao;
        this.adminDao = (AdminDao) adminDao;
    }

    @Override
    public Integer add(User user) throws EntityExistsException {
        LOGGER.debug("email = {} , phone = {}", user.getEmail(), user.getPhone());
        if (userDao.isDistinctUser(user.getEmail(), user.getPhone())) {
            LOGGER.debug("New user");
            user.setPassword(DigestUtils.sha256Hex(user.getPassword()));
            user.setOtp(Utils.generateOtp(Constants.OTP_LENGTH));
            user.setUserCreatedActively(true);
            user.setCreatedAt(new Date());
            Integer userId = userDao.add(user);
            LOGGER.debug("User created with Id = {}", userId);
            return userId;
        } else {
            LOGGER.info("User already exists");
            throw new EntityExistsException();
        }
    }

    @Override
    public String[] authenticate(User user) {
        user.setPassword(DigestUtils.sha256Hex(user.getPassword()));
        User loggedinUser = userDao.getUserByPhoneEmailAndPassword(user.getPhone(), user.getEmail(), user.getPassword());
        if (loggedinUser == null) {
            LOGGER.info("loggedinUser is NULL. Wrong Phone/Email/Password maybe?");
            throw new AuthenticationException();
        } else {
            LOGGER.debug("loggedinUser = {}", loggedinUser);
            String userStatus = loggedinUser.getStatus();
            String authToken = Utils.getUserAuthToken(loggedinUser.getId());
            String[] result = new String[3];
            result[0] = userStatus;
            result[1] = authToken;
            result[2] = String.valueOf(loggedinUser.getId());
            LOGGER.debug("Returning status = {} with token = {} for userId = {}", result[0], result[1], result[2]);
            return result;
        }
    }

    @Override
    public boolean verifyOtpAndActivate(User user) {
        return userDao.verifyOtpAndActivate(user);
    }

    @Override
    public User getRolewiseSchools(Integer userId) {
        User user = userDao.find(userId);
        LOGGER.debug("user = {}", user);

        if (user == null) {
            return null;
        }

        List<Admin> adminList = user.getAdminList();
        if (adminList != null) {
            adminList.stream().map((admin) -> admin.getSchool()).forEachOrdered((school) -> {
                school.toString();
            });
        }

        List<Teacher> teacherList = user.getTeacherList();
        if (teacherList != null) {
            teacherList.stream().map((teacher) -> teacher.getSchool()).forEachOrdered((school) -> {
                school.toString();
            });
        }

        List<Parent> parentList = user.getParentList();
        if (parentList != null) {
            parentList.stream().map((parent) -> parent.getStudentParentMappingList()).filter((studentParentMappingList) -> (studentParentMappingList != null)).forEachOrdered((studentParentMappingList) -> {
                studentParentMappingList.stream().map((spm) -> spm.getStudent()).forEachOrdered((student) -> {
                    student.toString();
                    List<StudentClassMapping> studentClassMappingList = student.getStudentClassMappingList();
                    studentClassMappingList.forEach((scm) -> {
                        scm.getClass1().toString();
                    });
                });
            });
        }
        return user;
    }

    @Override
    public String hasRoleAccess(String role, Integer userId, Integer roleId, Integer schoolId, Integer studentId) {
        LOGGER.debug("role = {}", role);
        switch (role.toUpperCase()) {
            case Constants.ADMIN:
                Integer adminId = adminDao.getAdminAccessId(userId, schoolId);
                LOGGER.debug("adminId = {}", adminId);
                if (roleId.equals(adminId)) {
                    return Utils.generateRoleAuthToken(role, roleId, userId, schoolId, studentId);
                }
                break;
            case Constants.TEACHER:
                Integer teacherId = teacherDao.getTeacherAccessId(userId, schoolId);
                LOGGER.debug("teacherId = {}", teacherId);
                if (roleId.equals(teacherId)) {
                    return Utils.generateRoleAuthToken(role, roleId, userId, schoolId, studentId);
                }
                break;
            case Constants.PARENT:
                if (studentId == null) {
                    LOGGER.info("studentId is null hence, returning");
                    return null;
                }
                Integer parentId = parentDao.getParentAccessId(userId, schoolId, studentId);
                LOGGER.debug("parentId = {}", parentId);
                if (roleId.equals(parentId)) {
                    return Utils.generateRoleAuthToken(role, roleId, userId, schoolId, studentId);
                }
                break;
        }
        LOGGER.debug("adminId/teacherId/parentId did not match with roleId = {}, hence returning null", roleId);
        return null;
    }

    @Override
    public boolean changePassword(Integer userId, String oldPassword,
            String newPassword, Boolean isOldPasswordNeeded) {

        LOGGER.debug("userId = {}", userId);
        User user = userDao.find(userId);
        if (user == null) {
            LOGGER.debug("No user found with userId = {} . Returning false", userId);
            return false;
        }
        if (isOldPasswordNeeded && !user.getPassword().equals(DigestUtils.sha256Hex(oldPassword))) {
            LOGGER.debug("Old password entered and the old password found in records does not match. Returning false.");
            return false;
        } else {
            user.setPassword(DigestUtils.sha256Hex(newPassword));
            user.setActive(true);
            user.setStatus(Constants.ACTIVE);
            userDao.saveOrUpdate(user);
            LOGGER.debug("Password changed successfully. Returning true");
            return true;
        }
    }

    @Override
    public boolean forgotPassword(String phone) {
        User user = userDao.getUserByPhone(phone);

        if (user == null) {
            LOGGER.debug("User with phone = {} not found.", phone);
            return false;
        } else {
            LOGGER.debug("User with phone = {} is {}.", phone, user);
            user.setOtp(Utils.generateOtp(Constants.OTP_LENGTH));
            return true;
        }
    }

    @Override
    public Integer forgotPassword(String phone, Integer otp) {
        User user = userDao.getUserByPhone(phone);

        if (user.getOtp().equals(otp)) {
            return user.getId();
        } else {
            return -1;
        }
    }
}
