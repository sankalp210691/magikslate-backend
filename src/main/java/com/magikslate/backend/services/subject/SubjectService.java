/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.subject;

import com.magikslate.backend.entity.Student;
import com.magikslate.backend.entity.Subject;
import com.magikslate.backend.onboarding.StudentTeacherInput;
import com.magikslate.backend.services.GenericService;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public interface SubjectService extends GenericService<Subject, Integer> {

    public List<Subject> getSubjectListBySchool(Integer schoolId);

    public List<Subject> createBulk(List<StudentTeacherInput> studentTeacherInputList, Integer schoolId);

    public List<Integer> assignTeacher(boolean b, List<Integer> classId, Integer schoolId, Integer roleId, Integer teacherId, Integer subjectId);

    public List<Subject> getSubjectsForStudent(Student student);
    
    @Override
    public void update(Subject subject);
}
