/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.subject;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.dao.clazz.ClassDao;
import com.magikslate.backend.dao.school.SchoolDao;
import com.magikslate.backend.dao.student.StudentDao;
import com.magikslate.backend.dao.studygroup.StudyGroupDao;
import com.magikslate.backend.dao.subject.SubjectDao;
import com.magikslate.backend.dao.teacher.TeacherDao;
import com.magikslate.backend.entity.Class;
import com.magikslate.backend.entity.School;
import com.magikslate.backend.entity.Student;
import com.magikslate.backend.entity.StudentStudyGroupMapping;
import com.magikslate.backend.entity.StudyGroup;
import com.magikslate.backend.entity.StudyGroupTeacherMapping;
import com.magikslate.backend.entity.Subject;
import com.magikslate.backend.entity.SubjectClassTeacherMapping;
import com.magikslate.backend.entity.Teacher;
import com.magikslate.backend.exception.InvalidOnboardingInputException;
import com.magikslate.backend.onboarding.StudentTeacherInput;
import com.magikslate.backend.services.GenericServiceImpl;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import java.time.Year;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author sankalpkulshrestha
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SubjectServiceImpl extends GenericServiceImpl<Subject, Integer>
        implements SubjectService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubjectServiceImpl.class);

    private SubjectDao subjectDao;

    private TeacherDao teacherDao;

    private SchoolDao schoolDao;

    private StudentDao studentDao;

    private ClassDao classDao;

    private StudyGroupDao studyGroupDao;

    @Autowired
    public SubjectServiceImpl(@Qualifier("subjectDaoImpl") GenericDao<Subject, Integer> subjectDao,
            @Qualifier("teacherDaoImpl") GenericDao<Teacher, Integer> teacherDao,
            @Qualifier("schoolDaoImpl") GenericDao<School, Integer> schoolDao,
            @Qualifier("studentDaoImpl") GenericDao<Student, Integer> studentDao,
            @Qualifier("classDaoImpl") GenericDao<Class, Integer> classDao,
            @Qualifier("studyGroupDaoImpl") GenericDao<StudyGroup, Integer> studyGroupDao) {
        super(subjectDao);
        this.subjectDao = (SubjectDao) subjectDao;
        this.teacherDao = (TeacherDao) teacherDao;
        this.schoolDao = (SchoolDao) schoolDao;
        this.classDao = (ClassDao) classDao;
        this.studyGroupDao = (StudyGroupDao) studyGroupDao;
        this.studentDao = (StudentDao) studentDao;
    }

    @Override
    public List<Subject> getSubjectListBySchool(Integer schoolId) {
        return subjectDao.getSubjectListBySchool(schoolId);
    }

    @Override
    public List<Subject> createBulk(List<StudentTeacherInput> studentTeacherInputList, Integer schoolId) {
        List<Teacher> teacherList = teacherDao.getTeacherBySchool(schoolId);
        List<Student> studentList = studentDao.getStudentListBySchool(schoolId);
        List<Class> classList = classDao.getClassListBySchool(schoolId, null);

        Map<String, Teacher> teacherMap = new HashMap();
        Map<String, Student> studentMap = new HashMap();
        Map<String, Class> classMap = new HashMap();
        teacherList.forEach(teacher -> {
            teacherMap.put(teacher.getEmployeeId(), teacher);
            teacher.getClassTeacherMappingList().forEach(ctm -> {
            });
        });

        studentList.forEach(student -> {
            studentMap.put(student.getAdmissionCode(), student);
        });

        classList.forEach(clazz -> {
            classMap.put(Utils.getClassCode(clazz), clazz);
        });

        String allClassesValid = allClassesValid(studentTeacherInputList, classMap);
        if (allClassesValid != null) {
            throw new InvalidOnboardingInputException(allClassesValid);
        }
        String allStudentsValid = allStudentsValid(studentTeacherInputList, studentMap);
        if (allStudentsValid != null) {
            throw new InvalidOnboardingInputException(allStudentsValid);
        }
        String allTeachersValid = allTeachersValid(studentTeacherInputList, teacherMap);
        if (allTeachersValid != null) {
            throw new InvalidOnboardingInputException(allTeachersValid);
        }

        Date date = new Date();
        School school = new School(schoolId);

        Map<String, Subject> subjectMap = new HashMap();
        studentTeacherInputList.forEach(studentTeacherInput -> {
            String subjectCode = Utils.getSubjectCode(studentTeacherInput.getSubject());
            if (subjectCode != null && !subjectMap.containsKey(subjectCode)) {
                Subject subject = new Subject();
                subject.setCreatedAt(date);
                subject.setSchool(school);
                subject.setName(studentTeacherInput.getSubject());
                subjectMap.put(subjectCode, subject);
            }
        });
        List<Subject> subjectList = new ArrayList(subjectMap.values());
        List<Integer> subjectIdList = subjectDao.createBulk(subjectList, schoolId);
        int index = 0;
        for (Subject subject : subjectList) {
            subject.setId(subjectIdList.get(index));
            index++;
        }

        createSubjectStudentClassTeacherMappings(subjectMap, classMap,
                studentMap, teacherMap, studentTeacherInputList, schoolId);

        school.setUpdatedAt(date);
        if (schoolDao.updateStage(5, schoolId)) {
            return subjectList;
        } else {
            return new ArrayList();
        }
    }

    private void createSubjectStudentClassTeacherMappings(Map<String, Subject> subjectMap,
            Map<String, Class> classMap, Map<String, Student> studentMap,
            Map<String, Teacher> teacherMap,
            List<StudentTeacherInput> studentTeacherInputList, Integer schoolId) {

        Date currentDate = new Date();

        studentTeacherInputList.forEach(studentTeacherInput -> {
            if (studentTeacherInput.getSubject() != null) {
                Subject subject = subjectMap.get(Utils.getSubjectCode(studentTeacherInput.getSubject()));
                if (studentTeacherInput.getAdmissionCodeList() == null) { //linking to classes
                    String classCode = Utils.getClassCode(studentTeacherInput.getStandard(), studentTeacherInput.getSection());

                    SubjectClassTeacherMapping sctm = new SubjectClassTeacherMapping();
                    sctm.setClass1(classMap.get(classCode));
                    sctm.setTeacher(teacherMap.get(studentTeacherInput.getEmployeeId()));
                    sctm.setSubject(subject);
                    sctm.setSessionYear(Year.now().getValue());
                    sctm.setCreatedAt(currentDate);

                    List<SubjectClassTeacherMapping> subjectClassTeacherMappingList = subject.getSubjectClassTeacherMappingList();
                    if (subjectClassTeacherMappingList == null) {
                        subjectClassTeacherMappingList = new ArrayList();
                        subject.setSubjectClassTeacherMappingList(subjectClassTeacherMappingList);
                    }
                    subjectClassTeacherMappingList.add(sctm);
                } else {//linking to study groups
                    String[] admissionIdArray = studentTeacherInput.getAdmissionCodeList();
                    StudyGroup studyGroup = new StudyGroup();
                    studyGroup.setCreatedAt(currentDate);
                    studyGroup.setSubject(subject);

                    List<StudyGroupTeacherMapping> studyGroupTeacherMappingList = studyGroup.getStudyGroupTeacherMappingList();
                    if (studyGroupTeacherMappingList == null) {
                        studyGroupTeacherMappingList = new ArrayList();
                        studyGroup.setStudyGroupTeacherMappingList(studyGroupTeacherMappingList);
                    }
                    StudyGroupTeacherMapping sgtm = new StudyGroupTeacherMapping();
                    sgtm.setCreatedAt(currentDate);
                    sgtm.setStudyGroup(studyGroup);
                    sgtm.setTeacher(teacherMap.get(studentTeacherInput.getEmployeeId()));
                    studyGroupTeacherMappingList.add(sgtm);

                    studyGroup.setSchool(new School(schoolId));

                    List<StudyGroup> studyGroupList = subject.getStudyGroupList();
                    if (studyGroupList == null) {
                        studyGroupList = new ArrayList();
                        subject.setStudyGroupList(studyGroupList);
                    }
                    studyGroupList.add(studyGroup);

                    List<StudentStudyGroupMapping> sgmList = new ArrayList();
                    studyGroup.setStudentStudyGroupMappingList(sgmList);

                    for (String admissionId : admissionIdArray) {
                        if (!Utils.isNullOrEmpty(admissionId)) {
                            StudentStudyGroupMapping sgm = new StudentStudyGroupMapping();
                            sgm.setStudyGroup(studyGroup);
                            sgm.setCreatedAt(currentDate);
                            sgm.setStudent(studentMap.get(admissionId));
                            sgmList.add(sgm);
                        }
                    }
                }
            }
        });
        subjectDao.mapToTeacherClassStudyGroup(subjectMap.values());
    }

    private String allClassesValid(List<StudentTeacherInput> studentTeacherInputList, Map<String, Class> classMap) {
        int index = 2;
        for (StudentTeacherInput studentTeacherInput : studentTeacherInputList) {
            String classCode = Utils.getClassCode(studentTeacherInput.getStandard(), studentTeacherInput.getSection());
            if (classCode != null && !classMap.containsKey(classCode)) {
                LOGGER.info("There is no " + studentTeacherInput.getStandard() + "-"
                        + studentTeacherInput.getSection() + " in your class list. Please create this class or check your onboarding file at row " + index);
                return "There is no " + studentTeacherInput.getStandard() + "-"
                        + studentTeacherInput.getSection() + " in your class list. Please create this class or check your onboarding file at row " + index;
            }
            index++;
        }
        return null;
    }

    private String allStudentsValid(List<StudentTeacherInput> studentTeacherInputList, Map<String, Student> studentMap) {
        int index = 2;
        for (StudentTeacherInput studentTeacherInput : studentTeacherInputList) {
            List<Student> studentList = new ArrayList();
            if (studentTeacherInput.getAdmissionCodeList() != null) {
                for (String admissionCode : studentTeacherInput.getAdmissionCodeList()) {
                    if (!Utils.isNullOrEmpty(admissionCode) && !studentMap.containsKey(admissionCode)) {
                        LOGGER.info("There is no student with admission code " + admissionCode + " in your records. Please add the student first or check your onboarding file at row " + index);
                        return "There is no student with admission code " + admissionCode + " in your records. Please add the student first or check your onboarding file at row " + index;
                    }
                    Student student = new Student();
                    student.setAdmissionCode(admissionCode);
                    studentList.add(student);
                }
            }
            index++;
        }
        return null;
    }

    private String allTeachersValid(List<StudentTeacherInput> studentTeacherInputList, Map<String, Teacher> teacherMap) {
        int index = 2;
        for (StudentTeacherInput studentTeacherInput : studentTeacherInputList) {
            if (!teacherMap.containsKey(studentTeacherInput.getEmployeeId())) {
                LOGGER.info("There is no student with admission code "
                        + studentTeacherInput.getEmployeeId()
                        + " in your records. Please add the teacher first or check your onboarding file at row " + index);
                return "There is no student with admission code "
                        + studentTeacherInput.getEmployeeId()
                        + " in your records. Please add the teacher first or check your onboarding file at row " + index;
            }
            index++;
        }
        return null;
    }

    @Override
    public List<Integer> assignTeacher(boolean isClassIdList, List<Integer> idList, Integer schoolId, Integer adminId, Integer teacherId, Integer subjectId) {

        Teacher teacher = teacherDao.find(teacherId);
        if (teacher == null || teacher.getDeletedAt() != null || !teacher.getSchool().getId().equals(schoolId)) {
            return null;
        }
        Subject subject = subjectDao.find(subjectId);
        if (subject == null || subject.getDeletedAt() != null) {//add school_id column in subject?
            return null;
        }

        Date date = new Date();
        if (isClassIdList) {

            List<Class> classList = classDao.getClassListBySchool(schoolId, null);
            Set<Integer> classIdSet = idList.stream().collect(Collectors.toCollection(HashSet::new));

            List<SubjectClassTeacherMapping> sctmList = new ArrayList();

            for (Class clazz : classList) {
                if (classIdSet.contains(clazz.getId())) {
                    SubjectClassTeacherMapping sctm = new SubjectClassTeacherMapping();
                    sctm.setClass1(clazz);
                    sctm.setSessionYear(Year.now().getValue());
                    sctm.setSubject(subject);
                    sctm.setTeacher(teacher);
                    sctm.setCreatedAt(date);
                    sctm.setUpdatedAt(date);
                    sctmList.add(sctm);
                }
            }
            return subjectDao.createSctm(sctmList);
        } else {

            List<StudyGroup> studyGroupList = studyGroupDao.getStudyGroupBySchool(schoolId, null);
            Set<Integer> sgIdSet = idList.stream().collect(Collectors.toCollection(HashSet::new));

            List<StudyGroupTeacherMapping> sgtmList = new ArrayList();

            for (StudyGroup studyGroup : studyGroupList) {
                if (sgIdSet.contains(studyGroup.getId())) {
                    StudyGroupTeacherMapping sgtm = new StudyGroupTeacherMapping();
                    sgtm.setCreatedAt(date);
                    sgtm.setStudyGroup(studyGroup);
                    sgtm.setTeacher(teacher);
                    sgtmList.add(sgtm);
                }
            }

            return subjectDao.createSgtm(sgtmList);
        }
    }

    @Override
    public List<Subject> getSubjectsForStudent(Student student) {
        return subjectDao.getSubjectsForStudent(student);
    }

    @Override
    public void update(Subject subject) {
        if (subject == null || subject.getId() == null) {
            return;
        }
        Date today = new Date();
        Subject sbj = subjectDao.find(subject.getId());

        if (!Utils.isNullOrEmpty(subject.getName())) {
            sbj.setName(subject.getName());
        }

        List<SubjectClassTeacherMapping> existingSubjectClassTeacherMappingList = sbj.getSubjectClassTeacherMappingList();
        List<SubjectClassTeacherMapping> newSubjectClassTeacherMappingList = subject.getSubjectClassTeacherMappingList();
        
        //TODO: add here the code to remove existingSubjectClassTeacherMappingList entries where deletedAt != NULL...refer update func in SchoolServiceImpl.java ... .filter func

        if (newSubjectClassTeacherMappingList == null || newSubjectClassTeacherMappingList.isEmpty()) {
            if (!existingSubjectClassTeacherMappingList.isEmpty()) {
                existingSubjectClassTeacherMappingList.forEach(sctm -> {
                    sctm.setDeletedAt(today);
                });
            }
        } else {
            Set<String> newClassIdSet = new HashSet();
            Set<String> existingClassIdSet = new HashSet();
            newSubjectClassTeacherMappingList.forEach(subjectClassTeacherMapping -> {
                newClassIdSet.add(subjectClassTeacherMapping.getClass1().getId() + Constants.UNDERSCORE_SEPARATOR + subjectClassTeacherMapping.getTeacher().getId());
            });
            existingSubjectClassTeacherMappingList.forEach(subjectClassTeacherMapping -> {
                existingClassIdSet.add(subjectClassTeacherMapping.getClass1().getId() + Constants.UNDERSCORE_SEPARATOR + subjectClassTeacherMapping.getTeacher().getId());
            });

            for (SubjectClassTeacherMapping sctm : existingSubjectClassTeacherMappingList) {
                if (!newClassIdSet.contains(sctm.getClass1().getId())) {
                    sctm.setDeletedAt(today);
                }
            }

            List<Subject> subjectList = new ArrayList();
            
            List<SubjectClassTeacherMapping> finalSubjectClassTeacherMappingList = new ArrayList();
            newSubjectClassTeacherMappingList.forEach(sctm -> {
                if (!existingClassIdSet.contains(sctm.getClass1().getId())) {
                    finalSubjectClassTeacherMappingList.add(sctm);
                }
            });
            if (!finalSubjectClassTeacherMappingList.isEmpty()) {
                sbj.setSubjectClassTeacherMappingList(finalSubjectClassTeacherMappingList);
                subjectList.add(sbj);
            }
            
            subjectDao.mapToTeacherClassStudyGroup(subjectList);
        }
    }

}
