/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.teacher;

import com.magikslate.backend.entity.Class;
import com.magikslate.backend.entity.Teacher;
import com.magikslate.backend.entity.User;
import com.magikslate.backend.onboarding.TeacherInput;
import com.magikslate.backend.services.GenericService;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public interface TeacherService extends GenericService<Teacher, Integer> {

    public User getUserByTeacherId(int teacherId);

    public List<Teacher> createBulk(List<TeacherInput> teacherInputList, Integer schoolId);
    
    public List<Teacher> getTeacherBySchool(Integer schoolId);

    public List<Teacher> getTeachersByClassList(List<Class> classIdList);

    public Teacher add(TeacherInput teacherInput, Integer schoolId);
    
    @Override
    public void update(Teacher teacher);
    
    @Override
    public void remove(Teacher teacher);

}
