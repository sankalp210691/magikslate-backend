/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.teacher;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.dao.clazz.ClassDao;
import com.magikslate.backend.dao.school.SchoolDao;
import com.magikslate.backend.dao.teacher.TeacherDao;
import com.magikslate.backend.dao.user.UserDao;
import com.magikslate.backend.entity.Class;
import com.magikslate.backend.entity.ClassTeacherMapping;
import com.magikslate.backend.entity.School;
import com.magikslate.backend.entity.Teacher;
import com.magikslate.backend.entity.User;
import com.magikslate.backend.exception.InvalidOnboardingInputException;
import com.magikslate.backend.onboarding.TeacherInput;
import com.magikslate.backend.services.GenericServiceImpl;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import java.time.Year;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author sankalpkulshrestha
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class TeacherServiceImpl extends GenericServiceImpl<Teacher, Integer>
        implements TeacherService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TeacherServiceImpl.class);

    private final TeacherDao teacherDao;

    private final SchoolDao schoolDao;

    private UserDao userDao;

    private ClassDao classDao;

    @Autowired
    public TeacherServiceImpl(@Qualifier("teacherDaoImpl") GenericDao<Teacher, Integer> teacherDao,
            @Qualifier("schoolDaoImpl") GenericDao<School, Integer> schoolDao,
            @Qualifier("userDaoImpl") GenericDao<User, Integer> userDao,
            @Qualifier("classDaoImpl") GenericDao<Class, Integer> classDao) {

        super(teacherDao);
        this.userDao = (UserDao) userDao;
        this.teacherDao = (TeacherDao) teacherDao;
        this.schoolDao = (SchoolDao) schoolDao;
        this.classDao = (ClassDao) classDao;
    }

    @Override
    public List<Teacher> createBulk(List<TeacherInput> teacherInputList, Integer schoolId) {
        List<Teacher> teacherList = handleTeacherCreation(teacherInputList, schoolId);
        if (schoolDao.updateStage(3, schoolId)) {
            return teacherList;
        } else {
            return new ArrayList();
        }
    }

    private List<Teacher> handleTeacherCreation(List<TeacherInput> teacherInputList, Integer schoolId) {
        List<Teacher> teacherList = new ArrayList();
        School school = schoolDao.find(schoolId);
        if (school == null) {

            LOGGER.debug("Returning empty teacher list because no school found with schoolId = {}", schoolId);
            return new ArrayList();
        }

        Date date = new Date();

        teacherInputList.forEach(teacherInput -> {
            Teacher teacher = new Teacher();

            teacher.setEmployeeId(teacherInput.getEmployeeId());
            teacher.setSchool(school);

            Integer dummyPasswordOtp = Utils.generateOtp(Constants.DUMMY_PASSWORD_LENGTH);

            User user = new User();
            user.setStatus(Constants.REG_PASSWORD_CHANGE_PENDING);
            user.setName(teacherInput.getName());
            user.setPhone(teacherInput.getPhone());
            user.setEmail(teacherInput.getEmail());
            user.setPassword(DigestUtils.sha256Hex(String.valueOf(dummyPasswordOtp)));
            user.setOtp(dummyPasswordOtp);
            user.setCreatedAt(date);
            teacher.setUser(user);
            teacher.setCreatedAt(date);

            teacherList.add(teacher);
        });

        LOGGER.debug("teacherList = {}", teacherList);

        List<String> phoneList = new ArrayList();
        List<String> emailList = new ArrayList();

        //collect all the phones and emails of all teachers
        teacherList.stream().map((p) -> {
            if (!Utils.isNullOrEmpty(p.getUser().getPhone())) {
                phoneList.add(p.getUser().getPhone());
            }
            return p;
        }).filter((p) -> (!Utils.isNullOrEmpty(p.getUser().getEmail()))).forEachOrdered((p) -> {
            emailList.add(p.getUser().getEmail());
        });

        LOGGER.debug("phoneList = {}, emailList = {}", phoneList, emailList);

        //get existing users
        List<User> existingUserList = userDao.getUserListByPhoneOrEmail(phoneList, emailList, true, true);
        LOGGER.debug("existingUserList = {}", existingUserList);

        //get existing user phone map
        Map<String, User> existingUserPhoneMap = new HashMap();
        Map<String, User> existingUserEmailMap = new HashMap();

        existingUserList.stream().map((existingUser) -> {
            existingUserPhoneMap.put(existingUser.getPhone(), existingUser);
            return existingUser;
        }).forEachOrdered((existingUser) -> {
            existingUserEmailMap.put(existingUser.getEmail(), existingUser);
        });

        List<Teacher> finalTeacherList = new ArrayList();

        Map<String, Teacher> existingPhoneTeacherMap = new HashMap();

        existingUserList.stream().map((user) -> teacherDao.filterTeacherListForSchool(user.getTeacherList(), schoolId)) //filterTeacherListForSchool sends back teacherList containing only teaches of the school
                .filter((existingTeacherList) -> (!existingTeacherList.isEmpty()))
                .forEachOrdered((existingTeacherList) -> {

                    existingTeacherList.forEach((teacher) -> {

                        finalTeacherList.add(teacher);
                        existingPhoneTeacherMap.put(teacher.getUser().getPhone(), teacher);
                    });
                });

        List<User> newUserList = new ArrayList();
        List<Teacher> newTeacherList = new ArrayList();

        handleConflictsByThrowingError(teacherList, existingUserPhoneMap, existingUserEmailMap, newUserList, newTeacherList, existingPhoneTeacherMap);

        List<Integer> newUserIdList = userDao.createBulk(newUserList);
        LOGGER.debug("newUserIdList = {}", newUserIdList);
        int index = 0;
        for (User user : newUserList) {
            user.setId(newUserIdList.get(index));
            index++;
        }

        List<Integer> newTeacherIdList = teacherDao.createBulk(newTeacherList);
        index = 0;
        for (Teacher teacher : newTeacherList) {
            teacher.setId(newTeacherIdList.get(index));
            index++;
        }

        finalTeacherList.addAll(newTeacherList);
        LOGGER.debug("Returning teacherIdList = {}", finalTeacherList);
        school.setUpdatedAt(date);
        school.setTeacherCount(this.getTeacherBySchool(schoolId).size());

        ////////////////For assigning classteacher-deputy teacher roles
        Map<String, Class> classMap = new HashMap();
        if (teacherInputList.get(0).getClassTeacherClassId() == null && teacherInputList.get(0).getClassTeacherStandard() != null) {
            List<Class> classList = classDao.getClassListBySchool(schoolId, null);
            classList.forEach(clazz -> {
                classMap.put(Utils.getClassCode(clazz), clazz);
            });
        }

        Map<String, Teacher> teacherMap = new HashMap();
        finalTeacherList.forEach(teacher -> {
            teacherMap.put(teacher.getEmployeeId(), teacher);
        });
        addCtDtRoles(teacherInputList, teacherMap, classMap);
        ////////////////////assigning code ends here

        return finalTeacherList;
    }

    private void addCtDtRoles(List<TeacherInput> teacherInputList, Map<String, Teacher> teacherMap, Map<String, Class> classMap) {
        Set<String> ctSet = new HashSet();
        Date currentDate = new Date();

        for (TeacherInput teacherInput : teacherInputList) {
            if (teacherInput.getClassTeacherClassId() == null && teacherInput.getClassTeacherStandard() == null) {
                continue;
            }
            Class clazz = null;
            Teacher teacher = teacherMap.get(teacherInput.getEmployeeId());
            if (teacherInput.getClassTeacherClassId() == null) {
                clazz = classMap.get(Utils.getClassCode(teacherInput.getClassTeacherStandard(), teacherInput.getClassTeacherSection()));
            } else {
                clazz = new Class(teacherInput.getClassTeacherClassId());
            }

            ClassTeacherMapping ctm = new ClassTeacherMapping();
            ctm.setCreatedAt(currentDate);
            ctm.setClass1(clazz);
            ctm.setTeacher(teacher);
            ctm.setClassTeacher(true);
            ctm.setSessionYear(Year.now().getValue());
            List<ClassTeacherMapping> classTeacherMappingList = teacher.getClassTeacherMappingList();
            if (classTeacherMappingList == null) {
                classTeacherMappingList = new ArrayList();
                teacher.setClassTeacherMappingList(classTeacherMappingList);
            }
            classTeacherMappingList.add(ctm);
            ctSet.add(Utils.getClassCode(teacherInput.getClassTeacherStandard(), teacherInput.getClassTeacherSection()));
        }

        classMap.entrySet().stream().map((codeClass) -> codeClass.getValue()).filter((clazz) -> (!ctSet.contains(Utils.getClassCode(clazz)))).map((clazz) -> {
            LOGGER.info(clazz.getStandard() + "-" + clazz.getSection() + " doesn't have any class teacher assigned to it.");
            return clazz;
        }).forEachOrdered((clazz) -> {
            throw new InvalidOnboardingInputException(clazz.getStandard() + "-" + clazz.getSection() + " doesn't have any class teacher assigned to it.");
        });
//        teacherDao.getTeacherEntityList(teacherMap.values());
        teacherDao.mapToClass(teacherMap.values());
    }

    @Override
    public User getUserByTeacherId(int teacherId) {
        return teacherDao.getUserByTeacherId(teacherId);
    }

    @Override
    public List<Teacher> getTeacherBySchool(Integer schoolId) {
        List<Teacher> teacherList = teacherDao.getTeacherBySchool(schoolId);
        teacherList.forEach((teacher) -> {
            teacher.getClassTeacherMappingList().forEach(ctm -> {
                ctm.getClass1().getId();
            });
        });
        return teacherList;
    }

    @Override
    public List<Teacher> getTeachersByClassList(List<Class> classList) {
        return teacherDao.getTeachersByClassList(classList.stream().map(clazz -> clazz.getId()).collect(Collectors.toList()));
    }

    @Deprecated
    private void handleConflictsByMerging(List<Teacher> teacherList, Map<String, User> existingUserPhoneMap, Map<String, User> existingUserEmailMap,
            List<User> newUserList, List<Teacher> newTeacherList, Map<String, Teacher> existingPhoneTeacherMap) {

        teacherList.forEach(teacher -> {

            LOGGER.debug("existingUserPhoneMap({}) = {}", teacher.getUser().getPhone(), existingUserPhoneMap.get(teacher.getUser().getPhone()));
            LOGGER.debug("existingUserEmailMap({}) = {}", teacher.getUser().getEmail(), existingUserPhoneMap.get(teacher.getUser().getEmail()));

            if (!existingUserPhoneMap.containsKey(teacher.getUser().getPhone()) && (teacher.getUser().getEmail() == null || (teacher.getUser().getEmail() != null && !existingUserEmailMap.containsKey(teacher.getUser().getEmail())))) {

                LOGGER.debug("Adding new user = {}", teacher.getUser());
                newUserList.add(teacher.getUser());

                LOGGER.debug("Adding new teacher = {}", teacher);
                newTeacherList.add(teacher);
            } else if (!existingUserPhoneMap.containsKey(teacher.getUser().getPhone()) && (teacher.getUser().getEmail() != null && existingUserEmailMap.containsKey(teacher.getUser().getEmail()))) {

                LOGGER.debug("User is already there as user = {} but different phone. So updating phone number", existingUserEmailMap.get(teacher.getUser().getEmail()));
                existingUserEmailMap.get(teacher.getUser().getEmail()).setPhone(teacher.getUser().getPhone());

                //User already there. Just checking if user is teacher or not
                if (existingPhoneTeacherMap.containsKey(teacher.getUser().getPhone())) {
                    LOGGER.debug("User is already a teacher for this school. Do nothing");
                } else {
                    LOGGER.debug("User is not teacher. Adding new teacher = {} and deleting his/her entry from all previous schools.", teacher);
                    teacher.setUser(existingUserPhoneMap.get(teacher.getUser().getPhone()));
                    newTeacherList.add(teacher);
                }

            } else if (existingUserPhoneMap.containsKey(teacher.getUser().getPhone()) && (teacher.getUser().getEmail() == null || (teacher.getUser().getEmail() != null && !existingUserEmailMap.containsKey(teacher.getUser().getEmail())))) {

                LOGGER.debug("User is already there as user = {} but different email. So updating email address", existingUserPhoneMap.get(teacher.getUser().getPhone()));
                existingUserPhoneMap.get(teacher.getUser().getPhone()).setEmail(teacher.getUser().getEmail());

                //User already there. Just checking if user is teacher or not
                if (existingPhoneTeacherMap.containsKey(teacher.getUser().getPhone())) {
                    LOGGER.debug("User is already a teacher. Do nothing");
                } else {
                    LOGGER.debug("User is not teacher. Adding new teacher = {} and deleting his/her entry from all previous schools.", teacher);
                    teacher.setUser(existingUserEmailMap.get(teacher.getUser().getEmail()));
                    newTeacherList.add(teacher);
                }

            } else {
                LOGGER.debug("User is already there as user = {}", existingUserPhoneMap.get(teacher.getUser().getPhone()));
                //User already there. Just checking if user is teacher or not
                if (existingPhoneTeacherMap.containsKey(teacher.getUser().getPhone())) {
                    LOGGER.debug("User is already a teacher. Do nothing");
                } else {
                    LOGGER.debug("User is not teacher. Adding new teacher = {} and deleting his/her entry from all previous schools.", teacher);
                    teacher.setUser(existingUserPhoneMap.get(teacher.getUser().getPhone()));
                    newTeacherList.add(teacher);
                }
            }
        });
    }

    private void handleConflictsByThrowingError(List<Teacher> teacherList, Map<String, User> existingUserPhoneMap, Map<String, User> existingUserEmailMap,
            List<User> newUserList, List<Teacher> newTeacherList, Map<String, Teacher> existingPhoneTeacherMap) {

        teacherList.forEach(teacher -> {

            LOGGER.debug("existingUserPhoneMap({}) = {}", teacher.getUser().getPhone(), existingUserPhoneMap.get(teacher.getUser().getPhone()));
            LOGGER.debug("existingUserEmailMap({}) = {}", teacher.getUser().getEmail(), existingUserPhoneMap.get(teacher.getUser().getEmail()));

            if (!existingUserPhoneMap.containsKey(teacher.getUser().getPhone()) && (teacher.getUser().getEmail() == null || (teacher.getUser().getEmail() != null && !existingUserEmailMap.containsKey(teacher.getUser().getEmail())))) {

                LOGGER.debug("Adding new user = {}", teacher.getUser());
                newUserList.add(teacher.getUser());

                LOGGER.debug("Adding new teacher = {}", teacher);
                newTeacherList.add(teacher);
            } else if (!existingUserPhoneMap.containsKey(teacher.getUser().getPhone()) && (teacher.getUser().getEmail() != null && existingUserEmailMap.containsKey(teacher.getUser().getEmail()))) {

                LOGGER.debug("User is already there as user = {} but different phone. So throwing error.", existingUserEmailMap.get(teacher.getUser().getEmail()));
                throw new InvalidOnboardingInputException("We already have a user with E-mail address " + teacher.getUser().getEmail()
                        + " but a different phone number. Please contact this person to update "
                        + "their phone number and try again later.");

            } else if (existingUserPhoneMap.containsKey(teacher.getUser().getPhone()) && (teacher.getUser().getEmail() == null || (teacher.getUser().getEmail() != null && !existingUserEmailMap.containsKey(teacher.getUser().getEmail())))) {

                if (teacher.getUser().getEmail() != null) {
                    LOGGER.debug("User is already there as user = {} but different email. So throwing error", existingUserPhoneMap.get(teacher.getUser().getPhone()));
                    throw new InvalidOnboardingInputException("We already have a user with phone number " + teacher.getUser().getPhone()
                            + " but a different E-mail address. Please contact this person to update "
                            + "their E-mail address and try again later.");
                } else {
                    LOGGER.debug("User is already there as user = {} but different email. But new email address is null. So not updating the email.", existingUserPhoneMap.get(teacher.getUser().getPhone()));
                }

                //User already there. Just checking if user is teacher or not
                if (existingPhoneTeacherMap.containsKey(teacher.getUser().getPhone())) {
                    LOGGER.debug("User is already a teacher. Do nothing");
                } else {
                    LOGGER.debug("User is not teacher. Adding new teacher = {} and deleting his/her entry from all previous schools.", teacher);
                    teacher.setUser(existingUserEmailMap.get(teacher.getUser().getEmail()));
                    newTeacherList.add(teacher);
                }

            } else {
                LOGGER.debug("User is already there as user = {}", existingUserPhoneMap.get(teacher.getUser().getPhone()));
                //User already there. Just checking if user is teacher or not
                if (existingPhoneTeacherMap.containsKey(teacher.getUser().getPhone())) {
                    LOGGER.debug("User is already a teacher. Do nothing");
                } else {
                    LOGGER.debug("User is not teacher. Adding new teacher = {} and deleting his/her entry from all previous schools.", teacher);
                    teacher.setUser(existingUserPhoneMap.get(teacher.getUser().getPhone()));
                    newTeacherList.add(teacher);
                }
            }
        });
    }

    @Override
    public Teacher add(TeacherInput teacherInput, Integer schoolId) {
        if (teacherInput == null) {
            return null;
        }

        List<TeacherInput> teacherInputList = new ArrayList();
        teacherInputList.add(teacherInput);

        List<Teacher> teacherList = handleTeacherCreation(teacherInputList, schoolId);
        if (!teacherList.isEmpty()) {
            return teacherList.get(0);
        } else {
            return null;
        }
    }

    @Override
    public void update(Teacher teacher) {
        if (teacher == null || teacher.getId() == null) {
            return;
        }
        Date today = new Date();
        Teacher tch = teacherDao.find(teacher.getId());

        List<ClassTeacherMapping> existingClassTeacherMappingList = tch.getClassTeacherMappingList();
        List<ClassTeacherMapping> newClassTeacherMappingList = teacher.getClassTeacherMappingList();

        Iterator iterator = existingClassTeacherMappingList.iterator();
        while (iterator.hasNext()) {
            ClassTeacherMapping ctm = (ClassTeacherMapping) iterator.next();
            if (ctm.getDeletedAt() != null) {
                iterator.remove();
            }
        }

        if (newClassTeacherMappingList == null || newClassTeacherMappingList.isEmpty()) {
            if (!existingClassTeacherMappingList.isEmpty()) {
                existingClassTeacherMappingList.forEach(ctm -> {
                    ctm.setDeletedAt(today);
                });
            }
        } else {
            Set<Integer> existingClassTeacherMappingSet = existingClassTeacherMappingList.stream().map(ctm -> ctm.getClass1().getId()).collect(Collectors.toCollection(HashSet::new));
            Set<Integer> newClassTeacherMappingSet = newClassTeacherMappingList.stream().map(ctm -> ctm.getClass1().getId()).collect(Collectors.toCollection(HashSet::new));

            existingClassTeacherMappingList.forEach(ctm -> {
                if (!newClassTeacherMappingSet.contains(ctm.getClass1().getId())) {
                    ctm.setDeletedAt(today);
                }
            });

            List<ClassTeacherMapping> finalClassTeacherMappingList = new ArrayList();
            newClassTeacherMappingList.forEach(ctm -> {
                if (!existingClassTeacherMappingSet.contains(ctm.getClass1().getId())) {
                    finalClassTeacherMappingList.add(ctm);
                }
            });
            if (!finalClassTeacherMappingList.isEmpty()) {
                tch.setClassTeacherMappingList(finalClassTeacherMappingList);
                List<Teacher> teacherCollection = new ArrayList();
                teacherCollection.add(tch);
                teacherDao.mapToClass(teacherCollection);
            }
        }
        tch.setEmployeeId(teacher.getEmployeeId());//we update only employee id. rest things the user has to update himself
    }

    @Override
    public void remove(Teacher teacher) {
        Date today = new Date();
        teacher = teacherDao.find(teacher.getId());
        if (!teacher.getClassTeacherMappingList().isEmpty()) {
            teacher.getClassTeacherMappingList().forEach(ctm -> {
                ctm.setDeletedAt(today);
            });
        }
        teacher.setDeletedAt(new Date());
    }

}
