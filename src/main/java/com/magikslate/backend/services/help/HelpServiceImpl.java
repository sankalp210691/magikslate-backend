/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.help;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.dao.help.HelpDao;
import com.magikslate.backend.entity.Help;
import com.magikslate.backend.services.GenericServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author sankalpkulshrestha
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class HelpServiceImpl extends GenericServiceImpl<Help, Integer>
        implements HelpService {

    private HelpDao helpDao;

    public HelpServiceImpl() {

    }

    @Autowired
    public HelpServiceImpl(@Qualifier("helpDaoImpl") GenericDao<Help, Integer> helpDao) {
        super(helpDao);
        this.helpDao = (HelpDao) helpDao;
    }
}
