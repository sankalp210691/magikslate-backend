/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.attendance;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.dao.attendance.AttendanceDao;
import com.magikslate.backend.entity.Attendance;
import com.magikslate.backend.entity.StudentClassMapping;
import com.magikslate.backend.entity.StudentStudyGroupMapping;
import com.magikslate.backend.services.GenericServiceImpl;
import com.magikslate.backend.util.Constants;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 *
 * @author sankalpkulshrestha
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AttendanceServiceImpl extends GenericServiceImpl<Attendance, Integer>
        implements AttendanceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AttendanceServiceImpl.class);

    private AttendanceDao attendanceDao;

    @Autowired
    public AttendanceServiceImpl(@Qualifier("attendanceDaoImpl") GenericDao<Attendance, Integer> attendanceDao) {
        super(attendanceDao);
        this.attendanceDao = (AttendanceDao) attendanceDao;
    }

    @Override
    public List<Integer> createBulk(List<Attendance> attendanceList, Date markAt) {

        if (attendanceList == null || attendanceList.isEmpty()) {
            return new ArrayList();
        }
        Attendance attendance = attendanceList.get(0);

        Integer studentIdentifier = null;

        if (attendance.getStudentClassMapping() != null) {
            StudentClassMapping scm = attendance.getStudentClassMapping();
            studentIdentifier = scm.getId();
        } else if (attendance.getStudentStudyGroupMapping() != null) {
            StudentStudyGroupMapping ssgm = attendance.getStudentStudyGroupMapping();
            studentIdentifier = ssgm.getId();
        } else {
            return new ArrayList();
        }

        Calendar myCal = Calendar.getInstance();
        myCal.setTime(markAt);
        myCal.add(Calendar.HOUR, +Constants.HOURS_PER_DAY);
        Date endDate = myCal.getTime();

        if (attendance.getCreatedAt().after(markAt) && attendance.getCreatedAt().before(endDate)) {
            List<Attendance> atList = attendanceDao.getAttendanceByStudentIdentifier(studentIdentifier, attendance.getStudentClassMapping() != null, markAt, endDate, 0);

            if (atList.isEmpty()) {
                return attendanceDao.createBulk(attendanceList);
            } else {
                return new ArrayList();
            }
        } else {
            return new ArrayList();
        }
    }

    @Override
    public void updateBulk(List<Attendance> attendanceList) {
        attendanceDao.updateBulk(attendanceList);
    }

    @Override
    public List<Attendance> getAttendance(Integer schoolId, Integer classId,
            Integer studyGroupId, Integer sessionYear, Date date) {

        List<Attendance> attendanceList = attendanceDao.getAttendance(schoolId, classId, studyGroupId,
                sessionYear, date);
        for (Attendance attendance : attendanceList) {
            attendance.getStudentClassMapping().getStudent().getAdmissionCode();
        }
        return attendanceList;
    }

    @Override//need to support study group attendance avg
    public Double getClassAvgAttendance(Integer schoolId, Integer classId) {

        List<Attendance> attendanceList = getAttendance(schoolId, classId, null, Year.now().getValue(), null);
        int presentCount = 0;
        int totalCount = 0;
        for (Attendance attendance : attendanceList) {

            if (attendance.getPresent()) {
                presentCount++;
            }
            totalCount++;
        }

        if (totalCount == 0) {
            return 0.0;
        } else {
            return presentCount * 100.0 / totalCount;
        }
    }

}
