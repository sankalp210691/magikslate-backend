/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.attendance;

import com.magikslate.backend.entity.Attendance;
import com.magikslate.backend.entity.Class;
import com.magikslate.backend.services.GenericService;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public interface AttendanceService extends GenericService<Attendance, Integer> {

    public List<Integer> createBulk(List<Attendance> attendanceList, Date markAt);

    public void updateBulk(List<Attendance> attendanceList);

    public List<Attendance> getAttendance(Integer schoolId, Integer classId,
            Integer studyGroupId, Integer sessionYear, Date date);

    public Double getClassAvgAttendance(Integer schoolId, Integer classId);

}
