/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.apply;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.dao.apply.ApplyDao;
import com.magikslate.backend.entity.UserSchoolInterest;
import com.magikslate.backend.services.GenericServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author sankalpkulshrestha
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ApplyServiceImpl extends GenericServiceImpl<UserSchoolInterest, Integer>
        implements ApplyService {
    
    private ApplyDao applyDao;
    
    @Autowired
    public ApplyServiceImpl(@Qualifier("applyDaoImpl") GenericDao<UserSchoolInterest, Integer> applyDao) {
        super(applyDao);
        this.applyDao = (ApplyDao) applyDao;
    }
}
