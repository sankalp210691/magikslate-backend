/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.apply;

import com.magikslate.backend.entity.UserSchoolInterest;
import com.magikslate.backend.services.GenericService;

/**
 *
 * @author sankalpkulshrestha
 */
public interface ApplyService extends GenericService<UserSchoolInterest,Integer> {
    
}
