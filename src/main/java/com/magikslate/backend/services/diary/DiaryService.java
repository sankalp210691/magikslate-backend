/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.diary;

import com.magikslate.backend.entity.AnecdotalRecord;
import com.magikslate.backend.entity.Diary;
import com.magikslate.backend.entity.ExtraCurricularDiary;
import com.magikslate.backend.entity.GeneralDiary;
import com.magikslate.backend.entity.Kudo;
import com.magikslate.backend.entity.LessonProgress;
import com.magikslate.backend.entity.Test;
import com.magikslate.backend.services.GenericService;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public interface DiaryService extends GenericService<Diary, Integer> {

    public Integer add(GeneralDiary generalDiary);

    public Integer add(Kudo kudoDiary);

    public Integer add(AnecdotalRecord anecdotalRecordDiary);

    public Integer add(LessonProgress lessonProgressDiary);

    public Integer add(ExtraCurricularDiary ecDiary);

    public Integer add(Test testDiary);

    public List<Diary> getDiary(String role, Integer roleId, Integer schoolId, Integer studentId, Integer classId, Integer subjectId, Integer studyGroupId, Integer offset);

}
