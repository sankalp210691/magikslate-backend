/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.diary;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.dao.diary.DiaryDao;
import com.magikslate.backend.entity.AnecdotalRecord;
import com.magikslate.backend.entity.Diary;
import com.magikslate.backend.entity.ExtraCurricularDiary;
import com.magikslate.backend.entity.GeneralDiary;
import com.magikslate.backend.entity.Kudo;
import com.magikslate.backend.entity.LessonProgress;
import com.magikslate.backend.entity.Test;
import com.magikslate.backend.services.GenericServiceImpl;
import com.magikslate.backend.util.Constants;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 *
 * @author sankalpkulshrestha
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class DiaryServiceImpl extends GenericServiceImpl<Diary, Integer>
        implements DiaryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DiaryServiceImpl.class);
    private static final long LONG_FUTURE_UNIX_TIMESTAMP = 253402257599000l;
    private static final Date START_DATE = new Date(0);
    private static final Date END_DATE = new Date(LONG_FUTURE_UNIX_TIMESTAMP);//9999-12-31 11:59:59

    private DiaryDao diaryDao;

    public DiaryServiceImpl() {

    }

    @Autowired
    public DiaryServiceImpl(@Qualifier("diaryDaoImpl") GenericDao<Diary, Integer> diaryDao) {
        
        super(diaryDao);
        this.diaryDao = (DiaryDao) diaryDao;
    }

    @Override
    public Diary get(Integer id) {
        Diary diary = diaryDao.find(id);
        LOGGER.info(diary.getStudentList().toString());
        if (diary.getType().equals(Constants.TEST_DIARY)) {
            LOGGER.info(diary.getTestList().toString());
        }
        return diary;
    }

    @Override
    public Integer add(GeneralDiary generalDiary) {
        Diary diary = generalDiary.getDiary();
        Integer id = diaryDao.add(diary);

        LOGGER.debug("diary entry created with id = {}", id);

        generalDiary.setDiaryId(id);
        diaryDao.add(generalDiary);

        LOGGER.debug("generalDiary added");
        LOGGER.debug("Returning id = {}", id);
        return id;
    }

    @Override
    public Integer add(Kudo kudoDiary) {
        Diary diary = kudoDiary.getDiary();
        Integer id = diaryDao.add(diary);

        LOGGER.debug("diary entry created with id = {}", id);

        kudoDiary.setDiaryId(id);
        diaryDao.add(kudoDiary);

        LOGGER.debug("kudoDiary added");
        LOGGER.debug("Returning id = {}", id);
        return id;
    }

    @Override
    public Integer add(AnecdotalRecord anecdotalRecordDiary) {
        Diary diary = anecdotalRecordDiary.getDiary();
        Integer id = diaryDao.add(diary);

        LOGGER.debug("diary entry created with id = {}", id);

        anecdotalRecordDiary.setDiaryId(id);
        diaryDao.add(anecdotalRecordDiary);

        LOGGER.debug("anecdotalRecordDiary added");
        LOGGER.debug("Returning id = {}", id);
        return id;
    }

    @Override
    public Integer add(LessonProgress lessonProgressDiary) {
        Diary diary = lessonProgressDiary.getDiary();
        Integer id = diaryDao.add(diary);

        LOGGER.debug("diary entry created with id = {}", id);

        lessonProgressDiary.setDiaryId(id);
        diaryDao.add(lessonProgressDiary);

        LOGGER.debug("lessonProgressDiary added");
        LOGGER.debug("Returning id = {}", id);
        return id;
    }

    @Override
    public Integer add(ExtraCurricularDiary ecDiary) {
        Diary diary = ecDiary.getDiary();
        Integer id = diaryDao.add(diary);

        LOGGER.debug("diary entry created with id = {}", id);

        ecDiary.setDiaryId(id);
        diaryDao.add(ecDiary);

        LOGGER.debug("ecDiary added");
        LOGGER.debug("Returning id = {}", id);
        return id;
    }

    @Override
    public Integer add(Test testDiary) {
        Diary diary = testDiary.getDiary();
        Integer id = diaryDao.add(diary);

        LOGGER.debug("diary entry created with id = {}", id);

        diary.setId(id);
        testDiary.setDiary(diary);
        diaryDao.add(testDiary);

        LOGGER.debug("testDiary added");
        LOGGER.debug("Returning id = {}", id);
        return id;
    }

    @Override
    public List<Diary> getDiary(String role, Integer roleId, Integer schoolId, Integer studentId, Integer classId, Integer subjectId, Integer studyGroupId, Integer offset) {
        Date startDate = START_DATE;
        Date endDate = END_DATE;
        return diaryDao.getDiary(role, roleId, schoolId, studentId, classId, subjectId, studyGroupId, offset, startDate, endDate);
    }

}
