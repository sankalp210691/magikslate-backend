/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.state;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.dao.state.StateDao;
import com.magikslate.backend.entity.State;
import com.magikslate.backend.services.GenericServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author sankalpkulshrestha
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class StateServiceImpl extends GenericServiceImpl<State, Integer>
        implements StateService {

    private StateDao stateDao;

    @Autowired
    public StateServiceImpl(@Qualifier("stateDaoImpl") GenericDao<State, Integer> stateDao) {
        super(stateDao);
        this.stateDao = (StateDao) stateDao;
    }
}
