/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.event;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.dao.event.EventDao;
import com.magikslate.backend.entity.Event;
import com.magikslate.backend.entity.EventAdminMapping;
import com.magikslate.backend.entity.EventParentMapping;
import com.magikslate.backend.entity.EventTeacherMapping;
import com.magikslate.backend.services.GenericServiceImpl;
import java.util.Date;
import java.util.List;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author sankalpkulshrestha
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class EventServiceImpl extends GenericServiceImpl<Event, Integer>
        implements EventService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventServiceImpl.class);

    private EventDao eventDao;

    @Autowired
    private RedissonClient redisson;

    public EventServiceImpl() {

    }

    @Autowired
    public EventServiceImpl(@Qualifier("eventDaoImpl") GenericDao<Event, Integer> eventDao) {
        super(eventDao);
        this.eventDao = (EventDao) eventDao;
    }

    @Override
    public Integer add(Event event) {

        return eventDao.add(event);
    }

    @Override
    public Event get(Integer id, Integer schoolId) {
        Event event = eventDao.find(id);
        LOGGER.debug("event found with id = {}. event = {}", id, event);

        if (!event.getSchool().getId().equals(schoolId)) {
            LOGGER.debug("Event doesn't belong to this school. event school = {} , requested school = {}", event.getSchool().getId(), schoolId);
            return null;
        }

        if (event.getClassList() != null) {
            event.getClassList().forEach(clazz -> {
                LOGGER.info(clazz.getId().toString());
            });
        }

        List<EventParentMapping> eventParentMappingList = event.getEventParentMappingList();
        List<EventTeacherMapping> eventTeacherMappingList = event.getEventTeacherMappingList();
        List<EventAdminMapping> eventAdminMappingList = event.getEventAdminMappingList();

        eventParentMappingList.stream().map((epm) -> {
            epm.getParent().getUser().getId();
            return epm;
        }).map((epm) -> {
            epm.getParent().getUser().getName();
            return epm;
        }).map((epm) -> {
            epm.getParent().getId();
            return epm;
        }).forEachOrdered((epm) -> {
            epm.getStatus();
        });

        eventTeacherMappingList.stream().map((etm) -> {
            etm.getTeacher().getUser().getId();
            return etm;
        }).map((etm) -> {
            etm.getTeacher().getUser().getName();
            return etm;
        }).map((etm) -> {
            etm.getTeacher().getId();
            return etm;
        }).forEachOrdered((etm) -> {
            etm.getStatus();
        });

        eventAdminMappingList.stream().map((eam) -> {
            eam.getAdmin().getUser().getId();
            return eam;
        }).map((eam) -> {
            eam.getAdmin().getUser().getName();
            return eam;
        }).map((eam) -> {
            eam.getAdmin().getId();
            return eam;
        }).forEachOrdered((eam) -> {
            eam.getStatus();
        });

        LOGGER.debug("Returning event");
        return event;
    }

    @Override//list(0) self events ... list(1) received events
    public List<List<Event>> getAll(Integer userId, String receiverType, Integer receiverId,
            Integer schoolId, Date startDate, Date endDate, Date eventStartDate,
            Date eventEndDate, Integer offset, boolean isSelfNeeded, boolean isReceivedNeeded) {

        return eventDao.getAll(userId, receiverType, receiverId,
                schoolId, startDate, endDate, eventStartDate, eventEndDate,
                offset, isSelfNeeded, isReceivedNeeded);
    }

    @Override
    public boolean confirm(Integer userId, Integer eventId, String role, Integer roleId, Integer status) {
        //save in redis hashmap which also stores when it would expire
        //periodically run a job iterating through redis keys, deleting the
        //expiring key-values and persisting result in mysql
//        return redisson.getMap("event_" + eventId).fastPut(userId, status);
        return eventDao.confirm(userId, eventId, role, roleId, status);
    }

}
