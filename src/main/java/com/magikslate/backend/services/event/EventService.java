/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.event;

import com.magikslate.backend.entity.Event;
import com.magikslate.backend.services.GenericService;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public interface EventService extends GenericService<Event,Integer> {
    
    public List<List<Event>> getAll(Integer userId, String receiverType, Integer receiverId, 
            Integer schoolId, Date startDate, Date endDate, Date eventStartDate,
            Date eventEndDate, Integer offset, boolean isSelfNeeded, boolean isReceivedNeeded);

    public boolean confirm(Integer userId, Integer eventId, String role, Integer roleId, Integer status);

    public Event get(Integer eventId, Integer schoolId);
}
