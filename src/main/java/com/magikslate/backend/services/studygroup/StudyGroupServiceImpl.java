/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.studygroup;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.dao.studygroup.StudyGroupDao;
import com.magikslate.backend.entity.StudyGroup;
import com.magikslate.backend.entity.StudyGroupTeacherMapping;
import com.magikslate.backend.services.GenericServiceImpl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author sankalpkulshrestha
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class StudyGroupServiceImpl extends GenericServiceImpl<StudyGroup, Integer>
        implements StudyGroupService {
    
    private StudyGroupDao studyGroupDao;
    
    @Autowired
    public StudyGroupServiceImpl(@Qualifier("studyGroupDaoImpl") GenericDao<StudyGroup, Integer> studyGroupDao) {
        
        super(studyGroupDao);
        this.studyGroupDao = (StudyGroupDao) studyGroupDao;
    }

    @Override
    public StudyGroupTeacherMapping getStudyGroupTeacherMapping(Integer ssgm) {
        return studyGroupDao.getStudyGroupTeacherMapping(ssgm);
    }

    @Override
    public List<StudyGroup> getStudyGroupBySchool(Integer schoolId, Integer offset) {
        return studyGroupDao.getStudyGroupBySchool(schoolId, offset);
    }


}
