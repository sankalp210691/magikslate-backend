/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.studygroup;

import com.magikslate.backend.entity.StudyGroup;
import com.magikslate.backend.entity.StudyGroupTeacherMapping;
import com.magikslate.backend.services.GenericService;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public interface StudyGroupService extends GenericService<StudyGroup, Integer> {

    public StudyGroupTeacherMapping getStudyGroupTeacherMapping(Integer ssgm);

    public List<StudyGroup> getStudyGroupBySchool(Integer schoolId, Integer offset);

}
