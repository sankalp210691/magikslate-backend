/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.review;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.dao.review.ParentSchoolReviewDao;
import com.magikslate.backend.dao.school.SchoolDao;
import com.magikslate.backend.entity.ParentSchoolReview;
import com.magikslate.backend.entity.School;
import com.magikslate.backend.services.GenericServiceImpl;
import com.magikslate.backend.util.Constants;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author sankalpkulshrestha
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ReviewServiceImpl extends GenericServiceImpl<ParentSchoolReview, Integer>
        implements ReviewService {

    private ParentSchoolReviewDao reviewDao;

    private SchoolDao schoolDao;
    
    @Autowired
    public ReviewServiceImpl(@Qualifier("parentSchoolReviewDaoImpl") GenericDao<ParentSchoolReview, Integer> reviewDao,
            @Qualifier("schoolDaoImpl") GenericDao<School, Integer> schoolDao) {

        super(reviewDao);
        this.reviewDao = (ParentSchoolReviewDao) reviewDao;
        this.schoolDao = (SchoolDao) schoolDao;
    }

    @Override
    public Integer add(ParentSchoolReview parentSchoolReview) {
        int id = reviewDao.add(parentSchoolReview);
        School school = schoolDao.find(parentSchoolReview.getSchool().getId());
        List<ParentSchoolReview> parentSchoolReviewList = getAll(null, null, school.getId(), Constants.OLD_DATE, Constants.FUTURE_DATE, 0, true, false);
        school.setRatingCount(parentSchoolReviewList.size());
        school.setAverageRating(getAverageRating(parentSchoolReviewList));
        schoolDao.update(school);
        return id;
    }

    @Override
    public List<ParentSchoolReview> getAll(String role, Integer roleId, Integer schoolId, Date startDate, Date endDate, Integer offset, boolean filterBySchool, boolean filterByRoleId) {
        return reviewDao.getAll(role, roleId, schoolId, startDate, endDate, offset, filterBySchool, filterByRoleId);
    }

    @Override
    public boolean updateParentSchoolReview(ParentSchoolReview psr, Integer currentUserRoleId) {
        Date currentDate = new Date();
        ParentSchoolReview parentSchoolReview = reviewDao.find(psr.getId());
        if (parentSchoolReview == null || !parentSchoolReview.getParent().getId().equals(currentUserRoleId)) {
            return false;
        }
        if (psr.getRating() >= 0) {
            parentSchoolReview.setRating(psr.getRating());
        }
        if (psr.getReview() != null) {
            parentSchoolReview.setReview(psr.getReview());
        }
        parentSchoolReview.setUpdatedAt(currentDate);
        reviewDao.update(parentSchoolReview);
        School school = schoolDao.find(parentSchoolReview.getSchool().getId());
        List<ParentSchoolReview> parentSchoolReviewList = getAll(null, null, school.getId(), Constants.OLD_DATE, Constants.FUTURE_DATE, 0, true, false);
        school.setRatingCount(parentSchoolReviewList.size());
        school.setAverageRating(getAverageRating(parentSchoolReviewList));
        schoolDao.update(school);
        return true;
    }

    @Override
    public double getAverageRating(List<ParentSchoolReview> reviewList) {
        if (reviewList == null || reviewList.isEmpty()) {
            return 0;
        } else {
            int sum = 0;
            for (ParentSchoolReview review : reviewList) {
                sum += review.getRating();
            }
            return (double) sum / reviewList.size();
        }
    }

    //TODO: we can abvoid this ... now storing ratingcount in school table itself...we can change the implementaion
    @Override
    public long getReviewCountForSchool(Integer schoolId) {
        return reviewDao.getReviewCountForSchool(schoolId);
    }

}
