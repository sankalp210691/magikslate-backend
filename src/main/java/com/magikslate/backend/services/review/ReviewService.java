/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.review;

import com.magikslate.backend.entity.ParentSchoolReview;
import com.magikslate.backend.services.GenericService;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public interface ReviewService extends GenericService<ParentSchoolReview, Integer> {
    
    @Override
    public Integer add(ParentSchoolReview parentSchoolReview);

    public List<ParentSchoolReview> getAll(String role, Integer roleId, Integer schoolId, Date startDate, Date endDate, Integer offset, boolean filterBySchool, boolean filterByRoleId);

    public boolean updateParentSchoolReview(ParentSchoolReview psr, Integer currentUserRoleId);

    public double getAverageRating(List<ParentSchoolReview> reviewList);

    public long getReviewCountForSchool(Integer schoolId);
    
}
