package com.magikslate.backend.services.school;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.dao.admin.AdminDao;
import com.magikslate.backend.dao.school.SchoolDao;
import com.magikslate.backend.entity.Admin;
import com.magikslate.backend.entity.School;
import com.magikslate.backend.entity.SchoolInstituteCategoryMapping;
import com.magikslate.backend.entity.User;
import com.magikslate.backend.services.GenericServiceImpl;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author sankalpkulshrestha
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SchoolServiceImpl extends GenericServiceImpl<School, Integer>
        implements SchoolService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SchoolServiceImpl.class);

    private SchoolDao schoolDao;
    private AdminDao adminDao;

    @Autowired
    public SchoolServiceImpl(@Qualifier("schoolDaoImpl") GenericDao<School, Integer> schoolDao,
            @Qualifier("adminDaoImpl") GenericDao<Admin, Integer> adminDao) {
        super(schoolDao);
        this.schoolDao = (SchoolDao) schoolDao;
        this.adminDao = (AdminDao) adminDao;
    }

    @Override
    public Integer add(School school, User user, List<SchoolInstituteCategoryMapping> schoolInstituteCategoryMappingList) {
        Integer id = schoolDao.add(school);
        LOGGER.debug("school created with id = {}", id);

        Admin admin = new Admin();
        admin.setUser(user);
        admin.setSelfCreated(Boolean.TRUE);
        admin.setSchool(school);
        admin.setCreatedAt(new Date());
        Integer adminId = adminDao.add(admin);
        LOGGER.debug("Admin created with id = {}", admin);
        admin.setId(adminId);
        List<Admin> adminList = new ArrayList();
        adminList.add(admin);
        school.setAdminList(adminList);
        LOGGER.debug("Returning id = {}", id);
        return id;
    }

    @Override
    public School get(Integer id) {
        School school = schoolDao.find(id);
        LOGGER.debug("Got school = {} for id = {} from DB", school, id);
        if (school == null) {
            return null;
        }
        school.getClassList().forEach((clazz) -> {
            clazz.toString();
        });
        school.setSchoolInstituteCategoryMappingList(school.getSchoolInstituteCategoryMappingList().stream().filter(sicm -> sicm.getDeletedAt() == null).collect(Collectors.toList()));
        List<Admin> adminList = school.getAdminList();
        if (adminList != null) {
            adminList.forEach((admin) -> {
                int adminId = admin.getId();
            });
        }
        return school;
    }

    @Override
    public void updateDisplayPic(Integer schoolId, String displayPicUrl) {
        School school = schoolDao.find(schoolId);
        school.setDisplayPic(displayPicUrl);
    }

    @Override
    public Integer increaseStage(Integer schoolId) {
        School school = schoolDao.find(schoolId);
        if (school.getStage() < 5) {
            school.setStage(school.getStage() + 1);
        }
        return school.getStage();
    }

    @Override
    public Integer getAdminId(Integer userId, Integer id) {
        return adminDao.getAdminAccessId(userId, id);
    }

    @Override
    public void updateCoverPic(Integer schoolId, String coverPicUrl) {
        School school = schoolDao.find(schoolId);
        school.setCoverPic(coverPicUrl);
    }

    @Override
    public void update(School school) {
        if (school == null || school.getId() == null) {
            return;
        }
        Date today = new Date();
        School sch = schoolDao.find(school.getId());
        
        sch.setName(school.getName());
        sch.setAbout(school.getAbout());
        sch.setAddress(school.getAddress());
        sch.setStudentCount(school.getStudentCount());
        sch.setTeacherCount(school.getTeacherCount());
        sch.setLatitude(school.getLatitude());
        sch.setLongitude(school.getLongitude());
        sch.setBoard(school.getBoard());
        sch.setLocality(school.getLocality());
        sch.setPincode(school.getPincode());
        sch.setUpdatedAt(today);
        
        List<SchoolInstituteCategoryMapping> existingSicmList = sch.getSchoolInstituteCategoryMappingList().stream().filter(sicm -> sicm.getDeletedAt() == null).collect(Collectors.toList());
        List<SchoolInstituteCategoryMapping> newSicmList = school.getSchoolInstituteCategoryMappingList();
        
        if(newSicmList == null || newSicmList.isEmpty()) {
            if(!existingSicmList.isEmpty()) {
                existingSicmList.forEach(sicm -> {
                    sicm.setDeletedAt(today);
                });
            }
        } else {
            Set<Integer> newIcIdSet = new HashSet();
            Set<Integer> existingIcIdSet = new HashSet();
            newSicmList.forEach(sicm -> {
                newIcIdSet.add(sicm.getInstituteCategory().getId());
            });
            existingSicmList.forEach(sicm -> {
                existingIcIdSet.add(sicm.getInstituteCategory().getId());
            });
            
            for (SchoolInstituteCategoryMapping sicm : existingSicmList) {
                if (!newIcIdSet.contains(sicm.getInstituteCategory().getId())) {
                    sicm.setDeletedAt(today);
                }
            }
            
            List<SchoolInstituteCategoryMapping> finalSicmList = new ArrayList();
            newSicmList.forEach(sicm -> {
                if (!existingIcIdSet.contains(sicm.getInstituteCategory().getId())) {
                    finalSicmList.add(sicm);
                }
            });
            
            sch.setSchoolInstituteCategoryMappingList(finalSicmList);
        }
    }

}
