/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.school;

import com.magikslate.backend.entity.School;
import com.magikslate.backend.entity.SchoolInstituteCategoryMapping;
import com.magikslate.backend.entity.User;
import com.magikslate.backend.services.GenericService;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public interface SchoolService extends GenericService<School,Integer> {

    public Integer add(School school, User user, List<SchoolInstituteCategoryMapping> schoolInstituteCategoryMappingList);

    public void updateDisplayPic(Integer schoolId, String displayPicUrl);

    public Integer increaseStage(Integer schoolId);

    public Integer getAdminId(Integer userId, Integer id);

    public void updateCoverPic(Integer schoolId, String coverPicUrl);
    
    @Override
    public void update(School school);
}
