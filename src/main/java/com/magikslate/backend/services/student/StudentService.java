/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.student;

import com.magikslate.backend.entity.Student;
import com.magikslate.backend.exception.EntityExistsException;
import com.magikslate.backend.onboarding.StudentInput;
import com.magikslate.backend.services.GenericService;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public interface StudentService extends GenericService<Student, Integer> {

    public List<Student> getStudentListBySchool(Integer schoolId);

    public List<Student> createBulk(List<StudentInput> studentInputList, Integer schoolId);

    public void updateProfilePic(Integer studentId, String profilePicUrl);
    
    public Student add(StudentInput studentInput, Integer schoolId) throws EntityExistsException;
    
    @Override
    public void remove(Student student);

    @Override
    public void update(Student student);

}
