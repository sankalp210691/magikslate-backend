/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.student;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.dao.attendance.AttendanceDao;
import com.magikslate.backend.dao.school.SchoolDao;
import com.magikslate.backend.dao.student.StudentDao;
import com.magikslate.backend.entity.Attendance;
import com.magikslate.backend.entity.Class;
import com.magikslate.backend.entity.Parent;
import com.magikslate.backend.entity.School;
import com.magikslate.backend.entity.Student;
import com.magikslate.backend.entity.StudentClassMapping;
import com.magikslate.backend.entity.StudentParentMapping;
import com.magikslate.backend.entity.StudentStudyGroupMapping;
import com.magikslate.backend.entity.User;
import com.magikslate.backend.exception.EntityExistsException;
import com.magikslate.backend.exception.InvalidOnboardingInputException;
import com.magikslate.backend.onboarding.StudentInput;
import com.magikslate.backend.onboarding.StudentInput.ParentInput;
import com.magikslate.backend.rest.StudentController;
import com.magikslate.backend.services.GenericServiceImpl;
import com.magikslate.backend.services.clazz.ClassService;
import com.magikslate.backend.services.parent.ParentService;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import java.time.Year;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author sankalpkulshrestha
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class StudentServiceImpl extends GenericServiceImpl<Student, Integer>
        implements StudentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentController.class);

    private StudentDao studentDao;

    private SchoolDao schoolDao;

    private AttendanceDao attendanceDao;

    @Autowired
    private ParentService parentService;

    @Autowired
    private ClassService classService;

    @Autowired
    public StudentServiceImpl(@Qualifier("studentDaoImpl") GenericDao<Student, Integer> studentDao,
            @Qualifier("schoolDaoImpl") GenericDao<School, Integer> schoolDao,
            @Qualifier("attendanceDaoImpl") GenericDao<Attendance, Integer> attendanceDao) {
        super(studentDao);
        this.studentDao = (StudentDao) studentDao;
        this.schoolDao = (SchoolDao) schoolDao;
        this.attendanceDao = (AttendanceDao) attendanceDao;
    }

    @Override
    public Student get(Integer id) {
        Student student = studentDao.find(id);

        List<StudentClassMapping> studentClassMappingList = student.getStudentClassMappingList();

        if (studentClassMappingList != null && !studentClassMappingList.isEmpty()) {

            Date latestCreatedAt = studentClassMappingList.get(0).getCreatedAt();
            StudentClassMapping latestScm = studentClassMappingList.get(0);

            for (StudentClassMapping scm : studentClassMappingList) {

                if (scm.getCreatedAt().after(latestCreatedAt)) {

                    latestCreatedAt = scm.getCreatedAt();
                    latestScm = scm;
                }
            }

            LOGGER.info("{}", latestScm.getAttendanceList());

            student.setStudentClassMappingList(new ArrayList());
            student.getStudentClassMappingList().add(latestScm);
        }
        return student;
    }

    @Override
    public List<Student> getStudentListBySchool(Integer schoolId) {
        List<Student> studentList = studentDao.getStudentListBySchool(schoolId);

        for (Student student : studentList) {

            List<StudentParentMapping> studentParentMappingList = student.getStudentParentMappingList();
            Iterator<StudentParentMapping> spmIterator = studentParentMappingList.iterator();

            while (spmIterator.hasNext()) {
                StudentParentMapping spm = spmIterator.next();

                if (spm.getDeletedAt() != null) {
                    spmIterator.remove();
                } else {
                    spm.getParent();
                    spm.getParent().getUser().toString();
                }
            }
            
            List<StudentClassMapping> studentClassMappingList = student.getStudentClassMappingList();
            Iterator<StudentClassMapping> scmIterator = studentClassMappingList.iterator();
            
            while(scmIterator.hasNext()) {
                StudentClassMapping scm = scmIterator.next();
                
                if(scm.getDeletedAt() != null) {
                    scmIterator.remove();
                } else {
                    scm.getClass1().toString();
                    
                    List<Attendance> attendanceList = scm.getAttendanceList();
                    Iterator<Attendance> attendanceIterator = attendanceList.iterator();
                    
                    while(attendanceIterator.hasNext()) {
                        Attendance attendance = attendanceIterator.next();
                        attendance.toString();
                    }
                }
            }
            
            List<StudentStudyGroupMapping> studentStudyGroupMappingList = student.getStudentStudyGroupMappingList();
            Iterator<StudentStudyGroupMapping> ssgmIterator = studentStudyGroupMappingList.iterator();
            
            while(ssgmIterator.hasNext()) {
                StudentStudyGroupMapping ssgm = ssgmIterator.next();
                
                if(ssgm.getDeletedAt() != null) {
                    ssgmIterator.remove();
                } else {
                    ssgm.getStudyGroup().toString();
                }
            }
        }
        return studentList;
    }

    @Override
    public List<Student> createBulk(List<StudentInput> studentInputList, Integer schoolId) {
        Map<String, Student> studentMap = handleStudentCreation(studentInputList, schoolId);
        if (schoolDao.updateStage(4, schoolId)) {
            return new ArrayList(studentMap.values());
        } else {
            return null;
        }
    }

    private Map<String, Student> handleStudentCreation(List<StudentInput> studentInputList, Integer schoolId) {
        School school = schoolDao.find(schoolId);
        List<Class> classList = classService.getClassListBySchool(schoolId, null);
        Map<String, Class> classMap = new HashMap();
        classList.forEach(clazz -> {
            classMap.put(Utils.getClassCode(clazz.getStandard(), clazz.getSection()), clazz);
        });

        studentInputList.forEach((studentInput) -> {
            String classCode = Utils.getClassCode(studentInput.getStandard(), studentInput.getSection());
            if (!classMap.containsKey(classCode)) {
                LOGGER.info("One or more invalid classes for admission code {}", studentInput.getAdmissionCode());
                throw new InvalidOnboardingInputException("One or more invalid classes for admission code " + studentInput.getAdmissionCode());
            }
        });

        Date date = new Date();

        Map<String, Parent> parentMap = createParentMap(studentInputList, date);
        Map<String, Student> studentMap = createStudentMap(studentInputList, school, date);
        createStudentClassMapping(studentInputList, studentMap, classMap, date);
        createStudentParentMapping(studentInputList, studentMap, parentMap, date);
        school.setUpdatedAt(date);
        school.setStudentCount(this.getStudentListBySchool(schoolId).size());
        return studentMap;
    }

    private Map<String, Parent> createParentMap(List<StudentInput> studentInputList, Date date) {
        LOGGER.debug("Creating parent map");

        Map<String, Parent> parentMap = new HashMap();
        List<Parent> parentList = new ArrayList();
        Set<String> phoneNumberSet = new HashSet();

        studentInputList.forEach(studentInput -> {
            List<ParentInput> parentInputList = studentInput.getParentList();
            parentInputList.forEach(parentInput -> {
                if (!phoneNumberSet.contains(parentInput.getPhone())) {
                    Parent parent = new Parent();
                    parent.setAddress(parentInput.getAddress());

                    Integer dummyPasswordOtp = Utils.generateOtp(Constants.DUMMY_PASSWORD_LENGTH);

                    User user = new User();
                    user.setStatus(Constants.REG_PASSWORD_CHANGE_PENDING);
                    user.setEmail(parentInput.getEmail());
                    user.setPhone(parentInput.getPhone());
                    user.setPassword(DigestUtils.sha256Hex(String.valueOf(dummyPasswordOtp)));
                    user.setOtp(dummyPasswordOtp);
                    user.setName(parentInput.getName());
                    user.setCreatedAt(date);
                    parent.setUser(user);
                    parent.setCreatedAt(date);
                    phoneNumberSet.add(parentInput.getPhone());
                    parentList.add(parent);
                }
            });
        });

        List<Integer> parentIdList = parentService.createBulk(parentList);
        int index = 0;
        for (Parent parent : parentList) {
            parent.setId(parentIdList.get(index));
            parentMap.put(parent.getUser().getPhone(), parent);
            index++;
        }
        return parentMap;
    }

    private Map<String, Student> createStudentMap(List<StudentInput> studentInputList,
            School school, Date date) {

        Map<String, Student> studentMap = new HashMap();

        studentInputList.forEach(studentInput -> {
            Student student = new Student();
            student.setName(studentInput.getName());
            student.setAdmissionCode(studentInput.getAdmissionCode());
            student.setDateOfBirth(studentInput.getDob());
            student.setSchool(school);
            student.setGender(studentInput.getGender());
            student.setProfilePic(studentInput.getProfilePic());
            student.setCreatedAt(date);

            studentMap.put(student.getAdmissionCode(), student);
        });
        List<Integer> studentIdList = studentDao.createBulk(new ArrayList(studentMap.values()), school.getId());

        int index = 0;
        for (Map.Entry<String, Student> keyValue : studentMap.entrySet()) {
            Student student = keyValue.getValue();
            student.setId(studentIdList.get(index));
            index++;
        }
        return studentMap;
    }

    private void createStudentClassMapping(List<StudentInput> studentInputList,
            Map<String, Student> studentMap, Map<String, Class> classMap, Date date) {

        List<StudentClassMapping> scmList = new ArrayList();

        studentInputList.forEach(studentInput -> {
            Student student = studentMap.get(studentInput.getAdmissionCode());
            Class clazz = classMap.get(Utils.getClassCode(studentInput.getStandard(), studentInput.getSection()));

            StudentClassMapping studentClassMapping = new StudentClassMapping();
            studentClassMapping.setStudent(student);
            studentClassMapping.setCreatedAt(date);
            studentClassMapping.setSessionYear(Year.now().getValue());
            studentClassMapping.setClass1(classMap.get(Utils.getClassCode(studentInput.getStandard(), studentInput.getSection())));

            List<StudentClassMapping> studentClassMappingList = student.getStudentClassMappingList();
            if (studentClassMappingList == null) {
                studentClassMappingList = new ArrayList();
                student.setStudentClassMappingList(studentClassMappingList);
            }
            studentClassMappingList.add(studentClassMapping);

            scmList.add(studentClassMapping);
        });
        studentDao.getExistingStudentClassMappingEntitiesList(scmList);
        studentDao.mapToClass(scmList);
    }

    private void createStudentParentMapping(List<StudentInput> studentInputList,
            Map<String, Student> studentMap, Map<String, Parent> parentMap, Date date) {

        List<StudentParentMapping> spmList = new ArrayList();

        studentInputList.forEach(studentInput -> {
            Student student = studentMap.get(studentInput.getAdmissionCode());

            List<StudentParentMapping> studentParentMappingList = student.getStudentParentMappingList();
            if (studentParentMappingList == null) {
                studentParentMappingList = new ArrayList();
                student.setStudentParentMappingList(studentParentMappingList);
            }

            for (ParentInput parentInput : studentInput.getParentList()) {
                Parent parent = parentMap.get(parentInput.getPhone());

                StudentParentMapping spm = new StudentParentMapping();
                spm.setCreatedAt(date);
                spm.setStudent(student);
                spm.setParent(parent);
                spmList.add(spm);
                studentParentMappingList.add(spm);
            }
        });
        studentDao.getStudentParentMappingEntitiesList(spmList);
        studentDao.mapToParent(spmList);
    }

    @Override
    public Student add(StudentInput studentInput, Integer schoolId) throws EntityExistsException {
        if (studentInput == null) {
            return null;
        }

        List<StudentInput> studentInputList = new ArrayList();
        studentInputList.add(studentInput);
        Map<String, Student> studentMap = handleStudentCreation(studentInputList, schoolId);

        Student student = studentMap.get(studentInput.getAdmissionCode());
        if (student == null) {
            return null;
        }
        return student;
    }

    @Override
    public void updateProfilePic(Integer studentId, String profilePicUrl) {
        Student student = studentDao.find(studentId);
        student.setProfilePic(profilePicUrl);
    }

    @Override
    public void remove(Student student) {
        student = studentDao.find(student.getId());
        student.setDeletedAt(new Date());
    }

    @Override
    public void update(Student student) {
        if (student != null && student.getId() != null) {

            Date today = new Date();

            Student st = studentDao.find(student.getId());
            st.setName(student.getName());
            st.setDateOfBirth(student.getDateOfBirth());
            st.setGender(student.getGender());
            st.setAdmissionCode(student.getAdmissionCode());
            st.setProfilePic(student.getProfilePic());

            //classes
            List<StudentClassMapping> studentClassMappingList = st.getStudentClassMappingList();
            if (studentClassMappingList != null) {
                for (StudentClassMapping scm : studentClassMappingList) {
                    if (scm.getDeletedAt() == null) {
                        StudentClassMapping studentScm = (student.getStudentClassMappingList()).get(0);
                        if (!scm.getClass1().getStandard().equals(studentScm.getClass1().getStandard())
                                || !scm.getClass1().getSection().equals(studentScm.getClass1().getSection())) {

                            List<Class> classList = classService.getClassListBySchool(student.getSchool().getId(), null);
                            if (scm.getSessionYear().equals(Year.now().getValue())) {

                                classList.stream().filter((clazz) -> (clazz.getStandard().equals(studentScm.getClass1().getStandard())
                                        && clazz.getSection().equals(studentScm.getClass1().getSection()))).forEachOrdered((clazz) -> {

                                    scm.setClass1(clazz);
                                });
                            } else {
                                scm.setDeletedAt(today);

                                List<StudentClassMapping> scmList = new ArrayList();

                                classList.stream().filter((clazz) -> (clazz.getStandard().equals(studentScm.getClass1().getStandard())
                                        && clazz.getSection().equals(studentScm.getClass1().getSection()))).forEachOrdered((clazz) -> {

                                    StudentClassMapping studentClassMapping = new StudentClassMapping();
                                    studentClassMapping.setStudent(st);
                                    studentClassMapping.setCreatedAt(today);
                                    studentClassMapping.setSessionYear(Year.now().getValue());
                                    studentClassMapping.setClass1(clazz);
                                    scmList.add(studentClassMapping);
                                });

                                st.setStudentClassMappingList(scmList);
                                break;
                            }
                        }
                    }
                }
            }

            //parents
            List<StudentParentMapping> studentParentMappingList = student.getStudentParentMappingList();
            Set<Integer> parentIdSet = new HashSet();

            if (studentParentMappingList != null) {

                studentParentMappingList.stream().filter((spm) -> (spm.getParent().getId() != null)).forEachOrdered((spm) -> {
                    parentIdSet.add(spm.getParent().getId());
                });
            } else {
                studentParentMappingList = new ArrayList();
            }

            List<StudentParentMapping> spmList = st.getStudentParentMappingList();
            if (spmList != null) {

                Iterator<StudentParentMapping> iterator = spmList.iterator();
                while (iterator.hasNext()) {
                    StudentParentMapping spm = (StudentParentMapping) iterator.next();

                    if (spm.getDeletedAt() == null) {
                        if (!parentIdSet.contains(spm.getParent().getId())) {
                            spm.setDeletedAt(today);
                        }
                    }
                }
            }

            List<ParentInput> newParentList = new ArrayList();

            studentParentMappingList.stream().filter((spm) -> (spm.getParent().getId() == null)).map((spm) -> new ParentInput(spm.getParent().getUser().getName(),
                    spm.getParent().getUser().getEmail(),
                    spm.getParent().getUser().getPhone(),
                    spm.getParent().getAddress(), 0)).forEachOrdered((parentInput) -> {
                newParentList.add(parentInput);
            });

            if (!newParentList.isEmpty()) {

                StudentClassMapping studentScm = (student.getStudentClassMappingList()).get(0);

                StudentInput studentInput = new StudentInput(student.getAdmissionCode(),
                        student.getName(), student.getDateOfBirth(), studentScm.getClass1().getStandard(),
                        studentScm.getClass1().getSection(), student.getGender(), newParentList, null);
                List<StudentInput> studentInputList = new ArrayList();
                studentInputList.add(studentInput);

                Map<String, Student> studentMap = new HashMap();
                studentMap.put(student.getAdmissionCode(), student);

                Map<String, Parent> parentMap = createParentMap(studentInputList, today);
                createStudentParentMapping(studentInputList, studentMap, parentMap, today);
            }
        }
    }

}
