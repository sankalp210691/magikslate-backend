/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.locality;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.dao.locality.LocalityDao;
import com.magikslate.backend.entity.Locality;
import com.magikslate.backend.services.GenericServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author sankalpkulshrestha
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class LocalityServiceImpl extends GenericServiceImpl<Locality, Integer>
        implements LocalityService {

    private LocalityDao localityDao;

    @Autowired
    public LocalityServiceImpl(@Qualifier("localityDaoImpl") GenericDao<Locality, Integer> localityDao) {
        super(localityDao);
        this.localityDao = (LocalityDao) localityDao;
    }
}
