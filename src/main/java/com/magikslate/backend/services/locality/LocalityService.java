/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.locality;

import com.magikslate.backend.entity.Locality;
import com.magikslate.backend.services.GenericService;


/**
 *
 * @author sankalpkulshrestha
 */
public interface LocalityService extends GenericService<Locality,Integer> {

}
