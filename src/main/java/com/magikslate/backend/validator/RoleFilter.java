/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.validator;

import com.magikslate.backend.services.user.UserService;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

/**
 *
 * @author sankalpkulshrestha
 */
@Component
public class RoleFilter extends GenericFilterBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(RoleFilter.class);

    @Autowired
    private UserService userService;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse res = (HttpServletResponse) response;

        final String roleAuthHeader = req.getHeader("roleauth");

        if ("OPTIONS".equals(req.getMethod())) {
            LOGGER.info("Received OPTIONS request. Returning status = {}", HttpServletResponse.SC_OK);
            res.setStatus(HttpServletResponse.SC_OK);

            chain.doFilter(request, response);
        } else {

            if (roleAuthHeader == null || !roleAuthHeader.startsWith("Bearer ")) {
                LOGGER.info("roleAuthHeader is null or doesn't start with Bearer. Returning {}", HttpServletResponse.SC_UNAUTHORIZED);
                res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Role not chosen");
            } else {

                final String token = roleAuthHeader.substring(7);
                Map<String, Object> claimMap = Utils.decipherRoleAuthToken(token);
                Date expirationDate = (Date) claimMap.get("exp");
                String role = (String) claimMap.get("role");
                Integer roleId = (Integer) claimMap.get("roleId");
                Integer userId = (Integer) claimMap.get("userId");
                Integer schoolId = (Integer) claimMap.get("schoolId");
                Integer studentId = (Integer) claimMap.get("studentId");

                Integer authUserId = (Integer) request.getAttribute("userId");
                if (!Objects.equals(authUserId, userId)) {
                    
                    LOGGER.info("authUserId and userId do not match.Suspicious token behaviour. Returning {}", HttpServletResponse.SC_UNAUTHORIZED);
                    res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Suspicious token behaviour"); //userId mentioned in authtoken and roleAuthToken do not match. Possible malicious user
                } else {
                    if (userId == null || Utils.isNullOrEmpty(role) || roleId == null || schoolId == null || (Constants.PARENT.equals(role.toUpperCase()) && studentId == null)) {
                        
                        LOGGER.info("Invalid role token because userId is null or doesn't have proper role or schoolId is null or student id is null despite being parent role user. Returning {}",
                                HttpServletResponse.SC_UNAUTHORIZED);
                        res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid role token");
                    } else {
                        Date currentDate = new Date();
                        if (currentDate.after(expirationDate)) {
                            
                            String roleAuthToken = userService.hasRoleAccess(role, userId, roleId, schoolId, studentId);
                            if (roleAuthToken == null) {
                                
                                LOGGER.info("roleAuthToken is null. You no longer have access. Returning {}", HttpServletResponse.SC_UNAUTHORIZED);
                                res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "You no longer have access");
                            } else {
                                res.setHeader("roleauth", "Bearer " + roleAuthToken);
                                request.setAttribute("ruserId", userId);
                                request.setAttribute("role", role);
                                request.setAttribute("roleId", roleId);
                                request.setAttribute("schoolId", schoolId);
                                request.setAttribute("studentId", studentId);
                                
                                String requestMetaData = userId + Constants.UNDERSCORE_SEPARATOR + role 
                                        + Constants.UNDERSCORE_SEPARATOR + roleId 
                                        + Constants.UNDERSCORE_SEPARATOR + schoolId
                                        + Constants.UNDERSCORE_SEPARATOR + studentId;
                                MDC.put("requestMetaData", requestMetaData);
                                
                                chain.doFilter(request, res);
                                MDC.remove("requestMetaData");
                            }
                        } else {
                            request.setAttribute("ruserId", userId);
                            request.setAttribute("role", role);
                            request.setAttribute("roleId", roleId);
                            request.setAttribute("schoolId", schoolId);
                            request.setAttribute("studentId", studentId);
                            
                            String requestMetaData = userId + Constants.UNDERSCORE_SEPARATOR + role 
                                        + Constants.UNDERSCORE_SEPARATOR + roleId 
                                        + Constants.UNDERSCORE_SEPARATOR + schoolId
                                        + Constants.UNDERSCORE_SEPARATOR + studentId;
                            MDC.put("requestMetaData", requestMetaData);
                            
                            chain.doFilter(request, res);
                            MDC.remove("requestMetaData");
                        }
                    }
                }
            }
        }
    }

}
