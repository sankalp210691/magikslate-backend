/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.validator;

import com.magikslate.backend.util.Utils;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author sankalpkulshrestha
 */
public class ClassTypeValidator implements ConstraintValidator<ValidClassType, String> {
    
    @Override
    public void initialize(ValidClassType constraintAnnotation) {       
    }
    
    @Override
    public boolean isValid(String classType, ConstraintValidatorContext context){   
        return (validateClassType(classType));
    }
    
    private boolean validateClassType(String classType) {
        return Utils.sanitizeClassStandard(classType) != null;
    }
}
