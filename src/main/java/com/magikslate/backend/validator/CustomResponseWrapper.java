/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.validator;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

/**
 *
 * @author sankalpkulshrestha
 */
public class CustomResponseWrapper extends HttpServletResponseWrapper {

    public CustomResponseWrapper(HttpServletResponse response) {
        super(response);
    }
}
