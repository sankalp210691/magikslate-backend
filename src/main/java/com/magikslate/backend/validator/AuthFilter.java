/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.validator;

import com.magikslate.backend.util.Utils;
import io.jsonwebtoken.ExpiredJwtException;
import java.io.IOException;
import java.util.Map;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.GenericFilterBean;

/**
 *
 * @author sankalpkulshrestha
 */
public class AuthFilter extends GenericFilterBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthFilter.class);

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse res = (HttpServletResponse) response;

        final String authHeader = req.getHeader("authorization");

        if ("OPTIONS".equals(req.getMethod())) {
            LOGGER.info("Received OPTIONS request. Returning status = {}", HttpServletResponse.SC_OK);
            res.setStatus(HttpServletResponse.SC_OK);
            LOGGER.info("Passing through filters");
            chain.doFilter(request, response);
        } else {

            if (authHeader == null || !authHeader.startsWith("Bearer ")) {
                LOGGER.info("Invalid authHeader = {}. Returning status = {}", HttpServletResponse.SC_UNAUTHORIZED);
                res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthenticated request");
            } else {

                final String token = authHeader.substring(7);
                LOGGER.debug("Received request with token = {}", token);
                try {
                    Map<String, Object> claimMap = Utils.decipherUserAuthToken(token);
                    Integer userId = (Integer) claimMap.get("userId");
                    if (userId == null) {
                        LOGGER.info("Extracted userId is NULL. Returning status = {}", HttpServletResponse.SC_UNAUTHORIZED);
                        res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid token. UserId not found");
                    } else {
                        LOGGER.info("Request userId = {}", userId);
                        request.setAttribute("userId", userId);
                        chain.doFilter(request, response);
                    }

                } catch (ExpiredJwtException ex) {
                    LOGGER.info("ExpiredJwtException ex = {}. Returning status = {}", ex.toString(), HttpServletResponse.SC_UNAUTHORIZED);
                    res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Auth Token expired");
                }
            }
        }
    }

}