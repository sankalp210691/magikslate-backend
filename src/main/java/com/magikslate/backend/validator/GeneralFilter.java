/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.validator;

import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import java.io.IOException;
import java.util.UUID;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.web.filter.GenericFilterBean;

/**
 *
 * @author sankalpkulshrestha
 */
public class GeneralFilter extends GenericFilterBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(GeneralFilter.class);

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String requestURI = httpServletRequest.getRequestURI();
        String requestId = null;
        if (!Constants.HEALTHCHECK_URL.equals(requestURI)) {
            requestId = UUID.randomUUID().toString();
            MDC.put("requestId", requestId);
        }
        String ip = Utils.getClientIp(httpServletRequest);
        MDC.put("ip", ip);

        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse res = (HttpServletResponse) response;

        if ("OPTIONS".equals(req.getMethod())) {
            LOGGER.info("Received OPTIONS request. Returning status = {}", HttpServletResponse.SC_OK);
            res.setStatus(HttpServletResponse.SC_OK);

            LOGGER.info("Passing through filters");
            chain.doFilter(request, response);
            MDC.remove("requestId");
        } else {

            if (requestId != null) {
                HttpServletResponse resp = (HttpServletResponse) response;
                CustomResponseWrapper customResponse = new CustomResponseWrapper(resp);
                customResponse.addHeader("request-id", requestId);
            }

            chain.doFilter(request, response);

            if (requestId != null) {
                MDC.remove("requestId");
            }
        }
        MDC.remove("ip");
    }

}
