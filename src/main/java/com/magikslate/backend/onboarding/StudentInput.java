/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.onboarding;

import com.magikslate.backend.exception.InvalidOnboardingInputException;
import com.magikslate.backend.util.Utils;
import io.vertx.core.json.JsonObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author sankalpkulshrestha
 */
public class StudentInput {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(StudentInput.class);

    private String admissionCode;
    private String name;
    private String dob;
    private String gender;
    private String standard;
    private String section;
    private String profilePic;
    private List<ParentInput> parentList;

    private String json;

    public StudentInput(String admissionCode, String name, String dob, String standard, String section, String gender, List<ParentInput> parentList, String profilePic) {
        if (Utils.isNullOrEmpty(admissionCode) || Utils.isNullOrEmpty(name) || dob == null
                || Utils.isNullOrEmpty(standard) || Utils.isNullOrEmpty(section) || parentList == null || parentList.isEmpty()) {
            
            LOGGER.info("One or more compulsory fields are empty");
            throw new InvalidOnboardingInputException("One or more compulsory fields are empty");
        }
        this.admissionCode = admissionCode;
        this.name = name;
        this.dob = dob;
        this.gender = gender;
        this.standard = Utils.sanitizeClassStandard(standard);
        this.section = Utils.sanitizeSectionType(section);
        this.parentList = parentList;
        this.profilePic = profilePic;

        Map<String, Object> jsonMap = new HashMap();
        jsonMap.put("admissionCode", admissionCode);
        jsonMap.put("name", name);
        jsonMap.put("dob", dob);
        jsonMap.put("gender", gender);
        jsonMap.put("standard", standard);
        jsonMap.put("parentList", parentList);
        json = (new JsonObject(jsonMap)).toString();

    }

    public static class ParentInput {
        
        private static final Logger LOGGER = LoggerFactory.getLogger(ParentInput.class);

        private String name;
        private String email;
        private String phone;
        private String address;

        public ParentInput(String name, String email, String phone, String address, int row) {
            if (!Utils.isValidPhone(phone) || Utils.isNullOrEmpty(name)) {
                
                LOGGER.info("Name or phone is empty or invalid. Check row {}", row);
                throw new InvalidOnboardingInputException("Name or phone is empty or invalid. Check row " + row);
            } else {
                this.name = name;
                this.email = Utils.isValidEmail(email) ? email : null;
                this.phone = phone;
                this.address = Utils.isNullOrEmpty(address)? null: address;
            }
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @return the email
         */
        public String getEmail() {
            return email;
        }

        /**
         * @return the phone
         */
        public String getPhone() {
            return phone;
        }

        /**
         * @return the address
         */
        public String getAddress() {
            return address;
        }

    }

    /**
     * @return the admissionCode
     */
    public String getAdmissionCode() {
        return admissionCode;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * @return the standard
     */
    public String getStandard() {
        return standard;
    }

    /**
     * @return the section
     */
    public String getSection() {
        return section;
    }

    /**
     * @return the parentList
     */
    public List<ParentInput> getParentList() {
        return parentList;
    }

    @Override
    public String toString() {
        return json;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @return the profilePic
     */
    public String getProfilePic() {
        return profilePic;
    }
}
