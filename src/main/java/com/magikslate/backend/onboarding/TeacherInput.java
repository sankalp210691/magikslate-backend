/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.onboarding;

import com.magikslate.backend.exception.InvalidOnboardingInputException;
import com.magikslate.backend.util.Utils;
import io.vertx.core.json.JsonObject;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author sankalpkulshrestha
 */
public class TeacherInput {

    private String employeeId;
    private String name;
    private String phone;
    private String email;
    
    private Integer classTeacherClassId;
    private String classTeacherStandard;
    private String classTeacherSection;

    private String json;

    public TeacherInput(String employeeId, String name, String phone, String email,
            String classTeacherStandard, String classTeacherSection) {
        
        this(employeeId, name, phone, email);
        this.classTeacherStandard = classTeacherStandard;
        this.classTeacherSection = classTeacherSection;
        createJson();
    }
    
    public TeacherInput(String employeeId, String name, String phone, String email,
            Integer classTeacherClassId) {
        this(employeeId, name, phone, email);
        this.classTeacherClassId = classTeacherClassId;
        createJson();
    }

    private TeacherInput(String employeeId, String name, String phone, String email) {
        if (Utils.isNullOrEmpty(employeeId) || Utils.isNullOrEmpty(name) || !Utils.isValidPhone(phone)) {
            throw new InvalidOnboardingInputException("One or more compulsory fields are empty");
        }
        this.employeeId = employeeId;
        this.name = name;
        this.phone = phone;
        this.email = Utils.isValidEmail(email) ? email : null;
    }

    private void createJson() {
        Map<String, Object> jsonMap = new HashMap();
        jsonMap.put("employeeId", employeeId);
        jsonMap.put("name", name);
        jsonMap.put("phone", phone);
        jsonMap.put("email", email);
        jsonMap.put("classTeacherClassId", getClassTeacherClassId());
        jsonMap.put("classTeacherStandard", getClassTeacherStandard());
        jsonMap.put("classTeacherSection", getClassTeacherSection());
        json = (new JsonObject(jsonMap)).toString();
    }

    /**
     * @return the employeeId
     */
    public String getEmployeeId() {
        return employeeId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return json;
    }

    /**
     * @return the classTeacherClassId
     */
    public Integer getClassTeacherClassId() {
        return classTeacherClassId;
    }

    /**
     * @return the classTeacherStandard
     */
    public String getClassTeacherStandard() {
        return classTeacherStandard;
    }

    /**
     * @return the classTeacherSection
     */
    public String getClassTeacherSection() {
        return classTeacherSection;
    }

}
