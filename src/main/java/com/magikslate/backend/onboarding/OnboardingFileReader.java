/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.onboarding;

import java.io.File;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public interface OnboardingFileReader {

    public List<StudentInput> readStudentInput(File file);

    public List<TeacherInput> readTeacherInput(File file);

    public List<StudentTeacherInput> readStudentTeacherInput(File writeToDisk);
}
