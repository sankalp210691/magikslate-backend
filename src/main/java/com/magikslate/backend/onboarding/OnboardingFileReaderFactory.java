/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.onboarding;

import com.magikslate.backend.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author sankalpkulshrestha
 */
public class OnboardingFileReaderFactory {

    @Autowired
    private CsvReader csvReader;

    public OnboardingFileReader get(String fileType) {
        switch (fileType) {
            case Constants.CSV:
                if (csvReader == null) {
                    csvReader = new CsvReader();
                }
                return csvReader;
            default: return null;
        }
    }
}
