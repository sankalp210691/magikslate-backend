/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.onboarding;

import com.magikslate.backend.exception.InvalidOnboardingInputException;
import com.magikslate.backend.util.Utils;
import io.vertx.core.json.JsonObject;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author sankalpkulshrestha
 */
public class StudentTeacherInput {

    private static final Logger LOGGER = LoggerFactory.getLogger(CsvReader.class);

    private String employeeId;
    private String subject;
    private String standard;
    private String section;
    private String[] admissionCodeList;

    private String json;

    public StudentTeacherInput(String employeeId, String subject, String standard, String section, String[] admissionCodeList) {
        if (Utils.isNullOrEmpty(employeeId)) {

            LOGGER.info("Employee Id cannot be blank");
            throw new InvalidOnboardingInputException("Employee Id cannot be blank");
        }

        standard = Utils.sanitizeClassStandard(standard);
        section = Utils.sanitizeSectionType(section);

        if (Utils.isNullOrEmpty(subject)) {
            LOGGER.info("Subject name cannot be empty. Check employee Id = {}", employeeId);
            throw new InvalidOnboardingInputException("Subject name cannot be empty. Check employee Id " + employeeId);
        }

        if ((Utils.isNullOrEmpty(section) || Utils.isNullOrEmpty(standard))) { //TODO: Uncomment when studygroup is ready .. && (admissionCodeList == null || admissionCodeList.length == 0)) {

            LOGGER.info("To assign a subject to a teacher, either list of students or class must be chosen. Check employee Id = {}", employeeId);
            throw new InvalidOnboardingInputException("To assign a subject to a teacher, either list of students or class must be chosen. Check employee Id " + employeeId);
        }

        String[] acArray = null;
        //TODO: Uncomment this once studygroups are ready
//        if (admissionCodeList != null && admissionCodeList.length != 0) {
//            List<String> acList = new ArrayList();
//            for (String admissionCode : admissionCodeList) {
//                admissionCode = admissionCode.trim();
//                if (!Utils.isNullOrEmpty(admissionCode)) {
//                    acList.add(admissionCode);
//                }
//            }
//            if (!acList.isEmpty()) {
//                acArray = acList.toArray(new String[acList.size()]);
//            }
//        }
        if (acArray != null) {
            LOGGER.debug("admissionCode list size = ", acArray.length);
        } else {
            LOGGER.debug("admissionCode list is null");
        }

        this.employeeId = employeeId;
        this.subject = Utils.isNullOrEmpty(subject) ? null : subject;
        this.standard = Utils.isNullOrEmpty(standard) ? null : standard;
        this.section = Utils.isNullOrEmpty(section) ? null : section;
        this.admissionCodeList = acArray;

        Map<String, Object> jsonMap = new HashMap();
        jsonMap.put("employeeId", employeeId);
        jsonMap.put("subject", subject);
        jsonMap.put("standard", standard);
        jsonMap.put("admissionCodeList", admissionCodeList);
        json = (new JsonObject(jsonMap)).toString();
    }

    /**
     * @return the employeeId
     */
    public String getEmployeeId() {
        return employeeId;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @return the standard
     */
    public String getStandard() {
        return standard;
    }

    /**
     * @return the section
     */
    public String getSection() {
        return section;
    }

    /**
     * @return the admissionCodeList
     */
    public String[] getAdmissionCodeList() {
        return admissionCodeList;
    }

    @Override
    public String toString() {
        return json;
    }

}
