/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.onboarding;

import com.google.common.io.Files;
import com.magikslate.backend.exception.InvalidOnboardingInputException;
import com.magikslate.backend.onboarding.StudentInput.ParentInput;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author sankalpkulshrestha
 */
public class CsvReader implements OnboardingFileReader {

    private static final Logger LOGGER = LoggerFactory.getLogger(CsvReader.class);

    @Override
    public List<StudentInput> readStudentInput(File file) throws InvalidOnboardingInputException {
        if (file == null) {
            throw new InvalidOnboardingInputException("Null file received for reading student input");
        }
        LOGGER.debug("In readStudentInput function with file = {}", file.getAbsolutePath());
        List<StudentInput> studentInputList = new ArrayList();

        Map<String, String> phoneIdentifierMap = new HashMap();
        Map<String, String> emailIdentifierMap = new HashMap();

        Iterable<CSVRecord> csvRecords = getCsvRecords(file);
        int index = 2;
        LOGGER.debug("Going to iterate over CSV records");
        Set<String> admissionCodeSet = new HashSet();
        for (CSVRecord csvRecord : csvRecords) {

            if (isBlankRow(csvRecord)) {
                continue;
            }

            LOGGER.debug(csvRecord.toString());

            String admissionCode = csvRecord.get(Constants.ADMISSION_CODE_COLUMN);
            String studentName = csvRecord.get(Constants.STUDENT_NAME_COLUMN);
            String dateOfBirth = csvRecord.get(Constants.DOB_COLUMN).replace("-", "/");
            String standard = csvRecord.get(Constants.STUDENT_STANDARD_COLUMN);
            String section = csvRecord.get(Constants.STUDENT_SECTION_COLUMN);
            String gender = null;

            String parent1Name = csvRecord.get(Constants.PARENT1_NAME_COLUMN);
            String parent1Email = csvRecord.get(Constants.PARENT1_EMAIL_COLUMN);
            String parent1CC = csvRecord.get(Constants.PARENT1_PHONE_IC_COLUMN);
            if (Utils.isNullOrEmpty(parent1CC)) {
                LOGGER.info("Invalid country code for parent 1. Check row {}", index);
                throw new InvalidOnboardingInputException("Invalid country code for parent 1. Check row " + index);
            }
            String parent1Phone = Utils.createPhoneNumber(parent1CC, csvRecord.get(Constants.PARENT1_PHONE_COLUMN));
            String parent1Address = csvRecord.get(Constants.PARENT1_ADDRESS_COLUMN);

            String parent2Name = csvRecord.get(Constants.PARENT2_NAME_COLUMN);
            String parent2Email = csvRecord.get(Constants.PARENT2_EMAIL_COLUMN);
            String parent2CC = csvRecord.get(Constants.PARENT2_PHONE_IC_COLUMN);
            String parent2Phone = Utils.createPhoneNumber(parent2CC, csvRecord.get(Constants.PARENT2_PHONE_COLUMN));
            if (!Utils.isNullOrEmpty(parent2Phone) && Utils.isNullOrEmpty(parent2CC)) {
                LOGGER.info("Invalid country code for parent 2. Check row {}", index);
                throw new InvalidOnboardingInputException("Invalid country code for parent 2. Check row " + index);
            }
            String parent2Address = csvRecord.get(Constants.PARENT2_ADDRESS_COLUMN);

            List<ParentInput> parentList = new ArrayList();
            ParentInput parentInput1 = new ParentInput(parent1Name, parent1Email, parent1Phone, parent1Address, index);
            LOGGER.debug("Adding parent 1 = {} to parentList", parentInput1);
            parentList.add(parentInput1);

            if (!Utils.isNullOrEmpty(parent2Name) && !Utils.isNullOrEmpty(parent2Phone)) {

                ParentInput parentInput2 = new ParentInput(parent2Name, parent2Email, parent2Phone, parent2Address, index);
                LOGGER.debug("Adding parent 2 = {} to parentList", parentInput1);
                parentList.add(parentInput2);

            }

            if ((phoneIdentifierMap.containsKey(parent1Phone) && phoneIdentifierMap.get(parent1Phone).equals(parent1Email)
                    && !Utils.isNullOrEmpty(parent2Phone) && phoneIdentifierMap.containsKey(parent2Phone) && phoneIdentifierMap.get(parent2Phone).equals(parent2Email))
                    || (!Utils.isNullOrEmpty(parent1Email) && emailIdentifierMap.containsKey(parent1Email) && emailIdentifierMap.get(parent1Email).equals(parent1Phone)
                    && !Utils.isNullOrEmpty(parent2Email) && emailIdentifierMap.containsKey(parent2Email) && emailIdentifierMap.get(parent2Email).equals(parent2Phone))) {

                LOGGER.debug("Found another parent with same identifiers. parent1Email = {} , parent1Phone = {} , parent2Email = {} , parent2Phone = {}");

            } else if (((phoneIdentifierMap.containsKey(parent1Phone) && !phoneIdentifierMap.get(parent1Phone).equals(parent1Email))
                    || !Utils.isNullOrEmpty(parent2Phone) && phoneIdentifierMap.containsKey(parent2Phone) && !phoneIdentifierMap.get(parent2Phone).equals(parent2Email))
                    || ((!Utils.isNullOrEmpty(parent1Email) && emailIdentifierMap.containsKey(parent1Email) && !emailIdentifierMap.get(parent1Email).equals(parent1Phone))
                    || !Utils.isNullOrEmpty(parent2Email) && emailIdentifierMap.containsKey(parent2Email) && !emailIdentifierMap.get(parent2Email).equals(parent2Phone))) {

                LOGGER.info("One or more duplicate Phone/Email found with unmatched Email/Phone. Check row " + index);
                throw new InvalidOnboardingInputException("One or more duplicate Phone/Email found with unmatched Email/Phone. Check row " + index);

            } else {

                if (!Utils.isNullOrEmpty(parent1Email)) {
                    phoneIdentifierMap.put(parent1Phone, parent1Email);
                    emailIdentifierMap.put(parent1Email, parent1Phone);
                }

                if (!Utils.isNullOrEmpty(parent2Phone)) {
                    phoneIdentifierMap.put(parent2Phone, parent2Email);
                }

                if (!Utils.isNullOrEmpty(parent2Email)) {
                    phoneIdentifierMap.put(parent2Email, parent2Phone);
                }

            }

            LOGGER.debug("Identifier set = {}", phoneIdentifierMap);
            StudentInput studentInput = new StudentInput(admissionCode, studentName, dateOfBirth, standard, section, gender, parentList, null);
            if (admissionCodeSet.contains(admissionCode)) {
                LOGGER.debug("There are more than one students with same admission code {}. Throwing InvalidOnboardingInputException", admissionCode);
                throw new InvalidOnboardingInputException("There are more than one students with same admission code " + admissionCode);
            }
            admissionCodeSet.add(admissionCode);
            LOGGER.debug("Adding studentInput = {} to studentInputList", studentInput);
            studentInputList.add(studentInput);
            index++;
        }
        LOGGER.debug("Returning studentInputList = {}", studentInputList);
        return studentInputList;
    }

    @Override
    public List<TeacherInput> readTeacherInput(File file) {

        if (file == null) {
            throw new InvalidOnboardingInputException("Null file received for reading teacher input");
        }
        LOGGER.debug("In readTeacherInput function with file = {}", file.getAbsolutePath());

        List<TeacherInput> teacherInputList = new ArrayList();

        Set<String> identifierSet = new HashSet();

        Iterable<CSVRecord> csvRecords = getCsvRecords(file);
        int index = 2;
        LOGGER.debug("Going to iterate over CSV records");
        for (CSVRecord csvRecord : csvRecords) {

            if (isBlankRow(csvRecord)) {
                continue;
            }

            LOGGER.debug(csvRecord.toString());

            String employeeId = csvRecord.get(Constants.TEACHER_EMPLOYEE_ID_COLUMN);
            String name = csvRecord.get(Constants.TEACHER_NAME_COLUMN);
            String cc = csvRecord.get(Constants.TEACHER_PHONE_IC_COLUMN);
            String phone = Constants.PLUS_SEPARATOR + cc + Constants.HYPHEN_SEPARATOR + csvRecord.get(Constants.TEACHER_PHONE_COLUMN);
            String email = csvRecord.get(Constants.TEACHER_EMAIL_COLUMN);
            String classTeacherStandard = csvRecord.get(Constants.CLASS_TEACHER_STANDARD);
            String classTeacherSection = csvRecord.get(Constants.CLASS_TEACHER_SECTION);

            TeacherInput teacherInput = new TeacherInput(employeeId, name, phone, email,
                    classTeacherStandard, classTeacherSection);
            
            if (identifierSet.contains(employeeId) || identifierSet
                    .contains(phone) || identifierSet.contains(email)) {

                LOGGER.info("One or more duplicate employeeId/Phone/Email found. Check row " + index);
                throw new InvalidOnboardingInputException("One or more duplicate employeeId/Phone/Email found. Check row " + index);
            } else {
                identifierSet.add(employeeId);
                identifierSet.add(phone);
                if (!Utils.isNullOrEmpty(email)) {
                    identifierSet.add(email);
                }
                LOGGER.debug("IdentifierSet = {}", identifierSet);
            }
            LOGGER.debug("Adding {} to teacherInputList", teacherInput);
            teacherInputList.add(teacherInput);
            index++;
        }
        LOGGER.debug("Returning teacherInputList = {}", teacherInputList);
        return teacherInputList;
    }

    @Override
    public List<StudentTeacherInput> readStudentTeacherInput(File file) {

        if (file == null) {
            throw new InvalidOnboardingInputException("Null file received for reading student-teacher input");
        }
        LOGGER.debug("Reading file = {}", file.getAbsolutePath());

        List<StudentTeacherInput> studentTeacherInputList = new ArrayList();

        Set<String> subjectClassTeacherSet = new HashSet();
        Set<String> subjectStudentTeacherSet = new HashSet();

        Iterable<CSVRecord> csvRecords = getCsvRecords(file);
        int index = 2;
        LOGGER.debug("Going to iterate over CSV records");
        for (CSVRecord csvRecord : csvRecords) {

            if (isBlankRow(csvRecord)) {
                continue;
            }

            LOGGER.debug(csvRecord.toString());

            String employeeId = csvRecord.get(Constants.TEACHER_EMPLOYEE_ID_COLUMN);
            String subject = csvRecord.get(Constants.TEACHER_SUBJECT_COLUMN);
            String standard = csvRecord.get(Constants.TEACHER_STANDARD_COLUMN);
            String section = csvRecord.get(Constants.TEACHER_SECTION_COLUMN);
            String[] admissionCodeArray = csvRecord.get(Constants.TEACHER_STUDENT_COLUMN).split(Constants.COMMA_SEPARATOR);

            String classCode = Utils.getClassCode(standard, section);


            StudentTeacherInput sti = new StudentTeacherInput(employeeId, subject, standard, section, admissionCodeArray);
            LOGGER.debug("studentTeacherInput = {}", sti);

            String subjectClassTeacherCode = Utils.getSubjectCode(subject)
                    + Constants.UNDERSCORE_SEPARATOR
                    + classCode
                    + Constants.UNDERSCORE_SEPARATOR
                    + employeeId;
            LOGGER.debug("subjectClassTeacherCode = {}", subjectClassTeacherCode);

            if (subjectClassTeacherSet.contains(subjectClassTeacherCode)) {

                LOGGER.error("Found duplicate teacher-subject-class-section mapping. Check row " + (index + 1));
                throw new InvalidOnboardingInputException("Found duplicate teacher-subject-class-section mapping. Check row " + (index + 1));
            } else {

                LOGGER.debug("Adding {} to subjectClassTeacherSet", subjectClassTeacherCode);
                subjectClassTeacherSet.add(subjectClassTeacherCode);
            }
            LOGGER.debug("subjectClassTeacherSet = {}", subjectClassTeacherSet);

            for (String admissionCode : admissionCodeArray) {
                String subjectStudentTeacherCode = Utils.getSubjectCode(subject)
                        + Constants.UNDERSCORE_SEPARATOR
                        + admissionCode
                        + Constants.UNDERSCORE_SEPARATOR
                        + employeeId;
                LOGGER.debug("subjectStudentTeacherCode = {}", subjectStudentTeacherCode);

                if (subjectStudentTeacherSet.contains(subjectStudentTeacherCode)) {

                    LOGGER.info("Found duplicate teacher-subject-student mapping. Check row {}", (index + 1));
                    throw new InvalidOnboardingInputException("Found duplicate teacher-subject-student mapping. Check row " + (index + 1));
                } else {

                    LOGGER.debug("Adding {} to subjectStudentTeacherSet", subjectClassTeacherCode);
                    subjectStudentTeacherSet.add(subjectClassTeacherCode);
                }
            }
            LOGGER.debug("Adding {} to studentTeacherInputList", sti);
            studentTeacherInputList.add(sti);
            index++;
        }
        LOGGER.debug("Returning studentTeacherInputList = {}", studentTeacherInputList);
        return studentTeacherInputList;
    }

    private Iterable<CSVRecord> getCsvRecords(File file) {
        LOGGER.debug("Inside getCsvRecords");
        Reader reader = null;
        CSVParser csvParser = null;
        Iterable<CSVRecord> csvRecords = null;
        try {
            LOGGER.debug("Reading {}", file.getAbsolutePath());
            reader = Files.newReader(file, Charset.defaultCharset());
            csvParser = new CSVParser(reader, CSVFormat.DEFAULT
                    .withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim()
                    .withIgnoreEmptyLines());

            csvRecords = csvParser.getRecords();
        } catch (FileNotFoundException ex) {
            LOGGER.error(ex.toString());
        } catch (IOException ex) {
            LOGGER.error(ex.toString());
        } finally {
            try {
                if (reader != null) {
                    LOGGER.debug("Closing reader");
                    reader.close();
                }
                if (csvParser != null) {
                    LOGGER.debug("Closing csvParser");
                    csvParser.close();
                }
            } catch (IOException ex) {
                LOGGER.error(ex.toString());
            }
        }
        LOGGER.debug("Returning csvRecords");
        return csvRecords;
    }

    private boolean isBlankRow(CSVRecord csvRecord) {
        LOGGER.debug("Inside isBlankRow with record {}", csvRecord);
        int length = csvRecord.size();
        int count = 0;
        for (String record : csvRecord) {
            if (record == null || record.trim().isEmpty()) {
                count++;
            }
        }
        LOGGER.debug("Record is {}", length == count ? "blank" : "not blank");
        return length == count;
    }

}
