/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.review;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.entity.ParentSchoolReview;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public interface ParentSchoolReviewDao extends GenericDao<ParentSchoolReview, Integer> {

    public List<ParentSchoolReview> getAll(String role, Integer roleId,
            Integer schoolId, Date startDate, Date endDate, Integer offset, boolean filterBySchool, boolean filterByRoleId);

    public long getReviewCountForSchool(Integer schoolId);

}
