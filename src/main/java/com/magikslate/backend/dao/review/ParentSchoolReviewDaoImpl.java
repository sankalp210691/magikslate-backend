/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.review;

import com.magikslate.backend.dao.GenericDaoImpl;
import com.magikslate.backend.entity.ParentSchoolReview;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author sankalpkulshrestha
 */
@Repository
public class ParentSchoolReviewDaoImpl extends GenericDaoImpl<ParentSchoolReview, Integer>
        implements ParentSchoolReviewDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParentSchoolReviewDaoImpl.class);

    @Override
    public List<ParentSchoolReview> getAll(String role, Integer roleId,
            Integer schoolId, Date startDate, Date endDate, Integer offset,
            boolean filterBySchool, boolean filterByRoleId) {

        if (filterBySchool && filterByRoleId) {
            return getReviewsBySchoolAndRole(schoolId, role, roleId, startDate, endDate, offset);
        } else if (filterBySchool) {
            return getReviewsBySchool(schoolId, startDate, endDate, offset);
        } else if (filterByRoleId) {
            return getReviewsByRole(role, roleId, startDate, endDate, offset);
        } else {
            return null;
        }
    }

    private List<ParentSchoolReview> getReviewsBySchoolAndRole(Integer schoolId, String role, Integer roleId, Date startDate, Date endDate, Integer offset) {
        String queryString = "from ParentSchoolReview psr where psr.school.id = :schoolId and psr.parent.id = :roleId"
                + " and psr.createdAt >= :startDate and psr.createdAt <= :endDate and psr.deletedAt IS NULL";
        return getAll(queryString, startDate, endDate, schoolId, roleId, offset);
    }

    private List<ParentSchoolReview> getReviewsBySchool(Integer schoolId, Date startDate, Date endDate, Integer offset) {
        String queryString = "from ParentSchoolReview psr where psr.school.id = :schoolId "
                + " and psr.createdAt >= :startDate and psr.createdAt <= :endDate and psr.deletedAt IS NULL";
        return getAll(queryString, startDate, endDate, schoolId, null, offset);
    }

    private List<ParentSchoolReview> getReviewsByRole(String role, Integer roleId, Date startDate, Date endDate, Integer offset) {
        String queryString = "from ParentSchoolReview psr where psr.parent.id = :roleId"
                + " and psr.createdAt >= :startDate and psr.createdAt <= :endDate and psr.deletedAt IS NULL";
        return getAll(queryString, startDate, endDate, null, roleId, offset);
    }

    private List<ParentSchoolReview> getAll(String queryString, Date startDate, Date endDate, Integer schoolId, Integer roleId, Integer offset) {
        Session session = currentSession();
        
        queryString += " order by psr.createdAt desc";

        Query query = session.createQuery(queryString);
        if (schoolId != null) {
            query.setParameter("schoolId", schoolId);
        }
        if (roleId != null) {
            query.setParameter("roleId", roleId);
        }
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        
        LOGGER.debug("{}", Utils.listQueryParams(query));
        
        if (offset != null) {
            query.setFirstResult(offset * Constants.LIST_LIMIT);
            query.setMaxResults(Constants.LIST_LIMIT);
        }
        
        List<ParentSchoolReview> parentSchoolReviewList = query.list();
        parentSchoolReviewList.forEach((psr) -> {
            String name = psr.getSchool().getName();
        });
        return parentSchoolReviewList;
    }

    @Override
    public long getReviewCountForSchool(Integer schoolId) {
        Session session = currentSession();
        
        String queryString = "select count(1) as count from ParentSchoolReview psr where psr.school.id = :schoolId and psr.deletedAt IS NULL";
        Query query = session.createQuery(queryString);
        query.setParameter("schoolId", schoolId);
        Long count = (Long) query.getSingleResult();
        return count;
    }
}
