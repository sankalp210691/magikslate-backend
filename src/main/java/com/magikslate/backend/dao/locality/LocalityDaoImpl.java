/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.locality;

import com.magikslate.backend.dao.GenericDaoImpl;
import com.magikslate.backend.entity.Locality;
import org.springframework.stereotype.Repository;

/**
 *
 * @author sankalpkulshrestha
 */
@Repository
public class LocalityDaoImpl extends GenericDaoImpl<Locality, Integer>
        implements LocalityDao {

}
