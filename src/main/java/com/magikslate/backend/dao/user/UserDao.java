/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.user;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.entity.User;
import java.util.List;


/**
 *
 * @author sankalpkulshrestha
 */
public interface UserDao extends GenericDao<User, Integer>{

    public User getUserByPhoneEmailAndPassword(String phone, String email, String password);

    public boolean isDistinctUser(String email, String phone);
    
    public List<User> getUserListByPhoneOrEmail(List<String> phoneList, List<String> emailList, Boolean allowInactive, Boolean allowDeleted);

    public List<Integer> createBulk(List<User> userList);

    public boolean verifyOtpAndActivate(User user);

    public User getUserByPhone(String phone);
    
}
