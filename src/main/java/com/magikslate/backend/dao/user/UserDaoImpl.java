/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.user;

import com.magikslate.backend.dao.GenericDaoImpl;
import com.magikslate.backend.entity.User;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author sankalpkulshrestha
 */
@Repository
public class UserDaoImpl extends GenericDaoImpl<User, Integer>
        implements UserDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);

    @Override
    public User getUserByPhoneEmailAndPassword(String phone, String email, String password) {
        String queryString = "from User where (Phone = :phone or Email = :email) and Password = :password and ((active = 1 and status = 'ACTIVE') OR (status = 'REG_PASSWORD_CHANGE_PENDING') OR (status = 'OTP_VERIFICATION_PENDING')) and DeletedAt is NULL";
        LOGGER.debug(queryString);
        Query query = currentSession().createQuery(queryString);
        query.setParameter("phone", phone);
        query.setParameter("email", email);
        query.setParameter("password", password);
        LOGGER.debug("{}", Utils.listQueryParams(query));
        List<User> list = query.list();
        if (list == null || list.isEmpty()) {
            LOGGER.debug("Empty result");
            return null;
        } else {
            User user = list.get(0);;
            LOGGER.debug("user = {}", user);
            return user;
        }
    }

    @Override
    public boolean isDistinctUser(String email, String phone) {
        String queryString = "from User where Email = :email or Phone = :phone";
        Query query = currentSession().createQuery(queryString);
        LOGGER.debug(queryString);
        query.setParameter("email", email);
        query.setParameter("phone", phone);
        LOGGER.debug("{}", Utils.listQueryParams(query));
        List<User> list = query.list();
        boolean isDistinctUser = list == null || list.isEmpty();
        LOGGER.debug("isDistinctUser = {}", isDistinctUser);
        return isDistinctUser;
    }

    @Override
    public List<User> getUserListByPhoneOrEmail(List<String> phoneList, List<String> emailList, Boolean allowInactive, Boolean allowDeleted) {
        StringBuilder queryBuilder = new StringBuilder("from User where Phone in (:phoneList)");
        if (emailList != null && !emailList.isEmpty()) {
            queryBuilder.append(" or Email in (:emailList)");
        }
        if (!allowInactive) {
            queryBuilder.append(" and active = 1");
        }
        if (!allowDeleted) {
            queryBuilder.append(" and DeletedAt is NULL");
        }
        LOGGER.debug(queryBuilder.toString());
        Query query = currentSession().createQuery(queryBuilder.toString());
        query.setParameter("phoneList", phoneList);
        if (emailList != null && !emailList.isEmpty()) {
            query.setParameter("emailList", emailList);
        }
//        LOGGER.debug("{}", Utils.listQueryParams(query));
        return query.list();
    }

    @Override
    public List<Integer> createBulk(List<User> userList) {
        List<Integer> idList = new ArrayList();

        Session session = currentSession();

        int index = 0;
        for (User user : userList) {
            idList.add((Integer) session.save(user));
            if (index % Constants.HIBERNATE_BATCH_SIZE == 0) {
                session.flush();
                session.clear();
            }
            index++;
        }
        return idList;
    }

    @Override
    public boolean verifyOtpAndActivate(User user) {
        Session session = currentSession();

        //in where we check for status = ACTIVE, so that no user can unblock him/herself by just using this API (assuming they remember their OTP)
        String queryString = "update User u set u.active = 1 , u.status = 'ACTIVE' where u.id = :userId and u.otp = :otp and u.deletedAt IS NULL and u.status != 'ACTIVE'";
        LOGGER.debug(queryString);
        Query query = session.createNativeQuery(queryString);
        query.setParameter("userId", user.getId());
        query.setParameter("otp", user.getOtp());
        LOGGER.debug("{}", Utils.listQueryParams(query));
        boolean didUpdateHappen = query.executeUpdate() > 0;
        LOGGER.debug("didUpdateHappen = {}", didUpdateHappen);
        return didUpdateHappen;
    }

    @Override
    public User getUserByPhone(String phone) {
        Session session = currentSession();

        Query query = session.createQuery("from User u where u.phone = :phone and u.deletedAt IS NULL");
        query.setParameter("phone", phone);

        List<User> list = query.list();
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        } else {
            return null;
        }

    }

}
