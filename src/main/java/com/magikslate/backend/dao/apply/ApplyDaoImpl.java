/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.apply;

import com.magikslate.backend.dao.GenericDaoImpl;
import com.magikslate.backend.entity.UserSchoolInterest;
import org.springframework.stereotype.Repository;

/**
 *
 * @author sankalpkulshrestha
 */
@Repository
public class ApplyDaoImpl extends GenericDaoImpl<UserSchoolInterest, Integer>
        implements ApplyDao {

}
