/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.help;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.entity.Help;

/**
 *
 * @author sankalpkulshrestha
 */
public interface HelpDao extends GenericDao<Help, Integer> {

}
