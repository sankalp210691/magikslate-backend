/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.admin;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.entity.Admin;
import com.magikslate.backend.entity.User;


/**
 *
 * @author sankalpkulshrestha
 */
public interface AdminDao extends GenericDao<Admin, Integer>{

    public Integer getAdminAccessId(Integer userId, Integer schoolId);

    public User getUserByAdminId(int adminId);
}
