/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.admin;

import com.magikslate.backend.dao.GenericDaoImpl;
import com.magikslate.backend.entity.Admin;
import com.magikslate.backend.entity.User;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author sankalpkulshrestha
 */
@Repository
public class AdminDaoImpl extends GenericDaoImpl<Admin, Integer>
        implements AdminDao {

    @Override
    public Integer getAdminAccessId(Integer userId, Integer schoolId) {
        Session session = currentSession();

        Query query = session.createNativeQuery("select a.id from Admin a join User u on u.id = a.user_id where a.school_id = :schoolId and u.id = :userId and a.deletedAt IS NULL and u.deletedAt IS NULL and u.active = 1 order by a.id desc");
        query.setParameter("userId", userId);
        query.setParameter("schoolId", schoolId);

        List<Object> result = query.list();
        if (result == null || result.isEmpty()) {
            return null;
        } else {
            return (Integer) result.get(0);
        }
    }

    @Override
    public User getUserByAdminId(int adminId) {
        Session session = currentSession();

        Query query = session.createNativeQuery("select u.id, u.name from User u join Admin a on a.user_id = u.id where a.deletedAt IS NULL and u.deletedAt IS NULL and u.active=1 and a.id = :adminId");
        query.setParameter("adminId", adminId);
        List<Object[]> result = query.list();
        if (!result.isEmpty()) {
            User user = new User((Integer) result.get(0)[0]);
            user.setName((String) result.get(0)[1]);
            return user;
        } else {
            return null;
        }
    }
}
