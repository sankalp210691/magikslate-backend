/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.school;

import com.magikslate.backend.dao.GenericDaoImpl;
import com.magikslate.backend.entity.School;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author sankalpkulshrestha
 */
@Repository
public class SchoolDaoImpl extends GenericDaoImpl<School, Integer>
        implements SchoolDao {

    @Override
    public boolean updateStage(Integer stage, Integer schoolId) {
        Session session = currentSession();
        
        Query query = session.createQuery("update School s set s.stage = :stage where s.id = :schoolId and s.stage < :stage");
        query.setParameter("stage", stage);
        query.setParameter("schoolId", schoolId);
        return query.executeUpdate() < 2;
    }

}
