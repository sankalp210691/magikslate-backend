/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.school;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.entity.School;


/**
 *
 * @author sankalpkulshrestha
 */
public interface SchoolDao extends GenericDao<School, Integer>{

    public boolean updateStage(Integer stage, Integer schoolId);

}
