/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao;

import com.magikslate.backend.dto.CityDto;
import com.magikslate.backend.dto.LocalityDto;
import com.magikslate.backend.dto.StateDto;
import com.magikslate.backend.entity.Board;
import com.magikslate.backend.entity.City;
import com.magikslate.backend.entity.Country;
import com.magikslate.backend.entity.Locality;
import com.magikslate.backend.entity.State;
import com.magikslate.backend.services.board.BoardService;
import com.magikslate.backend.services.locality.LocalityService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author sankalpkulshrestha
 */
@Component
public class InMemoryCache {

    @Autowired
    private BoardService boardService;

    @Autowired
    private LocalityService localityService;

    private Map<Integer, Board> boardMap;

    private Map<Integer, List<StateDto>> countryStateMap;
    private Map<Integer, List<CityDto>> stateCityMap;
    private Map<Integer, List<LocalityDto>> cityLocalityMap;

    @PostConstruct
    public void init() {
        loadInMemoryCache();
    }

    private void loadInMemoryCache() {
        //BOARDS
        boardMap = new HashMap();
        List<Board> boardList = boardService.getAll();
        boardList.forEach(dbBoard -> {
            Board board = new Board();
            board.setId(dbBoard.getId());
            board.setName(dbBoard.getName());
            getBoardMap().put(board.getId(), board);
        });

//        //LOCATION
//        countryStateMap = new HashMap();
//        stateCityMap = new HashMap();
//        cityLocalityMap = new HashMap();
//        
//        Set<Integer> cityIdList = new HashSet();
//        Set<Integer> stateIdList = new HashSet();
//        
//        List<Locality> localityList = localityService.getAll();
//        localityList.forEach(dbLocality -> {
//
//            City dbCity = dbLocality.getCity();
//            State dbState = dbCity.getState();
//            Country dbCountry = dbState.getCountry();
//
//            List<LocalityDto> cityLocalityList = cityLocalityMap.get(dbCity.getId());
//            if (cityLocalityList == null) {
//                cityLocalityList = new ArrayList();
//                cityLocalityMap.put(dbCity.getId(), cityLocalityList);
//            }
//            LocalityDto localityDto = new LocalityDto();
//            localityDto.setId(dbLocality.getId());
//            localityDto.setName(dbLocality.getName());
//            
//            CityDto cityDto = new CityDto();
//            cityDto.setId(dbCity.getId());
//            cityDto.setName(dbCity.getName());
//            
//            StateDto stateDto = new StateDto();
//            stateDto.setId(dbState.getId());
//            stateDto.setName(dbState.getName());
//            
//            localityDto.setCity(cityDto);
//            
//            cityLocalityList.add(localityDto);
//
//            List<CityDto> stateCityList = stateCityMap.get(dbState.getId());
//            if (stateCityList == null) {
//                stateCityList = new ArrayList();
//                stateCityMap.put(dbState.getId(), stateCityList);
//            }
//            if (!cityIdList.contains(dbCity.getId())) {
//                stateCityList.add(cityDto);
//                cityIdList.add(cityDto.getId());
//            }
//
//            List<StateDto> countryStateList = countryStateMap.get(dbCountry.getId());
//            if (countryStateList == null) {
//                countryStateList = new ArrayList();
//                countryStateMap.put(dbCountry.getId(), countryStateList);
//            }
//            if (!stateIdList.contains(dbState.getId())) {
//                countryStateList.add(stateDto);
//                stateIdList.add(stateDto.getId());
//            }
//
//        });
    }

    /**
     * @return the boardMap
     */
    public Map<Integer, Board> getBoardMap() {
        return boardMap;
    }

    /**
     * @return the cityLocalityMap
     */
    public Map<Integer, List<LocalityDto>> getCityLocalityMap() {
        return cityLocalityMap;
    }

    /**
     * @return the stateCityMap
     */
    public Map<Integer, List<CityDto>> getStateCityMap() {
        return stateCityMap;
    }
    
    /**
     * @return the countryStateMap
     */
    public Map<Integer, List<StateDto>> getCountryStateMap() {
        return countryStateMap;
    }
}
