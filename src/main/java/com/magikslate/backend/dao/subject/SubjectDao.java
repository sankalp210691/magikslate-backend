/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.subject;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.entity.Student;
import com.magikslate.backend.entity.StudyGroupTeacherMapping;
import com.magikslate.backend.entity.Subject;
import com.magikslate.backend.entity.SubjectClassTeacherMapping;
import java.util.Collection;
import java.util.List;


/**
 *
 * @author sankalpkulshrestha
 */
public interface SubjectDao extends GenericDao<Subject, Integer>{

    public List<Integer> createBulk(List<Subject> subjectList, Integer schoolId);

    public List<Subject> getSubjectListBySchool(Integer schoolId);

    public void mapToTeacherClassStudyGroup(Collection<Subject> values);

    public List<Integer> createSctm(List<SubjectClassTeacherMapping> sctmList);

    public List<Integer> createSgtm(List<StudyGroupTeacherMapping> sgtmList);

    public List<Subject> getSubjectsForStudent(Student student);
    
}
