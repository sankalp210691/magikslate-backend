/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.subject;

import com.google.common.base.Joiner;
import com.magikslate.backend.dao.GenericDaoImpl;
import com.magikslate.backend.entity.Student;
import com.magikslate.backend.entity.StudyGroup;
import com.magikslate.backend.entity.StudyGroupTeacherMapping;
import com.magikslate.backend.entity.Subject;
import com.magikslate.backend.entity.SubjectClassTeacherMapping;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author sankalpkulshrestha
 */
@Repository
public class SubjectDaoImpl extends GenericDaoImpl<Subject, Integer>
        implements SubjectDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubjectDaoImpl.class);

    @Override
    public List<Integer> createBulk(List<Subject> subjectList, Integer schoolId) {
        List<Integer> idList = new ArrayList();

        List<Subject> existingSubjectList = getSubjectListBySchool(schoolId);
        Map<String, Integer> subjectNameMap = new HashMap();
        existingSubjectList.forEach(subject -> {
            subjectNameMap.put(Utils.getSubjectCode(subject.getName()), subject.getId());
        });

        Session session = currentSession();
        int index = 0;
        for (Subject subject : subjectList) {
            String subjectCode = Utils.getSubjectCode(subject.getName());
            if (subjectNameMap.containsKey(subjectCode)) {
                idList.add(subjectNameMap.get(subjectCode));
            } else {
                idList.add((Integer) session.save(subject));
                if (index % Constants.HIBERNATE_BATCH_SIZE == 0) {
                    session.flush();
                    session.clear();
                }
                index++;
            }
        }
        session.flush();
        session.clear();
        return idList;
    }

    @Override
    public List<Subject> getSubjectListBySchool(Integer schoolId) {
        Session session = currentSession();

        String queryString = "from Subject s where s.school.id = :schoolId";
        Query query = session.createQuery(queryString);
        query.setParameter("schoolId", schoolId);

        return query.list();
    }

    @Override
    public void mapToTeacherClassStudyGroup(Collection<Subject> subjectCollection) {
        for (Subject subject : subjectCollection) {
            List<SubjectClassTeacherMapping> subjectClassTeacherMappingList = subject.getSubjectClassTeacherMappingList();
            List<StudyGroup> studyGroupList = subject.getStudyGroupList();

            Session session = currentSession();

            int index = 0;
            if(studyGroupList != null && !studyGroupList.isEmpty()) {
                for (StudyGroup studyGroup : studyGroupList) {
                    session.saveOrUpdate(studyGroup);
                    if (index % Constants.HIBERNATE_BATCH_SIZE == 0) {
                        session.flush();
                        session.clear();
                    }
                    index++;
                }
            } else if (subjectClassTeacherMappingList != null) {
                for (SubjectClassTeacherMapping sctm : subjectClassTeacherMappingList) {
                    session.saveOrUpdate(sctm);
                    if (index % Constants.HIBERNATE_BATCH_SIZE == 0) {
                        session.flush();
                        session.clear();
                    }
                    index++;
                }
            }
        }
    }

    @Override
    public List<Integer> createSctm(List<SubjectClassTeacherMapping> sctmList) {
        Session session = currentSession();

        StringBuilder queryBuilder = new StringBuilder("select id, concat(class_id,'_',teacher_id,'_',subject_id,'_',sessionYear) from SubjectClassTeacherMapping "
                + "where concat(class_id,'_',teacher_id,'_',subject_id,'_',sessionYear) in (");
        List<String> queryBuilderClauseList = new ArrayList();
        sctmList.forEach((sctm) -> {
            queryBuilderClauseList.add("'" + sctm.getClass1().getId() + Constants.UNDERSCORE_SEPARATOR + sctm.getTeacher().getId()
                    + Constants.UNDERSCORE_SEPARATOR + sctm.getSubject().getId()
                    + Constants.UNDERSCORE_SEPARATOR + sctm.getSessionYear() + "'");
        });
        queryBuilder.append(Joiner.on(Constants.COMMA_SEPARATOR).join(queryBuilderClauseList));
        queryBuilder.append(") and deletedAt IS NULL");
        Query query = session.createNativeQuery(queryBuilder.toString());
        List<Object[]> result = query.list();
        Set<String> existingSctmCodeSet = new HashSet();
        List<Integer> idList = new ArrayList();
        result.stream().map((res) -> {
            idList.add((Integer) res[0]);
            return res;
        }).forEachOrdered((res) -> {
            existingSctmCodeSet.add((String) res[1]);
        });

        int index = 0;
        for (SubjectClassTeacherMapping sctm : sctmList) {
            String code = sctm.getClass1().getId() + Constants.UNDERSCORE_SEPARATOR + sctm.getTeacher().getId()
                    + Constants.UNDERSCORE_SEPARATOR + sctm.getSubject().getId()
                    + Constants.UNDERSCORE_SEPARATOR + sctm.getSessionYear();
            if (!existingSctmCodeSet.contains(code)) {
                session.saveOrUpdate(sctm);
                idList.add(sctm.getId());
                if (index % Constants.HIBERNATE_BATCH_SIZE == 0) {
                    session.flush();
                    session.clear();
                }
                index++;
            }

        }
        return idList;
    }

    @Override
    public List<Integer> createSgtm(List<StudyGroupTeacherMapping> sgtmList) {
        Session session = currentSession();

        StringBuilder queryBuilder = new StringBuilder("select id, concat(StudyGroup_Id,'_',teacher_id) from StudyGroupTeacherMapping "
                + "where concat(StudyGroup_Id,'_',teacher_id) in (");
        List<String> queryBuilderClauseList = new ArrayList();
        sgtmList.forEach((sgtm) -> {
            queryBuilderClauseList.add("'" + sgtm.getStudyGroup().getId()
                    + Constants.UNDERSCORE_SEPARATOR + sgtm.getTeacher().getId() + "'");
        });
        queryBuilder.append(Joiner.on(Constants.COMMA_SEPARATOR).join(queryBuilderClauseList));
        queryBuilder.append(") and deletedAt IS NULL");
        Query query = session.createNativeQuery(queryBuilder.toString());
        List<Object[]> result = query.list();
        Set<String> existingSgtmCodeSet = new HashSet();
        List<Integer> idList = new ArrayList();
        result.stream().map((res) -> {
            idList.add((Integer) res[0]);
            return res;
        }).forEachOrdered((res) -> {
            existingSgtmCodeSet.add((String) res[1]);
        });

        int index = 0;
        for (StudyGroupTeacherMapping sgtm : sgtmList) {
            String code = sgtm.getStudyGroup().getId() + Constants.UNDERSCORE_SEPARATOR + sgtm.getTeacher().getId();
            if (!existingSgtmCodeSet.contains(code)) {
                session.saveOrUpdate(sgtm);
                idList.add(sgtm.getId());
                if (index % Constants.HIBERNATE_BATCH_SIZE == 0) {
                    session.flush();
                    session.clear();
                }
                index++;
            }

        }
        return idList;
    }

    @Override
    public List<Subject> getSubjectsForStudent(Student student) {
        Session session = currentSession();

        List<Subject> subjectList = new ArrayList();

        String queryString = "select sb.id, sb.name from Subject sb "
                + "join StudyGroup sg on sg.subject_id = sb.id "
                + "join StudentStudyGroupMapping ssgm on ssgm.studygroup_id = sg.id "
                + "where ssgm.student_id = :studentId and sg.school_id = :schoolId";

        LOGGER.debug(queryString);

        Query query = session.createNativeQuery(queryString);
        query.setParameter("studentId", student.getId());
        query.setParameter("schoolId", student.getSchool().getId());
        LOGGER.debug("{}", Utils.listQueryParams(query));
        List<Object[]> result = query.list();

        if (result != null) {

            for (Object[] res : result) {
                Subject subject = new Subject((Integer) res[0]);
                subject.setName((String) res[1]);
                subjectList.add(subject);
            }
        }

        if (student.getStudentClassMappingList() != null && !student.getStudentClassMappingList().isEmpty()) {

            queryString = "select sb.id, sb.name from Subject sb "
                    + "join SubjectClassTeacherMapping sctm on sctm.subject_id = sb.id "
                    + "join StudentClassMapping scm on scm.class_id = sctm.class_id "
                    + "where scm.student_id = :studentId and "
                    + "scm.sessionYear = :sessionYear and sb.school_id = :schoolId";

            LOGGER.debug(queryString);

            query = session.createNativeQuery(queryString);
            query.setParameter("studentId", student.getId());
            query.setParameter("schoolId", student.getSchool().getId());
            query.setParameter("sessionYear", student.getStudentClassMappingList().get(0).getSessionYear());//assuming the 0th index  has latest class
            LOGGER.debug("{}", Utils.listQueryParams(query));
            result = query.list();

            for (Object[] res : result) {
                Subject subject = new Subject((Integer) res[0]);
                subject.setName((String) res[1]);
                subjectList.add(subject);
            }
        }
        return subjectList;
    }

}
