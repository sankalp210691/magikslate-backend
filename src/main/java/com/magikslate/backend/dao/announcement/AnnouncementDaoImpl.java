/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.announcement;

import com.magikslate.backend.dao.GenericDaoImpl;
import com.magikslate.backend.entity.Announcement;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author sankalpkulshrestha
 */
@Repository
public class AnnouncementDaoImpl extends GenericDaoImpl<Announcement, Integer>
        implements AnnouncementDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnnouncementDaoImpl.class);

    @Override
    public List<List<Announcement>> getAll(Integer userId, String role, Integer roleId,
            Integer schoolId, Date startDate, Date endDate, Integer offset,
            boolean isSelfNeeded, boolean isReceivedNeeded) {

        List<List<Announcement>> announcementListList = new ArrayList();

        if (isSelfNeeded) {
            LOGGER.debug("isSelfNeeded = {}", isSelfNeeded);
            List<Announcement> selfAnnouncementList = getAnnouncementListByUser(userId, schoolId, startDate, endDate, offset);
            LOGGER.debug("selfAnnouncementList = {}", selfAnnouncementList);
            announcementListList.add(selfAnnouncementList);
        } else {
            LOGGER.debug("isSelfNeeded = {}", isSelfNeeded);
            announcementListList.add(null);
        }

        if (isReceivedNeeded) {
            LOGGER.debug("isReceivedNeeded = {}", isReceivedNeeded);
            List<Announcement> receivedAnnouncementList = getAnnouncementListByReceiver(roleId, role, schoolId, startDate, endDate, offset);
            LOGGER.debug("receivedAnnouncementList = {}", receivedAnnouncementList);
            announcementListList.add(receivedAnnouncementList);
        } else {
            LOGGER.debug("isReceivedNeeded = {}", isReceivedNeeded);
            announcementListList.add(null);
        }

        return announcementListList;
    }

    private List<Announcement> getAnnouncementListByUser(Integer userId, Integer schoolId, Date startDate, Date endDate, Integer offset) {

        Session session = currentSession();

        String queryString = "from Announcement a where a.admin.school.id = :schoolId and a.createdAt >= :startDate and a.createdAt <= :endDate and a.admin.user.id = :userId and a.deletedAt IS NULL order by a.createdAt desc";
        LOGGER.debug(queryString);

        Query query = session.createQuery(queryString);
        query.setParameter("schoolId", schoolId);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        query.setParameter("userId", userId);

        LOGGER.debug("{}", Utils.listQueryParams(query));

        if (offset != null) {
            query.setFirstResult(offset * Constants.LIST_LIMIT);
            query.setMaxResults(Constants.LIST_LIMIT);
        }

        return query.list();
    }

    private List<Announcement> getAnnouncementListByReceiver(Integer roleId, String role, Integer schoolId, Date startDate, Date endDate, Integer offset) {

        Session session = currentSession();

        String queryString;
        String classTeacherQueryString = null;
        switch (role) {
            case Constants.ADMIN:
                queryString = "from Announcement a where a.admin.school.id = :schoolId and a.createdAt >= :startDate and a.createdAt <= :endDate and a.deletedAt IS NULL order by a.createdAt desc";
                break;
            case Constants.TEACHER:
                queryString = "select distinct a from Announcement a join a.classList as cl where cl.id in (select c.id from Class c join c.subjectClassTeacherMappingList as sctml where sctml.teacher.id = :roleId) and a.sendToStaff = true and a.admin.school.id = :schoolId and a.createdAt >= :startDate and a.createdAt <= :endDate and a.deletedAt IS NULL order by a.createdAt desc";
                classTeacherQueryString = "select distinct a from Announcement a join a.classList as cl where cl.id in (select c.id from Class c join c.classTeacherMappingList as ctml where ctml.teacher.id = :roleId) and a.sendToStaff = true and a.admin.school.id = :schoolId and a.createdAt >= :startDate and a.createdAt <= :endDate and a.deletedAt IS NULL order by a.createdAt desc";
                break;
            case Constants.PARENT:
                queryString = "select distinct a from Announcement a join a.classList as cl where cl.id in (select c.id from Class c join c.studentClassMappingList as scml join scml.student as s join s.studentParentMappingList spml where spml.parent.id = :roleId) and a.sendToParents = true and a.admin.school.id = :schoolId and a.createdAt >= :startDate and a.createdAt <= :endDate and a.deletedAt IS NULL order by a.createdAt desc";
                break;
            default:
                return null;
        }

        LOGGER.debug(queryString);

        Query query = session.createQuery(queryString);
        query.setParameter("schoolId", schoolId);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        if (!Constants.ADMIN.equals(role)) {
            query.setParameter("roleId", roleId);
        }

        LOGGER.debug("{}", Utils.listQueryParams(query));

        if (offset != null) {
            query.setFirstResult(offset * Constants.LIST_LIMIT);
            query.setMaxResults(Constants.LIST_LIMIT);
        }

        List<Announcement> announcementList = query.list();
        List<Announcement> classTeacherAnnouncementList = null;

        if (classTeacherQueryString != null) {

            LOGGER.debug(queryString);

            Query classTeacherQuery = session.createQuery(classTeacherQueryString);
            classTeacherQuery.setParameter("schoolId", schoolId);
            classTeacherQuery.setParameter("startDate", startDate);
            classTeacherQuery.setParameter("endDate", endDate);
            classTeacherQuery.setParameter("roleId", roleId);

            LOGGER.debug("{}", Utils.listQueryParams(query));

            if (offset != null) {
                classTeacherQuery.setFirstResult(offset * Constants.LIST_LIMIT);
                classTeacherQuery.setMaxResults(Constants.LIST_LIMIT);
            }

            classTeacherAnnouncementList = query.list();
        }

        if (Constants.ADMIN.equals(role) || Constants.PARENT.equals(role)) {

            LOGGER.debug("Returning announcementList = {}", announcementList);
            return announcementList;
        } else {
            if (classTeacherAnnouncementList == null) {

                LOGGER.debug("Returning announcementList = {}", announcementList);
                return announcementList;
            } else {
                if (announcementList != null) {

                    Set<Integer> announcementIdSet = new HashSet();

                    for (Announcement announcement : announcementList) {
                        announcementIdSet.add(announcement.getId());
                    }

                    for (Announcement announcement : classTeacherAnnouncementList) {
                        if (!announcementIdSet.contains(announcement.getId())) {
                            announcementList.addAll(classTeacherAnnouncementList);
                        }
                    }

                    LOGGER.debug("Returning announcementList = {}", announcementList);
                    return announcementList;
                } else {
                    LOGGER.debug("Returning announcementList = {}", classTeacherAnnouncementList);
                    return classTeacherAnnouncementList;
                }
            }
        }
    }
}
