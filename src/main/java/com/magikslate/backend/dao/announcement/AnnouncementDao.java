/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.announcement;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.entity.Announcement;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public interface AnnouncementDao extends GenericDao<Announcement, Integer> {

    public List<List<Announcement>> getAll(Integer userId, String role, Integer roleId,
            Integer schoolId, Date startDate, Date endDate, Integer offset, boolean isSelfNeeded, boolean isReceivedNeeded);

}
