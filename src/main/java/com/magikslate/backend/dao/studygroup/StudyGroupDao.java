/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.studygroup;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.entity.StudyGroup;
import com.magikslate.backend.entity.StudyGroupTeacherMapping;
import java.util.List;


/**
 *
 * @author sankalpkulshrestha
 */
public interface StudyGroupDao extends GenericDao<StudyGroup, Integer>{

    public List<StudyGroup> getStudyGroupBySchool(Integer schoolId, Integer offset);

    public StudyGroupTeacherMapping getStudyGroupTeacherMapping(Integer id);

}
