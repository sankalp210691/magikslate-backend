/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.studygroup;

import com.magikslate.backend.dao.GenericDaoImpl;
import com.magikslate.backend.entity.StudyGroup;
import com.magikslate.backend.entity.StudyGroupTeacherMapping;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author sankalpkulshrestha
 */
@Repository
public class StudyGroupDaoImpl extends GenericDaoImpl<StudyGroup, Integer>
        implements StudyGroupDao {
    
        private static final Logger LOGGER = LoggerFactory.getLogger(StudyGroupDaoImpl.class);

    @Override
    public List<StudyGroup> getStudyGroupBySchool(Integer schoolId, Integer offset) {
        Session session = currentSession();

        String queryString = "from StudyGroup sg where sg.school.id = :schoolId and sg.deletedAt IS NULL";
        Query query = session.createQuery(queryString);
        LOGGER.debug(queryString);
        query.setParameter("schoolId", schoolId);
        LOGGER.debug("{}", Utils.listQueryParams(query));
        if (offset != null) {
            LOGGER.debug("firstResult = {} , maxResult = {}", offset * Constants.LIST_LIMIT, Constants.LIST_LIMIT);
            query.setFirstResult(offset * Constants.LIST_LIMIT);
            query.setMaxResults(Constants.LIST_LIMIT);
        }
        List<StudyGroup> studyGroupList = query.list();
        for(StudyGroup sg: studyGroupList) {
            String subjectName = sg.getSubject().getName();
        }
        LOGGER.debug("Returning studyGroupList = {}", studyGroupList);
        return studyGroupList;
    }

    @Override
    public StudyGroupTeacherMapping getStudyGroupTeacherMapping(Integer id) {
        
        Session session = currentSession();
        
        Query query = session.createQuery("from StudyGroupTeacherMapping sgtm where sgtm.id = :id");
        query.setParameter("id", id);
        List<StudyGroupTeacherMapping> result = query.list();
        if(result != null){
            return result.get(0);
        }else{
            return null;
        }
    }

}
