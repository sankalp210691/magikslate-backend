/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.content;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.entity.Attachment;
import com.magikslate.backend.entity.Content;
import com.magikslate.backend.entity.Note;
import com.magikslate.backend.entity.UrlContent;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author sankalpkulshrestha
 */
public interface ContentDao extends GenericDao<Content, Integer> {

    public Boolean add(UrlContent urlContent);

    public boolean add(Attachment attachment);

    public boolean add(Note note);

    public List<Content> getContent(String role, Integer roleId,
            Integer schoolId, Integer subjectId, Integer studentId,
            Integer classId, Integer studyGroupId, Date startDate, Date endDate, Integer offset);

    public boolean isAllowedToCreateContent(Content content);

    public Map<String, Integer> getStats(Date startDate, Date endDate, Integer teacherId, Integer[] classId, Integer[] studyGroupId, Integer schoolId);

}
