/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.content;

import com.magikslate.backend.dao.GenericDaoImpl;
import com.magikslate.backend.dao.student.StudentDao;
import com.magikslate.backend.dao.student.StudentDaoImpl;
import com.magikslate.backend.entity.Attachment;
import com.magikslate.backend.entity.Class;
import com.magikslate.backend.entity.Content;
import com.magikslate.backend.entity.Note;
import com.magikslate.backend.entity.Student;
import com.magikslate.backend.entity.StudentClassMapping;
import com.magikslate.backend.entity.StudentStudyGroupMapping;
import com.magikslate.backend.entity.StudyGroup;
import com.magikslate.backend.entity.StudyGroupTeacherMapping;
import com.magikslate.backend.entity.SubjectClassTeacherMapping;
import com.magikslate.backend.entity.UrlContent;
import com.magikslate.backend.util.Constants;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author sankalpkulshrestha
 */
@Repository
public class ContentDaoImpl extends GenericDaoImpl<Content, Integer>
        implements ContentDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentDaoImpl.class);

    private final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private StudentDao studentDao;

    @Override
    public Map<String, Integer> getStats(Date startDate, Date endDate, Integer teacherId, Integer[] classIdArray, Integer[] studyGroupIdArray, Integer schoolId) {

        Map<String, Integer> statMap = new LinkedHashMap();

        StringBuilder queryString = new StringBuilder("select distinct(concat(date(c.createdAt),'_',c.id)) as res from Content c");

        if (classIdArray != null) {
            queryString.append(" left join SubjectClassTeacherContentMapping sctcm on sctcm.content_id = c.id left join SubjectClassTeacherMapping sctm on sctm.id = sctcm.SubjectClassTeacherMapping_Id left join Class cl on cl.id = sctm.class_id");
            if (teacherId != null) {
                queryString.append(" join Teacher t on t.id = sctcm.teacher_id join User u on u.id = t.user_id");
            }
        }

        if (studyGroupIdArray != null) {
            queryString.append(" left join StudyGroupTeacherContentMapping sgtcm on sgtcm.content_id = c.id left join StudyGroupTeacherMapping sgtm on sgtm.id = sgtcm.StudyGroupTeacherMapping_Id left join StudyGroup sg on sg.id = sgtm.StudyGroup_id");
            if (teacherId != null) {
                queryString.append(" join Teacher t on t.id = sgtcm.teacher_id join User u on u.id = t.user_id");
            }
        }

        queryString.append(" where 1=1");

        if (teacherId != null) {
            queryString.append(" and t.id = :teacherId and t.deletedAt IS NULL and u.deletedAt IS NULL and u.active = 1");
        }

        if (classIdArray != null) {
            queryString.append(" and cl.id in :classIdArray and c.school_id = :schoolId");
        }
        if (studyGroupIdArray != null) {
            queryString.append(" and sg.id in :studyGroupIdArray and sg.school_id = :schoolId");
        }

        queryString.append(" and c.createdAt >= :startDate and c.createdAt <= :endDate");

        LOGGER.debug(queryString.toString());

        Query query = currentSession().createNativeQuery(queryString.toString());

        if (classIdArray != null) {
            query.setParameter("classIdArray", Arrays.asList(classIdArray));
        }

        if (studyGroupIdArray != null) {
            query.setParameter("studyGroupIdArray", Arrays.asList(studyGroupIdArray));
        }

        if (teacherId != null) {
            query.setParameter("teacherId", teacherId);
        }

        query.setParameter("schoolId", schoolId);
        query.setParameter("startDate", dateTimeFormat.format(startDate));
        query.setParameter("endDate", dateTimeFormat.format(endDate));

        LOGGER.debug("teacherId = {} , schoolId = {} , startDate = {} , endDate = {}"
                + " , classIdArray = {} , studyGroupIdArray = {}", teacherId,
                schoolId, dateTimeFormat.format(startDate), dateTimeFormat.format(endDate),
                classIdArray != null ? Arrays.asList(classIdArray) : "",
                studyGroupIdArray != null ? Arrays.asList(studyGroupIdArray) : "");

        List<String> statList = query.list();
        statList.forEach((result) -> {
            if (statMap.containsKey(result.split("_")[0])) {
                statMap.put(result.split("_")[0], statMap.get(result.split("_")[0]) + 1);
            } else {
                statMap.put(result.split("_")[0], 1);
            }

        });

        return statMap;
    }

    //TODO: convert all these content-type insertions into one fucntion
    @Override
    public Boolean add(UrlContent urlContent) {
        Query query = currentSession().createNativeQuery("insert into UrlContent values(:contentId, :url)");
        query.setParameter("contentId", urlContent.getContentId());
        query.setParameter("url", urlContent.getUrl());
        return query.executeUpdate() > 0;
    }

    @Override
    public boolean add(Attachment attachment) {
        Query query = currentSession().createNativeQuery("insert into Attachment values(:contentId, :fileUrl, :filename)");
        query.setParameter("contentId", attachment.getContentId());
        query.setParameter("fileUrl", attachment.getFileUrl());
        query.setParameter("filename", attachment.getFileName());
        return query.executeUpdate() > 0;
    }

    @Override
    public boolean add(Note note) {
        Query query = currentSession().createNativeQuery("insert into Note values(:contentId, :name, :noteContent)");
        query.setParameter("contentId", note.getContentId());
        query.setParameter("name", note.getName());
        query.setParameter("noteContent", note.getNoteContent());
        return query.executeUpdate() > 0;
    }

    @Override
    public boolean isAllowedToCreateContent(Content content) {
        List<StudyGroupTeacherMapping> studyGroupTeacherMappingList = content.getStudyGroupTeacherMappingList();
        List<SubjectClassTeacherMapping> subjectClassTeacherMappingList = content.getSubjectClassTeacherMappingList();

        LOGGER.debug("studyGroupTeacherMappingList = {}", studyGroupTeacherMappingList);
        LOGGER.debug("subjectClassTeacherMappingList = {}", subjectClassTeacherMappingList);

        String queryString = null;
        List<Integer> idList = null;
        int teacherId = -1;
        int expectedSize = 0;
        if (studyGroupTeacherMappingList != null && !studyGroupTeacherMappingList.isEmpty()) {
            queryString = "select sgtm.id from StudyGroupTeacherMapping sgtm"
                    + " join Teacher t on t.id = sgtm.teacher_id join User u on "
                    + "u.id = t.user_id where sgtm.id in (:idList) and sgtm.teacher_id = :teacherId "
                    + "and sgtm.deletedAt IS NULL and t.deletedAt IS NULL and u.deletedAt IS NULL and u.active = 1";
            expectedSize = studyGroupTeacherMappingList.size();
            idList = studyGroupTeacherMappingList.stream()
                    .map(studyGroupTeacherMapping -> studyGroupTeacherMapping
                    .getId()).collect(Collectors.toList());
            teacherId = studyGroupTeacherMappingList.get(0).getTeacher().getId();
        } else if (subjectClassTeacherMappingList != null && !subjectClassTeacherMappingList.isEmpty()) {
            queryString = "select sctm.id from SubjectClassTeacherMapping sctm "
                    + "join Teacher t on t.id = sctm.teacher_id join User u on "
                    + "u.id = t.user_id where sctm.id in (:idList) and sctm.teacher_id = :teacherId "
                    + "and sctm.deletedAt IS NULL and t.deletedAt IS NULL and u.deletedAt IS NULL and u.active = 1";
            expectedSize = subjectClassTeacherMappingList.size();
            idList = subjectClassTeacherMappingList.stream()
                    .map(subjectClassTeacherMapping -> subjectClassTeacherMapping
                    .getId()).collect(Collectors.toList());
            teacherId = subjectClassTeacherMappingList.get(0).getTeacher().getId();
        }
        if (queryString == null) {
            LOGGER.debug("queryString is null. Returning false.");
            return false;
        } else {
            LOGGER.debug(queryString);
            Query query = currentSession().createNativeQuery(queryString);
            query.setParameter("teacherId", teacherId);
            query.setParameter("idList", idList);
            LOGGER.debug("teacherId = {} , idList = {}", teacherId, idList);
            LOGGER.debug("query.list().size() = {}, expectedSize = {}", query.list().size(), expectedSize);
            return query.list().size() == expectedSize;
        }
    }

    @Override
    public List<Content> getContent(String role, Integer roleId,
            Integer schoolId, Integer subjectId, Integer studentId,
            Integer classId, Integer studyGroupId, Date startDate, Date endDate,
            Integer offset) {

        String queryString = "from Content c where c.createdAt >= :startDate and c.createdAt <= :endDate and c.school.id = :schoolId order by c.createdAt desc";
        Query query = currentSession().createQuery(queryString);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        query.setParameter("schoolId", schoolId);

        query.setFirstResult(offset * Constants.LIST_LIMIT);
        query.setMaxResults(Constants.LIST_LIMIT);

        List<Content> contentList = query.list();

        if (null == role) {
            return new ArrayList();
        } else switch (role) {
            case Constants.PARENT:
                return filterContentForParent(subjectId, studentId, contentList);
            case Constants.ADMIN:
                return filterContentForAdmin(contentList);
            case Constants.TEACHER:
                if (classId == null && studyGroupId == null) {
                    return new ArrayList();
                }
                return filterContentForTeacher(subjectId, contentList, roleId, classId, studyGroupId);//teacherId
            default:
                return new ArrayList();
        }
    }

    private List<Content> filterContentForTeacher(Integer subjectId, List<Content> contentList, Integer teacherId, Integer classId, Integer studyGroupId) {

        List<Content> finalContentList = new ArrayList();

        for (Content content : contentList) {

            if (classId != null) {

                List<SubjectClassTeacherMapping> subjectClassTeacherMappingList = content.getSubjectClassTeacherMappingList();

                for (SubjectClassTeacherMapping sctm : subjectClassTeacherMappingList) {

                    if (sctm.getTeacher().getId().equals(teacherId)) {

                        if (sctm.getClass1().getId().equals(classId)) {

                            if ((subjectId != null && sctm.getSubject().getId().equals(subjectId)) || subjectId == null) {

                                sctm.getSubject().getId();
                                sctm.getSubject().getName();
                                sctm.getClass1().getId();
                                sctm.getClass1().getStandard();
                                sctm.getClass1().getSection();
                                sctm.getTeacher().getUser().getName();

                                finalContentList.add(content);
                            }
                        }
                    }
                }
            } else {

                List<StudyGroupTeacherMapping> studyGroupTeacherMappingList = content.getStudyGroupTeacherMappingList();

                for (StudyGroupTeacherMapping sgtm : studyGroupTeacherMappingList) {

                    if (sgtm.getTeacher().getId().equals(teacherId)) {

                        if (sgtm.getStudyGroup().getId().equals(studyGroupId)) {

                            if ((subjectId != null && sgtm.getStudyGroup().getSubject().getId().equals(subjectId)) || subjectId == null) {

                                sgtm.getStudyGroup().getSubject().getId();
                                sgtm.getStudyGroup().getSubject().getName();
                                sgtm.getStudyGroup().getId();
                                sgtm.getTeacher().getUser().getName();

                                finalContentList.add(content);
                            }
                        }
                    }
                }
            }
        }

        return finalContentList;
    }

    private List<Content> filterContentForParent(Integer subjectId, Integer studentId, List<Content> contentList) {

        Student student = studentDao.find(studentId);
        Integer classId = null;
        Integer studyGroupId = null;

        List<StudentClassMapping> studentClassMappingList = student.getStudentClassMappingList();

        if (studentClassMappingList != null && !studentClassMappingList.isEmpty()) {

            Integer currentClassId = studentClassMappingList.get(0).getClass1().getId();
            Date latestDate = studentClassMappingList.get(0).getCreatedAt();

            for (StudentClassMapping scm : studentClassMappingList) {
                if (scm.getCreatedAt().after(latestDate)) {
                    currentClassId = scm.getClass1().getId();
                    latestDate = scm.getCreatedAt();
                }
            }

            classId = currentClassId;
        }

        List<StudentStudyGroupMapping> studentStudyGroupMappingList = student.getStudentStudyGroupMappingList();
        if (studentStudyGroupMappingList != null && !studentStudyGroupMappingList.isEmpty()) {

            Integer currentStudyGroupId = studentStudyGroupMappingList.get(0).getStudyGroup().getId();
            Date latestDate = studentStudyGroupMappingList.get(0).getCreatedAt();

            for (StudentStudyGroupMapping ssgm : studentStudyGroupMappingList) {
                if (ssgm.getCreatedAt().after(latestDate)) {
                    currentStudyGroupId = ssgm.getStudyGroup().getId();
                    latestDate = ssgm.getCreatedAt();
                }
            }

            studyGroupId = currentStudyGroupId;
        }

        List<Content> finalContentList = new ArrayList();

        for (Content content : contentList) {

            if (classId != null) {

                List<SubjectClassTeacherMapping> subjectClassTeacherMappingList = content.getSubjectClassTeacherMappingList();

                for (SubjectClassTeacherMapping sctm : subjectClassTeacherMappingList) {

                    Class clazz = sctm.getClass1();

                    if (clazz.getId().equals(classId)) {

                        if ((subjectId != null && subjectId.equals(sctm.getSubject().getId())) || subjectId == null) {

                            sctm.getSubject().getId();
                            sctm.getSubject().getName();
                            sctm.getClass1().getId();
                            sctm.getClass1().getStandard();
                            sctm.getClass1().getSection();

                            finalContentList.add(content);
                        }
                    }
                }
            }

            if (studyGroupId != null) {

                List<StudyGroupTeacherMapping> studyGroupTeacherMappingList = content.getStudyGroupTeacherMappingList();

                for (StudyGroupTeacherMapping sgtm : studyGroupTeacherMappingList) {

                    StudyGroup studyGroup = sgtm.getStudyGroup();

                    if (studyGroup.getId().equals(studyGroupId)) {

                        if ((subjectId != null && subjectId.equals(sgtm.getStudyGroup().getSubject().getId())) || subjectId == null) {

                            sgtm.getStudyGroup().getSubject().getId();
                            sgtm.getStudyGroup().getSubject().getName();
                            sgtm.getStudyGroup().getId();

                            finalContentList.add(content);
                        }
                    }
                }
            }

        }

        for (Content content : finalContentList) {

            switch (content.getType().toUpperCase()) {
                case Constants.URL_CONTENT_TYPE:
                    UrlContent uc = content.getUrlContent();
                    uc.getUrl();
                    break;
                case Constants.FILE_UPLOAD_FOLDER:
                    Attachment attachment = content.getAttachment();
                    attachment.getFileUrl();
                    attachment.getFileName();
                    break;
                case Constants.NOTE_CONTENT_TYPE:
                    Note note = content.getNote();
                    note.getName();
                    note.getNoteContent();
                    break;
            }
        }

        return finalContentList;
    }

    private List<Content> filterContentForAdmin(List<Content> contentList) {

        contentList.stream().map((content) -> {
            List<SubjectClassTeacherMapping> subjectClassTeacherMappingList = content.getSubjectClassTeacherMappingList();
            if (subjectClassTeacherMappingList != null) {
                subjectClassTeacherMappingList.stream().map((sctm) -> {
                    sctm.getSubject().getId();
                    return sctm;
                }).map((sctm) -> {
                    sctm.getSubject().getName();
                    return sctm;
                }).map((sctm) -> {
                    sctm.getClass1().getId();
                    return sctm;
                }).map((sctm) -> {
                    sctm.getClass1().getStandard();
                    return sctm;
                }).forEachOrdered((sctm) -> {
                    sctm.getClass1().getSection();
                });
            }
            List<StudyGroupTeacherMapping> studyGroupTeacherMappingList = content.getStudyGroupTeacherMappingList();
            return studyGroupTeacherMappingList;
        }).filter((studyGroupTeacherMappingList) -> (studyGroupTeacherMappingList != null)).forEachOrdered((studyGroupTeacherMappingList) -> {
            studyGroupTeacherMappingList.stream().map((sgtm) -> {
                sgtm.getStudyGroup().getSubject().getId();
                return sgtm;
            }).map((sgtm) -> {
                sgtm.getStudyGroup().getSubject().getName();
                return sgtm;
            }).forEachOrdered((sgtm) -> {
                sgtm.getStudyGroup().getId();
            });
        });

        return contentList;
    }
}
