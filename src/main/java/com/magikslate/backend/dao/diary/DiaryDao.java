/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.diary;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.entity.AnecdotalRecord;
import com.magikslate.backend.entity.Diary;
import com.magikslate.backend.entity.ExtraCurricularDiary;
import com.magikslate.backend.entity.GeneralDiary;
import com.magikslate.backend.entity.Kudo;
import com.magikslate.backend.entity.LessonProgress;
import com.magikslate.backend.entity.Test;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public interface DiaryDao extends GenericDao<Diary, Integer> {

    public List<Diary> getByClass(Integer classId, Integer offset);

    public void add(Test testDiary);

    public void add(ExtraCurricularDiary ecDiary);

    public void add(LessonProgress lessonProgressDiary);

    public void add(AnecdotalRecord anecdotalRecordDiary);

    public void add(Kudo kudoDiary);

    public void add(GeneralDiary generalDiary);

    public List<Diary> getDiary(String role, Integer roleId, Integer schoolId, Integer studentId, Integer classId, Integer subjectId, Integer studyGroupId, Integer offset, Date startDate, Date endDate);

}
