/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.diary;

import com.magikslate.backend.dao.GenericDaoImpl;
import com.magikslate.backend.entity.AnecdotalRecord;
import com.magikslate.backend.entity.Diary;
import com.magikslate.backend.entity.ExtraCurricularDiary;
import com.magikslate.backend.entity.GeneralDiary;
import com.magikslate.backend.entity.Kudo;
import com.magikslate.backend.entity.LessonProgress;
import com.magikslate.backend.entity.Subject;
import com.magikslate.backend.entity.Teacher;
import com.magikslate.backend.entity.Test;
import com.magikslate.backend.entity.User;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author sankalpkulshrestha
 */
@Repository
public class DiaryDaoImpl extends GenericDaoImpl<Diary, Integer>
        implements DiaryDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(DiaryDaoImpl.class);

    @Override
    public List<Diary> getByClass(Integer classId, Integer offset) {
        Query query;
        try (Session session = currentSession()) {

            LOGGER.debug(Constants.DIARY_BY_CLASS);
            query = session.createQuery(Constants.DIARY_BY_CLASS);
            query.setParameter("classId", classId);

            LOGGER.debug("{}", Utils.listQueryParams(query));
            query.setMaxResults(Constants.LIST_LIMIT);
            query.setFirstResult(offset);
            LOGGER.debug("offset = {}, max results = {}", offset, Constants.LIST_LIMIT);
            return query.list();
        }
    }

    @Override
    public void add(Test testDiary) {
        String queryString = "insert into Test(school_id,"
                + " StudyGroupTeacherMapping_Id, SubjectClassTeacherMapping_Id, Title, "
                + "Description, MaxMarks, TestDate, CreatedAt, UpdatedAt, Diary_Id )"
                + " values(:schoolId, :sgtmId, :sctmId, :title, :description, :maxMarks,"
                + ":testDate, NOW(), NOW(), :diaryId)";

        LOGGER.debug(Constants.DIARY_BY_CLASS);

        Query query = currentSession().createNativeQuery(queryString);

        query.setParameter("diaryId", testDiary.getDiary().getId());
        query.setParameter("schoolId", testDiary.getSchool().getId());
        query.setParameter("sgtmId", testDiary.getStudyGroupTeacherMapping());
        query.setParameter("sctmId", testDiary.getSubjectClassTeacherMapping());
        query.setParameter("title", testDiary.getTitle());
        query.setParameter("description", testDiary.getDescription());
        query.setParameter("maxMarks", testDiary.getMaxMarks());
        query.setParameter("testDate", testDiary.getTestDate());

        LOGGER.debug("{}", Utils.listQueryParams(query));
        query.executeUpdate();
    }

    @Override
    public void add(ExtraCurricularDiary ecDiary) {
        String queryString = "insert into ExtraCurricularDiary values(:diaryId, :type, :description)";

        LOGGER.debug(queryString);

        Query query = currentSession().createNativeQuery(queryString);

        query.setParameter("diaryId", ecDiary.getDiaryId());
        query.setParameter("type", ecDiary.getType());
        query.setParameter("description", ecDiary.getDescription());

        LOGGER.debug("{}", Utils.listQueryParams(query));
        query.executeUpdate();
    }

    @Override
    public void add(LessonProgress lessonProgressDiary) {
        String queryString = "insert into LessonProgress values(:diaryId, :chapterName, :pagesCompleted)";

        LOGGER.debug(queryString);

        Query query = currentSession().createNativeQuery(queryString);
        query.setParameter("diaryId", lessonProgressDiary.getDiaryId());
        query.setParameter("chapterName", lessonProgressDiary.getChapterName());
        query.setParameter("pagesCompleted", lessonProgressDiary.getPagesCompleted());

        LOGGER.debug("{}", Utils.listQueryParams(query));
        query.executeUpdate();
    }

    @Override
    public void add(AnecdotalRecord anecdotalRecordDiary) {
        String queryString = "insert into AnecdotalRecord values(:diaryId, :description)";

        LOGGER.debug(queryString);

        Query query = currentSession().createNativeQuery(queryString);
        query.setParameter("diaryId", anecdotalRecordDiary.getDiaryId());
        query.setParameter("description", anecdotalRecordDiary.getDescription());

        LOGGER.debug("{}", Utils.listQueryParams(query));
        query.executeUpdate();
    }

    @Override
    public void add(Kudo kudoDiary) {
        String queryString = "insert into Kudo values(:diaryId, :description)";

        LOGGER.debug(queryString);

        Query query = currentSession().createNativeQuery(queryString);
        query.setParameter("diaryId", kudoDiary.getDiaryId());
        query.setParameter("description", kudoDiary.getDescription());

        LOGGER.debug("{}", Utils.listQueryParams(query));
        query.executeUpdate();
    }

    @Override
    public void add(GeneralDiary generalDiary) {
        String queryString = "insert into GeneralDiary values(:diaryId, :description, :title)";

        LOGGER.debug(queryString);

        Query query = currentSession().createNativeQuery(queryString);
        query.setParameter("diaryId", generalDiary.getDiaryId());
        query.setParameter("title", generalDiary.getTitle());
        query.setParameter("description", generalDiary.getDescription());

        LOGGER.debug("{}", Utils.listQueryParams(query));
        query.executeUpdate();
    }

    @Override
    public List<Diary> getDiary(String role, Integer roleId, Integer schoolId,
            Integer studentId, Integer classId, Integer subjectId,
            Integer studyGroupId, Integer offset, Date startDate, Date endDate) {

        switch (role) {
            case Constants.PARENT:
                return getDiaryListByStudent(roleId, schoolId, studentId, classId, subjectId, studyGroupId, offset, startDate, endDate);
            case Constants.TEACHER:
                return getDiaryListForTeacher(roleId, schoolId, classId, subjectId, studyGroupId, offset, startDate, endDate);
            case Constants.ADMIN:
                return getDiaryListForAdmin(schoolId, offset, startDate, endDate);
            default:
                return new ArrayList();
        }
    }

    private List<Diary> getDiaryListByStudent(Integer parentId, Integer schoolId, Integer studentId, Integer classId, Integer subjectId, Integer studyGroupId, Integer offset, Date startDate, Date endDate) {

        if (studentId == null) {
            return new ArrayList();
        }

        Session session = currentSession();

        StringBuilder queryBuilder = new StringBuilder("select distinct(a.did) as did, a.dtype, a.chapterName, a.pagesCompleted,"
                + "a.ardesc, a.kdesc, a.gdesc, a.ectype, a.ecdesc, a.testid, a.testtitle, a.tdesc, a.tmm, a.ttd, a.subject_id, a.name, a.createdAt, a.gdtitle from ("
                + "((select d.id as did, d.type as dtype,"
                + " lp.chapterName, lp.pagesCompleted, "
                + " ar.description as ardesc, k.description as kdesc, gd.description as gdesc,"
                + "ec.type as ectype, ec.description as ecdesc, t.id as testid, t.title as testtitle,"
                + "t.description as tdesc, t.maxMarks as tmm, t.testDate as ttd, d.subject_id, sb.name, d.createdAt, gd.title as gdtitle from Diary d "
                + "left join LessonProgress lp on lp.diary_id = d.id left join AnecdotalRecord ar on ar.diary_id = d.id "
                + "left join Kudo k on k.diary_id = d.id left join GeneralDiary gd on gd.diary_id = d.id left join "
                + "ExtraCurricularDiary ec on ec.diary_id = d.id left join Test t on t.diary_id = d.id "
                + "left join Subject sb on sb.id = d.subject_id "
                + "left join SubjectClassTeacherMapping sctm on sctm.id = d.SubjectClassTeacherMapping_Id left join Class "
                + "c on c.id = sctm.class_id left join StudentClassMapping scm on scm.class_id = sctm.class_id "
                + "left join Student s on s.id = scm.student_id where "
                + "c.school_id = :schoolId and s.id = :studentId ");

        if (subjectId != null) {
            queryBuilder.append(" and sb.id = :subjectId ");
        }
        if (classId != null) {
            queryBuilder.append(" and c.id = :classId");
        }

        queryBuilder = queryBuilder.append(" and sctm.deletedAt IS NULL and sb.deletedAt IS NULL and scm.deletedAt IS NULL ");

        if (startDate != null) {
            queryBuilder.append(" and d.createdAt >= :startDate");
        }

        if (endDate != null) {
            queryBuilder.append(" and d.createdAt <= :endDate");
        }

        queryBuilder = queryBuilder.append(")"
                + "UNION"
                + "("
                + "select d.id as did, d.type as dtype,"
                + " lp.chapterName, lp.pagesCompleted, "
                + " ar.description as ardesc, k.description as kdesc, gd.description as gdesc,"
                + "ec.type as ectype, ec.description as ecdesc, t.id as testid, t.title as testtitle,"
                + "t.description as tdesc, t.maxMarks as tmm, t.testDate as ttd, d.subject_id, sb.name, d.createdAt, gd.title as gdtitle from Diary d "
                + "left join LessonProgress lp on lp.diary_id = d.id left join AnecdotalRecord ar on ar.diary_id = d.id "
                + "left join Kudo k on k.diary_id = d.id left join GeneralDiary gd on gd.diary_id = d.id left join "
                + "ExtraCurricularDiary ec on ec.diary_id = d.id left join Test t on t.diary_id = d.id "
                + "left join Subject sb on sb.id = d.subject_id "
                + "left join StudyGroupTeacherMapping sgtm on sgtm.id = d.StudyGroupTeacherMapping_Id left "
                + "join StudentStudyGroupMapping ssgm on ssgm.studygroup_id = sgtm.studygroup_id left join StudyGroup sg on "
                + "sg.id = sgtm.studygroup_id left join Student st on st.id = ssgm.student_id where sg.school_id = :schoolId "
                + "and st.id = :studentId ");

        if (subjectId != null) {
            queryBuilder.append(" and sb.id = :subjectId ");
        }

        if (studyGroupId != null) {
            queryBuilder.append(" and sg.id = :studyGroupId");
        }

        if (startDate != null) {
            queryBuilder.append(" and d.createdAt >= :startDate");
        }

        if (endDate != null) {
            queryBuilder.append(" and d.createdAt <= :endDate");
        }

        queryBuilder = queryBuilder.append(" and sgtm.deletedAt IS NULL and sb.deletedAt IS NULL and ssgm.deletedAt IS NULL)");

        queryBuilder = queryBuilder.append("UNION")
                .append("(")
                .append("select d.id as did, d.type as dtype,"
                        + " lp.chapterName, lp.pagesCompleted, "
                        + " ar.description as ardesc, k.description as kdesc, gd.description as gdesc,"
                        + "ec.type as ectype, ec.description as ecdesc, t.id as testid, t.title as testtitle,"
                        + "t.description as tdesc, t.maxMarks as tmm, t.testDate as ttd, d.subject_id, sb.name, d.createdAt, gd.title as gdtitle from Diary d "
                        + "left join LessonProgress lp on lp.diary_id = d.id left join AnecdotalRecord ar on ar.diary_id = d.id "
                        + "left join Kudo k on k.diary_id = d.id left join GeneralDiary gd on gd.diary_id = d.id left join "
                        + "ExtraCurricularDiary ec on ec.diary_id = d.id left join Test t on t.diary_id = d.id "
                        + "left join Subject sb on sb.id = d.subject_id "
                        + "left join StudentDiaryMapping sdm on sdm.diary_id = d.id "
                        + "left join Student st on st.id = sdm.student_id "
                        + "left join School s on s.id = st.school_id "
                        + "where st.id = :studentId and st.deletedAt IS NULL and s.deletedAt IS NULL and s.deletedAt IS NULL and s.id = :schoolId");

        if (subjectId != null) {
            queryBuilder.append(" and sb.id = :subjectId ");
        }

        if (startDate != null) {
            queryBuilder.append(" and d.createdAt >= :startDate");
        }

        if (endDate != null) {
            queryBuilder.append(" and d.createdAt <= :endDate");
        }

        queryBuilder = queryBuilder.append(")");

        queryBuilder = queryBuilder.append("))a order by a.createdAt desc");

        LOGGER.debug(queryBuilder.toString());

        Query query = session.createNativeQuery(queryBuilder.toString());

        query.setFirstResult(offset * Constants.LIST_LIMIT);
        query.setMaxResults(Constants.LIST_LIMIT);

        query.setParameter("studentId", studentId);
        query.setParameter("schoolId", schoolId);

        if (subjectId != null) {
            query.setParameter("subjectId", subjectId);
        }

        if (classId != null) {
            query.setParameter("classId", classId);
        }

        if (studyGroupId != null) {
            query.setParameter("studyGroupId", studyGroupId);
        }

        if (startDate != null) {
            query.setParameter("startDate", startDate);
        }

        if (endDate != null) {
            query.setParameter("endDate", endDate);
        }

        LOGGER.debug("{}", Utils.listQueryParams(query));

        List<Diary> diaryList = new ArrayList();
        List<Object[]> result = query.list();

        for (Object[] row : result) {
            int diaryId = (int) row[0];

            String diaryType = (String) row[1];
            String chapterName = (String) row[2];
            String pagesCompleted = (String) row[3];
            String arDesc = (String) row[4];
            String kDesc = (String) row[5];
            String gdDesc = (String) row[6];
            String ecType = (String) row[7];
            String ecDesc = (String) row[8];
            Integer testId = (Integer) row[9];
            String testTitle = (String) row[10];
            String testDesc = (String) row[11];
            Integer testMarks = (Integer) row[12];
            Date testDate = (Date) row[13];
            Integer sid = (Integer) row[14];
            String sName = (String) row[15];
            Date createdAt = (Date) row[16];
            String gdTitle = (String) row[17];

            Diary diary = new Diary(diaryId);
            diary.setType(diaryType);
            diary.setSubject(new Subject(sid));
            diary.setCreatedAt(createdAt);

            switch (diaryType) {

                case Constants.KUDO_DIARY:
                    Kudo kudo = new Kudo(diaryId);
                    kudo.setDescription(kDesc);
                    kudo.setDiary(diary);
                    diary.setKudo(kudo);
                    break;
                case Constants.ANECDOTAL_RECORD_DIARY:
                    AnecdotalRecord ar = new AnecdotalRecord(diaryId);
                    ar.setDescription(arDesc);
                    ar.setDiary(diary);
                    diary.setAnecdotalRecord(ar);
                    break;
                case Constants.GENERAL_DIARY:
                    GeneralDiary gd = new GeneralDiary(diaryId);
                    gd.setTitle(gdTitle);
                    gd.setDescription(gdDesc);
                    gd.setDiary(diary);
                    diary.setGeneralDiary(gd);
                    break;
                case Constants.LESSON_PROGRESS_DIARY:
                    LessonProgress lp = new LessonProgress(diaryId);
                    lp.setChapterName(chapterName);
                    lp.setPagesCompleted(pagesCompleted);
                    lp.setDiary(diary);
                    diary.setLessonProgress(lp);
                    break;
                case Constants.EX_DIARY:
                    ExtraCurricularDiary ec = new ExtraCurricularDiary(diaryId);
                    ec.setDescription(ecDesc);
                    ec.setDiary(diary);
                    diary.setExtraCurricularDiary(ec);
                    break;
                case Constants.TEST_DIARY:
                    Test test = new Test(testId);
                    test.setDiary(diary);
                    test.setTitle(testTitle);
                    test.setDescription(testDesc);
                    test.setMaxMarks(testMarks);
                    test.setTestDate(testDate);
                    diary.setTestList(Arrays.asList(test));
            }
            diaryList.add(diary);
        }

        LOGGER.debug("Returning diaryList = {}", diaryList);

        return diaryList;
    }

    private List<Diary> getDiaryListForTeacher(Integer teacherId, Integer schoolId, Integer classId, Integer subjectId, Integer studyGroupId, Integer offset, Date startDate, Date endDate) {

        if (teacherId == null) {
            return new ArrayList();
        }

        if (classId == null && studyGroupId == null) {
            return new ArrayList();
        }

        Session session = currentSession();

        StringBuilder queryBuilder = new StringBuilder("select distinct(a.did) as did, a.dtype, a.chapterName, a.pagesCompleted,"
                + "a.ardesc, a.kdesc, a.gdesc, a.ectype, a.ecdesc, a.testid, a.testtitle, a.tdesc, a.tmm, a.ttd, a.subject_id, a.sbname, a.dc, a.gdtitle from"
                + "((");
        if (classId != null) {
            queryBuilder = queryBuilder.append("select d.id as did, d.type as dtype,"
                    + " lp.chapterName, lp.pagesCompleted, "
                    + " ar.description as ardesc, k.description as kdesc, gd.description as gdesc,"
                    + "ec.type as ectype, ec.description as ecdesc, t.id as testid, t.title as testtitle,"
                    + "t.description as tdesc, t.maxMarks as tmm, t.testDate as ttd, d.subject_id, sb.name sbname, d.createdAt dc, gd.title as gdtitle"
                    + " from Diary d left join LessonProgress lp on lp.diary_id = d.id left join AnecdotalRecord ar on ar.diary_id = d.id "
                    + "left join Kudo k on k.diary_id = d.id left join GeneralDiary gd on gd.diary_id = d.id left join "
                    + "ExtraCurricularDiary ec on ec.diary_id = d.id left join Test t on t.diary_id = d.id "
                    + "left join Subject sb on sb.id = d.subject_id "
                    + "left join SubjectClassTeacherMapping sctm on sctm.id = d.SubjectClassTeacherMapping_Id left "
                    + "join Class c on c.id = sctm.class_id left join StudentClassMapping scm on scm.class_id = sctm.class_id "
                    + "left join Student s on s.id = scm.student_id where 1=1 and d.teacher_id = :teacherId "
                    + "and c.id = :classId and c.school_id = :schoolId and sctm.deletedAt IS NULL and sb.deletedAt IS NULL and scm.deletedAt IS NULL");
        } else if (studyGroupId != null) {
            queryBuilder = queryBuilder.append("select d.id as did, d.type as dtype,"
                    + " lp.chapterName, lp.pagesCompleted, "
                    + " ar.description as ardesc, k.description as kdesc, gd.description as gdesc,"
                    + "ec.type as ectype, ec.description as ecdesc, t.id as testid, t.title as testtitle,"
                    + "t.description as tdesc, t.maxMarks as tmm, t.testDate as ttd, d.subject_id, sb.name sbname, d.createdAt dc, gd.title as gdtitle"
                    + " from Diary d left join LessonProgress lp on lp.diary_id = d.id left join AnecdotalRecord ar on ar.diary_id = d.id "
                    + "left join Kudo k on k.diary_id = d.id left join GeneralDiary gd on gd.diary_id = d.id left join "
                    + "ExtraCurricularDiary ec on ec.diary_id = d.id left join Test t on t.diary_id = d.id "
                    + "left join Subject sb on sb.id = d.subject_id "
                    + "left join StudyGroupTeacherMapping sgtm on sgtm.id = d.StudyGroupTeacherMapping_Id left "
                    + "join StudyGroup sg on sg.id = sgtm.studygroup_id left join StudentStudyGroupMapping ssgm "
                    + "on ssgm.studygroup_id = sgtm.studygroup_id left join Student s on s.id = ssgm.student_id"
                    + "and d.teacher_id = :teacherId and sg.id = :studyGroupId and sg.school_id = :schoolId"
                    + " and sgtm.deletedAt IS NULL and sb.deletedAt IS NULL and ssgm.deletedAt IS NULL");
        }
        queryBuilder = queryBuilder.append(") UNION (");
        queryBuilder = queryBuilder.append("select d.id as did, d.type as dtype,")
                .append(" lp.chapterName, lp.pagesCompleted, "
                        + " ar.description as ardesc, k.description as kdesc, gd.description as gdesc,"
                        + "ec.type as ectype, ec.description as ecdesc, t.id as testid, t.title as testtitle,"
                        + "t.description as tdesc, t.maxMarks as tmm, t.testDate as ttd, d.subject_id, sb.name sbname, d.createdAt dc, gd.title as gdtitle"
                        + " from Diary d left join LessonProgress lp on lp.diary_id = d.id left join AnecdotalRecord ar on ar.diary_id = d.id "
                        + "left join Kudo k on k.diary_id = d.id left join GeneralDiary gd on gd.diary_id = d.id left join "
                        + "ExtraCurricularDiary ec on ec.diary_id = d.id left join Test t on t.diary_id = d.id "
                        + "left join Subject sb on sb.id = d.subject_id "
                        + "left join StudentDiaryMapping sdm on sdm.diary_id = d.id "
                        + "where sb.school_id = :schoolId and d.teacher_id = :teacherId and sb.deletedAt IS NULL");
        queryBuilder = queryBuilder.append(")) a where 1=1 ");

        if (subjectId != null) {
            queryBuilder.append(" and a.subject_id = :subjectId");
        }
        
        if (startDate != null) {
            queryBuilder.append(" and a.dc >= :startDate");
        }

        if (endDate != null) {
            queryBuilder.append(" and a.dc <= :endDate");
        }

        queryBuilder.append(" order by a.dc desc");

        LOGGER.debug(queryBuilder.toString());

        Query query = session.createNativeQuery(queryBuilder.toString());

        query.setParameter("teacherId", teacherId);
        query.setParameter("schoolId", schoolId);
        if (classId != null) {
            query.setParameter("classId", classId);

        } else if (studyGroupId != null) {
            query.setParameter("studyGroupId", studyGroupId);
        }

        if (subjectId != null) {
            query.setParameter("subjectId", subjectId);
        }
        
        if (startDate != null) {
            query.setParameter("startDate", startDate);
        }

        if (endDate != null) {
            query.setParameter("endDate", endDate);
        }

        LOGGER.debug("{}", Utils.listQueryParams(query));

        query.setFirstResult(offset * Constants.LIST_LIMIT);
        query.setMaxResults(Constants.LIST_LIMIT);

        LOGGER.debug("offset = {}, max results = {}", offset, Constants.LIST_LIMIT);

        List<Diary> diaryList = new ArrayList();
        List<Object[]> result = query.list();

        if (result == null) {
            return new ArrayList();
        } else {

            result.forEach(row -> {
                int diaryId = (int) row[0];
                String diaryType = (String) row[1];
                String chapterName = (String) row[2];
                String pagesCompleted = (String) row[3];
                String arDesc = (String) row[4];
                String kDesc = (String) row[5];
                String gdDesc = (String) row[6];
                String ecType = (String) row[7];
                String ecDesc = (String) row[8];
                Integer testId = (Integer) row[9];
                String testTitle = (String) row[10];
                String testDesc = (String) row[11];
                Integer testMarks = (Integer) row[12];
                Date testDate = (Date) row[13];
                Integer sid = (Integer) row[14];
                String sName = (String) row[15];
                Date createdAt = (Date) row[16];
                String gdTitle = (String) row[17];

                Diary diary = new Diary(diaryId);
                diary.setType(diaryType);
                diary.setSubject(new Subject(sid));
                diary.setCreatedAt(createdAt);

                switch (diaryType) {

                    case Constants.KUDO_DIARY:
                        Kudo kudo = new Kudo(diaryId);
                        kudo.setDescription(kDesc);
                        kudo.setDiary(diary);
                        diary.setKudo(kudo);
                        break;
                    case Constants.ANECDOTAL_RECORD_DIARY:
                        AnecdotalRecord ar = new AnecdotalRecord(diaryId);
                        ar.setDescription(arDesc);
                        ar.setDiary(diary);
                        diary.setAnecdotalRecord(ar);
                        break;
                    case Constants.GENERAL_DIARY:
                        GeneralDiary gd = new GeneralDiary(diaryId);
                        gd.setDescription(gdDesc);
                        gd.setTitle(gdTitle);
                        gd.setDiary(diary);
                        diary.setGeneralDiary(gd);
                        break;
                    case Constants.LESSON_PROGRESS_DIARY:
                        LessonProgress lp = new LessonProgress(diaryId);
                        lp.setChapterName(chapterName);
                        lp.setPagesCompleted(pagesCompleted);
                        lp.setDiary(diary);
                        diary.setLessonProgress(lp);
                        break;
                    case Constants.EX_DIARY:
                        ExtraCurricularDiary ec = new ExtraCurricularDiary(diaryId);
                        ec.setDescription(ecDesc);
                        ec.setDiary(diary);
                        diary.setExtraCurricularDiary(ec);
                        break;
                    case Constants.TEST_DIARY:
                        Test test = new Test(testId);
                        test.setDiary(diary);
                        test.setTitle(testTitle);
                        test.setDescription(testDesc);
                        test.setMaxMarks(testMarks);
                        test.setTestDate(testDate);
                        diary.setTestList(Arrays.asList(test));
                }
                diaryList.add(diary);
            });
        }

        LOGGER.debug("Returning diaryList = {}", diaryList);

        return diaryList;
    }

    private List<Diary> getDiaryListForAdmin(Integer schoolId, Integer offset, Date startDate, Date endDate) {

        if (schoolId == null) {
            return new ArrayList();
        }

        Session session = currentSession();

        String queryString = "select d.id as did, d.type as dtype,"
                + " lp.chapterName, lp.pagesCompleted, "
                + " ar.description as ardesc, k.description as kdesc, gd.description as gdesc,"
                + "ec.type as ectype, ec.description as ecdesc, t.id as testid, t.title as testtitle,"
                + "t.description as tdesc, t.maxMarks as tmm, t.testDate as ttd, d.subject_id, sb.name sbname, d.createdAt dc, gd.title as gdtitle,"
                + " tch.id as teacher_id, u.id as teacher_user_id, u.name as teacherName"
                + " from Diary d left join LessonProgress lp on lp.diary_id = d.id left join AnecdotalRecord ar on ar.diary_id = d.id "
                + "left join Kudo k on k.diary_id = d.id left join GeneralDiary gd on gd.diary_id = d.id left join "
                + "ExtraCurricularDiary ec on ec.diary_id = d.id left join Test t on t.diary_id = d.id "
                + "left join Subject sb on sb.id = d.subject_id join Teacher tch on tch.id = d.teacher_id"
                + " join User u on u.id = tch.user_id where sb.school_id = :schoolId and tch.deletedAt IS NULL and u.deletedAt IS NULL "
                + "and d.createdAt >= :startDate and d.createdAt <= :endDate order by d.createdAt desc";
        
        LOGGER.debug(queryString);
        
        Query query = session.createNativeQuery(queryString);
        
        query.setParameter("schoolId", schoolId);
        
        if (startDate != null) {
            query.setParameter("startDate", startDate);
        }

        if (endDate != null) {
            query.setParameter("endDate", endDate);
        }
        
        LOGGER.debug("{}", Utils.listQueryParams(query));
        
        query.setFirstResult(offset * Constants.LIST_LIMIT);
        query.setMaxResults(Constants.LIST_LIMIT);

        LOGGER.debug("offset = {}, max results = {}", offset, Constants.LIST_LIMIT);

        List<Diary> diaryList = new ArrayList();
        List<Object[]> result = query.list();

        if (result == null) {
            return new ArrayList();
        } else {

            result.forEach(row -> {
                int diaryId = (int) row[0];
                String diaryType = (String) row[1];
                String chapterName = (String) row[2];
                String pagesCompleted = (String) row[3];
                String arDesc = (String) row[4];
                String kDesc = (String) row[5];
                String gdDesc = (String) row[6];
                String ecType = (String) row[7];
                String ecDesc = (String) row[8];
                Integer testId = (Integer) row[9];
                String testTitle = (String) row[10];
                String testDesc = (String) row[11];
                Integer testMarks = (Integer) row[12];
                Date testDate = (Date) row[13];
                Integer sid = (Integer) row[14];
                String sName = (String) row[15];
                Date createdAt = (Date) row[16];
                String gdTitle = (String) row[17];
                Integer teacherId = (Integer) row[18];
                Integer teacherUserId = (Integer) row[19];
                String teacherName = (String) row[20];

                Diary diary = new Diary(diaryId);
                diary.setType(diaryType);
                diary.setSubject(new Subject(sid));
                diary.setCreatedAt(createdAt);
                
                Teacher teacher = new Teacher(teacherId);
                User teacherUser = new User(teacherUserId);
                teacherUser.setName(teacherName);
                teacher.setUser(teacherUser);
                diary.setTeacher(teacher);

                switch (diaryType) {

                    case Constants.KUDO_DIARY:
                        Kudo kudo = new Kudo(diaryId);
                        kudo.setDescription(kDesc);
                        kudo.setDiary(diary);
                        diary.setKudo(kudo);
                        break;
                    case Constants.ANECDOTAL_RECORD_DIARY:
                        AnecdotalRecord ar = new AnecdotalRecord(diaryId);
                        ar.setDescription(arDesc);
                        ar.setDiary(diary);
                        diary.setAnecdotalRecord(ar);
                        break;
                    case Constants.GENERAL_DIARY:
                        GeneralDiary gd = new GeneralDiary(diaryId);
                        gd.setDescription(gdDesc);
                        gd.setTitle(gdTitle);
                        gd.setDiary(diary);
                        diary.setGeneralDiary(gd);
                        break;
                    case Constants.LESSON_PROGRESS_DIARY:
                        LessonProgress lp = new LessonProgress(diaryId);
                        lp.setChapterName(chapterName);
                        lp.setPagesCompleted(pagesCompleted);
                        lp.setDiary(diary);
                        diary.setLessonProgress(lp);
                        break;
                    case Constants.EX_DIARY:
                        ExtraCurricularDiary ec = new ExtraCurricularDiary(diaryId);
                        ec.setDescription(ecDesc);
                        ec.setDiary(diary);
                        diary.setExtraCurricularDiary(ec);
                        break;
                    case Constants.TEST_DIARY:
                        Test test = new Test(testId);
                        test.setDiary(diary);
                        test.setTitle(testTitle);
                        test.setDescription(testDesc);
                        test.setMaxMarks(testMarks);
                        test.setTestDate(testDate);
                        diary.setTestList(Arrays.asList(test));
                }
                diaryList.add(diary);
            });
        }

        LOGGER.debug("Returning diaryList = {}", diaryList);

        return diaryList;
    }

}
