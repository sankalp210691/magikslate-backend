/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.board;

import com.magikslate.backend.dao.GenericDaoImpl;
import com.magikslate.backend.entity.Board;
import org.springframework.stereotype.Repository;

/**
 *
 * @author sankalpkulshrestha
 */
@Repository
public class BoardDaoImpl extends GenericDaoImpl<Board, Integer>
        implements BoardDao {

}
