/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.board;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.entity.Board;

/**
 *
 * @author sankalpkulshrestha
 */
public interface BoardDao extends GenericDao<Board, Integer> {

}
