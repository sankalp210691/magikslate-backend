/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.student;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.entity.Student;
import com.magikslate.backend.entity.StudentClassMapping;
import com.magikslate.backend.entity.StudentParentMapping;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public interface StudentDao extends GenericDao<Student, Integer> {

    public List<Integer> createBulk(List<Student> studentsList, Integer schoolId);
    
    public void getExistingStudentClassMappingEntitiesList(List<StudentClassMapping> studentClassMappingList);

    public void mapToClass(List<StudentClassMapping> studentClassMappingList);
    
    public void getStudentParentMappingEntitiesList(List<StudentParentMapping> studentParentMappingList);

    public void mapToParent(List<StudentParentMapping> studentParentMappingList);

    public List<Student> getStudentListBySchool(Integer schoolId);

    public StudentClassMapping getCurrentStudentClassMapping(Integer studentId);

}
