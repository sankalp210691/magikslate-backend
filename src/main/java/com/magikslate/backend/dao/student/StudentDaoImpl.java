/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.student;

import com.magikslate.backend.dao.GenericDaoImpl;
import com.magikslate.backend.entity.Student;
import com.magikslate.backend.entity.StudentClassMapping;
import com.magikslate.backend.entity.StudentParentMapping;
import com.magikslate.backend.entity.StudentStudyGroupMapping;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author sankalpkulshrestha
 */
@Repository
public class StudentDaoImpl extends GenericDaoImpl<Student, Integer>
        implements StudentDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentDaoImpl.class);

    @Override
    public Student find(Integer id) {
        Student student = super.find(id);
        if (student == null) {
            return null;
        }
        List<StudentClassMapping> studentClassMappingList = student.getStudentClassMappingList();
        List<StudentStudyGroupMapping> studentStudyGroupMapping = student.getStudentStudyGroupMappingList();
        if (studentClassMappingList != null) {
            for (StudentClassMapping scm : studentClassMappingList) {
                LOGGER.info(scm.toString());
            }
        }
        if (studentStudyGroupMapping != null) {
            for (StudentStudyGroupMapping ssgm : studentStudyGroupMapping) {
                LOGGER.info(ssgm.toString());
            }
        }
        List<StudentParentMapping> studentParentMappingList = student.getStudentParentMappingList();
        for (StudentParentMapping spm : studentParentMappingList) {
            LOGGER.info(spm.toString());
            LOGGER.info(spm.getParent().getUser().toString());
            LOGGER.info(spm.getParent().getAddress());
        }
        return student;
    }

    @Override
    public List<Integer> createBulk(List<Student> studentList, Integer schoolId) {
        List<Integer> idList = new ArrayList();

        Session session = currentSession();
        Query query = session.createQuery("select s.id, s.admissionCode from Student s where s.admissionCode in (:admissionCodeList) and s.school.id = :schoolId");
        List<String> admissionCodeList = studentList.stream().map(Student::getAdmissionCode)
                .collect(Collectors.toCollection(ArrayList::new));

        query.setParameterList("admissionCodeList", admissionCodeList);
        query.setParameter("schoolId", schoolId);

        List<Object[]> rows = query.list();
        Set<String> admissionCodeSet = new HashSet();
        rows.forEach(row -> {
            idList.add((Integer) row[0]);
            admissionCodeSet.add((String) row[1]);
        });

        int index = 0;
        for (Student student : studentList) {
            if (admissionCodeSet.contains(student.getAdmissionCode())) {
                continue;
            }
            idList.add((Integer) session.save(student));
            if (index % Constants.HIBERNATE_BATCH_SIZE == 0) {
                session.flush();
                session.clear();
            }
            index++;
        }
        session.flush();
        session.clear();
        return idList;
    }

    @Override
    public void mapToClass(List<StudentClassMapping> studentClassMappingList) {
        Session session = currentSession();
        int index = 0;
        for (StudentClassMapping studentClassMapping : studentClassMappingList) {
            session.saveOrUpdate(studentClassMapping);
            if (index % Constants.HIBERNATE_BATCH_SIZE == 0) {
                session.flush();
                session.clear();
            }
            index++;
        }
    }

    @Override
    public void mapToParent(List<StudentParentMapping> studentParentMappingList) {
        Session session = currentSession();
        int index = 0;
        for (StudentParentMapping studentParentMapping : studentParentMappingList) {
            session.saveOrUpdate(studentParentMapping);
            if (index % Constants.HIBERNATE_BATCH_SIZE == 0) {
                session.flush();
                session.clear();
            }
            index++;
        }
    }

    @Override
    public List<Student> getStudentListBySchool(Integer schoolId) {
        Query query = currentSession().createQuery("from Student s where s.school.id = :schoolId and s.deletedAt IS NULL");
        query.setParameter("schoolId", schoolId);
        return query.list();
    }

    @Override
    public void getExistingStudentClassMappingEntitiesList(List<StudentClassMapping> studentClassMappingList) {
        if (studentClassMappingList != null && !studentClassMappingList.isEmpty()) {
            Session session = currentSession();
            Query query = session.createNativeQuery("select id, concat(student_id,\"_\", class_id, \"_\", sessionYear) as scs from StudentClassMapping where concat(student_id,\"_\", class_id, \"_\", sessionYear) in (:scs)");
            List<String> scsList = new ArrayList();
            studentClassMappingList.forEach((scm) -> {
                scsList.add(scm.getStudent().getId() + Constants.UNDERSCORE_SEPARATOR + scm.getClass1().getId() + Constants.UNDERSCORE_SEPARATOR + scm.getSessionYear());
            });
            query.setParameter("scs", scsList);

            List<Object[]> scmList = query.list();
            if (scmList != null) {
                Map<String, Integer> scsToscmIdMap = new HashMap();
                scmList.forEach((row) -> {
                    Integer id = (Integer) row[0];
                    String scs = (String) row[1];

                    scsToscmIdMap.put(scs, id);
                });
                studentClassMappingList.forEach((scm) -> {
                    String key = scm.getStudent().getId() + Constants.UNDERSCORE_SEPARATOR + scm.getClass1().getId() + Constants.UNDERSCORE_SEPARATOR + scm.getSessionYear();
                    if (scsToscmIdMap.get(key) != null) {
                        scm.setId(scsToscmIdMap.get(key));
                    }
                });
            }
        }
    }

    @Override
    public void getStudentParentMappingEntitiesList(List<StudentParentMapping> studentParentMappingList) {
        if (studentParentMappingList != null && !studentParentMappingList.isEmpty()) {
            Session session = currentSession();
            Query query = session.createNativeQuery("select id, concat(student_id,\"_\", parent_id) as sp from StudentParentMapping where concat(student_id,\"_\", parent_id) in (:sp)");
            List<String> spList = new ArrayList();
            studentParentMappingList.forEach((spm) -> {
                spList.add(spm.getStudent().getId() + Constants.UNDERSCORE_SEPARATOR + spm.getParent().getId());
            });
            query.setParameter("sp", spList);

            List<Object[]> spmList = query.list();
            if (spmList != null) {
                Map<String, Integer> spTospmIdMap = new HashMap();
                for (Object[] row : spmList) {

                    Integer id = (Integer) row[0];
                    String sp = (String) row[1];

                    spTospmIdMap.put(sp, id);
                }
                for (StudentParentMapping spm : studentParentMappingList) {

                    String key = spm.getStudent().getId() + Constants.UNDERSCORE_SEPARATOR + spm.getParent().getId();
                    if (spTospmIdMap.get(key) != null) {
                        spm.setId(spTospmIdMap.get(key));
                    }
                }
            }
        }
    }

    @Override
    public StudentClassMapping getCurrentStudentClassMapping(Integer studentId) {

        Session session = currentSession();

        String queryString = "from StudentClassMapping scm where scm.student.id = :studentId and scm.deletedAt IS NULL order by scm.createdAt desc";
        LOGGER.debug(queryString);
        Query query = session.createQuery(queryString);
        query.setParameter("studentId", studentId);
        LOGGER.debug("{}", Utils.listQueryParams(query));
        query.setMaxResults(1);//1 because we just need last result.
        List<StudentClassMapping> scmList = query.list();
        if (scmList == null || scmList.isEmpty()) {
            return null;
        } else {
            return scmList.get(0);
        }
    }

}
