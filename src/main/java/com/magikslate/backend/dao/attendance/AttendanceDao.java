/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.attendance;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.entity.Attendance;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public interface AttendanceDao extends GenericDao<Attendance, Integer> {

    public List<Integer> createBulk(List<Attendance> attendanceList);

    public List<Attendance> getAttendance(Integer schoolId, Integer classId, 
            Integer studyGroupId, Integer sessionYear, Date date);

    public void updateBulk(List<Attendance> attendanceList);

    public List<Attendance> getAttendanceByStudentIdentifier(Integer studentIdentifier, boolean b, Date startDate, Date endDate, Integer offset);

}
