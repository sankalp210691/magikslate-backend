/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.attendance;

import com.google.common.base.Joiner;
import com.magikslate.backend.dao.GenericDaoImpl;
import com.magikslate.backend.entity.Attendance;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import java.time.Year;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author sankalpkulshrestha
 */
@Repository
public class AttendanceDaoImpl extends GenericDaoImpl<Attendance, Integer>
        implements AttendanceDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(AttendanceDaoImpl.class);

    @Override
    public List<Integer> createBulk(List<Attendance> attendanceList) {
        List<Integer> idList = new ArrayList();

        Session session = currentSession();

        int index = 0;
        for (Attendance attendance : attendanceList) {
            idList.add((Integer) session.save(attendance));
            if (index % Constants.HIBERNATE_BATCH_SIZE == 0) {
                session.flush();
                session.clear();
            }
            index++;
        }

        return idList;
    }

    @Override
    public List<Attendance> getAttendance(Integer schoolId, Integer classId,
            Integer studyGroupId, Integer sessionYear, Date date) {

        Session session = currentSession();

        String queryString = null;
        Integer id = null;

        if (classId != null) {
            queryString = "select a from Attendance a where a.studentClassMapping.class1.id = :id and a.sessionYear = :sessionYear and a.studentClassMapping.class1.school.id = :schoolId and a.studentClassMapping.class1.school.deletedAt IS NULL";
            id = classId;
        } else if (studyGroupId != null) {
            queryString = "select a from Attendance a where a.studentStudyGroupMapping.studyGroup.id = :id and a.sessionYear = :sessionYear and a.studentStudyGroupMapping.studyGroup.school.id = :schoolId and a.studentStudyGroupMapping.studyGroup.school.deletedAt IS NULL";
            id = studyGroupId;
        } else {
            return new ArrayList();
        }

        Date endDate = null;
        if (date != null) {
            Calendar myCal = Calendar.getInstance();
            myCal.setTime(date);
            myCal.add(Calendar.HOUR, +Constants.HOURS_PER_DAY);
            endDate = myCal.getTime();
            queryString += " and a.createdAt >= :date and a.createdAt <= :endDate";
        }

        LOGGER.debug(queryString);

        Query query = session.createQuery(queryString);
        query.setParameter("sessionYear", sessionYear);
        query.setParameter("id", id);
        query.setParameter("schoolId", schoolId);
        if (date != null) {
            query.setParameter("date", date);
            query.setParameter("endDate", endDate);
        }
        LOGGER.debug("parameters : {}", Utils.listQueryParams(query));
        List<Attendance> attendanceList = query.list();

        return attendanceList;
    }

    @Override
    public void updateBulk(List<Attendance> attendanceList) {

        if (attendanceList == null || attendanceList.isEmpty()) {
            return;
        }

        StringBuilder querybuilder = new StringBuilder("update Attendance set present = CASE");
        attendanceList.forEach((attendance) -> {
            int present = attendance.getPresent() ? 1 : 0;
            querybuilder.append(" WHEN id = ").append(attendance.getId()).append(" THEN ").append(present);
        });
        querybuilder.append(" END, updatedAt=NOW() where id in (").append(Joiner.on(Constants.COMMA_SEPARATOR)
                .join(attendanceList.stream()
                        .map(attendance -> attendance.getId())
                        .collect(Collectors.toList()))).append(")");

        Query query = currentSession().createNativeQuery(querybuilder.toString());
        query.executeUpdate();
    }

    @Override
    public List<Attendance> getAttendanceByStudentIdentifier(Integer studentIdentifier, boolean isStudentIdentifierClass, Date startDate, Date endDate, Integer offset) {

        int year = Year.now().getValue();
        String queryString = null;

        if (isStudentIdentifierClass) {
            queryString = "from Attendance a where a.studentClassMapping.id = :studentIdentifier and a.sessionYear=:year";
        } else {
            queryString = "from Attendance a where a.studentStudyGroupMapping.id = :studentIdentifier and a.sessionYear=:year";
        }

        if (startDate != null) {
            queryString += " and a.createdAt >= :startDate";
        }
        if (endDate != null) {
            queryString += " and a.createdAt <= :endDate";
        }
        queryString += " order by a.createdAt desc";

        Query query = currentSession().createQuery(queryString);
        query.setParameter("studentIdentifier", studentIdentifier);
        query.setParameter("year", year);
        if (startDate != null) {
            query.setParameter("startDate", startDate);
        }
        if (endDate != null) {
            query.setParameter("endDate", endDate);
        }

        if (offset != null) {

            query.setFirstResult(offset * Constants.LIST_LIMIT);
            query.setMaxResults(Constants.LIST_LIMIT);
        }

        List<Attendance> attendanceList = query.list();

        if (attendanceList != null) {

            attendanceList.stream().map((attendance) -> {
                attendance.getTeacher().getId();
                return attendance;
            }).map((attendance) -> {
                attendance.getTeacher().getUser().getId();
                return attendance;
            }).map((attendance) -> {
                attendance.getStudentClassMapping().getStudent().getId();
                return attendance;
            }).map((attendance) -> {
                attendance.getStudentClassMapping().getStudent().getName();
                return attendance;
            }).forEachOrdered((attendance) -> {
                attendance.getTeacher().getUser().getName();
            });
        }

        return attendanceList;
    }

}
