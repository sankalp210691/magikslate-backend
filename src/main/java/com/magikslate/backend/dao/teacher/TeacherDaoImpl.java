/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.teacher;

import com.magikslate.backend.dao.GenericDaoImpl;
import com.magikslate.backend.entity.ClassTeacherMapping;
import com.magikslate.backend.entity.Teacher;
import com.magikslate.backend.entity.User;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author sankalpkulshrestha
 */
@Repository
public class TeacherDaoImpl extends GenericDaoImpl<Teacher, Integer>
        implements TeacherDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(TeacherDaoImpl.class);

    @Override
    public List<Integer> createBulk(List<Teacher> teacherList) {

        LOGGER.debug("teacherList = {}", teacherList);
        List<Integer> idList = new ArrayList();

        Session session = currentSession();

        List<Integer> existingTeacherUserIdList = new ArrayList();

        for (Teacher teacher : teacherList) {
            if (teacher.getUser().getId() != null) {

                existingTeacherUserIdList.add(teacher.getUser().getId());
            }
        }

        LOGGER.debug("existingTeacherUserIdList = {}", existingTeacherUserIdList);

        String queryString = "update Teacher t set t.deletedAt = NOW() where t.user_id in (:uidList) and deletedAt IS NULL";
        LOGGER.debug(queryString);

        Query query = session.createNativeQuery(queryString);
        query.setParameter("uidList", existingTeacherUserIdList);

        LOGGER.debug("uidList = {}", existingTeacherUserIdList);

        query.executeUpdate();

        LOGGER.debug("executeUpdates called");
        //Now create new teachers
        int index = 0;
        LOGGER.debug("Creating bulk teachers");
        for (Teacher teacher : teacherList) {
            idList.add((Integer) session.save(teacher));
            if (index % Constants.HIBERNATE_BATCH_SIZE == 0) {
                session.flush();
                session.clear();
            }
            index++;
        }
        LOGGER.debug("Returning idList = {}", idList);
        return idList;
    }

    @Override
    public User getUserByTeacherId(int teacherId) {

        Session session = currentSession();

        String queryString = "from User u join Teacher t on t.user.id = u.id where t.deletedAt IS NULL and u.deletedAt IS NULL and u.active=1 and t.id = :teacherId";

        LOGGER.debug(queryString);

        Query query = session.createQuery(queryString);
        query.setParameter("teacherId", teacherId);

        LOGGER.debug("{}", Utils.listQueryParams(query));

        return (User) query.getSingleResult();
    }

    @Override
    public List<Teacher> getTeacherBySchool(Integer schoolId) {
        Session session = currentSession();

        String queryString = "select t from Teacher t join User u on u.id = t.user.id where t.school.id = :schoolId and t.deletedAt IS NULL and u.deletedAt IS NULL";

        Query query = session.createQuery(queryString);
        query.setParameter("schoolId", schoolId);

        LOGGER.debug("{}", Utils.listQueryParams(query));

        List<Teacher> teacherList = query.list();

        if (teacherList == null) {
            return new ArrayList();
        } else {
            return teacherList;
        }
    }

    @Override
    public void mapToClass(Collection<Teacher> teacherCollection) {

        Session session = currentSession();

        int index = 0;

        LOGGER.debug("Iterating over teacherCollection = {}", teacherCollection);

        for (Teacher teacher : teacherCollection) {
            List<ClassTeacherMapping> classTeacherMappingList = teacher.getClassTeacherMappingList();
            if (classTeacherMappingList != null) {
                for (ClassTeacherMapping ctm : classTeacherMappingList) {
                    session.saveOrUpdate(ctm);
                    if (index % Constants.HIBERNATE_BATCH_SIZE == 0) {
                        session.flush();
                        session.clear();
                    }
                    index++;
                }
            }
        }
        session.flush();
        session.clear();

        LOGGER.debug("Mapping done");
    }

    @Override
    public int getTeacherAccessId(Integer userId, Integer schoolId) {
        Session session = currentSession();

        String queryString = "select t.id from Teacher t join User u on u.id = t.user_id where school_id = :schoolId and u.id = :userId and t.deletedAt IS NULL and u.deletedAt IS NULL and u.active = 1 order by t.id desc";
        LOGGER.debug(queryString);

        Query query = session.createNativeQuery(queryString);
        query.setParameter("userId", userId);
        query.setParameter("schoolId", schoolId);

        LOGGER.debug("{}", Utils.listQueryParams(query));

        if (query.list().isEmpty()) {

            LOGGER.debug("Result is empty, Returning -1");
            return -1;
        } else {

            LOGGER.debug("Returning {}", (Integer) query.getSingleResult());
            return (Integer) query.getSingleResult();
        }
    }

    @Override
    public void getTeacherEntityList(Collection<Teacher> teacherCollection) {
        if (teacherCollection != null && !teacherCollection.isEmpty()) {

            List<String> ctList = new ArrayList();

            LOGGER.debug("Creating ctList = {}", ctList);

            teacherCollection.stream().map((teacher) -> teacher.getClassTeacherMappingList()).forEachOrdered((classTeacherMappingList) -> {
                classTeacherMappingList.forEach((ctm) -> {
                    ctList.add(ctm.getClass1().getId() + Constants.UNDERSCORE_SEPARATOR + ctm.getTeacher().getId() + Constants.UNDERSCORE_SEPARATOR + ctm.getSessionYear());
                });
            });

            LOGGER.debug("ctList = {}", ctList);

            Session session = currentSession();

            String queryString = "select id, concat(class_id,\"_\", teacher_id, \"_\", sessionyear) as cts from ClassTeacherMapping where concat(class_id,\"_\", teacher_id, \"_\", sessionyear) in (:cts)";
            LOGGER.debug(queryString);

            Query query = session.createNativeQuery(queryString);
            query.setParameter("cts", ctList);

            LOGGER.debug("cts = {}", ctList);

            List<Object[]> ctmList = query.list();
            if (ctmList != null) {
                Map<String, Integer> ctToctmIdMap = new HashMap();
                ctmList.forEach((row) -> {
                    Integer id = (Integer) row[0];
                    String ct = (String) row[1];

                    ctToctmIdMap.put(ct, id);
                });

                LOGGER.debug("ctToctmIdMap = {}", ctToctmIdMap);
                teacherCollection.stream().map((teacher) -> teacher.getClassTeacherMappingList()).forEachOrdered((classTeacherMappingList) -> {
                    classTeacherMappingList.forEach((ctm) -> {
                        String key = ctm.getClass1().getId() + Constants.UNDERSCORE_SEPARATOR + ctm.getTeacher().getId() + Constants.UNDERSCORE_SEPARATOR + ctm.getSessionYear();
                        if (ctToctmIdMap.get(key) != null) {
                            ctm.setId(ctToctmIdMap.get(key));
                        }
                    });
                });
            } else {
                LOGGER.debug("ctmList is null");
            }
        } else {
            LOGGER.debug("teacherCollection is null or empty");
        }
    }

    @Override
    public List<Teacher> filterTeacherListForSchool(List<Teacher> teacherList, Integer schoolId) {

        LOGGER.debug("Filtering teacherList = {} for schoolId = {}", teacherList, schoolId);

        List<Teacher> filteredTeacherList = new ArrayList();

        if (teacherList == null || teacherList.isEmpty()) {
            LOGGER.debug("Returning empty filteredTeacherList because teacherList is null or empty");
            return filteredTeacherList;
        }

        Session session = currentSession();

        Map<Integer, Teacher> teacherMap = new HashMap();
        LOGGER.debug("Iterating over teacherList to create teacherMap(teacherId -> teacher)");
        teacherList.forEach(teacher -> {
            teacherMap.put(teacher.getId(), teacher);
        });

        String queryString = "select t.id from Teacher t where t.id in (:tidList) and t.school_id = :schoolId and t.deletedAt IS NULL";
        LOGGER.debug(queryString);

        Query query = session.createNativeQuery(queryString);
        query.setParameter("tidList", teacherList.stream().map(teacher -> teacher.getId()).collect(Collectors.toList()));
        query.setParameter("schoolId", schoolId);

        LOGGER.debug("tidList: {}, schoolId: {}", teacherList.stream().map(teacher -> teacher.getId()).collect(Collectors.toList()), schoolId);

        List<Object> result = query.list();
        if (result == null) {

            LOGGER.debug("result is null. Returning filteredTeacherList = null");
            return filteredTeacherList;
        } else {
            result.stream().map((tid) -> (Integer) tid).forEachOrdered((teacherId) -> {
                filteredTeacherList.add(teacherMap.get(teacherId));
            });
        }
        LOGGER.debug("Returning filteredTeacherList = {}", filteredTeacherList);
        return filteredTeacherList;
    }

    @Override
    public List<Teacher> getTeachersByClassList(List<Integer> classIdList) {
        Session session = currentSession();

        String queryString = "select distinct t from Teacher t join SubjectClassTeacherMapping sctm on sctm.teacher = t where sctm.deletedAt IS NULL and sctm.class1.id in (:classIdList)";
        Query query = session.createQuery(queryString);
        LOGGER.debug(queryString);

        query.setParameter("classIdList", classIdList);
        LOGGER.debug("classIdList = {}", classIdList);

        return query.list();
    }

}
