/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.teacher;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.entity.Teacher;
import com.magikslate.backend.entity.User;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;


/**
 *
 * @author sankalpkulshrestha
 */
public interface TeacherDao extends GenericDao<Teacher, Integer>{
    
    public List<Integer> createBulk(List<Teacher> teacherList);

    public User getUserByTeacherId(int teacherId);

    public List<Teacher> getTeacherBySchool(Integer schoolId);
    
    public void getTeacherEntityList(Collection<Teacher> teacherCollection);

    public void mapToClass(Collection<Teacher> teacherCollection);

    public int getTeacherAccessId(Integer userId, Integer schoolId);

    public List<Teacher> filterTeacherListForSchool(List<Teacher> teacherList, Integer schoolId);

    public List<Teacher> getTeachersByClassList(List<Integer> classIdList);
}
