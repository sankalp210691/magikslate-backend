/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao;

import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 * @param <E>
 * @param <K>
 */
public interface GenericDao<E, K> {

    public K add(E entity);

    public void saveOrUpdate(E entity);

    public void update(E entity);

    public void remove(E entity);

    public E find(K key);
    
    public List<E> find(List<K> keyList);

    public List<E> getAll();
}
