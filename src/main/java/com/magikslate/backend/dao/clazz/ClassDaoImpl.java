/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.clazz;

import com.magikslate.backend.dao.GenericDaoImpl;
import com.magikslate.backend.entity.Class;
import com.magikslate.backend.entity.SubjectClassTeacherMapping;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author sankalpkulshrestha
 */
@Repository
public class ClassDaoImpl extends GenericDaoImpl<Class, Integer>
        implements ClassDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClassDaoImpl.class);

    @Override
    public List<Integer> createBulk(List<Class> classList) {
        List<Integer> idList = new ArrayList();

        Session session = currentSession();

        int index = 0;
        for (Class clazz : classList) {
            idList.add((Integer) session.save(clazz));
            if (index % Constants.HIBERNATE_BATCH_SIZE == 0) {
                session.flush();
                session.clear();
            }
            index++;
        }
        return idList;
    }

    @Override
    public List<Class> getClassListBySchool(Integer schoolId, Integer offset) {
        Session session = currentSession();

        String queryString = "from Class c where c.school.id = :schoolId";
        LOGGER.debug(queryString);
        Query query = session.createQuery(queryString);
        query.setParameter("schoolId", schoolId);
        LOGGER.debug("{}", Utils.listQueryParams(query));
        if (offset != null) {
            LOGGER.debug("firstResult = {} , maxResult = {}", offset * Constants.LIST_LIMIT, Constants.LIST_LIMIT);
            query.setFirstResult(offset * Constants.LIST_LIMIT);
            query.setMaxResults(Constants.LIST_LIMIT);
        }
        List<Class> classList = query.list();
        LOGGER.debug("Returning classList = {}", classList);
        return classList;
    }

    @Override
    public List<Object[]> getClassListByTeacher(Integer teacherId, Integer offset) {
        Session session = currentSession();

        //if there are no students in a class, this query won't include that whole row in the result
        LOGGER.debug(Constants.CLASSES_BY_TEACHER);
        LOGGER.debug(Constants.SG_BY_TEACHER);
        Query query = session.createNativeQuery(Constants.CLASSES_BY_TEACHER);
        query.setParameter("teacherId", teacherId);
        LOGGER.debug("{}", Utils.listQueryParams(query));
        //this is being put as a hack because app code is badly designed. put the limit = Constants.LIST_LIMIT later. right now its hardcoded to 500
        int limit = 500;
        if (offset != null) {
            LOGGER.debug("firstResult = {} , maxResult = {}", offset * limit, limit);
            query.setFirstResult(offset * limit);
            query.setMaxResults(limit);
        }
        List<Object[]> classList = query.list();
        
        query = session.createNativeQuery(Constants.SG_BY_TEACHER);
        query.setParameter("teacherId", teacherId);
        LOGGER.debug("{}", Utils.listQueryParams(query));
        if (offset != null) {
            LOGGER.debug("firstResult = {} , maxResult = {}", offset * limit, limit);
            query.setFirstResult(offset * limit);
            query.setMaxResults(limit);
        }
        List<Object[]> sgList = query.list();
        
        if(classList != null)
            classList.addAll(sgList);
        else
            classList = sgList;
        
        LOGGER.debug("Returning result = {}", classList);
        return classList;
    }

    @Override
    public SubjectClassTeacherMapping getSubjectClassTeacherMapping(Integer id) {
        Session session = currentSession();
        
        Query query = session.createQuery("from SubjectClassTeacherMapping sctm where sctm.id = :id");
        query.setParameter("id", id);
        List<SubjectClassTeacherMapping> result = query.list();
        if(result != null){
            return result.get(0);
        }else{
            return null;
        }
    }

}
