/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.clazz;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.entity.Class;
import com.magikslate.backend.entity.SubjectClassTeacherMapping;
import java.util.List;


/**
 *
 * @author sankalpkulshrestha
 */
public interface ClassDao extends GenericDao<Class, Integer>{
    
    public List<Integer> createBulk(List<Class> classList);

    public List<Class> getClassListBySchool(Integer schoolId, Integer offset);

    public List<Object[]> getClassListByTeacher(Integer teacherId, Integer offset);

    public SubjectClassTeacherMapping getSubjectClassTeacherMapping(Integer id);

}
