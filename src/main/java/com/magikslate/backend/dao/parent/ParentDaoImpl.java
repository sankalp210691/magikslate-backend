/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.parent;

import com.magikslate.backend.dao.GenericDaoImpl;
import com.magikslate.backend.entity.Parent;
import com.magikslate.backend.util.Constants;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author sankalpkulshrestha
 */
@Repository
public class ParentDaoImpl extends GenericDaoImpl<Parent, Integer>
        implements ParentDao {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ParentDaoImpl.class);

    @Override
    public List<Integer> createBulk(List<Parent> parentList) {
        List<Integer> idList = new ArrayList();

        Session session = currentSession();

        int index = 0;
        for (Parent parent : parentList) {
            idList.add((Integer) session.save(parent));
            if (index % Constants.HIBERNATE_BATCH_SIZE == 0) {
                session.flush();
                session.clear();
            }
            index++;
        }
        return idList;
    }

    @Override
    public int getParentAccessId(Integer userId, Integer schoolId, Integer studentId) {
        Session session = currentSession();

        Query query = session.createNativeQuery("select p.id from School s join Student st on st.school_id = s.id join StudentParentMapping spm on st.id = spm.student_id join Parent p on p.id = spm.parent_id join User u on u.id = p.user_id where u.id = :userId and s.id = :schoolId and st.id = :studentId and u.deletedAt IS NULL and p.deletedAt IS NULL and st.deletedAt IS NULL and spm.deletedAt IS NULL order by p.id desc");
        query.setParameter("userId", userId);
        query.setParameter("schoolId", schoolId);
        query.setParameter("studentId", studentId);
        if(query.list() == null || query.list().isEmpty()){
            return -1;
        }
        return (Integer) query.getSingleResult();
    }

    @Override
    public List<Parent> getParentsByClassList(List<Integer> classIdList) {
        Session session = currentSession();
        
        String queryString = "select distinct p from Parent p join StudentParentMapping spm on spm.parent = p join StudentClassMapping scm on scm.student = spm.student where scm.class1.id in (:classIdList) and scm.deletedAt IS NULL and spm.deletedAt IS NULL";
        Query query = session.createQuery(queryString);
        LOGGER.debug(queryString);
        
        query.setParameter("classIdList", classIdList);
        LOGGER.debug("classIdList = {}", classIdList);
        
        return query.list();
    }

}
