/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.parent;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.entity.Parent;
import java.util.List;


/**
 *
 * @author sankalpkulshrestha
 */
public interface ParentDao extends GenericDao<Parent, Integer>{
    
    public List<Integer> createBulk(List<Parent> parentsList);

    public int getParentAccessId(Integer userId, Integer schoolId, Integer studentId);

    public List<Parent> getParentsByClassList(List<Integer> classIdList);
}
