/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.event;

import com.magikslate.backend.dao.GenericDaoImpl;
import com.magikslate.backend.entity.Event;
import com.magikslate.backend.entity.EventAdminMapping;
import com.magikslate.backend.entity.EventParentMapping;
import com.magikslate.backend.entity.EventParentMappingPK;
import com.magikslate.backend.entity.EventTeacherMapping;
import com.magikslate.backend.entity.EventTeacherMappingPK;
import com.magikslate.backend.entity.Parent;
import com.magikslate.backend.entity.Teacher;
import com.magikslate.backend.util.Constants;
import com.magikslate.backend.util.Utils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author sankalpkulshrestha
 */
@Repository
public class EventDaoImpl extends GenericDaoImpl<Event, Integer>
        implements EventDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventDaoImpl.class);

    @Override
    public List<List<Event>> getAll(Integer userId, String role, Integer roleId,
            Integer schoolId, Date startDate, Date endDate, Date eventStartDate,
            Date eventEndDate, Integer offset, boolean isSelfNeeded, boolean isReceivedNeeded) {

        List<List<Event>> eventListList = new ArrayList();

        if (isSelfNeeded) {
            LOGGER.debug("isSelfNeeded = {}", isSelfNeeded);
            List<Event> selfEventList = getEventListByRole(userId, role, schoolId, startDate, endDate, eventStartDate, eventEndDate, offset);
            retrieveLinkedProperties(selfEventList);
            LOGGER.debug("selfEventList = {}", selfEventList);
            eventListList.add(selfEventList);
        } else {
            LOGGER.debug("isSelfNeeded = {}", isSelfNeeded);
            eventListList.add(null);
        }

        if (isReceivedNeeded) {
            LOGGER.debug("isReceivedNeeded = {}", isReceivedNeeded);
            List<Event> receivedEventList = getEventListByReceiver(userId, roleId, role, schoolId, startDate, endDate, eventStartDate, eventEndDate, offset);
            retrieveLinkedProperties(receivedEventList);
            LOGGER.debug("receivedEventList = {}", receivedEventList);
            eventListList.add(receivedEventList);
        } else {
            LOGGER.debug("isReceivedNeeded = {}", isReceivedNeeded);
            eventListList.add(null);
        }

        return eventListList;
    }

    private List<Event> getEventListByRole(Integer userId, String role, Integer schoolId, Date startDate, Date endDate, Date eventStartDate, Date eventEndDate, Integer offset) {

        Session session = currentSession();

        String queryString = "from Event e where e.school.id = :schoolId and e.user.id = :userId and e.creatorRole = :role and e.deletedAt IS NULL";
        if (startDate != null) {
            queryString += " and e.createdAt >= :startDate";
        }
        if (endDate != null) {
            queryString += " and e.createdAt <= :endDate";
        }
        if (eventStartDate != null) {
            queryString += " and e.eventStartDate >= :eventStartDate";
        }
        if (eventEndDate != null) {
            queryString += " and e.eventEndDate <= :eventEndDate";
        }
        if (eventStartDate != null) {
            queryString += " order by e.eventStartDate";
        } else {
            queryString += " order by e.createdAt desc";
        }
        LOGGER.debug(queryString);

        Query query = session.createQuery(queryString);
        query.setParameter("schoolId", schoolId);
        query.setParameter("userId", userId);
        query.setParameter("role", role);

        if (startDate != null) {
            query.setParameter("startDate", startDate);
        }
        if (endDate != null) {
            query.setParameter("endDate", endDate);
        }

        if (eventStartDate != null) {
            query.setParameter("eventStartDate", eventStartDate);
        }
        if (eventEndDate != null) {
            query.setParameter("eventEndDate", eventEndDate);
        }

        LOGGER.debug("{}", Utils.listQueryParams(query));

        if (offset != null) {
            query.setFirstResult(offset * Constants.LIST_LIMIT);
            query.setMaxResults(Constants.LIST_LIMIT);
        }

        return query.list();

    }

    private List<Event> getEventListByReceiver(Integer userId, Integer roleId, String role, Integer schoolId, Date startDate, Date endDate, Date eventStartDate, Date eventEndDate, Integer offset) {

        Session session = currentSession();

        String queryString;
        String classTeacherQueryString = null;
        switch (role) {
            case Constants.ADMIN:
                queryString = "from Event e where e.school.id = :schoolId and e.deletedAt IS NULL";
                break;
            case Constants.TEACHER:
                queryString = "select distinct e from Event e join e.classList as cl where cl.id in (select c.id from Class c join c.subjectClassTeacherMappingList as sctml where sctml.teacher.id = :roleId) and e.eventForStaff = true and e.school.id = :schoolId";
                classTeacherQueryString = "select distinct e from Event e join e.classList as cl where cl.id in (select c.id from Class c join c.classTeacherMappingList as ctml where ctml.teacher.id = :roleId) and e.eventForStaff = true and e.school.id = :schoolId";
                break;
            case Constants.PARENT:
                queryString = "select distinct e from Event e join e.classList as cl where cl.id in (select c.id from Class c join c.studentClassMappingList as scml join scml.student as s join s.studentParentMappingList spml where spml.parent.id = :roleId) and e.eventForParents = true and e.school.id = :schoolId";
                break;
            default:
                return null;
        }

        if (startDate != null) {
            queryString += " and e.createdAt >= :startDate";
        }
        if (endDate != null) {
            queryString += " and e.createdAt <= :endDate";
        }
        if (eventStartDate != null) {
            queryString += " and e.eventStartDate >= :eventStartDate";
        }
        if (eventEndDate != null) {
            queryString += " and e.eventEndDate <= :eventEndDate";
        }
        if (eventStartDate != null) {
            queryString += " order by e.eventStartDate";
        } else {
            queryString += " order by e.createdAt desc";
        }
        LOGGER.debug(queryString);

        Query query = session.createQuery(queryString);
        query.setParameter("schoolId", schoolId);
//        query.setParameter("userId", userId);
        if (!Constants.ADMIN.equals(role)) {
            query.setParameter("roleId", roleId);
        }

        if (startDate != null) {
            query.setParameter("startDate", startDate);
        }
        if (endDate != null) {
            query.setParameter("endDate", endDate);
        }

        if (eventStartDate != null) {
            query.setParameter("eventStartDate", eventStartDate);
        }
        if (eventEndDate != null) {
            query.setParameter("eventEndDate", eventEndDate);
        }

        LOGGER.debug("{}", Utils.listQueryParams(query));

        if (offset != null) {
            query.setFirstResult(offset * Constants.LIST_LIMIT);
            query.setMaxResults(Constants.LIST_LIMIT);
        }

        List<Event> eventList = query.list();
        List<Event> classTeacherEventList = null;

        if (Constants.TEACHER.equals(role)) {

            LOGGER.debug(queryString);

            Query classTeacherQuery = session.createQuery(classTeacherQueryString);
            classTeacherQuery.setParameter("schoolId", schoolId);
            classTeacherQuery.setParameter("roleId", roleId);

            if (startDate != null) {
                query.setParameter("startDate", startDate);
            }
            if (endDate != null) {
                query.setParameter("endDate", endDate);
            }

            if (eventStartDate != null) {
                query.setParameter("eventStartDate", eventStartDate);
            }
            if (eventEndDate != null) {
                query.setParameter("eventEndDate", eventEndDate);
            }

            LOGGER.debug("{}", Utils.listQueryParams(query));

            if (offset != null) {
                classTeacherQuery.setFirstResult(offset * Constants.LIST_LIMIT);
                classTeacherQuery.setMaxResults(Constants.LIST_LIMIT);
            }

            classTeacherEventList = query.list();

            //getting self created events
            List<Event> selfCreatedEvents = getEventListByRole(userId, role, schoolId, startDate, endDate, eventStartDate, eventEndDate, offset);
            Collections.reverse(selfCreatedEvents);

            //mixing with class teacher events for now #hack!
            if (classTeacherEventList == null && selfCreatedEvents != null) {
                classTeacherEventList = new ArrayList();
            }
            if (selfCreatedEvents != null) {

                Set<Integer> eventIdSet = new HashSet();
                for (Event event : classTeacherEventList) {
                    eventIdSet.add(event.getId());
                }

                for (Event event : selfCreatedEvents) {
                    if (!eventIdSet.contains(event.getId())) {
                        classTeacherEventList.add(event);
                    }
                }
            }
        }

        if (Constants.ADMIN.equals(role) || Constants.PARENT.equals(role)) {

            LOGGER.debug("Returning eventList = {}", eventList);
            return eventList;
        } else {
            if (classTeacherEventList == null) {

                LOGGER.debug("Returning eventList = {}", eventList);
                return eventList;
            } else {
                if (eventList != null) {

                    Set<Integer> eventIdSet = new HashSet();

                    for (Event event : eventList) {
                        eventIdSet.add(event.getId());
                    }

                    for (Event event : classTeacherEventList) {
                        if (!eventIdSet.contains(event.getId())) {
                            eventList.add(event);
                        }
                    }

                    eventList.sort((e1, e2) -> {
                        return e1.getCreatedAt().compareTo(e2.getCreatedAt());
                    });

                    LOGGER.debug("Returning eventList = {}", eventList);

                    return eventList;
                } else {
                    classTeacherEventList.sort((e1, e2) -> {
                        return e1.getCreatedAt().compareTo(e2.getCreatedAt());
                    });

                    LOGGER.debug("Returning eventList = {}", classTeacherEventList);
                    return classTeacherEventList;
                }
            }
        }
    }

    @Override
    public boolean confirm(Integer userId, Integer eventId, String role, Integer roleId, Integer status) {
        Event event = this.find(eventId);
        if(event == null) {
            return false;
        }
        if(role.equals(event.getCreatorRole()) && userId.equals(event.getUser().getId())){
            return false;
        }
        switch (role) {
            case Constants.TEACHER:
                EventTeacherMappingPK etmpk = new EventTeacherMappingPK(eventId, roleId);
                EventTeacherMapping etm = new EventTeacherMapping(etmpk);
                etm.setEvent(new Event(eventId));
                etm.setStatus(status == 1);
                etm.setTeacher(new Teacher(roleId));
                etm.setCreatedAt(new Date());
                currentSession().saveOrUpdate(etm);
                break;
            case Constants.PARENT:
                EventParentMappingPK epmpk = new EventParentMappingPK(eventId, roleId);
                EventParentMapping epm = new EventParentMapping(epmpk);
                epm.setEvent(new Event(eventId));
                epm.setStatus(status == 1);
                epm.setParent(new Parent(roleId));
                epm.setCreatedAt(new Date());
                currentSession().saveOrUpdate(epm);
                break;
            default:
                return false;
        }
        return true;
    }

    private void retrieveLinkedProperties(List<Event> eventList) {

        eventList.stream().map((event) -> {
            if (event.getClassList() != null) {
                event.getClassList().stream().map((aClass) -> aClass.getId()).collect(Collectors.toList());
            }
            return event;
        }).map((event) -> {
            List<EventTeacherMapping> eventTeacherMappingList = event.getEventTeacherMappingList();
            if (eventTeacherMappingList != null) {
                eventTeacherMappingList.forEach(eventTeacherMapping -> {
                    eventTeacherMapping.getTeacher().getUser().getId();
                    eventTeacherMapping.getTeacher().getUser().getName();
                });
            }
            List<EventAdminMapping> eventAdminMappingList = event.getEventAdminMappingList();
            if (eventAdminMappingList != null) {
                eventAdminMappingList.forEach(eventAdminMapping -> {
                    eventAdminMapping.getAdmin().getUser().getId();
                    eventAdminMapping.getAdmin().getUser().getName();
                });
            }
            List<EventParentMapping> eventParentMappingList = event.getEventParentMappingList();
            return eventParentMappingList;
        }).filter((eventParentMappingList) -> (eventParentMappingList != null)).forEachOrdered((eventParentMappingList) -> {
            eventParentMappingList.forEach(eventParentMapping -> {
                eventParentMapping.getParent().getUser().getId();
                eventParentMapping.getParent().getUser().getName();
            });
        });
    }

}
