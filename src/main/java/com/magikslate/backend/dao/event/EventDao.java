/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dao.event;

import com.magikslate.backend.dao.GenericDao;
import com.magikslate.backend.entity.Event;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public interface EventDao extends GenericDao<Event, Integer> {

    public List<List<Event>> getAll(Integer userId, String receiverType, Integer receiverId, 
            Integer schoolId, Date startDate, Date endDate, Date eventStartDate,
            Date eventEndDate, Integer offset, boolean isSelfNeeded, boolean isReceivedNeeded);

    public boolean confirm(Integer userId, Integer eventId, String role, Integer roleId, Integer status);
}
