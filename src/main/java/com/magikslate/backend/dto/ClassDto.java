/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

/**
 *
 * @author sankalpkulshrestha
 */
public class ClassDto {
    private int id;
    private String standard;
    private String section;
    private TeacherDto classTeacher;
    private TeacherDto deputyTeacher;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the standard
     */
    public String getStandard() {
        return standard;
    }

    /**
     * @param standard the standard to set
     */
    public void setStandard(String standard) {
        this.standard = standard;
    }

    /**
     * @return the section
     */
    public String getSection() {
        return section;
    }

    /**
     * @param section the section to set
     */
    public void setSection(String section) {
        this.section = section;
    }

    @Override
    public String toString() {
        return "ClassDto{" + "id=" + id + ", standard=" + standard + ", section=" + section + '}';
    }

    /**
     * @return the classTeacher
     */
    public TeacherDto getClassTeacher() {
        return classTeacher;
    }

    /**
     * @param classTeacher the classTeacher to set
     */
    public void setClassTeacher(TeacherDto classTeacher) {
        this.classTeacher = classTeacher;
    }

    /**
     * @return the deputyTeacher
     */
    public TeacherDto getDeputyTeacher() {
        return deputyTeacher;
    }

    /**
     * @param deputyTeacher the deputyTeacher to set
     */
    public void setDeputyTeacher(TeacherDto deputyTeacher) {
        this.deputyTeacher = deputyTeacher;
    }
    
}
