/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public class StudentClassDto {
    
    private Integer classId;
    private String standard;
    private String section;
    private List<StudentDto> student;

    /**
     * @return the classId
     */
    public Integer getClassId() {
        return classId;
    }

    /**
     * @param classId the classId to set
     */
    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    /**
     * @return the standard
     */
    public String getStandard() {
        return standard;
    }

    /**
     * @param standard the standard to set
     */
    public void setStandard(String standard) {
        this.standard = standard;
    }

    /**
     * @return the section
     */
    public String getSection() {
        return section;
    }

    /**
     * @param section the section to set
     */
    public void setSection(String section) {
        this.section = section;
    }

    /**
     * @return the student
     */
    public List<StudentDto> getStudent() {
        return student;
    }

    /**
     * @param student the student to set
     */
    public void setStudent(List<StudentDto> student) {
        this.student = student;
    }

    @Override
    public String toString() {
        return "StudentClassDto{" + "classId=" + classId + ", standard=" + standard + ", section=" + section + ", student=" + student + '}';
    }
    
}
