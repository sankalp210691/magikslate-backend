/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import java.util.Date;

/**
 *
 * @author sankalpkulshrestha
 */
public class InstituteCategoryDto {
    private Integer id;
    private String name;
    private Integer schoolInstituteCategoryMappingId;
    private Date sicmCreatedAt;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the schoolInstituteCategoryMappingId
     */
    public Integer getSchoolInstituteCategoryMappingId() {
        return schoolInstituteCategoryMappingId;
    }

    /**
     * @param schoolInstituteCategoryMappingId the schoolInstituteCategoryMappingId to set
     */
    public void setSchoolInstituteCategoryMappingId(Integer schoolInstituteCategoryMappingId) {
        this.schoolInstituteCategoryMappingId = schoolInstituteCategoryMappingId;
    }

    /**
     * @return the sicmCreatedAt
     */
    public Date getSicmCreatedAt() {
        return sicmCreatedAt;
    }

    /**
     * @param sicmCreatedAt the sicmCreatedAt to set
     */
    public void setSicmCreatedAt(Date sicmCreatedAt) {
        this.sicmCreatedAt = sicmCreatedAt;
    }
}
