/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

/**
 *
 * @author sankalpkulshrestha
 */
public class SubjectTeacherMappingDto {

    private SubjectDto subject;
    private TeacherDto teacher;

    /**
     * @return the subject
     */
    public SubjectDto getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(SubjectDto subject) {
        this.subject = subject;
    }

    /**
     * @return the teacher
     */
    public TeacherDto getTeacher() {
        return teacher;
    }

    /**
     * @param teacher the teacher to set
     */
    public void setTeacher(TeacherDto teacher) {
        this.teacher = teacher;
    }
}
