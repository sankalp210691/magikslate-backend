/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import com.magikslate.backend.entity.Content;
import com.magikslate.backend.entity.StudyGroupTeacherMapping;
import com.magikslate.backend.entity.SubjectClassTeacherMapping;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sankalp.kulshrestha
 */
public class ContentDto {
    
    private Integer id;

    private String type;
    
    private String description;
    
    private Date date;
    
    private String subject;
    
    private Integer subjectId;
    
    private List<Integer> sgtmList;
    
    private List<Integer> sctmList;
    
    private ClassDto classDto;
    
    private Integer studyGroupId;//make this dto later
    
    private Integer teacherId;
    
    private String teacherName;
    
    private Integer teacherUserId;

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the sgtmList
     */
    public List<Integer> getSgtmList() {
        return sgtmList;
    }

    /**
     * @param sgtmList the sgtmList to set
     */
    public void setSgtmList(List<Integer> sgtmList) {
        this.sgtmList = sgtmList;
    }

    /**
     * @return the sctmList
     */
    public List<Integer> getSctmList() {
        return sctmList;
    }

    /**
     * @param sctmList the sctmList to set
     */
    public void setSctmList(List<Integer> sctmList) {
        this.sctmList = sctmList;
    }

    /**
     * @return the classDto
     */
    public ClassDto getClassDto() {
        return classDto;
    }

    /**
     * @param classDto the classDto to set
     */
    public void setClassDto(ClassDto classDto) {
        this.classDto = classDto;
    }

    /**
     * @return the studyGroupId
     */
    public Integer getStudyGroupId() {
        return studyGroupId;
    }

    /**
     * @param studyGroupId the studyGroupId to set
     */
    public void setStudyGroupId(Integer studyGroupId) {
        this.studyGroupId = studyGroupId;
    }

    @Override
    public String toString() {
        return "ContentDto{" + "id=" + id + ", type=" + type + ", description=" + description + ", date=" + date + ", subject=" + subject + ", sgtmList=" + sgtmList + ", sctmList=" + sctmList + ", classDto=" + classDto + ", studyGroupId=" + studyGroupId + '}';
    }
    
    static String getSubjectNameFromContent(Content content) {
        if ((content.getSubjectClassTeacherMappingList() == null || content.getSubjectClassTeacherMappingList().isEmpty())
                && (content.getStudyGroupTeacherMappingList() == null || content.getStudyGroupTeacherMappingList().isEmpty())) {
            return null;
        }

        if (content.getSubjectClassTeacherMappingList() != null) {
            for (SubjectClassTeacherMapping sctm : content.getSubjectClassTeacherMappingList()) {
                return sctm.getSubject().getName();
            }
        } else {
            for (StudyGroupTeacherMapping sgtm : content.getStudyGroupTeacherMappingList()) {
                return sgtm.getStudyGroup().getSubject().getName();
            }
        }

        return null;
    }

    /**
     * @return the subjectId
     */
    public Integer getSubjectId() {
        return subjectId;
    }

    /**
     * @param subjectId the subjectId to set
     */
    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    /**
     * @return the teacherId
     */
    public Integer getTeacherId() {
        return teacherId;
    }

    /**
     * @param teacherId the teacherId to set
     */
    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    /**
     * @return the teacherName
     */
    public String getTeacherName() {
        return teacherName;
    }

    /**
     * @param teacherName the teacherName to set
     */
    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    /**
     * @return the teacherUserId
     */
    public Integer getTeacherUserId() {
        return teacherUserId;
    }

    /**
     * @param teacherUserId the teacherUserId to set
     */
    public void setTeacherUserId(Integer teacherUserId) {
        this.teacherUserId = teacherUserId;
    }
    
}
