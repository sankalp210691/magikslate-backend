/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

/**
 *
 * @author sankalpkulshrestha
 */
public class OtpDto {
    private int otp;
    private int userId;

    /**
     * @return the otp
     */
    public int getOtp() {
        return otp;
    }

    /**
     * @param otp the otp to set
     */
    public void setOtp(int otp) {
        this.otp = otp;
    }

    /**
     * @return the userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "OtpDto{" + "otp=" + otp + ", userId=" + userId + '}';
    }
    
}
