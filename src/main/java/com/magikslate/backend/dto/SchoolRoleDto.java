/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

/**
 *
 * @author sankalpkulshrestha
 */
public class SchoolRoleDto {

    private Integer roleId;
    private SchoolDto school;
    private StudentDto student;

    /**
     * @return the roleId
     */
    public Integer getRoleId() {
        return roleId;
    }

    /**
     * @param roleId the roleId to set
     */
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    /**
     * @return the school
     */
    public SchoolDto getSchool() {
        return school;
    }

    /**
     * @param school the school to set
     */
    public void setSchool(SchoolDto school) {
        this.school = school;
    }

    /**
     * @return the student
     */
    public StudentDto getStudent() {
        return student;
    }

    /**
     * @param student the student to set
     */
    public void setStudent(StudentDto student) {
        this.student = student;
    }

    @Override
    public String toString() {
        return "SchoolRoleDto{" + "roleId=" + roleId + ", school=" + school + ", student=" + student + '}';
    }
    
}
