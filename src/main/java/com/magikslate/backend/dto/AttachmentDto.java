/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import com.magikslate.backend.entity.Content;
import com.magikslate.backend.util.Constants;

/**
 *
 * @author sankalpkulshrestha
 */
public class AttachmentDto extends ContentDto {

    private String fileUrl;
    private String filename;

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    @Override
    public String toString() {
        return "AttachmentDto{" + "fileUrl='" + fileUrl + "'}'";
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename the filename to set
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    public static AttachmentDto convertToAttachmentDto(Content content) {
        AttachmentDto attachmentDto = new AttachmentDto();
        attachmentDto.setId(content.getId());
        attachmentDto.setFileUrl(content.getAttachment().getFileUrl());
        attachmentDto.setFilename(content.getAttachment().getFileName());
        attachmentDto.setDescription(content.getDescription());

        if (content.getSubjectClassTeacherMappingList() != null) {
            ClassDto classDto = new ClassDto();
            classDto.setId(content.getSubjectClassTeacherMappingList().get(0).getClass1().getId());
            classDto.setStandard(content.getSubjectClassTeacherMappingList().get(0).getClass1().getStandard());
            classDto.setSection(content.getSubjectClassTeacherMappingList().get(0).getClass1().getSection());
            attachmentDto.setClassDto(classDto);

            attachmentDto.setSubject(content.getSubjectClassTeacherMappingList().get(0).getSubject().getName());
            attachmentDto.setSubjectId(content.getSubjectClassTeacherMappingList().get(0).getSubject().getId());

            attachmentDto.setTeacherId(content.getSubjectClassTeacherMappingList().get(0).getTeacher().getId());
            attachmentDto.setTeacherUserId(content.getSubjectClassTeacherMappingList().get(0).getTeacher().getUser().getId());
            attachmentDto.setTeacherName(content.getSubjectClassTeacherMappingList().get(0).getTeacher().getUser().getName());
        } else if (content.getStudyGroupTeacherMappingList() != null) {
            attachmentDto.setStudyGroupId(content.getStudyGroupTeacherMappingList().get(0).getStudyGroup().getId());
        }

        attachmentDto.setDate(content.getCreatedAt());
        attachmentDto.setType(Constants.ATTACHMENT_CONTENT_TYPE);
        attachmentDto.setSubject(getSubjectNameFromContent(content));

        return attachmentDto;
    }

}
