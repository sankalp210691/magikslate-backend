/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

/**
 *
 * @author sankalpkulshrestha
 */
public class LessonProgressDto extends DiaryDto {

    private String chapterName;
    private String pagesCompleted;

    public String getPagesCompleted() {
        return pagesCompleted;
    }

    public void setPagesCompleted(String pagesCompleted) {
        this.pagesCompleted = pagesCompleted;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    @Override
    public String toString() {
        return "LessonProgressDto{" + "chapterName=" + chapterName + ", pagesCompleted=" + pagesCompleted + '}';
    }

}
