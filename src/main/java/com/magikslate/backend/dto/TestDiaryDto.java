/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import java.util.Date;

/**
 *
 * @author sankalpkulshrestha
 */
public class TestDiaryDto extends DiaryDto {

    private String title;
    private String description;
    private int maxMarks;
    private Date testDate;

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the maxMarks
     */
    public int getMaxMarks() {
        return maxMarks;
    }

    /**
     * @param maxMarks the maxMarks to set
     */
    public void setMaxMarks(int maxMarks) {
        this.maxMarks = maxMarks;
    }

    /**
     * @return the testDate
     */
    public Date getTestDate() {
        return testDate;
    }

    /**
     * @param testDate the testDate to set
     */
    public void setTestDate(Date testDate) {
        this.testDate = testDate;
    }

    @Override
    public String toString() {
        return "TestDiaryDto{" + "title=" + title + ", description=" + description + ", maxMarks=" + maxMarks + ", testDate=" + testDate + '}';
    }
    
}
