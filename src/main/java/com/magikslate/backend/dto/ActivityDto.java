/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import io.vertx.core.json.JsonObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author sankalpkulshrestha
 */
public class ActivityDto {

    @NotNull
    @NotEmpty
    @Min(value = 1)
    private Integer teacherId;
    
    private List<Integer> studentIdList;
    
    @NotNull
    @NotEmpty
    private String content;

    /**
     * @return the teacherId
     */
    public Integer getTeacherId() {
        return teacherId;
    }

    /**
     * @param teacherId the teacherId to set
     */
    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    /**
     * @return the studentIdList
     */
    public List<Integer> getStudentIdList() {
        return studentIdList;
    }

    /**
     * @param studentIdList the studentIdList to set
     */
    public void setStudentIdList(List<Integer> studentIdList) {
        this.studentIdList = studentIdList;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "ActivityDto{" + "teacherId=" + teacherId + ", studentIdList=" + studentIdList + ", content=" + content + '}';
    }

}
