/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import com.magikslate.backend.entity.Content;
import com.magikslate.backend.util.Constants;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;

/**
 *
 * @author sankalpkulshrestha
 */
public class UrlContentDto extends ContentDto {

    @NotNull
    @NotEmpty
    @URL
    private String url;

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "UrlContentDto{" + "url=" + url + '}';
    }

    public static UrlContentDto convertToUrlContentDto(Content content) {
        UrlContentDto urlContentDto = new UrlContentDto();
        urlContentDto.setId(content.getId());
        urlContentDto.setUrl(content.getUrlContent().getUrl());
        urlContentDto.setDescription(content.getDescription());
        urlContentDto.setSubject(getSubjectNameFromContent(content));

        if (content.getSubjectClassTeacherMappingList() != null) {
            ClassDto classDto = new ClassDto();
            classDto.setId(content.getSubjectClassTeacherMappingList().get(0).getClass1().getId());
            classDto.setStandard(content.getSubjectClassTeacherMappingList().get(0).getClass1().getStandard());
            classDto.setSection(content.getSubjectClassTeacherMappingList().get(0).getClass1().getSection());
            urlContentDto.setClassDto(classDto);

            urlContentDto.setSubject(content.getSubjectClassTeacherMappingList().get(0).getSubject().getName());
            urlContentDto.setSubjectId(content.getSubjectClassTeacherMappingList().get(0).getSubject().getId());

            urlContentDto.setTeacherId(content.getSubjectClassTeacherMappingList().get(0).getTeacher().getId());
            urlContentDto.setTeacherUserId(content.getSubjectClassTeacherMappingList().get(0).getTeacher().getUser().getId());
            urlContentDto.setTeacherName(content.getSubjectClassTeacherMappingList().get(0).getTeacher().getUser().getName());
        } else if (content.getStudyGroupTeacherMappingList() != null) {
            urlContentDto.setStudyGroupId(content.getStudyGroupTeacherMappingList().get(0).getStudyGroup().getId());
        }

        urlContentDto.setDate(content.getCreatedAt());
        urlContentDto.setType(Constants.URL_CONTENT_TYPE);
        return urlContentDto;
    }

}
