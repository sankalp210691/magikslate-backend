/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import com.magikslate.backend.entity.Announcement;
import com.magikslate.backend.entity.Attendance;
import com.magikslate.backend.entity.Content;
import com.magikslate.backend.entity.Diary;
import com.magikslate.backend.entity.Event;
import com.magikslate.backend.feed.Feed;
import com.magikslate.backend.feed.FeedItem;
import com.magikslate.backend.util.Constants;
import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;

/**
 *
 * @author sankalpkulshrestha
 */
public class FeedDto {

    public static List<FeedDto> convertToFeedDto(Feed feed, Integer userId, ModelMapper modelMapper) {

        List<FeedDto> feedDtoList = new ArrayList();

        while (feed.hasNext()) {
            FeedItem feedItem = feed.next();

            FeedDto feedDto = new FeedDto();
            feedDto.setType(feedItem.getFeedType());

            switch (feedItem.getFeedType()) {
                case Constants.EVENT:
                    feedDto.setEvent(EventDto.convertToEventDto((Event) (feedItem.getEvent()), userId));
                    break;
                case Constants.ANNOUNCEMENT:
                    feedDto.setAnnouncement(AnnouncementDto.convertToAnnouncementDto((Announcement) (feedItem.getAnnouncement())));
                    break;
                case Constants.DIARY:
                    Diary diary = feedItem.getDiary();
                    feedDto.setDiary(DiaryDto.convertToDiaryDto(diary, diary.getType(), modelMapper));
                    break;
                case Constants.CONTENT:
                    Content content = feedItem.getContent();
                    switch (content.getType()) {
                        case Constants.URL_CONTENT_TYPE:
                            UrlContentDto urlContentDto = UrlContentDto.convertToUrlContentDto(content);
                            feedDto.setContent(urlContentDto);
                            break;
                        case Constants.ATTACHMENT_CONTENT_TYPE:
                            AttachmentDto attachmentDto = AttachmentDto.convertToAttachmentDto(content);
                            feedDto.setContent(attachmentDto);
                            break;
                        case Constants.NOTE_CONTENT_TYPE:
                            NoteDto noteDto = NoteDto.convertToNoteDto(content);
                            feedDto.setContent(noteDto);
                            break;
                    }
                    break;
                case Constants.ATTENDANCE:
                    Attendance attendance = feedItem.getAttendance();
                    feedDto.setAttendanceDto(AttendanceDto.convertToAttendanceDto(attendance));
                    break;
            }
            feedDtoList.add(feedDto);
        }

        return feedDtoList;
    }

    private String type;
    private ContentDto content;
    private DiaryDto diary;
    private EventDto event;
    private AnnouncementDto announcement;
    private AttendanceDto attendanceDto;

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the content
     */
    public ContentDto getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(ContentDto content) {
        this.content = content;
    }

    /**
     * @return the diary
     */
    public DiaryDto getDiary() {
        return diary;
    }

    /**
     * @param diary the diary to set
     */
    public void setDiary(DiaryDto diary) {
        this.diary = diary;
    }

    /**
     * @return the event
     */
    public EventDto getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(EventDto event) {
        this.event = event;
    }

    /**
     * @return the announcement
     */
    public AnnouncementDto getAnnouncement() {
        return announcement;
    }

    /**
     * @param announcement the announcement to set
     */
    public void setAnnouncement(AnnouncementDto announcement) {
        this.announcement = announcement;
    }

    /**
     * @return the attendanceDto
     */
    public AttendanceDto getAttendanceDto() {
        return attendanceDto;
    }

    /**
     * @param attendanceDto the attendanceDto to set
     */
    public void setAttendanceDto(AttendanceDto attendanceDto) {
        this.attendanceDto = attendanceDto;
    }
    
    @Override
    public String toString(){
        return "FeedDto{" + "type=" + type + ", content=" + content + ", diary=" + diary + ", event=" + event + ", announcement=" + announcement + ", attendanceDto=" + attendanceDto + '}';
    }

}
