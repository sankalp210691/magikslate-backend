/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import com.magikslate.backend.entity.Diary;
import com.magikslate.backend.util.Constants;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;


/**
 *
 * @author sankalpkulshrestha
 */
public class DiaryDto {

    private Integer id;
    private String type;

    private Integer sctm;
    private Integer ssgm;
    private List<Integer> studentId;
    private Date createdAt;
    private Integer subjectId;
    private Integer teacherId;
    private String teacherName;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the sctm
     */
    public Integer getSctm() {
        return sctm;
    }

    /**
     * @param sctm the sctm to set
     */
    public void setSctm(Integer sctm) {
        this.sctm = sctm;
    }

    /**
     * @return the ssgm
     */
    public Integer getSsgm() {
        return ssgm;
    }

    /**
     * @param ssgm the ssgm to set
     */
    public void setSsgm(Integer ssgm) {
        this.ssgm = ssgm;
    }

    /**
     * @return the studentId
     */
    public List<Integer> getStudentId() {
        return studentId;
    }

    /**
     * @param studentId the studentId to set
     */
    public void setStudentId(List<Integer> studentId) {
        this.studentId = studentId;
    }

    /**
     * @return the createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "DiaryDto{" + "id=" + id + ", type=" + type + ", sctm=" + sctm + ", ssgm=" + ssgm + ", "
                + "studentId=" + studentId 
                + ", createdAt=" + createdAt + '}';
    }
    
    public static <T extends DiaryDto> T convertToDiaryDto(Diary diary, String type, ModelMapper modelMapper) {

        if (diary == null) {
            return null;
        }

        Integer sctmId = diary.getSubjectClassTeacherMapping() != null
                ? diary.getSubjectClassTeacherMapping().getId() : null;
        Integer ssgmId = diary.getStudyGroupTeacherMapping() != null
                ? diary.getStudyGroupTeacherMapping().getId() : null;
        List<Integer> studentIdList = diary.getStudentList() != null
                ? diary.getStudentList().stream().map(student -> student.getId())
                        .collect(Collectors.toList()) : null;

        switch (type) {
            case Constants.GENERAL_DIARY:
                GeneralDiaryDto gDiary = modelMapper.map(diary, GeneralDiaryDto.class);
                gDiary.setSubjectId(diary.getSubject().getId());
                gDiary.setDescription(diary.getGeneralDiary().getDescription());
                gDiary.setTitle(diary.getGeneralDiary().getTitle());                
                gDiary.setSctm(sctmId);
                gDiary.setSsgm(ssgmId);
                gDiary.setStudentId(studentIdList);
                if(diary.getTeacher() != null) {
                    gDiary.setTeacherId(diary.getTeacher().getId());
                    if(diary.getTeacher().getUser() != null) {
                        gDiary.setTeacherName(diary.getTeacher().getUser().getName());
                    }
                    }
                return (T) gDiary;
            case Constants.KUDO_DIARY:
                KudoDto kDiary = modelMapper.map(diary, KudoDto.class);
                kDiary.setDescription(diary.getKudo().getDescription());
                kDiary.setSctm(sctmId);
                kDiary.setSsgm(ssgmId);
                kDiary.setStudentId(studentIdList);
                if(diary.getTeacher() != null) {
                    kDiary.setTeacherId(diary.getTeacher().getId());
                    if(diary.getTeacher().getUser() != null) {
                        kDiary.setTeacherName(diary.getTeacher().getUser().getName());
                    }
                }
                return (T) kDiary;
            case Constants.ANECDOTAL_RECORD_DIARY:
                AnecdotalRecordDto arDiary = modelMapper.map(diary, AnecdotalRecordDto.class);
                arDiary.setDescription(diary.getAnecdotalRecord().getDescription());
                arDiary.setSctm(sctmId);
                arDiary.setSsgm(ssgmId);
                arDiary.setStudentId(studentIdList);
                if(diary.getTeacher() != null) {
                    arDiary.setTeacherId(diary.getTeacher().getId());
                    if(diary.getTeacher().getUser() != null) {
                        arDiary.setTeacherName(diary.getTeacher().getUser().getName());
                    }
                }
                return (T) arDiary;
            case Constants.EX_DIARY:
                ExtraCurricularDiaryDto ecDiary = modelMapper.map(diary, ExtraCurricularDiaryDto.class);
                ecDiary.setDescription(diary.getExtraCurricularDiary().getDescription());
                ecDiary.setSctm(sctmId);
                ecDiary.setSsgm(ssgmId);
                ecDiary.setStudentId(studentIdList);
                if(diary.getTeacher() != null) {
                    ecDiary.setTeacherId(diary.getTeacher().getId());
                    if(diary.getTeacher().getUser() != null) {
                        ecDiary.setTeacherName(diary.getTeacher().getUser().getName());
                    }
                }
                return (T) ecDiary;
            case Constants.LESSON_PROGRESS_DIARY:
                LessonProgressDto lpDiary = modelMapper.map(diary, LessonProgressDto.class);
                lpDiary.setChapterName(diary.getLessonProgress().getChapterName());
                lpDiary.setPagesCompleted(diary.getLessonProgress().getPagesCompleted());
                lpDiary.setSctm(sctmId);
                lpDiary.setSsgm(ssgmId);
                lpDiary.setStudentId(studentIdList);
                if(diary.getTeacher() != null) {
                    lpDiary.setTeacherId(diary.getTeacher().getId());
                    if(diary.getTeacher().getUser() != null) {
                        lpDiary.setTeacherName(diary.getTeacher().getUser().getName());
                    }
                }
                return (T) lpDiary;
            case Constants.TEST_DIARY:
                TestDiaryDto testDiary = modelMapper.map(diary, TestDiaryDto.class);
                testDiary.setDescription(diary.getTestList().get(0).getDescription());
                testDiary.setMaxMarks(diary.getTestList().get(0).getMaxMarks());
                testDiary.setTitle(diary.getTestList().get(0).getTitle());
                testDiary.setTestDate(diary.getTestList().get(0).getTestDate());
                testDiary.setSctm(sctmId);
                testDiary.setSsgm(ssgmId);
                testDiary.setStudentId(studentIdList);
                if(diary.getTeacher() != null) {
                    testDiary.setTeacherId(diary.getTeacher().getId());
                    if(diary.getTeacher().getUser() != null) {
                        testDiary.setTeacherName(diary.getTeacher().getUser().getName());
                    }
                }
                return (T) testDiary;
            default:
                return null;
        }
    }

    /**
     * @return the subjectId
     */
    public Integer getSubjectId() {
        return subjectId;
    }

    /**
     * @param subjectId the subjectId to set
     */
    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    /**
     * @return the teacherId
     */
    public Integer getTeacherId() {
        return teacherId;
    }

    /**
     * @param teacherId the teacherId to set
     */
    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    /**
     * @return the teacherName
     */
    public String getTeacherName() {
        return teacherName;
    }

    /**
     * @param teacherName the teacherName to set
     */
    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }
    
}
