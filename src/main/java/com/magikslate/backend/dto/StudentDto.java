/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import com.magikslate.backend.entity.Class;
import com.magikslate.backend.entity.Parent;
import com.magikslate.backend.entity.Student;
import com.magikslate.backend.entity.StudentClassMapping;
import com.magikslate.backend.entity.StudentParentMapping;
import com.magikslate.backend.entity.User;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 *
 * @author sankalpkulshrestha
 */
public class StudentDto {

    private Integer id;
    private String name;
    private String profilePicUrl;
    private List<ParentDto> parent;
    private Integer studentClassMappingId;
    private Integer studentStudyGroupMappingId;
    private Double percAttendance;
    private Set<Date> absentDates;
    private List<SubjectDto> subjectList;
    private Double classAvgAttendance;
    private String admissionCode;
    private Date createdAt;
    private Date updatedAt;
    private SchoolDto school;
    private ClassDto clazz;
    private String dob;
    private String gender;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the parent
     */
    public List<ParentDto> getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(List<ParentDto> parent) {
        this.parent = parent;
    }

    /**
     * @return the studentClassMappingId
     */
    public Integer getStudentClassMappingId() {
        return studentClassMappingId;
    }

    /**
     * @param studentClassMappingId the studentClassMappingId to set
     */
    public void setStudentClassMappingId(Integer studentClassMappingId) {
        this.studentClassMappingId = studentClassMappingId;
    }

    /**
     * @return the studentStudyGroupMappingIdList
     */
    public Integer getStudentStudyGroupMappingId() {
        return studentStudyGroupMappingId;
    }

    /**
     * @param studentStudyGroupMappingId the studentStudyGroupMappingIdList to
     * set
     */
    public void setStudentStudyGroupMappingId(Integer studentStudyGroupMappingId) {
        this.studentStudyGroupMappingId = studentStudyGroupMappingId;
    }

    /**
     * @return the profilePicUrl
     */
    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    /**
     * @param profilePicUrl the profilePicUrl to set
     */
    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    /**
     * @return the percAttendance
     */
    public Double getPercAttendance() {
        return percAttendance;
    }

    /**
     * @param percAttendance the percAttendance to set
     */
    public void setPercAttendance(Double percAttendance) {
        this.percAttendance = percAttendance;
    }

    /**
     * @return the absentDates
     */
    public Set<Date> getAbsentDates() {
        return absentDates;
    }

    /**
     * @param absentDates the absentDates to set
     */
    public void setAbsentDates(Set<Date> absentDates) {
        this.absentDates = absentDates;
    }

    @Override
    public String toString() {
        return "StudentDto{" + "id=" + id + ", name=" + name + ", profilePicUrl="
                + profilePicUrl + ", parent=" + parent + ", studentClassMappingId="
                + studentClassMappingId + ", studentStudyGroupMappingId="
                + studentStudyGroupMappingId + ", percAttendance=" + percAttendance
                + ", absentDates=" + absentDates + ", subjectList=" + subjectList + '}';
    }

    /**
     * @return the subjectList
     */
    public List<SubjectDto> getSubjectList() {
        return subjectList;
    }

    /**
     * @param subjectList the subjectList to set
     */
    public void setSubjectList(List<SubjectDto> subjectList) {
        this.subjectList = subjectList;
    }

    /**
     * @return the classAvgAttendance
     */
    public Double getClassAvgAttendance() {
        return classAvgAttendance;
    }

    /**
     * @param classAvgAttendance the classAvgAttendance to set
     */
    public void setClassAvgAttendance(Double classAvgAttendance) {
        this.classAvgAttendance = classAvgAttendance;
    }

    /**
     * @return the admissionCode
     */
    public String getAdmissionCode() {
        return admissionCode;
    }

    /**
     * @param admissionCode the admissionCode to set
     */
    public void setAdmissionCode(String admissionCode) {
        this.admissionCode = admissionCode;
    }

    /**
     * @return the createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return the updatedAt
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt the updatedAt to set
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return the school
     */
    public SchoolDto getSchool() {
        return school;
    }

    /**
     * @param school the school to set
     */
    public void setSchool(SchoolDto school) {
        this.school = school;
    }

    /**
     * @return the clazz
     */
    public ClassDto getClazz() {
        return clazz;
    }

    /**
     * @param clazz the clazz to set
     */
    public void setClazz(ClassDto clazz) {
        this.clazz = clazz;
    }

    /**
     * @return the dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * @param dob the dob to set
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    public Student convertToStudent() {
        Student student = new Student(id);
        student.setName(name);
        student.setAdmissionCode(admissionCode);
        student.setDateOfBirth(dob);
        student.setProfilePic(profilePicUrl);
        student.setGender(gender);

        List<StudentClassMapping> scmList = new ArrayList();
        StudentClassMapping scm = new StudentClassMapping();
        scm.setStudent(student);

        Class c = new Class(clazz.getId());
        c.setStandard(clazz.getStandard());
        c.setSection(clazz.getSection());
        scm.setClass1(c);
        scmList.add(scm);
        student.setStudentClassMappingList(scmList);

        if (parent != null) {
            List<StudentParentMapping> spmList = new ArrayList();
            parent.stream().map((parentDto) -> {
                StudentParentMapping spm = new StudentParentMapping();
                spm.setStudent(student);
                Integer parentId = parentDto.getId();
                Parent parent;
                User user;
                if (parentId == null) {
                    parent = new Parent();
                    user = new User();
                } else {
                    parent = new Parent(parentId);
                    user = new User(parentDto.getUser().getId());
                }
                user.setName(parentDto.getUser().getName());
                user.setEmail(parentDto.getUser().getEmail());
                user.setPhone(parentDto.getUser().getPhone());
                parent.setUser(user);
                parent.setAddress(parentDto.getAddress());
                spm.setParent(parent);
                return spm;
            }).forEachOrdered((spm) -> {
                spmList.add(spm);
            });
            student.setStudentParentMappingList(spmList);
        }

        return student;
    }

}
