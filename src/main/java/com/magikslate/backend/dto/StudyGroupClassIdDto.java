/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public class StudyGroupClassIdDto {
    private List<Integer> studyGroupId;
    private List<Integer> classId;

    /**
     * @return the studyGroupId
     */
    public List<Integer> getStudyGroupId() {
        return studyGroupId;
    }

    /**
     * @param studyGroupId the studyGroupId to set
     */
    public void setStudyGroupId(List<Integer> studyGroupId) {
        this.studyGroupId = studyGroupId;
    }

    /**
     * @return the classId
     */
    public List<Integer> getClassId() {
        return classId;
    }

    /**
     * @param classId the classId to set
     */
    public void setClassId(List<Integer> classId) {
        this.classId = classId;
    }

    @Override
    public String toString() {
        return "StudyGroupClassIdDto{" + "studyGroupId=" + studyGroupId + ", classId=" + classId + '}';
    }
    
}
