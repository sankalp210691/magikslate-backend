/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

/**
 *
 * @author sankalpkulshrestha
 */
public class MarkAttendanceDto {
    
    private Integer student;
    private Boolean isPresent;

    /**
     * @return the student
     */
    public Integer getStudent() {
        return student;
    }

    /**
     * @param student the student to set
     */
    public void setStudent(Integer student) {
        this.student = student;
    }

    /**
     * @return the isPresent
     */
    public Boolean getIsPresent() {
        return isPresent;
    }

    /**
     * @param isPresent the isPresent to set
     */
    public void setIsPresent(Boolean isPresent) {
        this.isPresent = isPresent;
    }

    @Override
    public String toString() {
        return "MarkAttendanceDto{" + "student=" + student + ", isPresent=" + isPresent + '}';
    }

}
