/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import com.magikslate.backend.entity.Announcement;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author sankalpkulshrestha
 */
public class AnnouncementDto {
    
    private int id;
    
    @NotNull
    @NotEmpty
    private String title;
    private String description;
    
    @NotNull
    @NotEmpty
    private boolean sendToStaff;
    
    @NotNull
    @NotEmpty
    private boolean sendToParents;
    
    @NotNull
    @NotEmpty
    private List<Integer> classList;
    private String senderName;
    private Date createdAt;

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the sendToStaff
     */
    public boolean isSendToStaff() {
        return sendToStaff;
    }

    /**
     * @param sendToStaff the sendToStaff to set
     */
    public void setSendToStaff(boolean sendToStaff) {
        this.sendToStaff = sendToStaff;
    }

    /**
     * @return the sendToParents
     */
    public boolean isSendToParents() {
        return sendToParents;
    }

    /**
     * @param sendToParents the sendToParents to set
     */
    public void setSendToParents(boolean sendToParents) {
        this.sendToParents = sendToParents;
    }

    /**
     * @return the classList
     */
    public List<Integer> getClassList() {
        return classList;
    }

    /**
     * @param classList the classList to set
     */
    public void setClassList(List<Integer> classList) {
        this.classList = classList;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return the senderName
     */
    public String getSenderName() {
        return senderName;
    }

    /**
     * @param senderName the senderName to set
     */
    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    @Override
    public String toString() {
        return "AnnouncementDto{" + "id=" + id + ", title=" + title + ", description=" + description + ", sendToStaff=" + sendToStaff + ", sendToParents=" + sendToParents + ", classList=" + classList + ", senderName=" + senderName + ", createdAt=" + createdAt + '}';
    }
    
    public static AnnouncementDto convertToAnnouncementDto(Announcement announcement) {
        AnnouncementDto announcementDto = new AnnouncementDto();
        announcementDto.setId(announcement.getId());
        announcementDto.setTitle(announcement.getTitle());
        announcementDto.setDescription(announcement.getDescription());
        announcementDto.setCreatedAt(announcement.getCreatedAt());
        announcementDto.setSendToParents(announcement.getSendToParents());
        announcementDto.setSendToStaff(announcement.getSendToStaff());
        announcementDto.setSenderName(announcement.getAdmin().getUser().getName());
//        List<Integer> classIdList = new ArrayList();
//        announcement.getClassList().forEach((clazz) -> {
//            classIdList.add(clazz.getId());
//
//        });
//        announcementDto.setClassList(classIdList);
        return announcementDto;
    }
    
}
