/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import java.util.Date;

/**
 *
 * @author sankalpkulshrestha
 */
public class SchoolReviewDto {
    
    private Integer id;
    private Double rating;
    private String review;
    private Date createdAt;
    private boolean isSelfReview;
    private String schoolName;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the rating
     */
    public Double getRating() {
        return rating;
    }

    /**
     * @param rating the rating to set
     */
    public void setRating(Double rating) {
        this.rating = rating;
    }

    /**
     * @return the review
     */
    public String getReview() {
        return review;
    }

    /**
     * @param review the review to set
     */
    public void setReview(String review) {
        this.review = review;
    }

    /**
     * @return the createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
    
    @Override
    public String toString() {
        return "SchoolReviewDto{" + "id=" + id + ", rating=" + rating + ", review=" + review + '}';
    }

    /**
     * @return the isSelfReview
     */
    public boolean isIsSelfReview() {
        return isSelfReview;
    }

    /**
     * @param isSelfReview the isSelfReview to set
     */
    public void setIsSelfReview(boolean isSelfReview) {
        this.isSelfReview = isSelfReview;
    }

    /**
     * @return the schoolName
     */
    public String getSchoolName() {
        return schoolName;
    }

    /**
     * @param schoolName the schoolName to set
     */
    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }
    
}
