/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import java.util.List;

/**
 *
 * @author sankalp.kulshrestha
 */
public class TeacherClassDto {
    private Integer classId;
    private String standard;
    private String section;
    private Integer subjectId;
    private String subjectName;
    private Integer teacherId;
    private String teacherName;
    private List<StudentDto> student;
    private Integer studyGroupId;
    private Integer sgtmId;
    private Integer sctmId;
    
    /**
     * @return the classId
     */
    public Integer getClassId() {
        return classId;
    }

    /**
     * @param classId the classId to set
     */
    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    /**
     * @return the standard
     */
    public String getStandard() {
        return standard;
    }

    /**
     * @param standard the standard to set
     */
    public void setStandard(String standard) {
        this.standard = standard;
    }

    /**
     * @return the section
     */
    public String getSection() {
        return section;
    }

    /**
     * @param section the section to set
     */
    public void setSection(String section) {
        this.section = section;
    }

    /**
     * @return the subjectName
     */
    public String getSubjectName() {
        return subjectName;
    }

    /**
     * @param subjectName the subjectName to set
     */
    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    /**
     * @return the student
     */
    public List<StudentDto> getStudent() {
        return student;
    }

    /**
     * @param student the student to set
     */
    public void setStudent(List<StudentDto> student) {
        this.student = student;
    }

    /**
     * @return the subjectId
     */
    public Integer getSubjectId() {
        return subjectId;
    }

    /**
     * @param subjectId the subjectId to set
     */
    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    /**
     * @return the teacherId
     */
    public Integer getTeacherId() {
        return teacherId;
    }

    /**
     * @param teacherId the teacherId to set
     */
    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    /**
     * @return the teacherName
     */
    public String getTeacherName() {
        return teacherName;
    }

    /**
     * @param teacherName the teacherName to set
     */
    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    @Override
    public String toString() {
        return "TeacherClassDto{" + "classId=" + classId + ", standard=" + standard + ", section=" + section + ", subjectId=" + subjectId + ", subjectName=" + subjectName + ", teacherId=" + teacherId + ", teacherName=" + teacherName + ", student=" + student + '}';
    }

    /**
     * @return the studyGroupId
     */
    public Integer getStudyGroupId() {
        return studyGroupId;
    }

    /**
     * @param studyGroupId the studyGroupId to set
     */
    public void setStudyGroupId(Integer studyGroupId) {
        this.studyGroupId = studyGroupId;
    }

    /**
     * @return the sgtmId
     */
    public Integer getSgtmId() {
        return sgtmId;
    }

    /**
     * @param sgtmId the sgtmId to set
     */
    public void setSgtmId(Integer sgtmId) {
        this.sgtmId = sgtmId;
    }

    /**
     * @return the sctmId
     */
    public Integer getSctmId() {
        return sctmId;
    }

    /**
     * @param sctmId the sctmId to set
     */
    public void setSctmId(Integer sctmId) {
        this.sctmId = sctmId;
    }
    
}
