/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import java.util.Date;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public class SchoolDto {

    private List<Integer> adminIdList;

    private Integer id;

    private String name;

    private Integer boardId;

    private String boardName;

    private String address;

    private LocalityDto locality;

    private String pincode;

    private Integer stage;
    
    private List<ClassDto> classList;
    
    private String displayPic;
    
    private String coverPic;
    
    private Date createdAt;
    
    private Date updatedAt;
    
    private Integer currentUserAdminId;
    
    private List<InstituteCategoryDto> instituteCategoryList;
    
    private int studentCount;
    
    private int teacherCount;
    
    private double averageRating;
    
    private int ratingCount;
    
    private String about;
    
    private Float latitude;
    
    private Float longitude;
    
    private boolean searchable;
    
    private String website;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the boardId
     */
    public Integer getBoardId() {
        return boardId;
    }

    /**
     * @param boardId the boardId to set
     */
    public void setBoardId(Integer boardId) {
        this.boardId = boardId;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the pincode
     */
    public String getPincode() {
        return pincode;
    }

    /**
     * @param pincode the pincode to set
     */
    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    /**
     * @return the boardName
     */
    public String getBoardName() {
        return boardName;
    }

    /**
     * @param boardName the boardName to set
     */
    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }

    /**
     * @return the stage
     */
    public Integer getStage() {
        return stage;
    }

    /**
     * @param stage the stage to set
     */
    public void setStage(Integer stage) {
        this.stage = stage;
    }

    /**
     * @return the adminIdList
     */
    public List<Integer> getAdminIdList() {
        return adminIdList;
    }

    /**
     * @param adminIdList the adminIdList to set
     */
    public void setAdminIdList(List<Integer> adminIdList) {
        this.adminIdList = adminIdList;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "SchoolDto{" + "adminIdList=" + adminIdList + ", id=" + id + ", name=" + name + ", boardId=" + boardId + ", boardName=" + boardName + ", address=" + address + ", locality=" + locality + ", pincode=" + pincode + ", stage=" + stage + '}';
    }

    /**
     * @return the createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return the currentUserAdminId
     */
    public Integer getCurrentUserAdminId() {
        return currentUserAdminId;
    }

    /**
     * @param currentUserAdminId the currentUserAdminId to set
     */
    public void setCurrentUserAdminId(Integer currentUserAdminId) {
        this.currentUserAdminId = currentUserAdminId;
    }

    /**
     * @return the updatedAt
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt the updatedAt to set
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return the displayPic
     */
    public String getDisplayPic() {
        return displayPic;
    }

    /**
     * @param displayPic the displayPic to set
     */
    public void setDisplayPic(String displayPic) {
        this.displayPic = displayPic;
    }

    /**
     * @return the classList
     */
    public List<ClassDto> getClassList() {
        return classList;
    }

    /**
     * @param classList the classList to set
     */
    public void setClassList(List<ClassDto> classList) {
        this.classList = classList;
    }

    /**
     * @return the locality
     */
    public LocalityDto getLocality() {
        return locality;
    }

    /**
     * @param locality the locality to set
     */
    public void setLocality(LocalityDto locality) {
        this.locality = locality;
    }

    /**
     * @return the instituteCategoryList
     */
    public List<InstituteCategoryDto> getInstituteCategoryList() {
        return instituteCategoryList;
    }

    /**
     * @param instituteCategoryList the instituteCategoryList to set
     */
    public void setInstituteCategoryList(List<InstituteCategoryDto> instituteCategoryList) {
        this.instituteCategoryList = instituteCategoryList;
    }

    /**
     * @return the averageRating
     */
    public double getAverageRating() {
        return averageRating;
    }

    /**
     * @param averageRating the averageRating to set
     */
    public void setAverageRating(double averageRating) {
        this.averageRating = averageRating;
    }

    /**
     * @return the ratingCount
     */
    public int getRatingCount() {
        return ratingCount;
    }

    /**
     * @param ratingCount the ratingCount to set
     */
    public void setRatingCount(int ratingCount) {
        this.ratingCount = ratingCount;
    }

    /**
     * @return the studentCount
     */
    public int getStudentCount() {
        return studentCount;
    }

    /**
     * @param studentCount the studentCount to set
     */
    public void setStudentCount(int studentCount) {
        this.studentCount = studentCount;
    }

    /**
     * @return the teacherCount
     */
    public int getTeacherCount() {
        return teacherCount;
    }

    /**
     * @param teacherCount the teacherCount to set
     */
    public void setTeacherCount(int teacherCount) {
        this.teacherCount = teacherCount;
    }

    /**
     * @return the about
     */
    public String getAbout() {
        return about;
    }

    /**
     * @param about the about to set
     */
    public void setAbout(String about) {
        this.about = about;
    }

    /**
     * @return the latitude
     */
    public Float getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public Float getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the coverPic
     */
    public String getCoverPic() {
        return coverPic;
    }

    /**
     * @param coverPic the coverPic to set
     */
    public void setCoverPic(String coverPic) {
        this.coverPic = coverPic;
    }

    /**
     * @return the searchable
     */
    public boolean isSearchable() {
        return searchable;
    }

    /**
     * @param searchable the searchable to set
     */
    public void setSearchable(boolean searchable) {
        this.searchable = searchable;
    }

    /**
     * @return the website
     */
    public String getWebsite() {
        return website;
    }

    /**
     * @param website the website to set
     */
    public void setWebsite(String website) {
        this.website = website;
    }

}
