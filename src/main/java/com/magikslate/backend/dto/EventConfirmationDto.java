/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public class EventConfirmationDto {
    private List<String> accepted;
    private List<String> declined;

    /**
     * @return the accepted
     */
    public List<String> getAccepted() {
        return accepted;
    }

    /**
     * @param accepted the accepted to set
     */
    public void setAccepted(List<String> accepted) {
        this.accepted = accepted;
    }

    /**
     * @return the declined
     */
    public List<String> getDeclined() {
        return declined;
    }

    /**
     * @param declined the declined to set
     */
    public void setDeclined(List<String> declined) {
        this.declined = declined;
    }

    @Override
    public String toString() {
        return "EventConfirmationDto{" + "accepted=" + accepted + ", declined=" + declined + '}';
    }
    
}
