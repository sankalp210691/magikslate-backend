/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import com.magikslate.backend.entity.Content;
import com.magikslate.backend.util.Constants;

/**
 *
 * @author sankalpkulshrestha
 */
public class NoteDto extends ContentDto {

    private String name;
    private String noteContent;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the noteContent
     */
    public String getNoteContent() {
        return noteContent;
    }

    /**
     * @param noteContent the noteContent to set
     */
    public void setNoteContent(String noteContent) {
        this.noteContent = noteContent;
    }

    @Override
    public String toString() {
        return "NoteDto{" + "name=" + name + ", noteContent=" + noteContent + '}';
    }

    public static NoteDto convertToNoteDto(Content content) {
        NoteDto noteDto = new NoteDto();
        noteDto.setId(content.getId());
        noteDto.setName(content.getNote().getName());
        noteDto.setNoteContent(content.getNote().getNoteContent());
        noteDto.setDescription(content.getDescription());
        noteDto.setSubject(getSubjectNameFromContent(content));

        if (content.getSubjectClassTeacherMappingList() != null) {
            ClassDto classDto = new ClassDto();
            classDto.setId(content.getSubjectClassTeacherMappingList().get(0).getClass1().getId());
            classDto.setStandard(content.getSubjectClassTeacherMappingList().get(0).getClass1().getStandard());
            classDto.setSection(content.getSubjectClassTeacherMappingList().get(0).getClass1().getSection());
            noteDto.setClassDto(classDto);

            noteDto.setSubject(content.getSubjectClassTeacherMappingList().get(0).getSubject().getName());
            noteDto.setSubjectId(content.getSubjectClassTeacherMappingList().get(0).getSubject().getId());
            
            noteDto.setTeacherId(content.getSubjectClassTeacherMappingList().get(0).getTeacher().getId());
            noteDto.setTeacherUserId(content.getSubjectClassTeacherMappingList().get(0).getTeacher().getUser().getId());
            noteDto.setTeacherName(content.getSubjectClassTeacherMappingList().get(0).getTeacher().getUser().getName());
        } else if (content.getStudyGroupTeacherMappingList() != null) {
            noteDto.setStudyGroupId(content.getStudyGroupTeacherMappingList().get(0).getStudyGroup().getId());
        }

        noteDto.setDate(content.getCreatedAt());
        noteDto.setType(Constants.NOTE_CONTENT_TYPE);
        return noteDto;
    }

}
