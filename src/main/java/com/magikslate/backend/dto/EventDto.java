/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.magikslate.backend.entity.Event;
import com.magikslate.backend.entity.EventAdminMapping;
import com.magikslate.backend.entity.EventParentMapping;
import com.magikslate.backend.entity.EventTeacherMapping;
import com.magikslate.backend.entity.User;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author sankalpkulshrestha
 */
public class EventDto {

    private int id;

    @NotNull
    @NotEmpty
    private String title;

    private String description;

    @NotNull
    @NotEmpty
    private boolean eventForStaff;

    @NotNull
    @NotEmpty
    private boolean eventForParents;

    @NotNull
    @NotEmpty
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date eventStartDate;

    @NotNull
    @NotEmpty
    @Size(min = 1)
    private List<Integer> classList;

    @NotNull
    @Min(value = 1)
    private int creatorId;

    private String creatorName;

    private Integer totalInvited;

    private Boolean currentUserAccepted = null;

    private Date createdAt;

    private List<UserDto> accepted;
    private List<UserDto> declined;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the eventForStaff
     */
    public boolean isEventForStaff() {
        return eventForStaff;
    }

    /**
     * @param eventForStaff the eventForStaff to set
     */
    public void setEventForStaff(boolean eventForStaff) {
        this.eventForStaff = eventForStaff;
    }

    /**
     * @return the eventForParents
     */
    public boolean isEventForParents() {
        return eventForParents;
    }

    /**
     * @param eventForParents the eventForParents to set
     */
    public void setEventForParents(boolean eventForParents) {
        this.eventForParents = eventForParents;
    }

    /**
     * @return the classList
     */
    public List<Integer> getClassList() {
        return classList;
    }

    /**
     * @param classList the classList to set
     */
    public void setClassList(List<Integer> classList) {
        this.classList = classList;
    }

    /**
     * @return the creatorId
     */
    public int getCreatorId() {
        return creatorId;
    }

    /**
     * @param creatorId the creatorId to set
     */
    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    /**
     * @return the eventStartDate
     */
    public Date getEventStartDate() {
        return eventStartDate;
    }

    /**
     * @param eventStartDate the eventStartDate to set
     */
    public void setEventStartDate(Date eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    /**
     * @return the creatorName
     */
    public String getCreatorName() {
        return creatorName;
    }

    /**
     * @param creatorName the creatorName to set
     */
    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    /**
     * @return the totalInvited
     */
    public Integer getTotalInvited() {
        return totalInvited;
    }

    /**
     * @param totalInvited the totalInvited to set
     */
    public void setTotalInvited(Integer totalInvited) {
        this.totalInvited = totalInvited;
    }

    @Override
    public String toString() {
        return "EventDto{" + "id=" + id + ", title='" + title + "', description='" + description + "', eventForStaff=" + eventForStaff + ", eventForParents=" + eventForParents + ", eventStartDate='" + eventStartDate + "', classList=" + classList + ", creatorId=" + creatorId + ", creatorName='" + creatorName + "', totalInvited=" + totalInvited + ", accepted=" + accepted + ", declined=" + declined + '}';
    }

    public static EventDto convertToEventDto(Event event, Integer uid) {
        EventDto eventDto = new EventDto();
        eventDto.setId(event.getId());
        eventDto.setTitle(event.getTitle());
        eventDto.setDescription(event.getDescription());
        eventDto.setEventForParents(event.getEventForParents());
        eventDto.setEventForStaff(event.getEventForStaff());
        eventDto.setCreatorName(event.getUser().getName());
        eventDto.setCreatorId(event.getUser().getId());
        eventDto.setCreatedAt(event.getCreatedAt());
        eventDto.setEventStartDate(event.getEventStartDate());
        if (event.getUser().getId().equals(uid)) {
            eventDto.setCurrentUserAccepted(Boolean.TRUE);
        }
        if (event.getClassList() != null) {
            eventDto.setClassList(event.getClassList().stream().map(clazz
                    -> clazz.getId()).collect(Collectors.toList()));
        }
        eventDto.setTotalInvited(event.getTeacherSentCount() + event
                .getParentSentCount() + event.getAdminSentCount());

        List<UserDto> acceptedUserList = new ArrayList();
        List<UserDto> declinedUserList = new ArrayList();

        Map<Integer, UserDto> accepedUserIdMap = new HashMap();
        Map<Integer, UserDto> declinedUserIdMap = new HashMap();

        List<EventTeacherMapping> eventTeacherMappingList = event.getEventTeacherMappingList();
        List<EventAdminMapping> eventAdminMappingList = event.getEventAdminMappingList();
        List<EventParentMapping> eventParentMappingList = event.getEventParentMappingList();

        if (eventTeacherMappingList != null) {
            eventTeacherMappingList.forEach(eventTeacherMapping -> {
                Integer userId = eventTeacherMapping.getTeacher().getUser().getId();

                User user = eventTeacherMapping.getTeacher().getUser();
                UserDto userDto = new UserDto();
                userDto.setId(user.getId());
                userDto.setName(user.getName());

                if (eventTeacherMapping.getStatus()) {
                    accepedUserIdMap.put(userId, userDto);
                    declinedUserIdMap.remove(userId);
                } else if (!accepedUserIdMap.containsKey(userId)) {
                    declinedUserIdMap.put(userId, userDto);
                }
            });
        }

        if (eventAdminMappingList != null) {
            eventAdminMappingList.forEach(eventAdminMapping -> {
                Integer userId = eventAdminMapping.getAdmin().getUser().getId();

                User user = eventAdminMapping.getAdmin().getUser();
                UserDto userDto = new UserDto();
                userDto.setId(user.getId());
                userDto.setName(user.getName());

                if (eventAdminMapping.getStatus()) {
                    accepedUserIdMap.put(userId, userDto);
                    declinedUserIdMap.remove(userId);
                } else if (!accepedUserIdMap.containsKey(userId)) {
                    declinedUserIdMap.put(userId, userDto);
                }
            });
        }

        if (eventParentMappingList != null) {
            eventParentMappingList.forEach(eventParentMapping -> {
                Integer userId = eventParentMapping.getParent().getUser().getId();

                User user = eventParentMapping.getParent().getUser();
                UserDto userDto = new UserDto();
                userDto.setId(user.getId());
                userDto.setName(user.getName());

                if (eventParentMapping.getStatus()) {
                    accepedUserIdMap.put(userId, userDto);
                    declinedUserIdMap.remove(userId);
                } else if (!accepedUserIdMap.containsKey(userId)) {
                    declinedUserIdMap.put(userId, userDto);
                }
            });
        }

        accepedUserIdMap.keySet().forEach((userId) -> {
            acceptedUserList.add(accepedUserIdMap.get(userId));
            if (userId.equals(uid)) {
                eventDto.setCurrentUserAccepted(Boolean.TRUE);
            }
        });

        declinedUserIdMap.keySet().forEach((userId) -> {
            declinedUserList.add(declinedUserIdMap.get(userId));
            if(!userId.equals(event.getUser().getId()) && userId.equals(uid)) {
                eventDto.setCurrentUserAccepted(Boolean.FALSE);
            }
        });

        eventDto.setAccepted(acceptedUserList);
        eventDto.setDeclined(declinedUserList);
        return eventDto;
    }

    /**
     * @return the createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return the currentUserAccepted
     */
    public Boolean getCurrentUserAccepted() {
        return currentUserAccepted;
    }

    /**
     * @param currentUserAccepted the currentUserAccepted to set
     */
    public void setCurrentUserAccepted(Boolean currentUserAccepted) {
        this.currentUserAccepted = currentUserAccepted;
    }

    /**
     * @return the accepted
     */
    public List<UserDto> getAccepted() {
        return accepted;
    }

    /**
     * @param accepted the accepted to set
     */
    public void setAccepted(List<UserDto> accepted) {
        this.accepted = accepted;
    }

    /**
     * @return the declined
     */
    public List<UserDto> getDeclined() {
        return declined;
    }

    /**
     * @param declined the declined to set
     */
    public void setDeclined(List<UserDto> declined) {
        this.declined = declined;
    }

}
