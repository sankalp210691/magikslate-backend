/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import com.magikslate.backend.entity.Attendance;
import java.util.Date;

/**
 *
 * @author sankalpkulshrestha
 */
public class AttendanceDto {

    static AttendanceDto convertToAttendanceDto(Attendance attendance) {
        AttendanceDto attendanceDto = new AttendanceDto();
        
        attendanceDto.setId(attendance.getId());
        attendanceDto.setPresent(attendance.getPresent());
        attendanceDto.setCreatedAt(attendance.getCreatedAt());
        attendanceDto.setUserId(attendance.getTeacher().getUser().getId());
        attendanceDto.setTeacherId(attendance.getTeacher().getId());
        attendanceDto.setTeacherName(attendance.getTeacher().getUser().getName());
        attendanceDto.setStudentId(attendance.getStudentClassMapping().getStudent().getId());
        attendanceDto.setStudentName(attendance.getStudentClassMapping().getStudent().getName());
        
        return attendanceDto;
    }

    private Integer id;
    private boolean present;
    private Date createdAt;
    private Integer userId;
    private Integer teacherId;
    private String teacherName;
    private Integer studentId;
    private String studentName;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the present
     */
    public boolean isPresent() {
        return present;
    }

    /**
     * @param present the present to set
     */
    public void setPresent(boolean present) {
        this.present = present;
    }

    /**
     * @return the createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the teacherId
     */
    public Integer getTeacherId() {
        return teacherId;
    }

    /**
     * @param teacherId the teacherId to set
     */
    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    /**
     * @return the teacherName
     */
    public String getTeacherName() {
        return teacherName;
    }

    /**
     * @param teacherName the teacherName to set
     */
    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    /**
     * @return the studentId
     */
    public Integer getStudentId() {
        return studentId;
    }

    /**
     * @param studentId the studentId to set
     */
    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    /**
     * @return the studentName
     */
    public String getStudentName() {
        return studentName;
    }

    /**
     * @param studentName the studentName to set
     */
    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }
    
    @Override
    public String toString() {
        return "AttendanceDto{" + "id=" + id + ", present=" + present + ", createdAt='" + createdAt + "', studentName='" + studentName + "', userId=" + userId + ", teacherId=" + teacherId + ", teacherName='" + teacherName + "', studentId=" + studentId + '}';
    }
}
