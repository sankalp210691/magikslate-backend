/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import com.magikslate.backend.entity.ClassTeacherMapping;
import com.magikslate.backend.entity.Class;
import com.magikslate.backend.entity.Teacher;
import com.magikslate.backend.entity.User;
import java.time.Year;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public class TeacherDto {

    private Integer id;
    private String name;
    private String phone;
    private String email;
    private String employeeId;
    private Integer ctClass;
    private List<Integer> dtClass;
    private List<SubjectObject> subject;
    private UserDto user;

    public static class SubjectObject {

        private Integer id;
        private String name;
        private List<Integer> classList;
        private List<Integer> studentList;

        /**
         * @return the id
         */
        public Integer getId() {
            return id;
        }

        /**
         * @param id the id to set
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the classList
         */
        public List<Integer> getClassList() {
            return classList;
        }

        /**
         * @param classList the classList to set
         */
        public void setClassList(List<Integer> classList) {
            this.classList = classList;
        }

        /**
         * @return the studentList
         */
        public List<Integer> getStudentList() {
            return studentList;
        }

        /**
         * @param studentList the studentList to set
         */
        public void setStudentList(List<Integer> studentList) {
            this.studentList = studentList;
        }
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the employeeId
     */
    public String getEmployeeId() {
        return employeeId;
    }

    /**
     * @param employeeId the employeeId to set
     */
    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    /**
     * @return the ctClass
     */
    public Integer getCtClass() {
        return ctClass;
    }

    /**
     * @param ctClass the ctClass to set
     */
    public void setCtClass(Integer ctClass) {
        this.ctClass = ctClass;
    }

    /**
     * @return the dtClass
     */
    public List<Integer> getDtClass() {
        return dtClass;
    }

    /**
     * @param dtClass the dtClass to set
     */
    public void setDtClass(List<Integer> dtClass) {
        this.dtClass = dtClass;
    }

    /**
     * @return the subject
     */
    public List<SubjectObject> getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(List<SubjectObject> subject) {
        this.subject = subject;
    }

    /**
     * @return the user
     */
    public UserDto getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(UserDto user) {
        this.user = user;
    }

    public Teacher convertToTeacher() {
        Teacher teacher;
        User u;
        if (id == null) {
            teacher = new Teacher();
            u = new User();
        } else {
            teacher = new Teacher(id);
            u = new User(user.getId());
        }
        teacher.setEmployeeId(employeeId);
        if (ctClass != null) {
            Date currentDate = new Date();
            List<ClassTeacherMapping> classTeacherMappingList = new ArrayList();
            ClassTeacherMapping ctm = new ClassTeacherMapping();
            ctm.setClass1(new Class(ctClass));
            ctm.setClassTeacher(true);
            ctm.setDeputyTeacher(false);
            ctm.setTeacher(teacher);
            ctm.setCreatedAt(currentDate);
            ctm.setSessionYear(Year.now().getValue());
            classTeacherMappingList.add(ctm);
            teacher.setClassTeacherMappingList(classTeacherMappingList);
        }

        u.setName(user.getName());
        u.setPhone(user.getPhone());
        u.setEmail(user.getEmail());
        u.setProfilePic(user.getProfilePic());

        teacher.setUser(u);
        return teacher;
    }

    @Override
    public String toString() {
        return "TeacherDto{" + "id=" + id + ", name=" + name + ", user=" + user + ", phone=" + phone + ", email=" + email + ", employeeId=" + employeeId + ", ctClass=" + ctClass + ", dtClass=" + dtClass + ", subject=" + subject + '}';
    }

}
