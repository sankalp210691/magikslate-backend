/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import com.magikslate.backend.validator.ValidClassType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author sankalpkulshrestha
 */
public class ClassSectionDto {

    @NotNull
    @NotEmpty
    @ValidClassType
    private String standard;

    @NotNull
    @Min(value = 1)
    @Max(value = 26)
    private Integer sectionCount;

    /**
     * @return the standard
     */
    public String getStandard() {
        return standard;
    }

    /**
     * @param standard the standard to set
     */
    public void setStandard(String standard) {
        this.standard = standard;
    }

    /**
     * @return the sectionCount
     */
    public Integer getSectionCount() {
        return sectionCount;
    }

    /**
     * @param sectionCount the sectionCount to set
     */
    public void setSectionCount(Integer sectionCount) {
        this.sectionCount = sectionCount;
    }

    @Override
    public String toString() {
        return "ClassSectionDto{" + "standard=" + standard + ", sectionCount=" + sectionCount + '}';
    }
    
}
