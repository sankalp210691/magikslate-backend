/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import com.magikslate.backend.entity.School;
import com.magikslate.backend.entity.Subject;
import com.magikslate.backend.entity.SubjectClassTeacherMapping;
import com.magikslate.backend.entity.Class;
import com.magikslate.backend.entity.Teacher;
import com.magikslate.backend.util.Constants;
import java.time.Year;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author sankalpkulshrestha
 */
public class SubjectDto {

    private Integer id;
    private String name;
    private Map<String, String> classTeacherMap;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SubjectDto{" + "id=" + id + ", name=" + name + '}';
    }

    /**
     * @return the classTeacherMap
     */
    public Map<String, String> getClassTeacherMap() {
        return classTeacherMap;
    }

    /**
     * @param classTeacherMap the classTeacherMap to set
     */
    public void setClassTeacherMap(Map<String, String> classTeacherMap) {
        this.classTeacherMap = classTeacherMap;
    }

    public Subject convertToSubject(School school) {
        Subject subject = new Subject();
        if (id != null) {
            subject.setId(id);
        }
        if (school != null) {
            subject.setSchool(school);
        }
        subject.setName(name);
        if (classTeacherMap != null) {
            Date currentDate = new Date();
            List<SubjectClassTeacherMapping> subjectClassTeacherMappingList = new ArrayList();
            classTeacherMap.keySet().stream().map((classInfoString) -> {
                String teacherInfoString = classTeacherMap.get(classInfoString);
                int classId = Integer.parseInt(classInfoString.split(Constants.UNDERSCORE_SEPARATOR)[0]);
                int teacherId = Integer.parseInt(teacherInfoString.split(Constants.UNDERSCORE_SEPARATOR)[0]);
                SubjectClassTeacherMapping sctm = new SubjectClassTeacherMapping();
                sctm.setSubject(subject);
                sctm.setClass1(new Class(classId));
                sctm.setTeacher(new Teacher(teacherId));
                sctm.setSessionYear(Year.now().getValue());
                sctm.setCreatedAt(currentDate);
                return sctm;
            }).forEachOrdered((sctm) -> {
                subjectClassTeacherMappingList.add(sctm);
            });
            subject.setSubjectClassTeacherMappingList(subjectClassTeacherMappingList);
        }
        return subject;
    }
}
