/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.dto;

import java.util.List;

/**
 *
 * @author sankalpkulshrestha
 */
public class AttendanceDisplayDto {

    private Double percClassAverageAttendance;

    private List<StudentDto> students;

    /**
     * @return the percClassAverageAttendance
     */
    public Double getPercClassAverageAttendance() {
        return percClassAverageAttendance;
    }

    /**
     * @param percClassAverageAttendance the percClassAverageAttendance to set
     */
    public void setPercClassAverageAttendance(Double percClassAverageAttendance) {
        this.percClassAverageAttendance = percClassAverageAttendance;
    }

    /**
     * @return the students
     */
    public List<StudentDto> getStudents() {
        return students;
    }

    /**
     * @param students the students to set
     */
    public void setStudents(List<StudentDto> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "AttendanceDisplayDto{" + "percClassAverageAttendance=" + percClassAverageAttendance + ", students=" + students + '}';
    }
    
}
