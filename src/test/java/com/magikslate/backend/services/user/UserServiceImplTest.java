/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magikslate.backend.services.user;

import com.magikslate.backend.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author sankalpkulshrestha
 */
@Test
@ContextConfiguration(locations = {"classpath:spring-test-config.xml"})
public class UserServiceImplTest {

    @Autowired
    UserServiceImpl userServiceImpl;

    public UserServiceImplTest() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

//    @Test
//    public void getUserAuthToken() {
//        int userId = 1;
//        System.out.println(" >>>>>>>>>> "+Utils.getUserAuthToken(userId));
//
//    }
//////    
//    @Test
//    public void getRoleauthToken() {
//        Integer userId = 1;
//        String role = "admin";
//        Integer roleId = 1;
//        Integer schoolId = 1;
//        Integer studentId = null;
//        System.out.println(" >>>>>>>>>> "+Utils.generateRoleAuthToken(role, roleId, userId, schoolId, studentId));
//
//    }
}
